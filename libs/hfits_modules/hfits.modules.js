(function() {
    'use strict';

    angular.module('hfits.services', []);
})();
(function() {
    'use strict';

    angular.module('hfits.factories', []);
})();
(function() {
    'use strict';

    angular.module('hfits.filters', []);
})();
(function() {
    'use strict';

    angular.module('hfits.formatters', ['hfits.services']);
})();
(function() {
    'use strict';

    angular.module('hfits.validators', ['hfits.factories', 'hfits.services']);
})();
(function() {
    'use strict';

    angular
        .module('hfits.modules', ['hfits.factories', 'hfits.services',
            'hfits.validators', 'hfits.formatters', 'hfits.filters'
        ]);
})();

(function() {
    'use strict';

    var htmlInjectorFactory = function($timeout, $log) {
        var factory = {};

        factory.injectInputErrorMessage = function(validatorName, inputName, errorMessage, tElement, tAttrs, cssClass) {
            var messageContainerTemplate = '<div ng-messages="{0}.{1}.$error">{2}</div>';
            var defaultCssClass = 'hfits-input-error';
            var messageTemplate = '<div ng-message="{0}" class="{1}">{2}</div>';
            var defaultErrorMessage = 'Invalid Input';

            errorMessage = errorMessage || defaultErrorMessage;

            cssClass = cssClass || defaultCssClass;
            var ngMessages = getNgMessages(tElement);
            var ngMessage = constructNgMessage(messageTemplate, validatorName, cssClass, errorMessage);

            if (ngMessages === undefined) {
                // add ngMessages tag
                var form = tElement.parent();

                while (form && form.prop('tagName') != 'FORM') {
                    form = form.parent();

                    if (form.html() === form.parent().html()) {
                        break;
                    }
                }

                if (form.html() === form.parent().html()) {
                    $log.error('form does not exist');
                    return;
                }

                var formName = form.attr('name');
                //$log.debug('form name: ' + formName);
                messageContainerTemplate = messageContainerTemplate.replace('{0}', formName);
                messageContainerTemplate = messageContainerTemplate.replace('{1}', inputName);
                messageContainerTemplate = messageContainerTemplate.replace('{2}', ngMessage);

                tElement.after(messageContainerTemplate);
            } else {
                ngMessages.append(ngMessage);
            }
        };

        var constructNgMessage = function(messageTemplate, validatorName, cssClass, errorMessage) {
            messageTemplate = messageTemplate.replace('{0}', validatorName);
            messageTemplate = messageTemplate.replace('{1}', cssClass);
            messageTemplate = messageTemplate.replace('{2}', errorMessage);

            return messageTemplate;
        };

        var getNgMessages = function(tElement) {
            var sibling = tElement.next();

            if (sibling && sibling.attr('ng-messages') !== undefined) {
                return sibling;
            }

            return undefined;
        };

        return factory;
    };
    htmlInjectorFactory.$inject = ['$timeout', '$log'];
    angular
        .module('hfits.factories', [])
        .service('htmlInjectorFactory', htmlInjectorFactory);
})();

(function() {
    'use strict';

    var directiveName = 'currencyFormatter';
    var currencyFormatter = function currencyFormatter(formManagerService) {
        return {
            priority: 999,

            restrict: 'A', // restrict to use directive by attribute only
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var link = function(scope, iElement, iAttrs, ngModel) {
                    var regex = /^\d+(\.{1}\d{0,2})$/;
                    var format = function format(viewValue) {

                        var match = regex.exec(viewValue);
                        var decimal = '';
                        if (match) {
                            decimal = match[1];
                        }

                        viewValue = viewValue.replace(/\,/g, '');
                        viewValue = viewValue.replace(/\.\d*/, '');
                        var formattedValue = '';

                        var count = 0;

                        for (var i = viewValue.length - 1; i >= 0; i--) {
                            if (count % 3 === 0 && count !== 0) {
                                formattedValue = viewValue[i] + ',' + formattedValue;
                            } else {
                                formattedValue = viewValue[i] + formattedValue;
                            }

                            count++;
                        }

                        if (!decimal || decimal.length === 0) {
                            decimal = '.00';
                        }

                        if (decimal.length === 2) {
                            decimal = decimal + '0';
                        }

                        if (decimal.length === 1) {
                            decimal = decimal + '00';
                        }

                        formattedValue = formattedValue + decimal;
                        ngModel.$setViewValue(formattedValue.replace(/,/g, ''));
                        ngModel.$render();
                        return formattedValue;
                    };

                    var unFormat = function unFormat(viewValue) {
                        var match = regex.exec(viewValue);

                        viewValue = viewValue.replace(/\,/g, '');

                        return viewValue;
                    };

                    ngModel.$formatters.push(function(viewValue) {
                        if (!viewValue || viewValue.length === 0) {
                            return viewValue;
                        }
                        var formattedValue = format(viewValue);
                        if (iAttrs.isReadOnly === undefined) {
                            if (!formManagerService.getValidationResult(iAttrs.name)) {
                                return viewValue;
                            }

                            ngModel.$setViewValue(formattedValue.replace(/,/g, ''));
                            ngModel.$render();
                            iElement.val(formattedValue);
                            return formattedValue;
                        }

                        iElement.text(formattedValue);
                        return formattedValue;
                    });

                    formManagerService.addFormatter(iElement, iAttrs, directiveName, ngModel, format, unFormat, scope);
                };

                return {
                    post: link
                };
            }
        };
    };
    currencyFormatter.$inject = ["formManagerService"];

    angular
        .module('hfits.formatters')
        .directive(directiveName, currencyFormatter);
})();

(function() {
    'use strict';

    var directiveName = 'patternFormatter';

    var patternFormatter = function patternFormatter(formManagerService, $log) {
        return {
            priority: 999,
            restrict: 'A', // restrict to use directive by attribute only
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var postLink = function(scope, iElement, iAttrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }

                    var formatterPattern = iAttrs.patternFormatter;

                    ngModel.$formatters.push(function(value) {
                        //$log.('pattern formatter: ' + value);
                        if (!formatterPattern) {
                            return value;
                        }

                        if (!value || value.length === 0) {
                            return value;
                        }

                        var formattedValue = format(value, formatterPattern);
                        if (iAttrs.isReadOnly === undefined) {
                            $log.debug('input name: ' + iAttrs.name);
                            if (!formManagerService.getValidationResult(iAttrs.name)) {

                                $log.debug('pattern formatter validation failed');
                                return value;
                            }

                            iElement.val(formattedValue);
                            return formattedValue;
                        }

                        $log.debug('pattern formatter formatted value: ' + formattedValue);

                        iElement.text(formattedValue);
                        return formattedValue;

                    });

                    var isMaxLength = function isMaxLength(value, iAttrs) {
                        if (!value || value.length === 0) {
                            return false;
                        }

                        var maxLength = iAttrs.hfitsMaxLength;

                        if (!maxLength) {
                            return false;
                        }

                        maxLength = parseInt(maxLength);

                        if (isNaN(maxLength)) {
                            $log.error('invalid max-length syntax: ' + maxLength);
                            return false;
                        }

                        if (value.length < maxLength) {
                            return false;
                        }

                        return true;
                    };

                    var format = function(viewValue, formatterPattern) {
                        if (formatterPattern.replace(/[^\*]/, '').length < viewValue.length) {
                            return viewValue;
                        }

                        var formattedValue = '';
                        var valueIndexCount = 0;

                        for (var i in formatterPattern) {
                            if (valueIndexCount == viewValue.length) {
                                break;
                            }

                            var formatChar = formatterPattern[i];
                            if (formatChar === '*') {
                                formattedValue += viewValue[valueIndexCount];
                                valueIndexCount++;
                            } else {
                                formattedValue += formatChar;
                            }
                        }

                        return formattedValue;
                    };

                    var unFormat = function unFormat(viewValue, formatterPattern) {
                        var unFormattedValue = '';
                        for (var i in viewValue) {
                            var formatterPatternChar = formatterPattern[i];
                            if (formatterPatternChar === '*') {
                                unFormattedValue += viewValue[i];
                            } else if (viewValue[i] !== formatterPatternChar) {
                                return viewValue;
                            }
                        }

                        return unFormattedValue;
                    };

                    formManagerService.addFormatter(iElement, iAttrs, directiveName, ngModel, format, unFormat, scope);
                };

                return {
                    post: postLink
                };
            }
        };
    };

    patternFormatter.$inject = ["formManagerService", "$log"];

    angular
        .module('hfits.formatters')
        .directive(directiveName, patternFormatter);
})();

(function() {
    'use strict';

    angular
        .module('hfits.services')
        .service('formManagerService', ["$timeout", "$log", function($timeout, $log) {
            var service = this;
            var validatorsFormatterMap = {};
            var patternFormatterName = 'patternFormatter';

            this.getFormName = function(inputElement) {
                var form = inputElement.parent();

                while (form && form.prop('tagName') != 'FORM') {
                    form = form.parent();

                    if (form.html() === form.parent().html()) {
                        break;
                    }
                }

                if (form.html() === form.parent().html()) {
                    $log.error('form does not exist');
                    return null;
                }

                return form.attr('name');
            };

            this.addFormatter = function(element, attrs, formatterName, ngModel, formatFn, unFormatFn, scope) {
                var inputName = attrs.name;
                var formatter = {};
                formatter.formatFn = formatFn;
                formatter.unFormatFn = unFormatFn;
                formatter.name = formatterName;
                formatter.pattern = attrs.patternFormatter;

                var isPatternFormatter = formatterName === patternFormatterName;
                var isValidPatternFormatter = isPatternFormatter && attrs.patternFormatter;
                var formatterPattern = attrs.patternFormatter;

                if (!(inputName in validatorsFormatterMap)) {
                    validatorsFormatterMap[inputName] = {};
                    validatorsFormatterMap[inputName].model = ngModel;
                    validatorsFormatterMap[inputName].element = element;
                }

                var map = validatorsFormatterMap[inputName];

                map.formatter = formatter;

                scope.$on('$destroy', function() {
                    console.log('destroy formatter for: ' + inputName);

                    if (!(inputName in validatorsFormatterMap)) {
                        return;
                    }

                    delete validatorsFormatterMap[inputName];
                })

                element.bind('focus', function() {
                    if (attrs.readonly !== undefined) {
                        e.preventDefault();
                        return;
                    }

                    if (isPatternFormatter && !formatterPattern) {
                        $log.error('no pattern specified');
                        return;
                    }

                    if (service.getValidationResult(inputName)) {
                        var unFormattedValue = element.val();

                        if (isPatternFormatter) {
                            unFormattedValue = unFormatFn(element.val(), formatterPattern);
                        } else {
                            unFormattedValue = unFormatFn(element.val());
                        }

                        ngModel.$setViewValue(unFormattedValue);
                        ngModel.$render();
                    }
                });

                element.bind('blur', function() {
                    if (isPatternFormatter && !formatterPattern) {
                        $log.error('no pattern specified');
                        return;
                    }

                    service.runFormatter(attrs.name);
                });
            };

            this.runFormatter = function(inputName) {
                if (!inputName || inputName.length === 0) {
                    return;
                }

                if (!(inputName in validatorsFormatterMap)) {
                    return;
                }

                var map = validatorsFormatterMap[inputName];
                var element = map.element;

                if (map && map.formatter) {
                    var formatter = map.formatter;
                    var formatFn = formatter.formatFn;
                    var isPatternFormatter = formatter.name === patternFormatterName;
                    var formatterPattern = formatter.pattern;
                    var formattedValue = '';
                    if (element.isReadOnly !== undefined) {
                        formattedValue = element.text();
                        if (isPatternFormatter) {
                            formattedValue = formatFn(element.val(), formatterPattern);
                        } else {
                            formattedValue = formatFn(element.val());
                        }
                        element.text(formattedValue);
                    } else {
                        formattedValue = element.val();
                        if (service.getValidationResult(inputName)) {
                            if (isPatternFormatter) {
                                formattedValue = formatFn(element.val(), formatterPattern);
                            } else {
                                formattedValue = formatFn(element.val());
                            }
                            element.val(formattedValue);
                        }
                    }
                }
            };

            this.resetForm = function(formName, defaultValues) {

            };

            this.addValidator = function(element, attrs, validatorName, ngModel, validateFn, scope) {
                var inputName = attrs.name;
                var validator = {};
                var formatterPattern = attrs.patternFormatter;
                validator.validateFn = validateFn;
                validator.validatorName = validatorName;

                if (!(inputName in validatorsFormatterMap)) {
                    validatorsFormatterMap[inputName] = {};
                    validatorsFormatterMap[inputName].model = ngModel;
                    validatorsFormatterMap[inputName].element = element;
                    validatorsFormatterMap[inputName].attrs = attrs;
                }

                var map = validatorsFormatterMap[inputName];

                if (!map.validators) {
                    map.validators = [];
                }

                map.validators.push(validator);

                scope.$on('$destroy', function() {
                    console.log('destroy validator for: ' + inputName);

                    if (!(inputName in validatorsFormatterMap)) {
                        return;
                    }

                    service.clearValidateState(inputName);
                    delete validatorsFormatterMap[inputName];
                })

                element.bind('focus', function(e) {
                    element.isFocus = true;
                    if (attrs && attrs.readonly !== undefined) {
                        e.preventDefault();
                        return;
                    }

                    if (!map.element) {
                        return;
                    }

                    map.element.removeClass('no-margin-bottom');
                    ngModel.$setValidity(validatorName, true);
                    ngModel.$setViewValue(ngModel.$modelValue);
                    ngModel.$render();
                    element.hasFocus = true;
                });

                element.bind('blur', function() {
                    $timeout(function() {
                        if (!element) {
                            return;
                        }

                        element.isFocus = false;
                        var formatter = map.formatter;
                        var rawValue = element.val();
                        var value = rawValue;

                        if (formatter) {
                            if (formatter.name === patternFormatterName) {
                                if (formatter.pattern) {
                                    value = formatter.unFormatFn(value, formatter.pattern);
                                } else {
                                    $log.error('formatter pattern is not defined');
                                }
                            } else {
                                value = formatter.unFormatFn(value);
                            }
                        }

                        //var validateResult = validateFn(value);
                        service.runValidator(attrs.name, false, value);
                    }, 100);

                });
            };

            this.runAllValidatorsOnSubmit = function() {
                var temp = this.runAllValidators(true);
                //$log.debug('runAllValidatorsOnSubmit:'+temp);
                return temp;
            };

            this.enableValidator = function(inputNames) {
                if (!inputNames || inputNames.length === 0) {
                    return;
                }

                for (var i in inputNames) {
                    var inputElem = validatorsFormatterMap[inputNames[i]];
                    inputElem.element.removeAttr('disabled-validator');

                }
            };

            this.clearValidateState = function(inputName, inputDefaultValue) {
                if (!(inputName in validatorsFormatterMap)) {
                    $log.error('invalid input name: ' + inputName);
                    return;
                }

                inputDefaultValue = inputDefaultValue || '';

                var map = validatorsFormatterMap[inputName];
                var validators = map.validators;
                map.element.removeClass('no-margin-bottom');
                map.element.hasFocus = false;

                for (var i in validators) {
                    map.model.$setValidity(validators[i].validatorName, true);
                }

                map.model.$setViewValue(inputDefaultValue);
                map.model.$render();
            };

            this.clearAllValidateStates = function() {
                $log.debug('clear all validation states');
                for (var inputName in validatorsFormatterMap) {
                    this.clearValidateState(inputName);
                }
            };

            this.runAllFormatters = function() {
                for (var key in validatorsFormatterMap) {
                    service.runFormatter(key);
                }
            };

            this.runAllValidators = function(isSubmit) {
                var isValid = true;
                for (var key in validatorsFormatterMap) {
                    // var inputElem = validatorsFormatterMap[key];
                    // var validators = inputElem.validators;

                    var result = service.runValidator(key, isSubmit);
                    if (!result) {
                        isValid = false;
                        $log.debug('invalid value: ' + key);
                    }
                    // for (var i in validators) {
                    //     var validator = validators[i];

                    //     var validateResult = validator.validateFn(inputElem.model.$modelValue, isSubmit, inputElem.attrs);

                    //     if (!validateResult) {
                    //         isValid = false;
                    //         inputElem.element.addClass('no-margin-bottom');
                    //         inputElem.model.$setValidity(validator.validatorName, validateResult);
                    //         var viewValue = inputElem.element.val();
                    //         inputElem.model.$setViewValue(inputElem.model.$modelValue);
                    //         inputElem.element.val(viewValue);
                    //     }
                    // }

                }
                $log.debug('run all validators is valid: ' + isValid);
                return isValid;
            };

            this.clearValidators = function() {
                validatorsFormatterMap = {};
            };

            this.deleteValidator = function(name) {
                delete validatorsFormatterMap[name];
            };

            this.disableValidator = function(inputNames) {
                if (!inputNames || inputNames.length === 0) {
                    return;
                }

                for (var i in inputNames) {
                    var inputElem = validatorsFormatterMap[inputNames[i]];
                    inputElem.element.attr('disabled-validator', 'true');
                    //$log.debug(inputElem);
                }

            };

            this.getValidationResult = function(inputName, value) {
                if (!inputName || inputName.length === 0) {
                    return;
                }

                if (!(inputName in validatorsFormatterMap)) {
                    return;
                }

                var inputElem = validatorsFormatterMap[inputName];
                var validators = validatorsFormatterMap[inputName].validators;
                value = value || inputElem.model.$modelValue;

                var isValid = true;
                for (var i in validators) {
                    var validator = validators[i];
                    var validateResult = validator.validateFn(value, false, inputElem.attrs);
                    if (!validateResult) {
                        isValid = false;
                    }
                }

                return isValid;
            };

            this.runValidator = function(inputName, isOnSubmit, value) {
                if (!inputName || inputName.length === 0) {
                    return;
                }

                if (!(inputName in validatorsFormatterMap)) {
                    return;
                }

                var inputElem = validatorsFormatterMap[inputName];

                if (inputElem.element.attr('disabled-validator') !== undefined) {
                    return true;
                }

                var validators = validatorsFormatterMap[inputName].validators;
                value = value || inputElem.model.$modelValue;
                var isValid = true;
                for (var i in validators) {
                    var validator = validators[i];
                    var validateResult = validator.validateFn(value, isOnSubmit, inputElem.attrs);
                    if (!validateResult) {
                        isValid = false;
                        inputElem.model.$setValidity(validator.validatorName, false);
                    } else {
                        inputElem.model.$setValidity(validator.validatorName, true);
                    }
                }

                if (isValid) {
                    inputElem.element.removeClass('no-margin-bottom');
                } else {
                    inputElem.element.addClass('no-margin-bottom');
                    inputElem.model.$setViewValue(value);
                    inputElem.model.$render();
                }

                return isValid;
            };

            $timeout(function() {
                service.runAllValidators(false);
            });
        }]);
})();

(function() {
    'use strict';

    var directiveName = 'ccValidator';
    var ccValidator = function ccValidator(htmlInjectorFactory, formManagerService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var errorMessage = tAttrs.ccValidatorErrorMsg;

                htmlInjectorFactory.injectInputErrorMessage(directiveName, tAttrs.name,
                    errorMessage, tElement, tAttrs);

                var postLink = function(scope, iElement, iAttrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }

                    var detectCCType = function detectCCType(value) {
                        var firstNum = value[0];

                        if (value.length == 13 && firstNum == '4') {
                            return 'Visa';
                        }

                        var firstTwoNum = value.substr(0, 2);

                        if (value.length == 15) {
                            if (firstTwoNum === '34' || firstTwoNum === '37') {
                                return 'Amex';
                            }
                        }

                        if (value.length == 16) {
                            var validTwoPrefix = ['51', '52', '53', '54', '55'];
                            var validFourPrefix = ['4026', '4405', '4508', '4844', '4913', '4917'];

                            var firstSixNum = value.substr(0, 6);
                            if (firstSixNum == '417500') {
                                return 'Visa Electron';
                            }

                            var firstFourNum = value.substr(0, 4);
                            if (validFourPrefix.indexOf(firstFourNum) != -1) {
                                return 'Visa Electron';
                            }

                            if (validTwoPrefix.indexOf(firstTwoNum) != -1) {
                                return 'Mastercard';
                            }

                            if (firstNum == '4') {
                                return 'Visa';
                            }
                        }

                        return undefined;
                    };

                    var validate = function validate(value, isOnSubmit, iAttrs) {
                        if (!value || value.length === 0) {
                            return true;
                        }

                        var ccType = detectCCType(value);

                        if (!ccType) {
                            return false;
                        }

                        var totalValue = 0;
                        if (value.length == 13 || value.length == 15) {
                            value = '0' + value;
                        }

                        for (var i in value) {
                            var remainder = parseInt(i) % 2;

                            switch (remainder) {
                                case 0:
                                    var tempValue = parseInt(value[i]) * 2;

                                    if (tempValue > 9) {
                                        tempValue = tempValue - 9;
                                    }

                                    totalValue += tempValue;
                                    break;
                                case 1:
                                    totalValue = totalValue + parseInt(value[i]);
                                    break;
                            }
                        }

                        if (totalValue === 0) {
                            return false;
                        }

                        var isFactorOfTen = totalValue % 10 === 0;

                        if (!isFactorOfTen) {
                            return false;
                        }

                        return true;
                    };

                    formManagerService.addValidator(iElement, iAttrs, directiveName, ngModel, validate, scope);
                };

                return {
                    post: postLink
                };
            }
        };
    };

    ccValidator.$inject = ['htmlInjectorFactory', 'formManagerService'];

    angular
        .module('hfits.validators')
        .directive(directiveName, ccValidator);
})();

(function() {
    'use strict';

    var directiveName = 'customValidator';

    var customValidator = function customValidator(htmlInjectorFactory, formManagerService, $log) {
        return {
            restrict: 'A',
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var errorMessage = tAttrs.customValidatorErrorMsg;

                htmlInjectorFactory.injectInputErrorMessage(directiveName, tAttrs.name,
                    errorMessage, tElement, tAttrs);
                var postLink = function(scope, iElement, iAttrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }

                    var validate = function validate(value, isOnSubmit, iAttrs) {
                        if (!value || value.length === 0) {
                            return true;
                        }

                        var validatorType = iAttrs.customValidator;

                        if (validatorType) {
                            var validateTypeResult = validateType(value, validatorType, iAttrs);
                            if (!validateTypeResult) {
                                return false;
                            }
                        }

                        var validatorPattern = iAttrs.hfitsValidatorPattern;

                        if (!validatorPattern) {
                            return true;
                        }

                        var validatePatternResult = validatePattern(value, validatorPattern);
                        return validatePatternResult;
                    };

                    var validateLength = function validateLength(value, iAttrs) {
                        var minLength = iAttrs.hfitsMinLength;

                        if (minLength !== undefined) {
                            minLength = parseInt(minLength);
                            if (isNaN(minLength)) {
                                $log.error('invalid min-length syntax: ' + minLength);
                            } else {
                                if (value.length < minLength) {
                                    return false;
                                }
                            }
                        }

                        var maxLength = iAttrs.hfitsMaxLength;

                        if (maxLength !== undefined) {
                            maxLength = parseInt(maxLength);

                            if (isNaN(maxLength)) {
                                $log.error('invalid max-length syntax: ' + maxLength);
                            } else {
                                if (value.length > maxLength) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    };

                    var validateValueRange = function validateValueRange(value, iAttrs) {
                        value = parseInt(value);

                        var minValue = iAttrs.hfitsMinValue;

                        if (minValue !== undefined) {
                            minValue = parseInt(minValue);
                            if (isNaN(minValue)) {
                                $log.error('invalid min-value syntax: ' + minValue);
                            } else {
                                // value must not less than min value
                                if (value < minValue) {
                                    return false;
                                }
                            }
                        }

                        var maxValue = iAttrs.hfitsMaxValue;

                        if (maxValue !== undefined) {
                            maxValue = parseInt(maxValue);
                            if (isNaN(maxValue)) {
                                $log.error('invalid max-value syntax: ' + maxValue);
                            } else {
                                // value must not more than max value
                                if (value > maxValue) {
                                    return false;
                                }
                            }
                        }

                        return true;
                    };

                    var validateType = function validateType(value, validatorType, iAttrs) {
                        switch (validatorType) {
                            case 'currency':
                                return /^\d+(\.|)(\d{1,2}){0,1}$/.test(value);
                            case 'num':
                                // check if contain number only
                                if (/[^\d]/.test(value)) {
                                    return false;
                                }

                                // value must be within defined range
                                if (!validateValueRange(value, iAttrs)) {
                                    return false;
                                }

                                // value must be within defined length
                                if (!validateLength(value, iAttrs)) {
                                    return false;
                                }

                                break;
                            case 'alpha':
                                // value must be within defined length
                                if (!validateLength(value, iAttrs)) {
                                    return false;
                                }

                                // value must not contain any non alphabet character
                                var nonAlphaRegex = /[^a-z]/i;
                                if (nonAlphaRegex.test(value)) {
                                    return false;
                                }

                                break;
                            case 'alphanum':
                                // value must be within defined length
                                if (!validateLength(value, iAttrs)) {
                                    return false;
                                }

                                // value must not contain any non alphanumeric character
                                var nonAlphaNumericRegex = /[^a-z0-9]/i;
                                if (nonAlphaNumericRegex.test(value)) {
                                    return false;
                                }
                                break;

                            case 'OldIC':
                                // value must be within defined length
                                if (!validateLength(value, iAttrs)) {
                                    return false;
                                }

                                // value must not contain any non alphanumeric character
                                var AlphaNumericRegex = /^[AHK0-9][0-9]*$/;

                                if (AlphaNumericRegex.test(value)) {
                                    return true;
                                } else {
                                    return false;
                                }
                                break;

                            case 'FullName':
                                // value must be within defined length
                                if (!validateLength(value, iAttrs)) {
                                    return false;
                                }

                                // value must not contain any non alphanumeric character
                                //var AlphaNumericRegex = /^[A-z\\s]*$/i;
                                var nonAlpha = /^[\s\d\W]*$/;

                                //var testName = /^\p{L}[A-z]+[\p{L}\p{Z}\p{P}][A-z]+[\p{L}\p{Z}\p{P}][A-z]*$/i;

                                if (nonAlpha.test(value)) {
                                    $log.debug('Error');
                                    return false;
                                }
                                //  //test for false case
                                // if(testName.test(value))
                                // {    $log.debug("true");
                                //     return true;
                                // }
                                // else{
                                //     $log.debug("false");
                                //     return false;
                                // }
                                break;
                            default:
                                $log.error("Unrecognized validator type");
                                return false;
                        }

                        return true;
                    };

                    var validatePattern = function validatePattern(value, validatorPattern) {
                        if (validatorPattern.length != value.length) {
                            return false;
                        }

                        for (var i in value) {
                            switch (validatorPattern[i]) {
                                case 'A':
                                    if (!/[a-z]/i.test(value[i])) {
                                        return false;
                                    }
                                    break;
                                case '9':
                                    if (!/\d/.test(value[i])) {
                                        return false;
                                    }
                                    break;
                                case '@':
                                    if (/[^\[\]\^\$\.\|\?\*\+\(\)\\~`\!@#%&\-_+={}'""<>:;, ]/.test(value[i])) {
                                        return false;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        return true;
                    };

                    formManagerService.addValidator(iElement, iAttrs, directiveName, ngModel, validate, scope);
                };

                return {
                    post: postLink
                };
            }
        };
    };

    customValidator.$inject = ['htmlInjectorFactory', 'formManagerService', '$log'];
    angular
        .module('hfits.validators')
        .directive(directiveName, customValidator);
})();

(function() {
    'use strict';

    var directiveName = 'emailValidator';
    var emailValidator = function emailValidator(htmlInjectorFactory, formManagerService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var errorMessage = tAttrs.emailValidatorErrorMsg;
                htmlInjectorFactory.injectInputErrorMessage(directiveName, tAttrs.name,
                    errorMessage, tElement, tAttrs);

                var postLink = function(scope, iElement, iAttrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }

                    var validate = function(value, isOnSubmit, iAttrs) {
                        if (!value || value.length === 0) {
                            return true;
                        }

                        if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,4}))$/.test(value)) {
                            return false;
                        }

                        return true;
                    };

                    formManagerService.addValidator(iElement, iAttrs, directiveName, ngModel, validate, scope);
                };
                return {
                    post: postLink
                };
            }
        };
    };
    emailValidator.$inject = ['htmlInjectorFactory', 'formManagerService'];

    angular
        .module('hfits.validators')
        .directive(directiveName, emailValidator);
})();

(function() {
    'use strict';

    var directiveName = 'isRequired';

    var isRequired = function isRequired(htmlInjectorFactory, formManagerService, $log) {
        return {
            restrict: 'A',
            require: 'ngModel',
            compile: function(tElement, tAttrs) {
                var errorMessage = tAttrs.isRequiredErrorMsg;

                htmlInjectorFactory.injectInputErrorMessage(directiveName, tAttrs.name,
                    errorMessage, tElement, tAttrs);

                var postLink = function(scope, iElement, iAttrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }

                    var model = iAttrs.ngModel;

                    if (ngModel.$options && ngModel.$options.getterSetter) {
                        model += '()';
                    }

                    // $log.debug('model :' + model);

                    scope.$watch(model, function() {
                        //$log.debug('watch');
                        if (!iElement.isFocus) {
                            formManagerService.runValidator(iAttrs.name);
                        } else {
                            //$log.debug('on focus');
                        }
                    });

                    var validate = function validate(value, isOnSubmit, iAttrs) {
                        //$log.debug('is required value: ' + value);
                        if ((iElement.hasFocus || isOnSubmit) && (!value || value.length === 0)) {
                            //$log.debug('is required false, input name: ' + iAttrs.name);
                            return false;
                        }

                        return true;
                    };
                    formManagerService.addValidator(iElement, iAttrs, directiveName, ngModel, validate, scope);
                };

                return {
                    post: postLink
                };
            }
        };
    };

    isRequired.$inject = ['htmlInjectorFactory', 'formManagerService', '$log'];

    angular
        .module('hfits.validators')
        .directive(directiveName, isRequired);
})();
