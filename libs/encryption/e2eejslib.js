/*
 * JavaScript implementation to include SHA1 capability on passwords before encryption
 * @author Yuan Kwang
 * @company DS3, http://www.ds3global.com
 * @version 1.0
 *
 */
var Is2048=false;

function RsaEncrypt(rsaPublicKey,password,random,pwdMigrationState)
{
    var rsaBlock1;
    var rsaBlock2;
    var encryptedPwd;
    
    if(pwdMigrationState == 0)
    {
        rsaBlock1 = EncryptSHA1Password(rsaPublicKey,password,random);
        rsaBlock2 = EncryptSHA256Password(rsaPublicKey,password,random);
    }
    else if(pwdMigrationState == 1)
    {
        rsaBlock1 = EncryptSHA256Password(rsaPublicKey,password,random);
        rsaBlock2 = EncryptPassword(rsaPublicKey,password,random);
    }
    else
    {
        throw new Exception("Invalid Password migration state!");
    }
    
    encryptedPwd = rsaBlock1 + "," + rsaBlock2 + "," + random;
    
    return encryptedPwd;
}

function getNewEncryptBlockChangePassword(oldpassword, password, publicKey, random, migrationState) {
    var newBlock;
    if (migrationState == 0){
        newBlock =  EncryptSHA256Password(publicKey, password, random);
    }else if (migrationState == 1){
        newBlock =  EncryptPasswordChange(publicKey, oldpassword, password, random);
    }
    return newBlock;
}
    
function getNewEncryptBlockResetPassword(password, publicKey, random, migrationState) {
    var newBlock;
    if (migrationState == 0){
        newBlock =  EncryptSHA256Password(publicKey, password, random);
    }else if (migrationState == 1){
        newBlock =  EncryptPassword(publicKey, password, random);
    }
    return newBlock;
}
 
function EncryptSHA1Password(rsaPublicKey,password,random){if(rsaPublicKey==null||rsaPublicKey.length==0){throw "RSA Public Key is empty!";return;}if(password==null||password.length==0){throw "Password is empty!";return;}if(random==null||random.length==0){throw "Random is empty!";return;}if(rsaPublicKey.length==512){Is2048=true;}var A=SHA1Hash(password);var B=random+A;var C=new BigInteger(B,16);var D=new RSAKey();D.setPublic(rsaPublicKey,"10001");var E=D.encrypt(C).toString(16);return E};

function EncryptSHA256Password(rsaPublicKey,password,random){if(rsaPublicKey==null||rsaPublicKey.length==0){throw "RSA Public Key is empty!";return;}if(password==null||password.length==0){throw "Password is empty!";return;}if(random==null||random.length==0){throw "Random is empty!";return;}if(rsaPublicKey.length==512){Is2048=true;}var A=sha256Hash(password);var B=random+A;var C=new BigInteger(B,16);var D=new RSAKey();D.setPublic(rsaPublicKey,"10001");var E=D.encrypt(C).toString(16);return E};

function EncryptPassword(rsaPublicKey,password,random){if(rsaPublicKey==null||rsaPublicKey.length==0){throw "RSA Public Key is empty!";return;}if(password==null||password.length==0){throw "Password is empty!";return;}if(random==null||random.length==0){throw "Random is empty!";return;}var rawRandom=random.substring(0,16);if(rsaPublicKey.length==512){Is2048=true;}var A=buildPKCS15BlockForPinVerify(password,rawRandom);var B=new RSAKey();B.setPublic(rsaPublicKey,"10001");var C=B.encryptNativeBytes(A);return C;};

function EncryptSHA256PasswordChange(rsaPublicKey,oldpassword,newpassword,random){if(rsaPublicKey==null||rsaPublicKey.length==0){throw "RSA Public Key is empty!";return;}if(oldpassword==null||oldpassword.length==0){throw "Existing password is empty!";return;}if(newpassword==null||newpassword.length==0){throw "New password is empty!";return;}if(random==null||random.length==0){throw "Random is empty!";return;}if(rsaPublicKey.length==512){Is2048=true;}var A=sha256Hash(oldpassword);var B=sha256Hash(newpassword);var C=random+B+A;var D=new BigInteger(C,16);var E=new RSAKey();E.setPublic(rsaPublicKey,"10001");var F=E.encrypt(D).toString(16);return F};

function EncryptPasswordChange(rsaPublicKey,oldpassword,newpassword,random){if(rsaPublicKey==null||rsaPublicKey.length==0){throw "RSA Public Key is empty!";return;}if(oldpassword==null||oldpassword.length==0){throw "Existing password is empty!";return;}if(newpassword==null||newpassword.length==0){throw "New password is empty!";return;}if(random==null||random.length==0){throw "Random is empty!";return;}if(rsaPublicKey.length==512){Is2048=true;}var rawRandom=random.substring(0,16);if(rsaPublicKey.length==512){Is2048=true;}var A=buildPKCS15BlockForPinChange(oldpassword,newpassword,rawRandom);var B=new RSAKey();B.setPublic(rsaPublicKey,"10001");var C=B.encryptNativeBytes(A);return C;};

/**
* This method builds a byte array that is in accordance with PKCS#1 v1.5
* standard according to section 10.1 of Group Internet Banking System (GIB)
* Communication Message Specification for PIN verify operation
* 
* @param password
*           The users password
* @param random
*           The random number as supplied from the Authentication Server
* @return a 128 byte array corresponding to the PKCS block
* @throws UnsupportedEncodingException
*            if ISO-8859-1 encoding is not supported
*/
function buildPKCS15BlockForPinVerify(password,random){if(password.length>30){alert("Password must be less than 30 bytes");return;}var bytes=new Array();var pwdBytes=Util.getByteArray(password);var passwordBlock=new Array(30);for(var i=0;i<30;i++){if (i<pwdBytes.length)passwordBlock[i]=pwdBytes[i];else     passwordBlock[i]=0xFF;}var randomBytes=Util.fromHexString(random);var maxSize=128;if(Is2048==true){maxSize=256}var padLength=maxSize-randomBytes.length-passwordBlock.length;var bytesPad=Util.randomBytes(padLength);for(var i = 0; i < padLength; i++){if(bytesPad[i]==0x00){bytesPad[i]=0x27;}}bytesPad[0]=0x00;bytesPad[1]=0x02;bytesPad[10]=0x00;bytes=bytesPad.concat(randomBytes);bytes=bytes.concat(passwordBlock);return bytes;}

/**
* This builds a byte array that is in accordance with PKCS#1 v1.5 standard
* according to section 10.1 of Group Internet Banking System (GIB)
* Communication Message Specification for a PIN Change operation
* 
* @param oldPassword
* @param newPassword
* @param random
* @return
* @throws UnsupportedEncodingException
*/
function buildPKCS15BlockForPinChange(oldPassword,newPassword,random){/*if(random.length!=16){alert(" random number must be 8 bytes");return;}*/if (oldPassword.length > 30){alert("Existing Pin length must be less than 30 bytes");return;}if(newPassword.length>30){alert("New Pin length must be less than 30 bytes");return;}var bytes=new Array();var oldPINBytes=Util.getByteArray(oldPassword);var oldPasswordBytes=new Array(30);for(var i=0;i<30;i++){if(i<oldPINBytes.length)oldPasswordBytes[i]=oldPINBytes[i];else oldPasswordBytes[i]=0xFF;}var newPINBytes=Util.getByteArray(newPassword);var newPasswordBytes=new Array(30);for(var i=0;i<30;i++){if(i<newPINBytes.length)newPasswordBytes[i]=newPINBytes[i];else newPasswordBytes[i]=0xFF;}var randomBytes=Util.fromHexString(random);var maxSize=128;if(Is2048==true){maxSize=256}var padLength=maxSize-randomBytes.length-newPasswordBytes.length-oldPasswordBytes.length;var bytesPad=Util.randomBytes(padLength);for(var i=0;i<padLength;i++){if(bytesPad[i]==0x00){bytesPad[i]=0x28;}}bytesPad[0]=0x00;bytesPad[1]=0x02;bytesPad[10]=0x00;bytes=bytesPad.concat(randomBytes);bytes=bytes.concat(newPasswordBytes);bytes=bytes.concat(oldPasswordBytes);return bytes;}

 /* This package includes code written by Tom Wu.
 *
 * Copyright (c) 2003-2005  Tom Wu
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 */
/*
 * Copyright (c) 2003-2005  Tom Wu
 * http://www-cs-students.stanford.edu/~tjw/jsbn/
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL TOM WU BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF
 * THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * In addition, the following condition applies:
 *
 * All redistributions must retain an intact copy of this copyright notice
 * and disclaimer.
 */
 function getByteArray(B){a=new Array();for(var A=0;A<B.length;A++){a[A]=B.charCodeAt(A)}return a}var dbits;var j_lm=(((244837814094590)&16777215)==15715070);function BigInteger(B,A,C){if(B!=null){if("number"==typeof B){this.fromNumber(B,A,C)}else{if(A==null&&"string"!=typeof B){this.fromString(B,256)}else{this.fromString(B,A)}}}}function nbi(){return new BigInteger(null)}function am1(E,A,B,D,G,F){while(--F>=0){var C=A*this[E++]+B[D]+G;G=Math.floor(C/67108864);B[D++]=C&67108863}return G}function am2(E,J,K,D,H,A){var G=J&32767,I=J>>15;while(--A>=0){var C=this[E]&32767;var F=this[E++]>>15;var B=I*C+F*G;C=G*C+((B&32767)<<15)+K[D]+(H&1073741823);H=(C>>>30)+(B>>>15)+I*F+(H>>>30);K[D++]=C&1073741823}return H}function am3(E,J,K,D,H,A){var G=J&16383,I=J>>14;while(--A>=0){var C=this[E]&16383;var F=this[E++]>>14;var B=I*C+F*G;C=G*C+((B&16383)<<14)+K[D]+H;H=(C>>28)+(B>>14)+I*F;K[D++]=C&268435455}return H}if(navigator.appName=="Nokia"){BigInteger.prototype.am=am3;dbits=28}else{if(j_lm&&(navigator.appName=="Microsoft Internet Explorer")){BigInteger.prototype.am=am2;dbits=30}else{if(j_lm&&(navigator.appName!="Netscape")){BigInteger.prototype.am=am1;dbits=26}else{if((4294967295==-1)&&(navigator.appName=="Netscape")){BigInteger.prototype.am=am1;dbits=26}else{BigInteger.prototype.am=am3;dbits=28}}}}BigInteger.prototype.DB=dbits;BigInteger.prototype.DM=((1<<dbits)-1);BigInteger.prototype.DV=(1<<dbits);var BI_FP=52;BigInteger.prototype.FV=Math.pow(2,BI_FP);BigInteger.prototype.F1=BI_FP-dbits;BigInteger.prototype.F2=2*dbits-BI_FP;var BI_RM="0123456789abcdefghijklmnopqrstuvwxyz";var BI_RC=new Array();var rr,vv;rr="0".charCodeAt(0);for(vv=0;vv<=9;++vv){BI_RC[rr++]=vv}rr="a".charCodeAt(0);for(vv=10;vv<36;++vv){BI_RC[rr++]=vv}rr="A".charCodeAt(0);for(vv=10;vv<36;++vv){BI_RC[rr++]=vv}function int2char(A){return BI_RM.charAt(A)}function intAt(B,A){var C=BI_RC[B.charCodeAt(A)];return(C==null)?-1:C}function bnpCopyTo(B){for(var A=this.t-1;A>=0;--A){B[A]=this[A]}B.t=this.t;B.s=this.s}function bnpFromInt(A){this.t=1;this.s=(A<0)?-1:0;if(A>0){this[0]=A}else{if(A<-1){this[0]=A+DV}else{this.t=0}}}function nbv(A){var B=nbi();B.fromInt(A);return B}function bnpFromString(G,B){var D;if(B==16){D=4}else{if(B==8){D=3}else{if(B==256){D=8}else{if(B==2){D=1}else{if(B==32){D=5}else{if(B==4){D=2}else{this.fromRadix(G,B);return}}}}}}this.t=0;this.s=0;var F=G.length,C=false,E=0;while(--F>=0){var A=(D==8)?G[F]&255:intAt(G,F);if(A<0){if(G.charAt(F)=="-"){C=true}continue}C=false;if(E==0){this[this.t++]=A}else{if(E+D>this.DB){this[this.t-1]|=(A&((1<<(this.DB-E))-1))<<E;this[this.t++]=(A>>(this.DB-E))}else{this[this.t-1]|=A<<E}}E+=D;if(E>=this.DB){E-=this.DB}}if(D==8&&(G[0]&128)!=0){this.s=-1;if(E>0){this[this.t-1]|=((1<<(this.DB-E))-1)<<E}}this.clamp();if(C){BigInteger.ZERO.subTo(this,this)}}function bnpClamp(){var A=this.s&this.DM;while(this.t>0&&this[this.t-1]==A){--this.t}}function bnToString(B){if(this.s<0){return"-"+this.negate().toString(B)}var C;if(B==16){C=4}else{if(B==8){C=3}else{if(B==2){C=1}else{if(B==32){C=5}else{if(B==4){C=2}else{return this.toRadix(B)}}}}}var E=(1<<C)-1,H,A=false,F="",D=this.t;var G=this.DB-(D*this.DB)%C;if(D-->0){if(G<this.DB&&(H=this[D]>>G)>0){A=true;F=int2char(H)}while(D>=0){if(G<C){H=(this[D]&((1<<G)-1))<<(C-G);H|=this[--D]>>(G+=this.DB-C)}else{H=(this[D]>>(G-=C))&E;if(G<=0){G+=this.DB;--D}}if(H>0){A=true}if(A){F+=int2char(H)}}}if(B==16&&F.length%2>0){F="0"+F}return A?F:"0"}function bnNegate(){var A=nbi();BigInteger.ZERO.subTo(this,A);return A}function bnAbs(){return(this.s<0)?this.negate():this}function bnCompareTo(A){var C=this.s-A.s;if(C!=0){return C}var B=this.t;C=B-A.t;if(C!=0){return C}while(--B>=0){if((C=this[B]-A[B])!=0){return C}}return 0}function nbits(A){var C=1,B;if((B=A>>>16)!=0){A=B;C+=16}if((B=A>>8)!=0){A=B;C+=8}if((B=A>>4)!=0){A=B;C+=4}if((B=A>>2)!=0){A=B;C+=2}if((B=A>>1)!=0){A=B;C+=1}return C}function bnBitLength(){if(this.t<=0){return 0}return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM))}function bnpDLShiftTo(C,B){var A;for(A=this.t-1;A>=0;--A){B[A+C]=this[A]}for(A=C-1;A>=0;--A){B[A]=0}B.t=this.t+C;B.s=this.s}function bnpDRShiftTo(C,B){for(var A=C;A<this.t;++A){B[A-C]=this[A]}B.t=Math.max(this.t-C,0);B.s=this.s}function bnpLShiftTo(H,D){var B=H%this.DB;var A=this.DB-B;var F=(1<<A)-1;var E=Math.floor(H/this.DB),G=(this.s<<B)&this.DM,C;for(C=this.t-1;C>=0;--C){D[C+E+1]=(this[C]>>A)|G;G=(this[C]&F)<<B}for(C=E-1;C>=0;--C){D[C]=0}D[E]=G;D.t=this.t+E+1;D.s=this.s;D.clamp()}function bnpRShiftTo(G,D){D.s=this.s;var E=Math.floor(G/this.DB);if(E>=this.t){D.t=0;return}var B=G%this.DB;var A=this.DB-B;var F=(1<<B)-1;D[0]=this[E]>>B;for(var C=E+1;C<this.t;++C){D[C-E-1]|=(this[C]&F)<<A;D[C-E]=this[C]>>B}if(B>0){D[this.t-E-1]|=(this.s&F)<<A}D.t=this.t-E;D.clamp()}function bnpSubTo(B,D){var C=0,E=0,A=Math.min(B.t,this.t);while(C<A){E+=this[C]-B[C];D[C++]=E&this.DM;E>>=this.DB}if(B.t<this.t){E-=B.s;while(C<this.t){E+=this[C];D[C++]=E&this.DM;E>>=this.DB}E+=this.s}else{E+=this.s;while(C<B.t){E-=B[C];D[C++]=E&this.DM;E>>=this.DB}E-=B.s}D.s=(E<0)?-1:0;if(E<-1){D[C++]=this.DV+E}else{if(E>0){D[C++]=E}}D.t=C;D.clamp()}function bnpMultiplyTo(B,D){var A=this.abs(),E=B.abs();var C=A.t;D.t=C+E.t;while(--C>=0){D[C]=0}for(C=0;C<E.t;++C){D[C+A.t]=A.am(0,E[C],D,C,0,A.t)}D.s=0;D.clamp();if(this.s!=B.s){BigInteger.ZERO.subTo(D,D)}}function bnpSquareTo(C){var A=this.abs();var B=C.t=2*A.t;while(--B>=0){C[B]=0}for(B=0;B<A.t-1;++B){var D=A.am(B,A[B],C,2*B,0,1);if((C[B+A.t]+=A.am(B+1,2*A[B],C,2*B+1,D,A.t-B-1))>=A.DV){C[B+A.t]-=A.DV;C[B+A.t+1]=1}}if(C.t>0){C[C.t-1]+=A.am(B,A[B],C,2*B,0,1)}C.s=0;C.clamp()}function bnpDivRemTo(J,G,F){var P=J.abs();if(P.t<=0){return}var H=this.abs();if(H.t<P.t){if(G!=null){G.fromInt(0)}if(F!=null){this.copyTo(F)}return}if(F==null){F=nbi()}var D=nbi(),A=this.s,I=J.s;var O=this.DB-nbits(P[P.t-1]);if(O>0){P.lShiftTo(O,D);H.lShiftTo(O,F)}else{P.copyTo(D);H.copyTo(F)}var L=D.t;var B=D[L-1];if(B==0){return}var K=B*(1<<this.F1)+((L>1)?D[L-2]>>this.F2:0);var S=this.FV/K,R=(1<<this.F1)/K,Q=1<<this.F2;var N=F.t,M=N-L,E=(G==null)?nbi():G;D.dlShiftTo(M,E);if(F.compareTo(E)>=0){F[F.t++]=1;F.subTo(E,F)}BigInteger.ONE.dlShiftTo(L,E);E.subTo(D,D);while(D.t<L){D[D.t++]=0}while(--M>=0){var C=(F[--N]==B)?this.DM:Math.floor(F[N]*S+(F[N-1]+Q)*R);if((F[N]+=D.am(0,C,F,M,0,L))<C){D.dlShiftTo(M,E);F.subTo(E,F);while(F[N]<--C){F.subTo(E,F)}}}if(G!=null){F.drShiftTo(L,G);if(A!=I){BigInteger.ZERO.subTo(G,G)}}F.t=L;F.clamp();if(O>0){F.rShiftTo(O,F)}if(A<0){BigInteger.ZERO.subTo(F,F)}}function Classic(A){this.m=A}function cConvert(A){if(A.s<0||A.compareTo(this.m)>=0){return A.mod(this.m)}else{return A}}function cRevert(A){return A}function cReduce(A){A.divRemTo(this.m,null,A)}function cMulTo(A,C,B){A.multiplyTo(C,B);this.reduce(B)}function cSqrTo(A,B){A.squareTo(B);this.reduce(B)}Classic.prototype.convert=cConvert;Classic.prototype.revert=cRevert;Classic.prototype.reduce=cReduce;Classic.prototype.mulTo=cMulTo;Classic.prototype.sqrTo=cSqrTo;function bnpInvDigit(){if(this.t<1){return 0}var A=this[0];if((A&1)==0){return 0}var B=A&3;B=(B*(2-(A&15)*B))&15;B=(B*(2-(A&255)*B))&255;B=(B*(2-(((A&65535)*B)&65535)))&65535;B=(B*(2-A*B%this.DV))%this.DV;return(B>0)?this.DV-B:-B}function Montgomery(A){this.m=A;this.mp=A.invDigit();this.mpl=this.mp&32767;this.mph=this.mp>>15;this.um=(1<<(A.DB-15))-1;this.mt2=2*A.t}function montConvert(A){var B=nbi();A.abs().dlShiftTo(this.m.t,B);B.divRemTo(this.m,null,B);if(A.s<0&&B.compareTo(BigInteger.ZERO)>0){this.m.subTo(B,B)}return B}function montRevert(A){var B=nbi();A.copyTo(B);this.reduce(B);return B}function montReduce(A){while(A.t<=this.mt2){A[A.t++]=0}for(var C=0;C<this.m.t;++C){var B=A[C]&32767;var D=(B*this.mpl+(((B*this.mph+(A[C]>>15)*this.mpl)&this.um)<<15))&A.DM;B=C+this.m.t;A[B]+=this.m.am(0,D,A,C,0,this.m.t);while(A[B]>=A.DV){A[B]-=A.DV;A[++B]++}}A.clamp();A.drShiftTo(this.m.t,A);if(A.compareTo(this.m)>=0){A.subTo(this.m,A)}}function montSqrTo(A,B){A.squareTo(B);this.reduce(B)}function montMulTo(A,C,B){A.multiplyTo(C,B);this.reduce(B)}Montgomery.prototype.convert=montConvert;Montgomery.prototype.revert=montRevert;Montgomery.prototype.reduce=montReduce;Montgomery.prototype.mulTo=montMulTo;Montgomery.prototype.sqrTo=montSqrTo;function bnpIsEven(){return((this.t>0)?(this[0]&1):this.s)==0}function bnpExp(F,G){var E=nbi(),A=nbi(),D=G.convert(this),C=nbits(F)-1;D.copyTo(E);while(--C>=0){G.sqrTo(E,A);if((F&(1<<C))>0){G.mulTo(A,D,E)}else{var B=E;E=A;A=B}}return G.revert(E)}function bnModPowInt(B,A){var C;if(B<256||A.isEven()){C=new Classic(A)}else{C=new Montgomery(A)}return this.exp(B,C)}function bnpBitwiseTo(B,F,D){var C,E,A=Math.min(B.t,this.t);for(C=0;C<A;++C){D[C]=F(this[C],B[C])}if(B.t<this.t){E=B.s&this.DM;for(C=A;C<this.t;++C){D[C]=F(this[C],E)}D.t=this.t}else{E=this.s&this.DM;for(C=A;C<B.t;++C){D[C]=F(E,B[C])}D.t=B.t}D.s=F(this.s,B.s);D.clamp()}function op_xor(A,B){return A^B}function bnXor(A){var B=nbi();this.bitwiseTo(A,op_xor,B);return B}function lbit(A){if(A==0){return -1}var B=0;if((A&65535)==0){A>>=16;B+=16}if((A&255)==0){A>>=8;B+=8}if((A&15)==0){A>>=4;B+=4}if((A&3)==0){A>>=2;B+=2}if((A&1)==0){++B}return B}BigInteger.prototype.copyTo=bnpCopyTo;BigInteger.prototype.fromInt=bnpFromInt;BigInteger.prototype.fromString=bnpFromString;BigInteger.prototype.clamp=bnpClamp;BigInteger.prototype.dlShiftTo=bnpDLShiftTo;BigInteger.prototype.subTo=bnpSubTo;BigInteger.prototype.rShiftTo=bnpRShiftTo;BigInteger.prototype.drShiftTo=bnpDRShiftTo;BigInteger.prototype.invDigit=bnpInvDigit;BigInteger.prototype.isEven=bnpIsEven;BigInteger.prototype.multiplyTo=bnpMultiplyTo;BigInteger.prototype.lShiftTo=bnpLShiftTo;BigInteger.prototype.divRemTo=bnpDivRemTo;BigInteger.prototype.squareTo=bnpSquareTo;BigInteger.prototype.exp=bnpExp;BigInteger.prototype.bitwiseTo=bnpBitwiseTo;BigInteger.prototype.toString=bnToString;BigInteger.prototype.negate=bnNegate;BigInteger.prototype.abs=bnAbs;BigInteger.prototype.compareTo=bnCompareTo;BigInteger.prototype.bitLength=bnBitLength;BigInteger.prototype.modPowInt=bnModPowInt;BigInteger.prototype.xor=bnXor;BigInteger.ZERO=nbv(0);BigInteger.ONE=nbv(1);
 
function parseBigInt(B,A){return new BigInteger(B,A)}function pkcs1pad2B(C,H){var G=C.length;if(G>H-11-4){throw"104"}var A=[0,2,255,255,255,255];var B=H-G-3-4;var F=randomBytes(B);var D=A.concat(F,[0],C);var E=new BigInteger(D);return E}function randomBytes(C){var A=[];var B=0;for(B=0;B<C;B++){A[B]=Math.ceil(Math.random()*255)}return A}function pkcs1pad2(F,A){var I=Math.ceil(F.bitLength()/8);if(A<I+11+4){alert("Message too long for RSA");return null}var E=[0,2,255,255,255,255];var B;B=A-I-7;var G=0;var D=6;while(D<B+6){G=0;while(G==0){G=Math.floor(Math.random()*255)}E[D++]=G}var H=new BigInteger(E);var C=H.toString(16)+"00"+F.toString(16);return new BigInteger(C,16)}function pkcs1pad2S(F,A){var I=Math.ceil(F.bitLength()/8);if(A<I+11){alert("Message too long for RSA");return null}var E=[0,2];var B;B=A-I-3;var D=2;while(D<B+2){var G=0;while(G==0){G=Math.floor(Math.random()*255)}E[D++]=G}var H=new BigInteger(E);var C=H.toString(16)+"00"+F.toString(16);return new BigInteger(C,16)}function RSAKey(){this.n=null;this.e=0;this.d=null}

RSAKey.prototype.setPublic=function(B,A){if(B!=null&&A!=null&&B.length>0&&A.length>0){this.n=parseBigInt(B,16);this.e=parseInt(A,16)}else{alert("Invalid RSA public key")}};

RSAKey.prototype.doPublic=function(A){return A.modPowInt(this.e,this.n)};RSAKey.prototype.encryptNativeHexStr=function(D){var G=D.length/2;var F=(this.n.bitLength()+7)>>3;if(G>F){throw"104"}var A=new BigInteger(D,16);var E=this.doPublic(A);if(E==null){return null}var C=E.toString(16);if(C.length>512){return null}if(C.length<512){for(var B=0;B<(512-C.length);B++){C="0"+C}}return C};

RSAKey.prototype.encryptNativeBytes=function(B){var G=B.length;var F=(this.n.bitLength()+7)>>3;if(G>F){throw"104"}var A=new BigInteger(B);var E=this.doPublic(A);if(E==null){return null}var D=E.toString(16);var maxSize=256;if(Is2048==true){maxSize=512}if(D.length>maxSize){return null}if(D.length<maxSize){for(var C=0;C<(maxSize-D.length);C++){D="0"+D}}return D};

RSAKey.prototype.encryptS=function(C){var A=pkcs1pad2S(C,(this.n.bitLength()+7)>>3);if(A==null){return null}var E=this.doPublic(A);if(E==null){return null}var D=E.toString(16);if(D.length>512){return null}if(D.length<512){for(var B=0;B<(512-D.length);B++){D="0"+D}}return D};

RSAKey.prototype.encrypt=function(C){var A=pkcs1pad2(C,(this.n.bitLength()+7)>>3);if(A==null){return null}var E=this.doPublic(A);if(E==null){return null}var D=E.toString(16);if(D.length>512){return null}var maxSize=256;if(Is2048==true){maxSize=512}if(D.lengthmaxSize){for(var B=0;B<(maxSize-D.length);B++){D="0"+D}}return D};RSAKey.prototype.encryptB=function(B){var A=pkcs1pad2B(B,(this.n.bitLength()+7)>>3);if(A==null){return null}var E=this.doPublic(A);if(E==null){return null}var D=E.toString(16);if(D.length>maxSize){return null}if(D.length<maxSize){for(var C=0;C<(maxSize-D.length);C++){D="0"+D}}return D};
 
/*
 * @fileOverview DS3 End-end encryption toolkit<br>DSSS, http://www.ds3global.com
 * @author Pedric Kng
 * @version 1.0
 */
function Util(){}Util.parseBigInt=function(B,A){return new BigInteger(B,A)};Util.randomString=function(C){var A="";var B=0;for(B=0;B<C;B++){A=A+String.fromCharCode(Math.ceil(Math.random()*255))}return A};Util.randomBytes=function(C){var A=[];var B=0;for(B=0;B<C;B++){A[B]=Math.ceil(Math.random()*255)}return A};Util.toHexString=function(D){var C="";for(var A=0;A<D.length;A++){var B;if(typeof D[A]=="number"){B=(D[A]).toString(16)}else{if(typeof D[A]=="string"){B=D.charCodeAt(A).toString(16)}}if(B.length==1){B="0"+B}C+=B}return C};Util.fromHexString=function(D){D=(D.length%2==0)?D:"0"+D;var A=D.length/2;var E=[];for(var C=0,B=0;C<A;C++,B++){var F=C*2;E[B]=parseInt("0x"+D.substring(F,F+2))}return E};Util.fromHexToString=function(C){C=(C.length%2==0)?C:"0"+C;var A=C.length/2;var D="";for(var B=0;B<A;B++){var E=B*2;D=D+String.fromCharCode(parseInt("0x"+C.substring(E,E+2)))}return D};Util.cByteArrayToNString=function(C){var A="";for(var B=0;B<C.length;B++){A+=String.fromCharCode(C[B])}return A};Util.getByteArray=function(B){a=new Array();for(var A=0;A<B.length;A++){a[A]=B.charCodeAt(A)}return a};Util.xorByteArray=function(C,B){if(C.length>B.length){throw"Invalid parameters."}var A=[];for(var D=0;D<C.length;D++){A[D]=C[D]^B[D]}return A};Util.stringToHex=function(C){var D="";var B=new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");for(var A=0;A<C.length;A++){D+=B[C.charCodeAt(A)>>4]+B[C.charCodeAt(A)&15]}return D};

/*
 * This package includes code written by Chris Veness.
 * 
 * Copyright (c) 2005-2009  Chris Veness
 * http://www.movable-type.co.uk/scripts/sha1.html 
 * 
 */
function SHA1Hash(C){var F=[1518500249,1859775393,2400959708,3395469782];C+=String.fromCharCode(128);var Q=C.length/4+2;var D=Math.ceil(Q/16);var E=new Array(D);for(var S=0;S<D;S++){E[S]=new Array(16);for(var R=0;R<16;R++){E[S][R]=(C.charCodeAt(S*64+R*4)<<24)|(C.charCodeAt(S*64+R*4+1)<<16)|(C.charCodeAt(S*64+R*4+2)<<8)|(C.charCodeAt(S*64+R*4+3))}}E[D-1][14]=((C.length-1)*8)/Math.pow(2,32);E[D-1][14]=Math.floor(E[D-1][14]);E[D-1][15]=((C.length-1)*8)&4294967295;var L=1732584193;var J=4023233417;var I=2562383102;var H=271733878;var G=3285377520;var A=new Array(80);var h,g,Z,X,V;for(var S=0;S<D;S++){for(var O=0;O<16;O++){A[O]=E[S][O]}for(var O=16;O<80;O++){A[O]=Y(A[O-3]^A[O-8]^A[O-14]^A[O-16],1)}h=L;g=J;Z=I;X=H;V=G;for(var O=0;O<80;O++){var P=Math.floor(O/20);var B=(Y(h,5)+U(P,g,Z,X)+V+F[P]+A[O])&4294967295;V=X;X=Z;Z=Y(g,30);g=h;h=B}L=(L+h)&4294967295;J=(J+g)&4294967295;I=(I+Z)&4294967295;H=(H+X)&4294967295;G=(G+V)&4294967295}return L.toHexStr()+J.toHexStr()+I.toHexStr()+H.toHexStr()+G.toHexStr();function U(M,K,T,N){switch(M){case 0:return(K&T)^(~K&N);case 1:return K^T^N;case 2:return(K&T)^(K&N)^(T&N);case 3:return K^T^N}}function Y(K,M){return(K<<M)|(K>>>(32-M))}}Number.prototype.toHexStr=function(){var C="",A;for(var B=7;B>=0;B--){A=(this>>>(B*4))&15;C+=A.toString(16)}return C};
 /* This package includes code written by Chris Veness.
 * 
 * Copyright (c) 2005-2009  Chris Veness
 * http://www.movable-type.co.uk/scripts/sha256.html 
 * 
 */
function sha256Hash(B){var E=[1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,2227730452,2361852424,2428436474,2756734187,3204031479,3329325298];var F=[1779033703,3144134277,1013904242,2773480762,1359893119,2600822924,528734635,1541459225];B+=String.fromCharCode(128);var L=B.length/4+2;var C=Math.ceil(L/16);var D=new Array(C);for(var P=0;P<C;P++){D[P]=new Array(16);for(var O=0;O<16;O++){D[P][O]=(B.charCodeAt(P*64+O*4)<<24)|(B.charCodeAt(P*64+O*4+1)<<16)|(B.charCodeAt(P*64+O*4+2)<<8)|(B.charCodeAt(P*64+O*4+3))}}D[C-1][14]=((B.length-1)*8)/Math.pow(2,32);D[C-1][14]=Math.floor(D[C-1][14]);D[C-1][15]=((B.length-1)*8)&4294967295;var A=new Array(64);var Y,X,V,U,T,S,R,Q;for(var P=0;P<C;P++){for(var I=0;I<16;I++){A[I]=D[P][I]}for(var I=16;I<64;I++){A[I]=(sigma1(A[I-2])+A[I-7]+sigma0(A[I-15])+A[I-16])&4294967295}Y=F[0];X=F[1];V=F[2];U=F[3];T=F[4];S=F[5];R=F[6];Q=F[7];for(var I=0;I<64;I++){var J=Q+Sigma1(T)+Ch(T,S,R)+E[I]+A[I];var G=Sigma0(Y)+Maj(Y,X,V);Q=R;R=S;S=T;T=(U+J)&4294967295;U=V;V=X;X=Y;Y=(J+G)&4294967295}F[0]=(F[0]+Y)&4294967295;F[1]=(F[1]+X)&4294967295;F[2]=(F[2]+V)&4294967295;F[3]=(F[3]+U)&4294967295;F[4]=(F[4]+T)&4294967295;F[5]=(F[5]+S)&4294967295;F[6]=(F[6]+R)&4294967295;F[7]=(F[7]+Q)&4294967295}return F[0].toHexStr()+F[1].toHexStr()+F[2].toHexStr()+F[3].toHexStr()+F[4].toHexStr()+F[5].toHexStr()+F[6].toHexStr()+F[7].toHexStr()}function ROTR(B,A){return(A>>>B)|(A<<(32-B))}function Sigma0(A){return ROTR(2,A)^ROTR(13,A)^ROTR(22,A)}function Sigma1(A){return ROTR(6,A)^ROTR(11,A)^ROTR(25,A)}function sigma0(A){return ROTR(7,A)^ROTR(18,A)^(A>>>3)}function sigma1(A){return ROTR(17,A)^ROTR(19,A)^(A>>>10)}function Ch(A,C,B){return(A&C)^(~A&B)}function Maj(A,C,B){return(A&C)^(A&B)^(C&B)}Number.prototype.toHexStr=function(){var C="",A;for(var B=7;B>=0;B--){A=(this>>>(B*4))&15;C+=A.toString(16)}return C};