(function() {
    'use strict';

    var gulp = require('gulp');
    var ngAnnotate = require('gulp-ng-annotate');
    var concat = require('gulp-concat');
    var uglify = require('gulp-uglify');
    var rename = require('gulp-rename');
    var minifyCss = require('gulp-minify-css');
    var minifyHTML = require('gulp-minify-html');
    var imagemin = require('gulp-imagemin');
    var pngquant = require('imagemin-pngquant');
    var del = require('del');
    var path = require('path');

    var src = {
        js: [],
        css: [],
        html: [],
        images: ['images/**/*'],
        fonts: [],
        resources: {}
    };

    gulp.task('dev-screen-config', []);

})();
