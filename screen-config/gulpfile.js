(function() {
    'use strict';

    var gulp = require('gulp');
    var ngAnnotate = require('gulp-ng-annotate');
    var concat = require('gulp-concat');
    var uglifyJs = require('gulp-uglify');
    var rename = require('gulp-rename');
    var minifyCss = require('gulp-minify-css');
    var minifyHTML = require('gulp-minify-html');
    var imagemin = require('gulp-imagemin');
    var pngquant = require('imagemin-pngquant');
    var del = require('del');
    var path = require('path');
    var jsonlint = require('gulp-jsonlint');
    var compress = require('compression');

    var argv = require('yargs').argv;
    var jscs = require('gulp-jscs');
    var jscsOptions = {
        'configPath': '../.jscsrc'
    };
    var stylish = require('gulp-jscs-stylish');
    var autoprefixer = require('gulp-autoprefixer');
    var eslint = require('gulp-eslint');
    var runSequence = require('run-sequence');
    var gulpif = require('gulp-if');

    var dist = 'dist';
    var distJs = path.join(dist, 'js');
    var distCss = path.join(dist, 'css');
    var distComponents = path.join(dist, 'ui-components');
    var distComponentsJs = path.join(distComponents, 'js');
    var distComponentsCss = path.join(distComponents, 'css');
    var distComponentsImages = path.join(distComponents, 'images');

    var componentsJsFileName = 'components.min.js';
    var componentsTemplateFileName = 'template.html';
    var screenConfigJsFileName = 'screenconfig.js';
    var screenConfigVendorJsFileName = 'vendor.js';

    var distFonts = dist + '/fonts/';

    var noop = function() {};
    var isProduction = false;

    var src = {
        js: {
            components: [
                '!src/ui-components/libs/**/*.js',
                'src/ui-components/components.module.js',
                'src/ui-components/components.constants.js',
                'src/ui-components/*/*.module.js',
                'src/ui-components/**/*.js'
            ],
            screenconfig: [
                'src/config/app.js',
                'src/config/*/*.module.js',
                'src/config/**/*.js'
            ],
            vendor: [
                'bower_components/jquery/dist/jquery.js',
                'bower_components/angular/angular.js',
                'bower_components/angular-resource/angular-resource.js',
                'bower_components/angular-ui-router/release/angular-ui-router.js',
                'bower_components/angular-cookies/angular-cookies.js',
                'bower_components/angular-translate/angular-translate.js',
                'bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
                'bower_components/footable/js/footable.js',
                'bower_components/footable/js/footable.sort.js',
                'bower_components/footable/js/footable.paginate.js',
                'bower_components/pdfobject/pdfobject.js',
                'bower_components/pdfjs-dist/build/pdf.js',
                'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
                'bower_components/blueimp-file-upload/js/jquery.iframe-transport.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-process.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-validate.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-ui.js',
                'bower_components/pickadate/lib/picker.js',
                'bower_components/pickadate/lib/picker.date.js',
                'bower_components/jquery-ui/jquery-ui.js',
                'bower_components/bootstrap/dist/js/bootstrap.js',
                'bower_components/angular-sanitize/angular-sanitize.js',
                'bower_components/angular-bootstrap/ui-bootstrap.js',
                'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
            ],
            pdfWorker: [
                'bower_components/pdfjs-dist/build/pdf.worker.js'
            ]
        },
        css: {
            screenconfig: ['css/*'],
            components: ['src/ui-components/**/*.css'],
            vendor: [
                'bower_components/bootstrap/dist/css/bootstrap.min.css',
                'bower_components/fontawesome/css/font-awesome.min.css',
                'bower_components/jquery-ui/themes/smoothness/jquery-ui.css',
                'bower_components/footable/css/footable.core.css',
                'bower_components/pickadate/lib/themes/classic.css',
                'bower_components/pickadate/lib/themes/classic.date.css',
                'bower_components/blueimp-file-upload/css/jquery.fileupload.css',
                'bower_components/blueimp-file-upload/css/jquery.fileupload-ui.css',
                'bower_components/animate.css/animate.min.css'
            ]
        },
        html: {
            components: ['src/ui-components/**/*.html'],
            screenconfig: ['index.html', 'src/config/**/*.html']
        },
        images: {
            components: ['src/ui-components/images/**/*'],
            screenconfig: ['images/**/*']
        },
        fonts: [
            'bower_components/bootstrap/dist/fonts/*',
            'bower_components/fontawesome/fonts/*',
            'bower_components/footable/css/fonts/*'
        ],
        resources: {
            json: ['../data/**/*.json'],
            html: ['../data/**/*.html']
        }
    };

    var browserSync = require('browser-sync');
    var reload = browserSync.reload;

    function runBrowserSync(baseDir, isProd) {
        isProd = isProd || false;
        var options = {
            server: {
                baseDir: baseDir,
                middleware: function(req, res, next) {
                    var gzip = compress();
                    gzip(req, res, next);
                }
            },
            https: false,
            port: argv.port || 8888,
            reloadOnRestart: !isProd,
            ghostMode: false,
            notify: false,
            open: false
        };

        browserSync(options);
    }

    gulp.task('reload-browser', function() {
        reload();
    });

    var minifyHTMLOptions = {
        empty: true,
        quotes: true,
        spare: true
    };

    var imageminOptions = {
        progressive: true,
        svgoPlugins: [{
            removeViewBox: false
        }],
        use: [pngquant()]
    };

    /******************************* serve *******************************/

    gulp.task('serve', function() {
        runBrowserSync('dist');
    });

    gulp.task('run-prod', function() {
        runBrowserSync('dist', true);
    });

    gulp.task('serve-prod', function(cb) {
        runSequence('build-prod', 'run-prod', cb);
    });

    /******************************** clean ******************************/

    gulp.task('clean', function(cb) {
        del(['dist/**/*.*'], cb);
    });

    /******************************** build-components ******************************/

    gulp.task('build-components-js', function() {
        return gulp.src(src.js.components)
            .pipe(jscs(jscsOptions))
            .on('error', noop)
            .pipe(stylish())
            .pipe(eslint())
            .pipe(eslint.format())
            .pipe(concat(componentsJsFileName))
            .pipe(ngAnnotate())
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distComponentsJs));
    });

    gulp.task('build-components-html', function() {
        return gulp.src(src.html.components)
            .pipe(concat(componentsTemplateFileName))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(distComponents));
    });

    gulp.task('build-components-css', function() {
        return gulp.src(src.css.components)
            .pipe(autoprefixer())
            .pipe(concat('components.min.css'))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(distComponentsCss));
    });

    gulp.task('build-components-images', function() {
        return gulp.src(src.images.components)
            .pipe(gulpif(isProduction, imagemin(imageminOptions)))
            .pipe(gulp.dest(distComponentsImages));
    });

    gulp.task('build-components', [
        'build-components-js',
        'build-components-html',
        'build-components-css',
        'build-components-images'
    ]);

    /********************************* build-screenconfig *****************************/

    gulp.task('build-screenconfig-js', function() {
        return gulp.src(src.js.screenconfig)
            .pipe(jscs(jscsOptions))
            .on('error', noop)
            .pipe(stylish())
            .pipe(eslint())
            .pipe(eslint.format())
            .pipe(concat(screenConfigJsFileName))
            .pipe(ngAnnotate())
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('build-screenconfig-vendor-js', function() {
        return gulp.src(src.js.vendor)
            .pipe(concat(screenConfigVendorJsFileName))
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('build-screenconfig-html', function() {
        return gulp.src(src.html.screenconfig)
            .pipe(rename(function(p) {
                if (['index', 'configurator'].indexOf(p.basename) === -1) {
                    p.dirname = path.join('templates', p.dirname);
                }
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('build-screenconfig-fonts', function() {
        return gulp.src(src.fonts)
            .pipe(rename({
                dirname: ''
            }))
            .pipe(gulp.dest(distFonts))
            .pipe(gulp.dest(path.join(dist, 'css', 'fonts')));
    });

    gulp.task('build-screenconfig-css', function() {
        return gulp.src(src.css.screenconfig)
            .pipe(concat('screenconfig.css'))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(distCss));
    });

    gulp.task('build-screenconfig-vendor-css', function() {
        return gulp.src(src.css.vendor)
            .pipe(concat('screenconfig.vendor.css'))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(distCss));
    });

    gulp.task('copy-pdf-worker-js', function() {
        return gulp.src(src.js.pdfWorker)
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('copy-resources-json', function() {
        return gulp.src(src.resources.json)
            .pipe(rename(function(p) {
                p.dirname = path.join('data', p.dirname);
            }))
            .pipe(jsonlint())
            .pipe(jsonlint.reporter())
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-resources-html', function() {
        return gulp.src(src.resources.html)
            .pipe(rename(function(p) {
                p.dirname = path.join('data', p.dirname);
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('build-screenconfig', [
        'build-screenconfig-js',
        'build-screenconfig-vendor-js',
        'copy-pdf-worker-js',
        'build-screenconfig-html',
        'build-screenconfig-fonts',
        'build-screenconfig-css',
        'build-screenconfig-vendor-css',
        'copy-resources-json',
        'copy-resources-html'
    ]);

    function watchAndReload(watchSrc, watchOptions, tasks) {
        watchOptions = watchOptions || {
            debounceDelay: 1000
        };

        tasks.push('reload-browser');
        gulp.watch(watchSrc, watchOptions, function() {
            runSequence.apply(this, tasks);
        });
    }

    gulp.task('watch', function() {
        watchAndReload(src.js.screenconfig, null, ['build-screenconfig-js']);
        watchAndReload(src.html.screenconfig, null, ['build-screenconfig-html']);
        watchAndReload(src.css.screenconfig, null, ['build-screenconfig-css']);
        watchAndReload(src.fonts, null, ['build-screenconfig-fonts']);
        watchAndReload(src.js.components, null, ['build-components-js']);
        watchAndReload(src.html.components, null, ['build-components-html']);
        watchAndReload(src.css.components, null, ['build-components-css']);
        watchAndReload(src.images.components, null, ['build-components-images']);
        watchAndReload(src.resources.json, null, ['copy-resources-json']);
        watchAndReload(src.resources.html, null, ['copy-resources-html']);
    });

    gulp.task('build', function(cb) {
        runSequence('clean', ['build-components', 'build-screenconfig'], cb);
    });

    gulp.task('build-prod', function(cb) {
        isProduction = true;
        runSequence('build', cb);
    });

    gulp.task('default', function(cb) {
        runSequence('build', 'serve', 'watch', cb);
    });
})();
