/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'currencyFormatConfig';
    var templatePath = 'templates/formatter/' + directiveName + '.html';

    /*@ngInject*/
    function currencyFormatConfig() {
        return {
            scope: {
                field: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: currencyFormatConfigCtrl
        };
    }

    /*@ngInject*/
    function currencyFormatConfigCtrl() {

    }

    angular.module('screenConfigurator').directive(directiveName, currencyFormatConfig);
})();
