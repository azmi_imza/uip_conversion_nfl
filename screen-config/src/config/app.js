/* globals saveAs, Blob, FileReader */
/* eslint no-alert:0 */

(function() {
    'use strict';
    var app = angular.module('configurator', []);

    app.run(function($log, $rootScope, $timeout) {
        window.onbeforeunload = function() {
            return 'Have you saved the screen configuration?';
        };

        $rootScope.resetViewport = function() {
            $log.debug('$rootScope.resetViewport');
            $timeout(function() {
                $('#viewport').attr('content',
                    'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1');

                $timeout(function() {
                    $('#viewport').attr('content', 'width=device-width, initial-scale=1');
                }, 100);
            });
        };
    });
})();

(function() {
    'use strict';

    function utils($log) {
        this.guid = function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        this.moveItem = function moveItem(arrays, fromIndex, toIndex) {
            if (!arrays) {
                $log.error('unable to move item, arrays undefined');
                return;
            }

            if (fromIndex < 0 || fromIndex > (arrays.length - 1)) {
                $log.error('unable to move item, invalid fromIndex');
                return;
            }

            if (toIndex < 0 || toIndex > (arrays.length - 1)) {
                $log.error('unable to move item, invalid toIndex');
                return;
            }

            arrays.splice(toIndex, 0, arrays.splice(fromIndex, 1)[0]);
        };
    }

    angular.module('configurator').service('utils', utils);
})();

(function() {
    'use strict';

    /*@ngInject*/
    function configurator() {
        return {
            scope: {},
            templateUrl: 'configurator.html',
            bindToController: true,
            controllerAs: 'ctrl',
            controller: configuratorCtrl
        };
    }

    /*@ngInject*/
    function configuratorCtrl($log, $attrs, $element, $timeout, previewStorageService, utils, API_PATHS, $http) {
        var ctrl = this;
        var componentsMap = {
            'dynamicForm': {
                'configTag': 'dynamic-form-configurator',
                'title': 'Form Configurator'
            },
            'tableListing': {
                'configTag': 'table-configurator',
                'title': 'Table Configurator'
            }
        };

        ctrl.componentsMap = componentsMap;
        ctrl.layout = {
            components: []
        };
        ctrl.inputList = {};
        ctrl.srcInputList = {};
        ctrl.destInputList = {};

        ctrl.addComponent = addComponent;
        ctrl.removeComponent = removeComponent;
        ctrl.preview = preview;
        ctrl.generateLayout = generateLayout;
        ctrl.loadLayout = loadLayout;
        ctrl.loadServerLayout = loadServerLayout;
        ctrl.saveServerLayout = saveServerLayout;
        ctrl.deleteServerLayout = deleteServerLayout;
        ctrl.updateServerLayout = updateServerLayout;
        ctrl.enterToAddComponent = enterToAddComponent;
        ctrl.moveComponent = moveComponent;

        ctrl.showBtn = 'saveBtn';

        var newPath = API_PATHS.FORM_LAYOUT;

        function loadServerLayout() {

            if (ctrl.screenCode) {
                newPath = newPath.replace('{screenCode}', ctrl.screenCode);

                $http.get(newPath).
                success(function(data) {
                        if (data) {
                            $log.debug('Load successfully.');
                            ctrl.showBtn = 'updateBtn';
                            ctrl.layout = undefined;

                            $timeout(function() {
                                ctrl.layout = null;
                                ctrl.inputList = {};

                                var layout = angular.fromJson(data);
                                layout = layout.layout;

                                if (!layout || !layout.components) {
                                    return;
                                }

                                layout.components.forEach(function(component) {
                                    if (!component.id) {
                                        component.id = utils.guid();
                                    }

                                    if (!component.srcMappings) {
                                        component.srcMappings = [];
                                    }

                                    if (!component.destMappings) {
                                        component.destMappings = [];
                                    }

                                    var sections = null;
                                    if (component.type === 'dynamicForm') {
                                        sections = component.sections;
                                    } else if (component.type === 'tableListing') {
                                        sections = component.filters.sections;
                                    }

                                    if (!sections) {
                                        return;
                                    }

                                    loadSections(sections, component);
                                });

                                ctrl.layout = layout;
                            }, 100);
                        } else {
                            alert('No data for this screen code');
                        }
                    })
                    .
                error(function() {
                    $log.error('Load failed. Please try again.');
                    alert('Load failed. Please try again.');
                });
            } else {
                alert('No screen code defined for this layout');
            }
        }

        function saveServerLayout() {

            if (ctrl.screenCode) {
                var layout = ctrl.layout;

                newPath = newPath.replace('{screenCode}', ctrl.screenCode);

                $http.post(newPath, {
                    layout: layout
                }).
                success(function() {
                    alert('Save succesfully');
                    ctrl.showBtn = 'updateBtn';
                }).
                error(function() {
                    alert('Save failed. Please try again.');
                });

            } else {
                alert('No screen code defined for this layout');
            }
        }

        function updateServerLayout() {

            if (ctrl.screenCode) {
                var layout = ctrl.layout;

                newPath = newPath.replace('{screenCode}', ctrl.screenCode);

                $http.put(newPath, {
                    layout: layout
                }).
                success(function() {
                    alert('Update succesfully');
                }).
                error(function() {
                    alert('Update failed. Please try again.');
                });

            } else {
                alert('No screen code defined for this layout');
            }
        }

        function deleteServerLayout() {

            if (ctrl.screenCode) {
                newPath = newPath.replace('{screenCode}', ctrl.screenCode);
                $http.delete(newPath).
                success(function() {
                    alert('Delete successfully.');
                    window.location.reload(false);
                    ctrl.showBtn = 'saveBtn';
                }).
                error(function() {
                    alert('Delete failed. Please try again.');
                });

            } else {
                alert('No screen code defined for this layout');
            }
        }

        function moveComponent(from, to) {
            utils.moveItem(ctrl.layout.components, from, to);
        }

        function enterToAddComponent($event) {
            if ($event.keyCode === 13) {
                addComponent();
            }
        }

        function loadLayout() {
            ctrl.showBtn = 'saveBtn';
            $log.log('load layout');
            $('#loadfile').click();
        }

        function loadSections(sections, component) {
            // load existing fields (for dependency reference)
            var inputFieldTypes = [
                'singleLineText',
                'multiLineText',
                'datePicker',
                'dropDownList',
                'radioGroup',
                'checkbox',
                'fileUpload',
                'autoCompleteInput',
                'buttonGroup'
            ];

            sections.forEach(function(section) {
                if (!section.id) {
                    section.id = utils.guid();
                }

                if (!section.body || !section.body.rowGroups) {
                    return;
                }

                if (section.body.loopData && section.body.loopData.content && section.body.loopData.content.rows) {
                    section.body.loopData.content.rows.forEach(function(row) {
                        if (!row.id) {
                            row.id = utils.guid();
                        }
                    });
                }

                var rowGroups = section.body.rowGroups;
                rowGroups.forEach(function(rowGroup) {
                    if (!rowGroup.id) {
                        rowGroup.id = utils.guid();
                    }

                    if (!rowGroup.rows) {
                        return;
                    }

                    var rows = rowGroup.rows;
                    rows.forEach(function(row) {
                        if (!row.id) {
                            row.id = utils.guid();
                        }

                        if (!row.columns) {
                            return;
                        }

                        row.columns.forEach(function(column) {
                            if (!column.id) {
                                column.id = utils.guid();
                            }

                            if (inputFieldTypes.indexOf(column.type) === -1) {
                                return;
                            }

                            if (!ctrl.inputList[component.id]) {
                                ctrl.inputList[component.id] = [];
                            }

                            ctrl.inputList[component.id].push(column.name);

                            if (!ctrl.srcInputList[component.id]) {
                                ctrl.srcInputList[component.id] = [];
                            }

                            ctrl.srcInputList[component.id].push(column.name);

                            if (!ctrl.destInputList[component.id]) {
                                ctrl.destInputList[component.id] = [];
                            }

                            ctrl.destInputList[component.id].push(column.name);

                        });
                    });

                });

            });
        }

        $('#loadfile').on('change', function() {
            var input = document.getElementById('loadfile');
            ctrl.layout = undefined;

            var reader = new FileReader();
            reader.onload = function() {
                var text = reader.result;
                $timeout(function() {
                    ctrl.layout = null;
                    ctrl.inputList = {};

                    var layout = angular.fromJson(text);

                    if (!layout || !layout.components) {
                        return;
                    }

                    layout.components.forEach(function(component) {
                        if (!component.id) {
                            component.id = utils.guid();
                        }

                        if (!component.srcMappings) {
                            component.srcMappings = [];
                        }

                        if (!component.destMappings) {
                            component.destMappings = [];
                        }

                        var sections = null;
                        if (component.type === 'dynamicForm') {
                            sections = component.sections;
                        } else if (component.type === 'tableListing') {
                            sections = component.filters.sections;
                        }

                        if (!sections) {
                            return;
                        }

                        loadSections(sections, component);
                    });

                    ctrl.layout = layout;
                }, 100);
            };

            if (input && input.files && input.files.length > 0) {
                reader.readAsText(input.files[0]);
            }
        });

        function addComponent() {
            if (!ctrl.selectedComponent) {
                alert('Please select type of component');
                return;
            }

            if (!ctrl.newComponentName) {
                alert('Please enter component name');
                return;
            }

            for (var i = 0; i < ctrl.layout.components.length; i++) {
                if (ctrl.newComponentName === ctrl.layout.components[i].name) {

                    alert('Component name already existed');
                    return;
                }
            }

            var selectedComponent = ctrl.selectedComponent;
            var componentId = utils.guid();
            $log.log('Add Component: ' + selectedComponent);

            var componentModel = {
                id: componentId,
                type: selectedComponent,
                name: ctrl.newComponentName,
                attrs: {},
                srcMappings: [],
                destMappings: []
            };

            if (selectedComponent === 'dynamicForm') {
                //componentModel.apiPath = '';
                ctrl.inputList[componentModel.id] = [];
                ctrl.srcInputList[componentModel.id] = [];
                ctrl.destInputList[componentModel.id] = [];
                componentModel.actions = [];
                componentModel.mode = 'edit';
                var error = {
                    'class': 'alert alert-danger',
                    'style': 'margin-bottom:20px;text-align:left;',
                    'attrs': {
                        'role': 'alert'
                    },
                    'errorItem': {
                        'prefix': {
                            'class': 'glyphicon glyphicon-remove-sign',
                            'style': '',
                            'attrs': {
                                'aria-hidden': 'true'
                            }
                        },
                        'message': {
                            'class': '',
                            'style': 'padding-left:5px',
                            'attrs': {}
                        },
                        'postfix': {
                            'class': '',
                            'style': '',
                            'attrs': {}
                        }
                    }
                };
                componentModel.error = error;

                componentModel.header = {
                    attrs: {}
                };

                componentModel.sections = [];
            } else if (selectedComponent === 'tableListing') {
                componentModel.pageSize = '';
                componentModel.dataSource = '';
                componentModel.filters = {
                    filterSource: '',
                    sections: []
                };
                componentModel.fields = [];
                componentModel.footer = {};
            }

            ctrl.layout.components.push(componentModel);
            ctrl.selectedComponent = '';
            ctrl.newComponentName = '';

            $log.log(ctrl.layout);
        }

        function removeComponent(index) {
            $log.log('remove component');
            ctrl.layout.components.splice(index, 1);

            if (!ctrl.layout.components || ctrl.layout.components.length === 0) {
                ctrl.layout.title = '';
            }
        }

        function preview() {
            $log.log('preview');
            if (!ctrl.layout) {
                alert('no layout defined');
                return;
            }

            if (!ctrl.layout.components) {
                alert('no components defined');
                return;
            }

            previewStorageService.setPreviewLayout(ctrl.layout);

            window.open(window.location.origin + window.location.pathname + '#/preview', '_blank');
        }

        function generateLayout() {
            var clonedModel = angular.copy(ctrl.layout);

            $log.log('generate layout');
            $log.log(angular.toJson(clonedModel));
            var fileName = 'layout.json';

            var blobType = 'octet/stream';

            var json = angular.toJson(clonedModel);
            var blob = new Blob([json], {
                type: blobType
            });

            try {
                saveAs(blob, fileName);
            } catch (e) {
                window.open('data:' + blobType + ',' + encodeURIComponent(json), '_blank', '');
            }

            // a.href = url;
            // a.download = fileName;

            // document.body.appendChild(a);
            // a.click();
            // window.URL.revokeObjectURL(url);
        }
    }

    angular.module('configurator').directive('configurator', configurator);
})();

(function() {
    'use strict';
    angular.module('screenConfigurator', [
        'ngResource',
        'ui.router',
        'ngCookies',
        'ui.bootstrap',
        'pascalprecht.translate',
        'preview',
        'configurator',
        'hfits.components'
    ]).config(
        /*@ngInject*/
        function($stateProvider, $urlRouterProvider, $provide) {
            // add lodash as injectable constant object
            $provide.constant('_', window._);

            // romove lodash from global reference to prevent direct usage
            delete window._;

            $stateProvider.state('configurator', {
                url: '/',
                template: '<configurator></configurator>'
            });
            $urlRouterProvider.otherwise('/');

            // $urlRouterProvider.otherwise(function($injector, $location) {
            //     var path = $location.path();
            //     var routeService = $injector.get('routeService');

            //     if (!path || path === '/') {
            //         $location.replace().path(routeService.getHomeUrl());
            //         return $location.path();
            //     }

            //     $location.replace().path(routeService.getPageNotFoundUrl());
            //     return $location.path();
            // });
        });
})();
