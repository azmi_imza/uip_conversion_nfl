/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'formSectionsGroupPanel';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formSectionsGroupPanel() {
        return {
            templateUrl: templatePath,
            scope: {
                formSections: '=',
                formName: '=',
                formMode: '=',
                inputList: '=?',
                srcInputList: '=?',
                destInputList: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formSectionsGroupPanelCtrl
        };
    }

    /*@ngInject*/
    function formSectionsGroupPanelCtrl($attrs, $element, utils) {
        var ctrl = this;
        ctrl.addFormSection = addFormSection;
        ctrl.removeFormSection = removeFormSection;
        ctrl.inputList = ctrl.inputList || [];
        ctrl.srcInputList = ctrl.srcInputList || [];
        ctrl.destInputList = ctrl.destInputList || [];
        ctrl.enterToAddFormSection = enterToAddFormSection;
        ctrl.moveSection = moveSection;

        function moveSection(from, to) {
            utils.moveItem(ctrl.formSections, from, to);
        }

        function enterToAddFormSection($event) {
            if ($event.keyCode === 13) {
                addFormSection();
            }
        }

        function addFormSection() {
            if (!ctrl.sectionName) {
                alert('Please enter form section name');
                return;
            }

            // if (!ctrl.sectionMode) {
            //     alert('Please select a form section mode');
            //     return;
            // }

            var sectionId = utils.guid();
            var section = {
                id: sectionId,
                name: ctrl.sectionName.replace(/ /g, '_'),
                header: {},
                body: {
                    rows: []
                },
                footer: {}
            };

            if (ctrl.sectionMode) {
                section.mode = ctrl.sectionMode;
            }

            ctrl.formSections.push(section);
            ctrl.sectionName = '';
            ctrl.sectionMode = '';
        }

        function removeFormSection(id) {
            ctrl.formSections = ctrl.formSections.filter(function(section) {
                if (section.id === id) {
                    return false;
                }

                return true;
            });
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formSectionsGroupPanel);
})();
