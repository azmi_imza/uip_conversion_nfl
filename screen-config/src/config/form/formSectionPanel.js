/* eslint no-alert:0 */

(function() {
    'use strict';

    var directiveName = 'formSectionPanel';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formSectionPanel() {
        return {
            scope: {
                section: '=',
                formInputList: '=',
                formSrcInputList: '=',
                formDestInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formSectionPanelCtrl
        };
    }

    /*@ngInject*/
    function formSectionPanelCtrl($attrs, $element, utils) {
        var ctrl = this;
        ctrl.addLoopContentRow = addLoopContentRow;
        ctrl.addRowGroup = addRowGroup;
        ctrl.removeRowGroup = removeRowGroup;
        ctrl.addFieldGroup = addFieldGroup;
        ctrl.removeFormSectionRow = removeFormSectionRow;
        ctrl.dependencyTypes = ['visible'];
        ctrl.moveRow = moveRow;

        function moveRow(from, to, group) {
            // if (ctrl.section.body.rows) {
            //     ctrl.section.body.rows.splice(to, 0, ctrl.section.body.rows.splice(from, 1)[0]);
            // }

            utils.moveItem(group, from, to);
        }

        function addLoopContentRow() {
            if (!ctrl.section.body.loopData.name) {
                alert('Loop Data Name is required for adding content row');
                return;
            }

            var rowId = utils.guid();
            var row = {
                id: rowId,
                columns: []
            };

            if (!ctrl.section.body) {
                ctrl.section.body = {};
            }

            if (!ctrl.section.body.loopData) {
                ctrl.section.body.loopData = {};
            }

            if (!ctrl.section.body.loopData.content) {
                ctrl.section.body.loopData.content = {};
            }

            if (!ctrl.section.body.loopData.content.rows) {
                ctrl.section.body.loopData.content.rows = [];
            }

            ctrl.section.body.loopData.content.rows.push(row);
        }

        function addRowGroup() {
            var rowGroupId = utils.guid();

            var rowGroup = {
                id: rowGroupId,
                rows: []
            };

            if (!ctrl.section.body) {
                ctrl.section.body = {};
            }

            if (!ctrl.section.body.rowGroups) {
                ctrl.section.body.rowGroups = [];
            }

            ctrl.section.body.rowGroups.push(rowGroup);
        }

        function removeRowGroup(id) {
            ctrl.section.body.rowGroups = ctrl.section.body.rowGroups.filter(function(rowGroup) {
                if (rowGroup.id === id) {
                    return false;
                }

                return true;
            });
        }

        function addFieldGroup(rowGroup) {
            var rowId = utils.guid();
            var row = {
                id: rowId,
                columns: []
            };

            rowGroup.rows.push(row);
        }

        function removeFormSectionRow(rowGroup, id) {
            rowGroup.rows = rowGroup.rows.filter(function(row) {
                if (row.id === id) {
                    return false;
                }

                return true;
            });
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formSectionPanel);
})();
