(function() {
    'use strict';
    var directiveName = 'formHeaderAttrsPanel';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formHeaderAttrsPanel() {
        return {
            scope: {
                formHeaderAttrs: '=',
                formName: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formHeaderAttrsPanelCtrl
        };
    }

    /*@ngInject*/
    function formHeaderAttrsPanelCtrl() {

    }

    angular.module('screenConfigurator').directive(directiveName, formHeaderAttrsPanel);
})();
