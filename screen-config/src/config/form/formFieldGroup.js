/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'formFieldGroup';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formFieldGroup() {
        return {
            scope: {
                fieldGroup: '=',
                formInputList: '=',
                formSrcInputList: '=',
                formDestInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formFieldGroupCtrl
        };
    }

    /*@ngInject*/
    function formFieldGroupCtrl($attrs, $element, utils) {
        var ctrl = this;
        ctrl.addField = addField;
        ctrl.removeField = removeField;
        ctrl.moveField = moveField;

        function moveField(from, to) {
            utils.moveItem(ctrl.fieldGroup.columns, from, to);
        }

        function addField() {
            if (!ctrl.selectedInputType) {
                alert('Please select a field type');
                return;
            }

            if (!ctrl.fieldName) {
                alert('Please enter field name');
                return;
            }

            ctrl.fieldName = ctrl.fieldName.replace(/ /g, '_');

            for (var i = 0; i < ctrl.formInputList.length; i++) {
                if (ctrl.fieldName === ctrl.formInputList[i]) {

                    alert('Field name already existed');
                    return;
                }
            }

            var field = {
                name: ctrl.fieldName,
                id: ctrl.fieldName + 'Id',
                type: ctrl.selectedInputType,
                label: {
                    name: '',
                    tooltip: ''
                },
                class: '',
                style: '',
                attrs: {},
                validators: {},
                dependencies: []
            };

            if (!ctrl.fieldGroup.columns) {
                ctrl.fieldGroup.columns = [];
            }

            ctrl.fieldGroup.columns.push(field);

            if (!ctrl.formInputList) {
                ctrl.formInputList = [];
            }

            ctrl.formInputList.push(ctrl.fieldName);

            if (!ctrl.formSrcInputList) {
                ctrl.formSrcInputList = [];
            }

            ctrl.formSrcInputList.push(ctrl.fieldName);

            if (!ctrl.formDestInputList) {
                ctrl.formDestInputList = [];
            }

            ctrl.formDestInputList.push(ctrl.fieldName);

            ctrl.selectedInputType = '';
            ctrl.fieldName = '';
        }

        function removeField(name) {
            ctrl.fieldGroup.columns = ctrl.fieldGroup.columns.filter(function(column) {
                if (column.name === name) {
                    return false;
                }

                return true;
            });

            ctrl.formInputList = ctrl.formInputList.filter(function(n) {
                if (n === name) {
                    return false;
                }

                return true;
            });

            ctrl.formSrcInputList = ctrl.formSrcInputList.filter(function(n) {
                if (n === name) {
                    return false;
                }

                return true;
            });

            ctrl.formDestInputList = ctrl.formDestInputList.filter(function(n) {
                if (n === name) {
                    return false;
                }

                return true;
            });
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formFieldGroup);
})();
