/* eslint no-alert:0 */

(function() {
    'use strict';

    var directiveName = 'formAttrsPanel';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formAttrsPanel() {
        return {
            scope: {
                formAttrs: '=',
                formName: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formAttrsPanelCtrl
        };
    }

    /*@ngInject*/
    function formAttrsPanelCtrl($element, $log) {
        var ctrl = this;
        $log.debug(directiveName + ' controller');
        $log.debug(ctrl.formAttrs);
        ctrl.addFormAttribute = addFormAttribute;
        ctrl.removeFormAttribute = removeFormAttribute;

        function addFormAttribute() {
            if (!ctrl.newAttrName || !ctrl.newAttrName.trim()) {
                alert('Please enter attribute name');
                return;
            }

            if (!ctrl.newAttrValue || !ctrl.newAttrValue.trim()) {
                alert('Please enter attribute value');
                return;
            }

            $log.debug('attribute name: ' + ctrl.newAttrName);
            $log.debug('attribute value: ' + ctrl.newAttrValue);

            ctrl.formAttrs[ctrl.newAttrName] = ctrl.newAttrValue;

            ctrl.newAttrName = '';
            ctrl.newAttrValue = '';

            $log.debug('form attribute added');
            $log.debug(ctrl.formAttrs);
        }

        function removeFormAttribute(attrName) {
            $log.debug('remove form attribute: ' + attrName);
            $log.debug('before delete');
            $log.debug(ctrl.formAttrs);

            delete ctrl.formAttrs[attrName];

            $log.debug('after delete');
            $log.debug(ctrl.formAttrs);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formAttrsPanel);
})();
