/* eslint no-alert:0 */

(function() {
    'use strict';

    var directiveName = 'formErrorPanel';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function formErrorPanel() {
        return {
            scope: {
                formError: '=',
                formName: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formErrorPanelCtrl
        };
    }

    /*@ngInject*/
    function formErrorPanelCtrl($log) {
        var ctrl = this;
        ctrl.addErrorAttribute = addErrorAttribute;
        ctrl.addErrorChildAttribute = addErrorChildAttribute;
        ctrl.removeErrorAttribute = removeErrorAttribute;
        ctrl.removeErrorChildAttribute = removeErrorChildAttribute;

        function addErrorAttribute() {
            if (!ctrl.newErrorAttrName || !ctrl.newErrorAttrName.trim()) {
                alert('Please enter error attribute name');
                return;
            }

            if (!ctrl.newErrorAttrValue || !ctrl.newErrorAttrValue.trim()) {
                alert('Please enter error attribute value');
                return;
            }

            $log.debug('error attribute name: ' + ctrl.newErrorAttrName);
            $log.debug('error attribute value: ' + ctrl.newErrorAttrValue);

            ctrl.formError.attrs[ctrl.newErrorAttrName] = ctrl.newErrorAttrValue;

            ctrl.newErrorAttrName = '';
            ctrl.newErrorAttrValue = '';

            $log.debug('form error attribute added');
            $log.debug(ctrl.formError.attrs);
        }

        function addErrorChildAttribute() {
            if (!ctrl.newErrorChildAttrName || !ctrl.newErrorChildAttrName.trim()) {
                alert('Please enter error child attribute name');
                return;
            }

            if (!ctrl.newErrorChildAttrValue || !ctrl.newErrorChildAttrValue.trim()) {
                alert('Please enter error child attribute value');
                return;
            }

            $log.debug('error child attribute name: ' + ctrl.newErrorChildAttrName);
            $log.debug('error child attribute value: ' + ctrl.newErrorChildAttrValue);

            ctrl.formError.childAttrs[ctrl.newErrorChildAttrName] = ctrl.newErrorChildAttrValue;

            ctrl.newErrorChildAttrName = '';
            ctrl.newErrorChildAttrValue = '';

            $log.debug('form error child attribute added');
            $log.debug(ctrl.formError.childAttrs);
        }

        function removeErrorAttribute(attrName) {
            $log.debug('remove form error attribute: ' + attrName);
            $log.debug('before delete');
            $log.debug(ctrl.formError.attrs);

            delete ctrl.formError.attrs[attrName];

            $log.debug('after delete');
            $log.debug(ctrl.formError.attrs);
        }

        function removeErrorChildAttribute(attrName) {
            $log.debug('remove form error child attribute: ' + attrName);
            $log.debug('before delete');
            $log.debug(ctrl.formError.childAttrs);

            delete ctrl.formError.childAttrs[attrName];

            $log.debug('after delete');
            $log.debug(ctrl.formError.childAttrs);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formErrorPanel);
})();
