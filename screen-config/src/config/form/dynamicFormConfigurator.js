/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'dynamicFormConfigurator';
    var templatePath = 'templates/form/' + directiveName + '.html';

    /*@ngInject*/
    function dynamicFormConfigurator() {
        return {
            scope: {
                componentConfig: '=',
                componentModel: '=',
                inputList: '=',
                srcInputList: '=',
                destInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dynamicFormConfiguratorCtrl
        };
    }

    /*@ngInject*/
    function dynamicFormConfiguratorCtrl($element, $log) {
        $log.debug('dynamicFormConfigurator controller');
        var ctrl = this;
        ctrl.addAction = addAction;
        ctrl.removeAction = removeAction;
        ctrl.inputList = ctrl.inputList || [];
        ctrl.srcInputList = ctrl.srcInputList || [];
        ctrl.destInputList = ctrl.destInputList || [];
        ctrl.addSrcMapping = addSrcMapping;
        ctrl.removeSrcMapping = removeSrcMapping;
        ctrl.addDestMapping = addDestMapping;
        ctrl.removeDestMapping = removeDestMapping;

        function addAction() {
            var action = {
                label: '',
                type: '',
                apiPath: ''
            };

            if (!ctrl.componentModel.actions) {
                ctrl.componentModel.actions = [];
            }

            ctrl.componentModel.actions.push(action);
        }

        function removeAction(index) {
            ctrl.componentModel.actions.splice(index, 1);
        }

        function addSrcMapping() {
            var srcMapping = {
                from: '',
                to: ctrl.selectedSrcMappingName
            };

            if (!ctrl.componentModel.srcMappings) {
                ctrl.componentModel.srcMappings = [];
            }

            if (!ctrl.selectedSrcMappingName) {
                alert('Please select a name.');
                return;
            }

            ctrl.componentModel.srcMappings.push(srcMapping);
        }

        function removeSrcMapping(index) {
            ctrl.componentModel.srcMappings.splice(index, 1);
        }

        function addDestMapping() {
            var destMapping = {
                from: ctrl.selectedDestMappingName,
                to: ''
            };

            if (!ctrl.componentModel.destMappings) {
                ctrl.componentModel.destMappings = [];
            }

            if (!ctrl.selectedDestMappingName) {
                alert('Please select a name.');
                return;
            }

            ctrl.componentModel.destMappings.push(destMapping);
        }

        function removeDestMapping(index) {
            ctrl.componentModel.destMappings.splice(index, 1);
        }

        ctrl.checkSrcDependency = function(name) {
            for (var i = 0; i < ctrl.componentModel.srcMappings.length; i++) {
                if (ctrl.componentModel.srcMappings[i].to === name) {
                    return false;
                }
            }

            return true;
        };

        ctrl.checkDestDependency = function(name) {
            for (var i = 0; i < ctrl.componentModel.destMappings.length; i++) {
                if (ctrl.componentModel.destMappings[i].from === name) {
                    return false;
                }
            }

            return true;
        };
    }

    angular.module('screenConfigurator').directive(directiveName, dynamicFormConfigurator);
})();
