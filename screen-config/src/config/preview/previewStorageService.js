(function() {
    'use strict';

    var serviceName = 'previewStorageService';

    /*@ngInject*/
    function previewStorageService() {
        this.setPreviewLayout = function(layoutData) {
            window.localStorage.setItem('preview_layout', angular.toJson(layoutData));
        };

        this.getPreviewLayout = function() {
            return angular.fromJson(window.localStorage.getItem('preview_layout'));
        };
    }

    angular.module('preview').service(serviceName, previewStorageService);
})();
