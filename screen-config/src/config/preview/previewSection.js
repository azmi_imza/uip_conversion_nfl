(function() {
    'use strict';
    var directiveName = 'previewSection';

    /*@ngInject*/
    function previewSection() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: previewSectionCtrl,
            templateUrl: 'templates/preview/preview.html'
        };
    }

    /*@ngInject*/
    function previewSectionCtrl($timeout, previewStorageService) {
        var ctrl = this;
        var layoutData = previewStorageService.getPreviewLayout();

        $timeout(function() {
            if (layoutData) {
                ctrl.layoutData = layoutData;
            }
        }, 500);
    }

    angular.module('preview').directive(directiveName, previewSection);
})();
