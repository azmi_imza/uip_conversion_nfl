(function() {
    'use strict';

    angular.module('preview', ['hfits.components'])
        .config(
            /*@ngInject*/
            function($stateProvider) {
                $stateProvider.state('preview', {
                    url: '/preview',
                    template: '<preview-section></preview-section>'
                });
            });
})();
