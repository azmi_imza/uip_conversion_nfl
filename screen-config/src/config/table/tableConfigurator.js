(function() {
    'use strict';

    var directiveName = 'tableConfigurator';
    var templatePath = 'templates/table/' + directiveName + '.html';

    /*@ngInject*/
    function tableConfigurator() {
        return {
            scope: {
                componentConfig: '=',
                componentModel: '=',
                inputList: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: tableConfiguratorCtrl,
            templateUrl: templatePath
        };
    }

    /*@ngInject*/
    function tableConfiguratorCtrl() {
        var ctrl = this;
        ctrl.componentModel.retrieveOnLoad = true;
    }

    angular.module('screenConfigurator').directive(directiveName, tableConfigurator);
})();
