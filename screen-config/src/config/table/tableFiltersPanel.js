/* eslint no-alert:0 */
(function() {
    'use strict';
    var directiveName = 'tableFiltersPanel';
    var templatePath = 'templates/table/' + directiveName + '.html';

    /*@ngInject*/
    function tableFiltersPanel() {
        return {
            scope: {
                tableFilters: '=',
                tableName: '=',
                inputList: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: tableFiltersPanelCtrl,
            templateUrl: templatePath
        };
    }

    /*@ngInject*/
    function tableFiltersPanelCtrl(utils) {
        var ctrl = this;
        ctrl.addFilterSection = addFilterSection;
        ctrl.removeFilterSection = removeFilterSection;

        function addFilterSection() {
            if (!ctrl.sectionName) {
                alert('Please enter filter section name');
                return;
            }

            if (!ctrl.sectionMode) {
                alert('Please select a filter section mode');
                return;
            }

            var sectionId = utils.guid();
            var section = {
                id: sectionId,
                name: ctrl.sectionName,
                mode: ctrl.sectionMode,
                separator: {},
                body: {
                    rows: []
                },
                footer: {}
            };

            if (!ctrl.tableFilters) {
                ctrl.tableFilters = {};
            }

            if (!ctrl.tableFilters.sections) {
                ctrl.tableFilters.sections = [];
            }

            ctrl.tableFilters.sections.push(section);
            ctrl.sectionName = '';
            ctrl.sectionMode = '';
        }

        function removeFilterSection(id) {
            ctrl.tableFilters.sections = ctrl.tableFilters.sections.filter(function(section) {
                if (section.id === id) {
                    return false;
                }

                return true;
            });
        }
    }

    angular.module('screenConfigurator').directive(directiveName, tableFiltersPanel);
})();
