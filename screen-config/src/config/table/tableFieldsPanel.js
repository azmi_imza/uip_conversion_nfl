/* eslint no-alert:0 */
(function() {
    'use strict';
    var directiveName = 'tableFieldsPanel';
    var templatePath = 'templates/table/' + directiveName + '.html';

    /*@ngInject*/
    function tableFieldsPanel() {
        return {
            scope: {
                tableFields: '=',
                tableName: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: tableFieldsPanelCtrl,
            templateUrl: templatePath
        };
    }

    /*@ngInject*/
    function tableFieldsPanelCtrl() {
        var ctrl = this;
        ctrl.addField = addField;
        ctrl.removeField = removeField;
        ctrl.toggleFieldDisplay = toggleFieldDisplay;
        ctrl.setDefaultField = setDefaultField;

        if (ctrl.tableFields) {
            for (var i = 0; i < ctrl.tableFields.length; i++) {
                var tableField = ctrl.tableFields[i];

                if (tableField.hide && tableField.hide.length > 0) {
                    for (var j = 0; j < tableField.hide.length; j++) {
                        ctrl[tableField.name + '_' + tableField.hide[j]] = true;
                    }
                }
            }
        }

        function addField() {
            if (!ctrl.fieldName) {
                alert('Please enter field name');
                return;
            }

            if (!ctrl.fieldType) {
                alert('Please select a field type');
                return;
            }

            for (var k = 0; k < ctrl.tableFields.length; k++) {
                if (ctrl.fieldName === ctrl.tableFields[k].name) {

                    alert('Field name already existed');
                    return;
                }
            }

            var field = {
                name: ctrl.fieldName,
                type: ctrl.fieldType,
                label: '',
                hide: [],
                ignore: false
            };

            if (ctrl.fieldType === 'date') {
                field.format = 'dd MMM yyyy';
            }

            if (!ctrl.tableFields) {
                ctrl.tableFields = [];
            }

            ctrl.tableFields.push(field);
            ctrl.fieldName = '';
            ctrl.fieldType = '';
        }

        function removeField(index) {
            ctrl.tableFields.splice(index, 1);
        }

        function toggleFieldDisplay(field, device) {
            var index = field.hide.indexOf(device);
            if (index < 0) {
                field.hide.push(device);
            } else {
                field.hide.splice(index, 1);
            }
        }

        function setDefaultField(field) {
            var setDefault = true;
            if (!field.default) {
                setDefault = false;
            }

            for (var k = 0; k < ctrl.tableFields.length; k++) {
                delete ctrl.tableFields[k].default;
            }

            if (setDefault) {
                field.default = true;
            }
        }
    }

    angular.module('screenConfigurator').directive(directiveName, tableFieldsPanel);
})();
