(function() {
    'use strict';

    angular.module('screenConfigurator')
        .constant('API_PATHS', {
            FORM_LAYOUT: 'http://52.74.67.29/uipcfo/rest/formlayout/{screenCode}?systemAccessProfileCode=SYS'
        });
})();
