(function() {
    'use strict';

    var directiveName = 'singleLineTextConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function singleLineTextConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: singleLineTextConfigCtrl
        };
    }

    /*@ngInject*/
    function singleLineTextConfigCtrl() {

    }

    angular.module('screenConfigurator').directive(directiveName, singleLineTextConfig);
})();
