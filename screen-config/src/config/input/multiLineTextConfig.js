(function() {
    'use strict';

    var directiveName = 'multiLineTextConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function multiLineTextConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: multiLineTextConfigCtrl
        };
    }

    /*@ngInject*/
    function multiLineTextConfigCtrl() {
    }

    angular.module('screenConfigurator').directive(directiveName, multiLineTextConfig);
})();
