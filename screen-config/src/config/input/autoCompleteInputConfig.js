(function() {
    'use strict';

    var directiveName = 'autoCompleteInputConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function autoCompleteInputConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: autoCompleteInputConfigCtrl
        };
    }

    /*@ngInject*/
    function autoCompleteInputConfigCtrl() {
        var ctrl = this;
        ctrl.addOptions = addOptions;
        ctrl.removeOptions = removeOptions;

        function addOptions() {
            var option = {
                value: ''
            };

            if (!ctrl.field.listItems) {
                ctrl.field.listItems = [];
            }

            ctrl.field.listItems.push(option);
        }

        function removeOptions(index) {
            ctrl.field.listItems.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, autoCompleteInputConfig);
})();
