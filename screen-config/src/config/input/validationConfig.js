/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'validationConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function validationConfig() {
        return {
            scope: {
                field: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: validationConfigCtrl
        };
    }

    /*@ngInject*/
    function validationConfigCtrl(utils) {
        var ctrl = this;
        ctrl.addValidator = addValidator;
        ctrl.removeValidator = removeValidator;
        ctrl.removePatternValidator = removePatternValidator;
        ctrl.toggleAllowedFileType = toggleAllowedFileType;

        function toggleAllowedFileType(detail, fileType) {
            var index = detail.fileExt.indexOf(fileType);
            if (index < 0) {
                if (fileType === 'JPG') {
                    detail.fileExt.push('JPEG');
                }

                detail.fileExt.push(fileType);
            } else {
                if (fileType === 'JPG') {
                    var indexJPEG = detail.fileExt.indexOf('JPEG');
                    detail.fileExt.splice(indexJPEG, 1);
                }

                detail.fileExt.splice(index, 1);
            }
        }

        function addValidator() {
            if (!ctrl.selectedValidatorType) {
                alert('Please select a validator');
                return;
            }

            if (ctrl.selectedValidatorType === 'isRequired') {
                ctrl.field.validators.isRequired = {
                    message: 'Field is required'
                };
            } else if (ctrl.selectedValidatorType === 'minLength') {
                ctrl.field.validators.minLength = {
                    message: 'Minimum field length is 10',
                    value: 10
                };
            } else if (ctrl.selectedValidatorType === 'maxLength') {
                ctrl.field.validators.maxLength = {
                    message: 'Maximum field length is 50',
                    value: 50
                };
            } else if (ctrl.selectedValidatorType === 'pattern') {
                var validatorId = utils.guid();
                var patternValidator = {
                    message: 'Only alphanumeric value is allowed',
                    value: '^[a-zA-Z0-9\\s]+$',
                    id: validatorId
                };

                if (!ctrl.field.validators.patterns) {
                    ctrl.field.validators.patterns = [];
                }

                ctrl.field.validators.patterns.push(patternValidator);
            } else if (ctrl.selectedValidatorType === 'ajax') {
                ctrl.field.validators.ajax = {
                    url: ''
                };
            } else if (ctrl.selectedValidatorType === 'blockSameFileName') {
                ctrl.field.validators.blockSameFileName = {
                    message: ' is existed in the list. \nPlease upload with a different file name.'
                };
            }

            ctrl.selectedValidatorType = '';
        }

        function removeValidator(key) {
            delete ctrl.field.validators[key];
        }

        function removePatternValidator(key) {
            if (ctrl.field.validators && ctrl.field.validators.patterns) {
                ctrl.field.validators.patterns = ctrl.field.validators.patterns.filter(function(patternValidator) {
                    if (patternValidator.id === key) {
                        return false;
                    }

                    return true;
                });
            }
        }
    }

    angular.module('screenConfigurator').directive(directiveName, validationConfig);
})();
