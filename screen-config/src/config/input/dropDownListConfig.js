(function() {
    'use strict';

    var directiveName = 'dropDownListConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function dropDownListConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dropDownListConfigCtrl
        };
    }

    /*@ngInject*/
    function dropDownListConfigCtrl() {
        var ctrl = this;
        ctrl.addOptions = addOptions;
        ctrl.removeOptions = removeOptions;

        function addOptions() {
            var option = {
                label: '',
                value: ''
            };

            if (!ctrl.field.ddlItems) {
                ctrl.field.ddlItems = [];
            }

            ctrl.field.ddlItems.push(option);
        }

        function removeOptions(index) {
            ctrl.field.ddlItems.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, dropDownListConfig);
})();
