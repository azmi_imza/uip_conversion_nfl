(function() {
    'use strict';

    var directiveName = 'fileUploadConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    function fileUploadConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: fileUploadConfigCtrl
        };
    }

    function fileUploadConfigCtrl() {
        var ctrl = this;
        ctrl.toggleAllowedFileType = toggleAllowedFileType;

        function toggleAllowedFileType(fileType) {
            var index = ctrl.field.allowedType.fileExt.indexOf(fileType);
            if (index < 0) {
                if (fileType === 'JPG') {
                    ctrl.field.allowedType.fileExt.push('JPEG');
                } else if (fileType === 'DOC') {
                    ctrl.field.allowedType.fileExt.push('DOCX');
                }

                ctrl.field.allowedType.fileExt.push(fileType);
            } else {
                if (fileType === 'JPG') {
                    var indexJPEG = ctrl.field.allowedType.fileExt.indexOf('JPEG');
                    ctrl.field.allowedType.fileExt.splice(indexJPEG, 1);
                } else if (fileType === 'DOC') {
                    var indexDOC = ctrl.field.allowedType.fileExt.indexOf('DOCX');
                    ctrl.field.allowedType.fileExt.splice(indexDOC, 1);
                }

                ctrl.field.allowedType.fileExt.splice(index, 1);
            }
        }

        if (!ctrl.field.allowedType) {
            ctrl.field.allowedType = {
                fileExt: [],
                message: 'This is not a supported filetype. \nOnly PDF, JPEG, JPG and PNG filetype are valid.'
            };
        }

        if (!ctrl.field.allowedSize) {
            ctrl.field.allowedSize = {
                value: '25000000',
                message: 'The file size is too large. \nMax allowed file size is 25MB.'
            };
        }

        for (var i = 0; i < ctrl.field.allowedType.fileExt.length; i++) {
            ctrl[ctrl.field.name + ctrl.field.allowedType.fileExt[i]] = true;
        }
    }

    angular.module('screenConfigurator').directive(directiveName, fileUploadConfig);
})();
