/* eslint no-alert:0 */
(function() {
    'use strict';
    var directiveName = 'dependencyConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function dependencyConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '=',
                dependencyTypes: '=?'
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dependencyConfigCtrl
        };
    }

    /*@ngInject*/
    function dependencyConfigCtrl() {
        var ctrl = this;
        ctrl.dependencyTypes = ctrl.dependencyTypes || ['visible', 'disable', 'update', 'reinitialize', 'reset'];
        ctrl.addDependency = addDependency;
        ctrl.removeDependency = removeDependency;
        ctrl.addValue = addValue;
        ctrl.removeValue = removeValue;
        ctrl.addAction = addAction;
        ctrl.removeAction = removeAction;
        ctrl.checkValidDependency = checkValidDependency;
        ctrl.isSupported = isSupported;

        function isSupported(dependency) {
            return ctrl.dependencyTypes.indexOf(dependency) > -1;
        }

        function checkValidDependency(name) {
            if (name === ctrl.field.name) {
                return false;
            }

            if (!ctrl.field.dependencies) {
                return true;
            }

            for (var i = 0; i < ctrl.field.dependencies.length; i++) {
                if (ctrl.field.dependencies[i].watch === name) {
                    return false;
                }
            }

            return true;
        }

        function addDependency() {
            if (!ctrl.selectedDependencyType) {
                alert('Please select a dependency');
                return;
            }

            var dependency = {
                watch: ctrl.selectedDependencyType,
                actionList: {}
            };

            if (!ctrl.field.dependencies) {
                ctrl.field.dependencies = [];
            }

            ctrl.field.dependencies.push(dependency);
            ctrl.selectedDependencyType = '';
        }

        function removeDependency(index) {
            ctrl.field.dependencies.splice(index, 1);
            ctrl.selectedDependencyType = '';
        }

        function addValue(dependency, value) {
            if (!value) {
                alert('Please enter a value');
                return;
            }

            dependency.actionList[value] = [];
            ctrl[dependency.watch] = '';
        }

        function removeValue(dependency, value) {
            delete dependency.actionList[value];
        }

        function addAction(actions) {
            var action = {
                actionType: '',
                value: ''
            };
            actions.push(action);
        }

        function removeAction(actions, actionIndex) {
            actions.splice(actionIndex, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, dependencyConfig);
})();
