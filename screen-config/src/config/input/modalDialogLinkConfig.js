(function() {
    'use strict';

    var directiveName = 'modalDialogLinkConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function modalDialogLinkConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: modalDialogLinkConfigCtrl
        };
    }

    /*@ngInject*/
    function modalDialogLinkConfigCtrl() {

    }

    angular.module('screenConfigurator').directive(directiveName, modalDialogLinkConfig);
})();
