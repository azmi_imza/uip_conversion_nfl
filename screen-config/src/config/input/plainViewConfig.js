(function() {
    'use strict';

    var directiveName = 'plainViewConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function plainViewConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: plainViewConfigCtrl
        };
    }

    /*@ngInject*/
    function plainViewConfigCtrl() {
        var ctrl = this;
        ctrl.dependencyTypes = ['visible'];
    }

    angular.module('screenConfigurator').directive(directiveName, plainViewConfig);
})();
