(function() {
    'use strict';

    var directiveName = 'plainTextConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function plainTextConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: plainTextConfigCtrl
        };
    }

    /*@ngInject*/
    function plainTextConfigCtrl() {
        var ctrl = this;
        ctrl.dependencyTypes = ['visible'];
    }

    angular.module('screenConfigurator').directive(directiveName, plainTextConfig);
})();
