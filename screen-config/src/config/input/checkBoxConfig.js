(function() {
    'use strict';

    var directiveName = 'checkBoxConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function checkBoxConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: checkBoxConfigCtrl
        };
    }

    /*@ngInject*/
    function checkBoxConfigCtrl() {
        var ctrl = this;
        ctrl.addOptions = addOptions;
        ctrl.removeOptions = removeOptions;

        function addOptions() {
            var option = {
                label: '',
                value: ''
            };

            if (!ctrl.field.items) {
                ctrl.field.items = [];
            }

            ctrl.field.items.push(option);
        }

        function removeOptions(index) {
            ctrl.field.items.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, checkBoxConfig);
})();
