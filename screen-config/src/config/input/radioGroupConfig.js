(function() {
    'use strict';

    var directiveName = 'radioGroupConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function radioGroupConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: radioGroupConfigCtrl
        };
    }

    /*@ngInject*/
    function radioGroupConfigCtrl() {
        var ctrl = this;
        ctrl.addOptions = addOptions;
        ctrl.removeOptions = removeOptions;

        function addOptions() {
            var option = {
                label: '',
                value: ''
            };

            if (!ctrl.field.items) {
                ctrl.field.items = [];
            }

            ctrl.field.items.push(option);
        }

        function removeOptions(index) {
            ctrl.field.items.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, radioGroupConfig);
})();
