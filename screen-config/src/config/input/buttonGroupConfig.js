(function() {
    'use strict';

    var directiveName = 'buttonGroupConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function buttonGroupConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: buttonGroupConfigCtrl
        };
    }

    /*@ngInject*/
    function buttonGroupConfigCtrl() {
        var ctrl = this;
        ctrl.addOptions = addOptions;
        ctrl.removeOptions = removeOptions;

        function addOptions() {
            var option = {
                label: '',
                value: '',
                activeImagePath: '',
                inactiveImagePath: '',
                tooltip: {}
            };

            if (!ctrl.field.items) {
                ctrl.field.items = [];
            }

            ctrl.field.items.push(option);
        }

        function removeOptions(index) {
            ctrl.field.items.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, buttonGroupConfig);
})();
