(function() {
    'use strict';

    var directiveName = 'formActionConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function formActionConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formActionConfigCtrl
        };
    }

    /*@ngInject*/
    function formActionConfigCtrl() {
        var ctrl = this;
        ctrl.addAction = addAction;
        ctrl.removeAction = removeAction;

        function addAction() {
            var action = {
                label: '',
                name: '',
                apiUrl: '',
                class: ''
            };

            if (!ctrl.field.actionItems) {
                ctrl.field.actionItems = [];
            }

            ctrl.field.actionItems.push(action);
        }

        function removeAction(index) {
            ctrl.field.actionItems.splice(index, 1);
        }
    }

    angular.module('screenConfigurator').directive(directiveName, formActionConfig);
})();
