(function() {
    'use strict';

    var directiveName = 'datePickerConfig';
    var templatePath = 'templates/input/' + directiveName + '.html';

    /*@ngInject*/
    function datePickerConfig() {
        return {
            scope: {
                field: '=',
                formInputList: '='
            },
            templateUrl: templatePath,
            bindToController: true,
            controllerAs: 'ctrl',
            controller: datePickerConfigCtrl
        };
    }

    /*@ngInject*/
    function datePickerConfigCtrl($timeout) {
        var ctrl = this;
        var picker = {};
        ctrl.openPicker = openPicker;

        ctrl.field.minDependency = ctrl.field.minDependency ? ctrl.field.minDependency : [];
        ctrl.field.maxDependency = ctrl.field.maxDependency ? ctrl.field.maxDependency : [];

        $timeout(function() {
            var minDate = $('#minDate').pickadate({
                selectYears: true,
                selectMonths: true,
                format: 'dd mmm yyyy'
            });

            var maxDate = $('#maxDate').pickadate({
                selectYears: true,
                selectMonths: true,
                format: 'dd mmm yyyy'
            });

            var defaultDate = $('#defaultDate').pickadate({
                selectYears: true,
                selectMonths: true,
                format: 'dd mmm yyyy'
            });

            picker.minPicker = minDate.pickadate('picker');
            picker.maxPicker = maxDate.pickadate('picker');
            picker.defaultPicker = defaultDate.pickadate('picker');
        }, 50);

        function openPicker($event, id) {
            picker[id].open();
            $event.stopPropagation();
            $event.preventDefault();
        }

        ctrl.addDependencyDate = function(list) {
            list.push({
                name: '',
                excludeSelected: false
            });
        };

        ctrl.removeDependencyDate = function(list, index) {
            list.splice(index, 1);
        };
    }

    angular.module('screenConfigurator').directive(directiveName, datePickerConfig);
})();
