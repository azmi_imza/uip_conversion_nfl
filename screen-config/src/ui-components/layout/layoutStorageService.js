(function() {
    'use strict';
    var serviceName = 'layoutStorageService';

    /*@ngInject*/
    var layoutStorageService = function() {
        var storage = {};

        this.setLayoutState = function(value) {
            window.localStorage.setItem('LAYOUT_STATE', angular.toJson(value));
        };

        this.getLayoutState = function() {
            return angular.fromJson(window.localStorage.getItem('LAYOUT_STATE'));
        };

        this.removeLayoutState = function() {
            window.localStorage.removeItem('LAYOUT_STATE');
        };

        this.setScreenCode = function(value) {
            window.localStorage.setItem('SCREEN_CODE', value);
        };

        this.get = function(key) {
            if (storage[key]) {
                return angular.fromJson(storage[key])[key];
            }

            return null;
        };

        this.set = function(key, value) {
            var clonedValue = angular.copy(value);
            var obj = {
                key: clonedValue
            };
            storage[key] = angular.toJson(obj);
        };

        this.remove = function(key) {
            delete storage[key];
        };

        this.clearAll = function() {
            storage = {};
        };
    };

    angular.module('components.layout')
        .service(serviceName, layoutStorageService);
})();
