(function() {
    'use strict';

    var directiveName = 'layout';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                layoutPath: '=?',
                layoutData: '=?',
                stateData: '=?',
                actions: '=?',
                formManager: '=?',
                formModel: '=?',
                additionalInfo: '=?',
                selectOnRowFn: '&',
                selectedValue: '=?',
                selectedObject: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            template: '<div ng-include="ctrl.templateName"></div>',
            controller: layoutCtrl
        };
    }

    /*@ngInject*/
    function layoutCtrl($log, $attrs, $element, $resource) {
        $log.debug('layout ctrl');

        var ctrl = this;
        ctrl.templateName = templateName;
        var layoutUrl = ctrl.layoutPath || $attrs.src;

        // if (ctrl.additionalInfo) {
        //     $log.debug('layout ctrl, additionalInfo');
        //     $log.debug(ctrl.additionalInfo);
        // }

        // var layoutStateData = layoutStorageService.getLayoutState();

        // if (layoutStateData) {
        //     $log.debug('layout state data');
        //     $log.debug(layoutStateData);
        //     ctrl.stateData = layoutStateData;
        //     layoutStorageService.removeLayoutState();
        // }

        $log.debug('ctrl.layoutData:', ctrl.layoutData);
        if (ctrl.layoutData) {
            $log.debug('initialize layout');
            ctrl.layout = ctrl.layoutData;
            ctrl.actions = ctrl.layout.components[0].actions;
            ctrl.loaded = true;
        } else if (layoutUrl) {
            var loadSuccess = function loadSuccess(data) {
                $log.debug(data);
                ctrl.layout = data;
                ctrl.loaded = true;
                $log.debug('layout loaded');
            };

            var loadFailed = function loadFailed() {
                $log.debug('fail to load layout');
            };

            $resource(layoutUrl).get({}, loadSuccess, loadFailed);
        }
    }

    angular.module('components.layout').directive(directiveName, directive);
})();
