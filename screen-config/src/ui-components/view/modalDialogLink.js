(function() {
    'use strict';

    var directiveName = 'modalDialogLink';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function modalDialogLink() {
        return {
            scope: {
                label: '=',
                icon: '=',
                style: '=',
                cssClass: '=',
                templateUrl: '=',
                title: '=',
                printInModal: '=',
                columnCssClass: '=?'
            },
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: modalDialogLinkCtrl
        };
    }

    /*@ngInject*/
    function modalDialogLinkCtrl($log, $rootScope, componentsModalService, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.openModal = openModal;
        $log.debug('modal title: ' + ctrl.title);
        $log.debug('print in modal: ' + ctrl.printInModal);

        if (ctrl.style) {
            var fieldStyle = ctrl.style.split(';');
            ctrl.styleObj = {};
            _.forEach(fieldStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.styleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        function openModal() {
            $log.debug('open modal');
            var template = '<modal-template print-in-modal="' + ctrl.printInModal + '" title="' + ctrl.title + '" template-url="' + ctrl.templateUrl + '"></modal-template>';
            var backdrop = 'static';
            var size = 'lg';
            var resolve = {};

            var close = function() {
                $log.debug('close modal');
            };

            componentsModalService.openModalWithTemplate(template, backdrop, size, resolve, close);
        }
    }

    angular.module('components.view').directive(directiveName, modalDialogLink);
})();
