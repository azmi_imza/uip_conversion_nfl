(function() {
    'use strict';

    var directiveName = 'acknowledgementView';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function acknowledgementView() {
        return {
            scope: {
                displayTitle: '=',
                contentDetails: '=',
                isVisible: '=',
                isSuccessView: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: acknowledgementViewCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function acknowledgementViewCtrl() {
        var ctrl = this;
        ctrl.templateName = templateName;

    }

    angular.module('components.view').directive(directiveName, acknowledgementView);
})();
