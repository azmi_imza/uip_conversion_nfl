(function() {
    'use strict';

    var directiveName = 'modalTemplate';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function modalTemplate() {
        return {
            scope: {},
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: modalTemplateCtrl
        };
    }

    /*@ngInject*/
    function modalTemplateCtrl($log, $attrs, $window, componentsModalService) {
        $log.debug('modalTemplateCtrl');
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.templateUrl = $attrs.templateUrl;
        ctrl.title = $attrs.title;
        ctrl.close = close;
        ctrl.print = print;
        ctrl.printInModal = $attrs.printInModal;
        $log.debug($attrs.printInModal);

        function close() {
            $log.debug('close modal');
            if ($('#printSection').length) {
                $('#printSection').remove();
            }

            componentsModalService.closeModal();
        }

        function print() {
            $log.debug('print modal');
            var printSection = document.getElementById('printSection');

            if (!printSection) {
                printSection = document.createElement('div');
                printSection.id = 'printSection';
                document.body.appendChild(printSection);
            }

            printSection.innerHTML = '';
            var elemToPrint = document.querySelector('.modal-print');
            if (elemToPrint) {
                printElement(elemToPrint, printSection);
            }
        }

        function printElement(elemToPrint, printSection) {
            var domClone = elemToPrint.cloneNode(true);
            printSection.appendChild(domClone);
            window.print();
        }
    }

    angular.module('components.view').directive(directiveName, modalTemplate);
})();
