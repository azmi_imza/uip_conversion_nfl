(function() {
    'use strict';

    var directiveName = 'imagePreview';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: imagePreviewCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function imagePreviewCtrl($log, componentsModalService, $timeout) {
        var ctrl = this;
        ctrl.templateName = templateName;

        var fileData = componentsModalService.modalStorage.fileData;

        $timeout(function() {
            var outputImage = document.getElementById('thumb-inside');
            var url = URL.createObjectURL(fileData);
            outputImage.src = url;
        }, 100);
    }

    angular.module('components.view').directive(directiveName, directive);
})();
