/* eslint no-undef:0 */

(function() {
    'use strict';

    var directiveName = 'pdfPreview';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                showModalFooter: '=',
                showModalFooterAtScrollEnd: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: pdfPreviewCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function pdfPreviewCtrl($log, $timeout, componentsModalService, COMP_CONST, $scope) {
        var ctrl = this;
        ctrl.templateName = templateName;

        var fileData = componentsModalService.modalStorage.fileData;
        var pageRendering = false;
        var scale = 1.0;
        var pages = [];

        ctrl.currentPage = 1;

        function zoomPage(pageNum) {
            var viewport = pages[pageNum].page.getViewport(scale);
            var canvas = pages[pageNum].canvas;
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };

            pages[pageNum].page.render(renderContext).then(function() {
                if (pageNum < pages.length - 1) {
                    pageNum++;
                    zoomPage(pageNum);
                } else {
                    pageRendering = false;
                }
            });
        }

        var onZoomIn = function() {
            if (!pageRendering && scale < 2.0) {
                pageRendering = true;
                scale += 0.1;
            } else {
                return;
            }

            zoomPage(1);
        };

        var onZoomOut = function() {
            if (!pageRendering && scale > 0.5) {
                pageRendering = true;
                scale -= 0.1;
            } else {
                return;
            }

            zoomPage(1);
        };

        $timeout(function() {
            var url = URL.createObjectURL(fileData);

            PDFJS.workerSrc = COMP_CONST.PDFJS_SRC_PATH;
            PDFJS.getDocument(url).then(function(pdf) {

                pageRendering = true;
                ctrl.totalPages = pdf.numPages;
                $scope.$apply();

                var pageNum = 1;
                pdf.getPage(pageNum).then(renderPage);

                function renderPage(page) {
                    var viewport = page.getViewport(scale);
                    var canvas = document.createElement('canvas');
                    canvas.setAttribute('id', 'page-' + pageNum);
                    canvas.style.display = 'block';
                    canvas.style.marginLeft = 'auto';
                    canvas.style.marginRight = 'auto';

                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };

                    page.render(renderContext).then(function() {
                        if (pageNum < pdf.numPages) {
                            pages[pageNum] = {};
                            pages[pageNum].page = page;
                            pages[pageNum].canvas = canvas;
                            pageNum++;
                            pdf.getPage(pageNum).then(renderPage);
                        } else {
                            pages[pageNum] = {};
                            pages[pageNum].page = page;
                            pages[pageNum].canvas = canvas;
                            for (var i = 1; i < pages.length; i++) {
                                document.getElementById('canvasWrapper').appendChild(pages[i].canvas);
                            }

                            pageRendering = false;
                        }
                    });
                }
            });

            $('#canvasWrapper').css('max-height', $(window).height() - 250 + 'px');
            $('#canvasWrapper').on('scroll', function() {
                var pageHeight = $('#page-1').height();
                var cutOffHeight = pageHeight * 0.2;

                for (var p = 1; p < pages.length; p++) {
                    if ($('#canvasWrapper').scrollTop() <= pageHeight * p - cutOffHeight) {
                        ctrl.currentPage = p;
                        $scope.$apply();
                        break;
                    }
                }
            });

            if (ctrl.showModalFooterAtScrollEnd) {
                var checkEnd = function() {
                    if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
                        $('#canvasWrapper').off('scroll', checkEnd);
                        ctrl.showModalFooter = true;
                        $scope.$apply();
                    }
                };

                $('#canvasWrapper').on('scroll', checkEnd);
            }

            $('#zoomIn').on('click', onZoomIn);
            $('#zoomOut').on('click', onZoomOut);
        }, 100);

    }

    angular.module('components.view').directive(directiveName, directive);
})();
