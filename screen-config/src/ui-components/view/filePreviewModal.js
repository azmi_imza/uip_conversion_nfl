(function() {
    'use strict';

    var directiveName = 'filePreviewModal';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: filePreviewModalCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function filePreviewModalCtrl($log, componentsModalService) {
        var ctrl = this;
        ctrl.showPDF = true;
        ctrl.showImage = true;
        ctrl.templateName = templateName;
        ctrl.ext = componentsModalService.modalStorage.ext;
        ctrl.showModalFooter = (componentsModalService.modalStorage.showModalFooter !== undefined) ?
            componentsModalService.modalStorage.showModalFooter : true;
        ctrl.showModalFooterAtScrollEnd = (componentsModalService.modalStorage.showModalFooterAtScrollEnd !== undefined) ?
            componentsModalService.modalStorage.showModalFooterAtScrollEnd : false;

        ctrl.close = function() {
            if (componentsModalService.modalStorage.primaryAction) {
                componentsModalService.modalStorage.primaryAction();
            }

            componentsModalService.closeModal();
        };

        ctrl.cancel = function() {
            componentsModalService.dismissModal();
        };
    }

    angular.module('components.view').directive(directiveName, directive);
})();
