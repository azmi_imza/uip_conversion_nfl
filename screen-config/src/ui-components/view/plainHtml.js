(function() {
    'use strict';

    var directiveName = 'plainHtml';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function plainHtml() {
        return {
            scope: {
                template: '='
            },
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: plainHtmlCtrl
        };
    }

    /*@ngInject*/
    function plainHtmlCtrl($log) {
        $log.debug('plainHtmlCtrl');
        var ctrl = this;
        ctrl.templateName = templateName;
    }

    angular.module('components.view').directive(directiveName, plainHtml);
})();
