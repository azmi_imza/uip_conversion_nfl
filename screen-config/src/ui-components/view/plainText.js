(function() {
    'use strict';

    var directiveName = 'plainText';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function plainText() {
        return {
            scope: {
                textContent: '=',
                formManager: '=?',
                fieldStyle: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                columnCssClass: '=?',
                labelCssClass: '=?',
                labelStyle: '=?',
                rowGroupIndex: '=?'
            },
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: plainTextCtrl
        };
    }

    /*@ngInject*/
    function plainTextCtrl($log, $rootScope, $scope, _) {
        $log.debug('plainTextCtrl');
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;
        ctrl.deregisterFuncs = [];
        ctrl.getFieldValue = getFieldValue;
        ctrl.setFieldValue = setFieldValue;

        ctrl.displayContent = ctrl.textContent.replace('{rowGroupIndex}', ctrl.rowGroupIndex + 1);

        if (ctrl.rowGroupIndex) {
            var watchIndex = $rootScope.$watch(function() {
                    return ctrl.rowGroupIndex;
                },

                function() {
                    ctrl.displayContent = ctrl.textContent.replace('{rowGroupIndex}', ctrl.rowGroupIndex + 1);
                });

            ctrl.deregisterFuncs.push(watchIndex);
        }

        if (ctrl.formManager) {
            if (ctrl.fieldDependencies) {
                for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                    var dep = ctrl.fieldDependencies[i];

                    if (ctrl.formManager.getValue(dep.watch)) {
                        applyDependency(dep);
                    }

                    watchValue('valueChange_' + dep.watch, dep);
                }
            }
        }

        if (ctrl.fieldStyle) {
            var fieldStyle = ctrl.fieldStyle.split(';');
            ctrl.fieldStyleObj = {};
            _.forEach(fieldStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        if (ctrl.labelStyle) {
            var labelStyle = ctrl.labelStyle.split(';');
            ctrl.labelStyleObj = {};
            _.forEach(labelStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                }
            });
        }

        $scope.$on('$destroy', function() {
            $log.debug('destroy plainText');
            destroy();
        });

        function destroy() {
            ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                deregisterFunc();
            });
        }

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        function setFieldValue(name, value) {
            $log.debug('SET field: ' + name + ', value: ' + value);
            ctrl.formManager.setValue(name, value);
        }

        function getFieldValue(name) {
            var fieldValue = ctrl.formManager.getValue(name);
            $log.debug('GET field: ' + name + ', value: ' + fieldValue);
            return fieldValue;
        }

        function applyDependency(dependency) {
            var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];

            if (listAction) {
                for (var j = 0; j < listAction.length; j++) {
                    var action = listAction[j];
                    if (action.actionType === 'visible') {
                        ctrl.isVisible = action.value;
                    }
                }
            }
        }
    }

    angular.module('components.view').directive(directiveName, plainText);
})();
