(function() {
    'use strict';

    var directiveName = 'autoCompleteInput';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formSectionMode: '=',
                url: '=',
                strictMode: '=',
                listType: '=',
                listItems: '=?',
                placeHolder: '=?',
                fieldUnit: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                formManager: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                columnCssClass: '=?',
                fieldId: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                fieldMinChar: '=?',
                fieldListNumber: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: autoCompleteInputCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function autoCompleteInputCtrl($scope, $element, $rootScope, $timeout, $log, $resource, COMP_CONST, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || 'col-xs-12';
        ctrl.inputCssClass = ctrl.inputCssClass || 'col-xs-12';
        ctrl.formSectionMode = ctrl.formSectionMode || 'edit';
        ctrl.isValid = true;
        ctrl.cssClass = 'form-control ' + ctrl.fieldClass;
        ctrl.deregisterFuncs = [];
        ctrl.isFocus = false;
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;
        ctrl.placeHolder = ctrl.placeHolder || '';
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        var minChar = parseInt(ctrl.fieldMinChar);
        var listNumber = parseInt(ctrl.fieldListNumber);
        var sortedList = [];

        ctrl.value = function(newValue) {

            return arguments.length ? (ctrl.formManager.setValue(ctrl.fieldName, newValue)) : ctrl.formManager.getValue(ctrl.fieldName);
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            $timeout(function() {
                if (!minChar || isNaN(minChar)) {
                    ctrl.fieldMinChar = '1';
                }

                if (!listNumber || isNaN(listNumber)) {
                    ctrl.fieldListNumber = '10';
                }

                if (!ctrl.fieldName) {
                    $log.error('Field name is undefined');
                }

                if (ctrl.fieldStyle) {
                    var fieldStyle = ctrl.fieldStyle.split(';');
                    ctrl.fieldStyleObj = {};
                    _.forEach(fieldStyle, function(style) {
                        var pair = style.split(':');
                        if (pair.length === 2) {
                            ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                        }
                    });
                }

                if (ctrl.labelStyle) {
                    var labelStyle = ctrl.labelStyle.split(';');
                    ctrl.labelStyleObj = {};
                    _.forEach(labelStyle, function(style) {
                        var pair = style.split(':');
                        if (pair.length === 2) {
                            ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                        }
                    });
                }

                if (ctrl.inputStyle) {
                    var inputStyle = ctrl.inputStyle.split(';');
                    ctrl.inputStyleObj = {};
                    _.forEach(inputStyle, function(style) {
                        var pair = style.split(':');
                        if (pair.length === 2) {
                            ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                        }
                    });
                }

                $rootScope.$on('formModeChange:' + ctrl.formSectionName, function(event, data) {
                    $log.debug('form mode change: ' + ctrl.formSectionName);
                    if (data === 'edit') {
                        if (ctrl.items && ctrl.items.length > 0) {
                            return;
                        }

                        initEditMode();
                        return;
                    }

                    if (data === 'view') {
                        initViewMode();
                        return;
                    }
                });

                $scope.$on('$destroy', function() {
                    $log.debug('destroy', directiveName, ctrl.fieldName);
                    destroy();
                });

                function destroy() {
                    ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                        deregisterFunc();
                    });
                }
            });

        }

        if (ctrl.validators && ctrl.validators.maxLength) {
            var newMaxLength = parseInt(ctrl.validators.maxLength.value);
            if (!isNaN(newMaxLength)) {
                ctrl.maxCharLength = newMaxLength;
            }
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                if (ctrl.formManager) {
                    ctrl.formManager.addComponent(ctrl.fieldName);
                }
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                ctrl.onChange = onChange;

                if (ctrl.formManager) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                }

                if (ctrl.validators) {
                    $rootScope.$on('formsubmit', function() {
                        validate();
                    });
                }

                if (ctrl.fieldDependencies) {
                    for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                        var d = ctrl.fieldDependencies[i];

                        //if (ctrl.formManager.getValue(d.watch)) {
                            applyDependency(d);
                        //}

                        watchValue(d.watch, d);
                    }
                }

                loadList();
                initValidation();
            }

            function watchValue(eventName, dependency) {
                var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                    applyDependency(dependency);
                });

                if (!ctrl.deregisterFuncs) {
                    ctrl.deregisterFuncs = [];
                }

                ctrl.deregisterFuncs.push(deregisterFunc);
            }

            function applyDependency(dependency) {
                var regexList = Object.keys(dependency.actionList);

                if (!regexList || regexList.length === 0) {
                    $log.error('no matched regex defined');
                    return;
                }

                var dependencyValue = ctrl.formManager.getValue(dependency.watch);
                var matchedRegex = null;
                regexList.some(function(regexText) {
                    var regex = new RegExp(regexText);

                    if (regex.test(dependencyValue)) {
                        matchedRegex = regexText;
                        return true;
                    }

                    return false;
                });

                if (!matchedRegex) {
                    return;
                }

                var matchedActions = dependency.actionList[matchedRegex];

                if (!matchedActions || matchedActions.length === 0) {
                    $log.error('no action defined for matched regex');
                    return;
                }

                matchedActions.forEach(function(action) {
                    if (action.actionType === 'disable') {
                        isDisabled = action.value;
                        reRunValidate(!action.value);
                    } else if (action.actionType === 'visible') {
                        isVisible = action.value;
                        if (ctrl.validators && ctrl.formSectionMode === 'edit') {
                            initEditMode();
                        }

                        reRunValidate(action.value);
                    } else if (action.actionType === 'update') {
                        $log.debug('action type: update');
                        ctrl.selectedValue = action.value;
                        onChange();
                    } else if (action.actionType === 'reset') {
                        reset();
                    }
                });
            }

            function reRunValidate(actionValue) {
                setFieldValid();
                if (ctrl.formManager) {
                    if (actionValue) {
                        ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                    } else {
                        ctrl.formManager.removeComponent(ctrl.fieldName);
                    }
                }
            }

            function setFieldValid() {
                ctrl.isValid = true;
                ctrl.errorMessage = '';
            }

            function setFieldInvalid(message) {
                ctrl.isValid = false;
                ctrl.errorMessage = message;
            }

            function reset() {
                var resetValue = ctrl.defaultValue || '';
                ctrl.formManager.setValue(ctrl.fieldName, resetValue);
                setFieldValid();
            }

            function onChange() {
                if (!ctrl.isFocus) {
                    $log.debug('broadcast autocomplete input value');
                    $rootScope.$emit('valueChange_' + ctrl.fieldName);
                }
            }

            function initValidation() {
                var inputElem = $element.find('input');

                inputElem.bind('focus', function() {
                    ctrl.isFocus = true;
                });

                inputElem.bind('blur', function() {
                    ctrl.isFocus = false;

                    $timeout(function() {
                        //manually set selected value to form manager instead of detect arguments
                        ctrl.formManager.setValue(ctrl.fieldName, inputElem.val());
                        onChange();
                    }, 300);

                });
            }

            function validate() {

                if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.disability()) {
                    setFieldValid();
                    return getValidateObj();
                }

                var value = ctrl.value();

                if (ctrl.validators.isRequired) {

                    if (!value) {
                        setFieldInvalid(ctrl.validators.isRequired.message);
                        return getValidateObj();
                    }
                }

                if (!validateMaxLength(ctrl.validators, value)) {
                    setFieldInvalid(ctrl.validators.maxLength.message);
                    return getValidateObj();
                }

                if (ctrl.validators.groupValidation) {
                    if (ctrl.validators.groupValidation.type === 'isRequired') {
                        if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                            var valid = true;
                            angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                                var pattern = member.pattern ? member.pattern : '^.+$';
                                var regex = new RegExp(pattern);
                                var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                                if (regex.test(memberValue)) {
                                    setFieldInvalid(ctrl.validators.groupValidation.message);
                                    valid = false;
                                    return;
                                }
                            });

                            if (!valid) {
                                return getValidateObj();
                            }

                            angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                                $rootScope.$emit('setValid_' + member.name);
                            });
                        }
                    }
                }

                if (value && ctrl.items.indexOf(value) < 0 && ctrl.strictMode === 'Y') {
                    setFieldInvalid('Invalid option');
                    return getValidateObj();
                }

                setFieldValid();
                return getValidateObj();
            }

            function getValidateObj() {
                if (isVisible) {
                    return {
                        isValid: ctrl.isValid,
                        message: ctrl.errorMessage
                        //label: ctrl.label.name
                    };
                } else {
                    return {
                        isValid: true
                    };
                }
            }

            function loadListFromAPI(url) {
                function loadSuccess(response) {
                    $log.debug('List loaded');
                    if (!response.data.items || response.data.items.length === 0) {
                        $log.error('zero item for List: ' + ctrl.fieldName);
                        return;
                    }

                    $timeout(function() {
                        ctrl.items = [];
                        if (ctrl.listType === 'static') {
                            angular.forEach(response.data.items, function(item) {
                                ctrl.items.push(item.label);
                            });
                        } else {
                            angular.forEach(response.data.items, function(item) {
                                ctrl.items.push(item[response.data.label]);
                            });
                        }

                        sortedList = ctrl.items.sort();

                        $('#' + ctrl.fieldName).autocomplete({
                            source: function(filterRequest, filterResponse) {
                                var results = $.ui.autocomplete.filter(sortedList, filterRequest.term);
                                filterResponse(results.slice(0, ctrl.fieldListNumber));
                            },

                            minLength: ctrl.fieldMinChar
                        });
                    });
                }

                function loadFailed() {
                    $log.error('Fail to load List: ' + ctrl.fieldName);
                }

                $resource(url).get({
                    lang: ctrl.defaultLang
                }, loadSuccess, loadFailed);
            }

            function validateMaxLength(validators, value) {
                var maxLength = validators.maxLength;
                if (maxLength !== undefined) {
                    maxLength = parseInt(maxLength.value);
                    if (isNaN(maxLength)) {
                        $log.error('invalid max-length syntax: ' + maxLength);
                    } else {
                        if (value.length > maxLength) {
                            return false;
                        }
                    }
                }

                return true;
            }

            function loadList() {
                ctrl.items = [];
                if (ctrl.url) {
                    var url = COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + COMP_CONST.API_BASE_PATH + ctrl.url;
                    loadListFromAPI(url);
                } else {
                    if (ctrl.listType !== 'static') {
                        $log.error('no url is defined for List');
                        return;
                    }

                    if (!ctrl.listItems || ctrl.listItems.length === 0) {
                        $log.error('no items loaded');
                        return;
                    }

                    angular.forEach(ctrl.listItems, function(item) {
                        ctrl.items.push(item.value);
                    });

                    sortedList = ctrl.items.sort();

                    $timeout(function() {
                        $('#' + ctrl.fieldName).autocomplete({
                            source: function(filterRequest, filterResponse) {
                                var results = $.ui.autocomplete.filter(sortedList, filterRequest.term);
                                filterResponse(results.slice(0, ctrl.fieldListNumber));
                            },

                            minLength: ctrl.fieldMinChar
                        });
                    });
                }
            }
        }

        $timeout(function() {
            init();
        });
    }

    angular.module('components.input').directive(directiveName, directive);
})();
