(function() {
    'use strict';

    var directiveName = 'singleLineText';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                formManager: '=',
                label: '=',
                fieldName: '=',
                formSectionMode: '=',
                maskInput: '=',

                // optional
                fieldInputFormat: '=?',
                placeHolder: '=?',
                fieldUnit: '=?',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                columnCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                fieldId: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: singleLineTextCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function singleLineTextCtrl($scope, $element, $compile, $rootScope, $timeout, $log, $q, $http, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.formSectionMode = ctrl.formSectionMode || 'edit';
        ctrl.isValid = true;
        ctrl.cssClass = 'form-control ' + ctrl.fieldClass;
        ctrl.onChange = onChange;
        ctrl.deregisterFuncs = [];
        ctrl.isFocus = false;
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;
        ctrl.placeHolder = ctrl.placeHolder || '';
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        ctrl.inputType = ctrl.maskInput ? 'password' : 'text';
        //ctrl.maxCharLength = 100;

        ctrl.value = function(newValue) {
            if (!arguments.length) {
                return ctrl.formManager.getValue(ctrl.fieldName);
            }

            onChange();
            ctrl.formManager.setValue(ctrl.fieldName, newValue);
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            if (!ctrl.fieldName) {
                $log.error('Field name is undefined');
            }

            if (ctrl.formManager) {
                ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
            }

            if (!ctrl.formManager.getValue(ctrl.fieldName) && ctrl.fieldDefaultValue) {
                ctrl.formManager.setValue(ctrl.fieldName, ctrl.fieldDefaultValue);
            }

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            $scope.$on('$destroy', function() {
                $log.debug('destroy singleLineText');
                destroy();
            });
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);
                if (ctrl.validators) {
                    initValidation();

                }

                if (ctrl.fieldDependencies) {
                    for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                        var d = ctrl.fieldDependencies[i];

                        //if (ctrl.formManager.getValue(d.watch)) {
                            applyDependency(d);
                        //}

                        watchValue(d.watch, d);
                    }
                }

                var deregisterSetValid = $rootScope.$on('setValid_' + ctrl.fieldName, function() {
                    setFieldValid();
                });

                ctrl.deregisterFuncs.push(deregisterSetValid);
            }
        }

        function initValidation() {
            var inputElem = $element.find('input');

            inputElem.bind('focus', function() {
                ctrl.isFocus = true;
            });

            inputElem.bind('blur', function() {
                ctrl.isFocus = false;
                $timeout(function() {
                    onChange();
                }, 300);
            });
        }

        if (ctrl.validators && ctrl.validators.maxLength) {
            var newMaxLength = parseInt(ctrl.validators.maxLength.value);
            if (!isNaN(newMaxLength)) {
                ctrl.maxCharLength = newMaxLength;
            }
        }

        function watchValue(eventName, dependency) {
            var deregisterValueChange = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterValueChange);
        }

        function applyDependency(dependency) {
            var regexList = Object.keys(dependency.actionList);

            if (!regexList || regexList.length === 0) {
                $log.error('no matched regex defined');
                return;
            }

            var dependencyValue = ctrl.formManager.getValue(dependency.watch);
            var matchedRegex = null;
            regexList.some(function(regexText) {
                var regex = new RegExp(regexText);

                if (regex.test(dependencyValue)) {
                    matchedRegex = regexText;
                    return true;
                }

                return false;
            });

            if (!matchedRegex) {
                return;
            }

            var matchedActions = dependency.actionList[matchedRegex];

            if (!matchedActions || matchedActions.length === 0) {
                $log.error('no action defined for matched regex');
                return;
            }

            matchedActions.forEach(function(action) {
                if (action.actionType === 'disable') {
                    isDisabled = action.value;
                    reRunValidate(!action.value);
                } else if (action.actionType === 'visible') {
                    isVisible = action.value;
                    if (ctrl.validators && ctrl.formSectionMode !== 'view') {
                        $timeout(function() {
                            initValidation();
                        });
                    }

                    reRunValidate(action.value);
                } else if (action.actionType === 'update') {
                    $log.debug('action type: update');
                    ctrl.value(action.value);
                } else if (action.actionType === 'reset') {
                    reset();
                }
            });
        }

        function onChange() {
            if (!ctrl.isFocus) {
                $log.debug('broadcast single line text value');
                $rootScope.$emit('valueChange_' + ctrl.fieldName);
            }
        }

        function reset() {
            var resetValue = ctrl.defaultValue || '';
            ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            setFieldValid();
        }

        function validate() {
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.disability()) {
                setFieldValid();
                return getValidateObj();
            }

            var value = ctrl.value();
            var validationFailedReason = 'Validation failed: ' + ctrl.fieldName;

            if (ctrl.validators.isRequired) {

                if (!value) {
                    setFieldInvalid(ctrl.validators.isRequired.message);
                    return getValidateObj();
                }
            }

            if (!validateMinLength(ctrl.validators, value)) {
                setFieldInvalid(ctrl.validators.minLength.message);
                return getValidateObj();
            }

            if (!validateMaxLength(ctrl.validators, value)) {
                setFieldInvalid(ctrl.validators.maxLength.message);
                return getValidateObj();
            }

            if (ctrl.validators.patterns && ctrl.validators.patterns.length > 0) {
                var isPatternValid = true;
                ctrl.validators.patterns.some(function(pattern) {
                    var regex = new RegExp(pattern.value);
                    if (!regex.test(value)) {
                        setFieldInvalid(pattern.message);
                        isPatternValid = false;
                        return getValidateObj();
                    }

                    return getValidateObj();
                });

                if (!isPatternValid) {
                    return getValidateObj();
                }
            }

            if (ctrl.validators.groupValidation) {
                if (ctrl.validators.groupValidation.type === 'isRequired') {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        var valid = true;
                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            var pattern = member.pattern ? member.pattern : '^.+$';
                            var regex = new RegExp(pattern);
                            var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                            if (regex.test(memberValue)) {
                                setFieldInvalid(ctrl.validators.groupValidation.message);
                                valid = false;
                                return;
                            }
                        });

                        if (!valid) {
                            return getValidateObj();
                        }

                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            $rootScope.$emit('setValid_' + member.name);
                        });
                    }
                }
            }

            // last validation
            if (ctrl.validators.ajax) {
                var deferred = $q.defer();

                $http({
                    method: 'GET',
                    url: ctrl.validators.ajax.url
                }).success(function() {
                    // if validation failed, reject promise
                    deferred.resolve(true);
                }).error(function() {
                    // if validation failed, reject promise
                    deferred.reject(validationFailedReason);
                });

                return deferred.promise;
            }

            setFieldValid();
            return getValidateObj();
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                    //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }

        function reRunValidate(actionValue) {
            setFieldValid();
            if (ctrl.formManager) {
                if (actionValue) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                }
            }
        }

        function validateMinLength(validators, value) {
            var minLength = validators.minLength;
            if (minLength !== undefined) {
                minLength = parseInt(minLength.value);
                if (isNaN(minLength)) {
                    $log.error('invalid min-length syntax: ' + minLength);
                } else {
                    if (value.length < minLength) {
                        return false;
                    }
                }
            }

            return true;
        }

        function validateMaxLength(validators, value) {
            if (value) {
                var maxLength = validators.maxLength;
                if (maxLength !== undefined) {
                    maxLength = parseInt(maxLength.value);
                    if (isNaN(maxLength)) {
                        $log.error('invalid max-length syntax: ' + maxLength);
                    } else {
                        if (value.length > maxLength) {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

        function destroy() {
            ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                deregisterFunc();
            });
        }

        $timeout(function() {
            init();
        });

    }

    angular.module('components.input').directive(directiveName, directive);
})();
