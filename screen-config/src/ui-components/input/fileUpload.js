/* eslint no-alert:0 */
/* eslint new-cap:0 */

(function() {
    'use strict';

    var directiveName = 'fileUpload';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formModel: '=',
                formSectionMode: '=',
                formManager: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                formErrors: '=?',
                formSectionName: '=?',
                validators: '=?',
                fieldMaxNumFile: '=?',
                fieldHostUrl: '=?',
                fieldModuleCode: '=?',
                fieldUuid: '=?',
                fieldAuthentication: '=?',
                fieldKeyName: '=?',
                fieldRequestHeaderName: '=?',
                fieldDependencies: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                allowedSize: '=?',
                allowedType: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                fieldId: '=?',
                columnCssClass: '=?',
                fieldButtonCssClass: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: fileUploadCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function fileUploadCtrl($http, $resource, componentsModalService, $timeout, $filter,
        APP_CONST, storageService, $rootScope, $scope, $log, $element, COMP_CONST, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || 'col-xs-12';
        ctrl.inputCssClass = ctrl.inputCssClass || 'col-xs-12';
        ctrl.isValid = true;
        ctrl.deregisterFuncs = [];
        ctrl.fieldMaxNumFile = ctrl.fieldMaxNumFile || 10;
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;
        var uploadSuccess = true;

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            $log.debug('initBase:', ctrl.fieldName);

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            // TODO: add base initialization
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);

                // TODO: add view mode initialization
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);

                // TODO: add edit mode initialization
            }
        }

        init();

        if (ctrl.formManager) {
            ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
            ctrl.attachedFile = ctrl.formManager.getValue('attachment') ? ctrl.formManager.getValue('attachment') : [];
        }

        initErrorTooltip();

        //-----------------------function for file upload----------------------
        ctrl.newUploadedFile = [];
        var tempCurrentUUID = null;
        var hostUrl = ctrl.fieldHostUrl;

        $timeout(function() {
            $('#myFile').change(function() {
                setFieldValid();
            });
        });

        $scope.$on('$destroy', function() {
            $(document).off('previewFile', previewFile);
            $(document).off('deleteFile', deleteFile);
            $(document).off('deleteUploadedFile', deleteUploadedFile);

        });

        function previewFile(e, file) {
            var ext = $filter('uppercase')(file.name.split('.').pop());

            var template = '<file-preview-modal></file-preview-modal>';
            var backdrop = 'static';
            var size = 'lg';

            componentsModalService.modalStorage.fileData = file;
            componentsModalService.modalStorage.ext = ext;
            componentsModalService.openModalWithTemplate(template, backdrop, size);
        }

        function deleteFile(e, row) {
            ctrl['delete'](row);
        }

        function deleteUploadedFile(e, row) {
            ctrl.deleteUploaded(row);
        }

        $(document).on('previewFile', previewFile);
        $(document).on('deleteFile', deleteFile);
        $(document).on('deleteUploadedFile', deleteUploadedFile);

        var beforeSend = function() {};

        if (ctrl.fieldAuthentication) {
            beforeSend = function(xhr) {
                var authToken = storageService.get(ctrl.fieldKeyName);
                xhr.setRequestHeader(ctrl.fieldRequestHeaderName, authToken);
            };
        }

        $timeout(function() {
            initializeFileUpload();
        });

        function initializeFileUpload() {
            // Initialize the jQuery File Upload widget:
            $('form').fileupload({
                // Uncomment the following to send cross-domain cookies:
                // xhrFields: {withCredentials: true},
                // forceIframeTransport: true,
                // url: getDFTCUploadURL(),
                formData: [],
                dataType: null,
                autoUpload: true,
                uploadTemplateId: null,
                downloadTemplateId: null,
                beforeSend: beforeSend,
                uploadTemplate: function(o) {
                    var rows = $();
                    $.each(o.files, function(index, file) {
                        var fileName = $filter('uppercase')(file.name);
                        var ext = fileName.split('.').pop();
                        var row = $('<tr class="template-upload fade">' +
                            '<td class="attached-filename"><p class="name"></p>' +
                            '<div class="error"></div>' +
                            '</td>' +
                            '<td>Uploading...' +
                            '<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                            '<div class="progress-bar progress-bar-success" style="width:0%;"></div></div>' +
                            '</td>' +
                            '<td class="align-right">' +
                            (!index ? ' <button class="hidden-field-size start">' +
                                '</button>' : '') +
                            (!index ? ' <button class="hidden-field-size cancel">' +
                                '</button>' : '') +
                            (!index && (ext !== 'DOC' && ext !== 'DOCX') ? ' <button class="secondary-button secondary-button--white-thin margin-bottom5 preFile">' +
                                '<i class="attachment-action--view"></i>' +
                                '<span>View</span>' +
                                '</button>' : '') +
                            (!index ? ' <button class="secondary-button secondary-button--white-thin margin-bottom5 fileDelete">' +
                                '<i class="glyphicon glyphicon-trash margin-right4"></i>' +
                                '<span>Delete</span>' +
                                '</button>' : '') +
                            '</td>' +
                            '</tr>');
                        row.find('.name').text(file.name);
                        $(row.find('.preFile')).click(function() {
                            $(document).trigger('previewFile', [file]);
                        });

                        $(row.find('.fileDelete')).click(function() {
                            $(document).trigger('deleteFile', [row]);
                        });

                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }

                        rows = rows.add(row);
                    });

                    return rows;
                },

                downloadTemplate: function(o) {
                    var rows = $();
                    $.each(o.files, function(index, data) {

                        var row = $(data.jqXHR.status === 200 ? '<tr class="template-download fade">' +
                            '<td class="attached-filename"><p class="name"></p>' +
                            '</td>' +
                            '<td></td>' +
                            '<td class="align-right">' +
                            ' <button ng-if="ctrl.checkFileType(data.files[0].name)" class="secondary-button secondary-button--white-thin margin-bottom5 preFile">' +
                            '<i class="attachment-action--view"></i>' +
                            '<span>View</span>' +
                            '</button>' +
                            ' <button class="secondary-button secondary-button--white-thin margin-bottom5 delete">' +
                            '<i class="glyphicon glyphicon-trash margin-right4"></i>' +
                            '<span>Delete</span>' +
                            '</button>' +
                            '</td>' +
                            '</tr>' :
                            '<tr class="template-upload fade">' +
                            '<td>Upload fail</td>' +

                            // '<td><p class="name"></p>' +
                            // '<div class="error"></div>' +
                            // '</td>' +
                            // '<td> <p class="size">Processing...</p>' +
                            // '<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                            // '<div class="progress-bar progress-bar-success" style="width:0%;"></div></div>' +
                            // '</td>' +
                            // '<td>' +
                            // (!index ? ' <div class="button button--red-thin preFile">' +
                            //     '<i class="attachment-action--view" style="margin-right:4px"></i>' +
                            //     '<span>View</span>' +
                            //     '</div>' : '') +
                            // (!index && !o.options.autoUpload ?
                            //     ' <button class="button button--red-thin retry">' +
                            //     '<i class="glyphicon glyphicon-upload" style="margin-right:4px"></i>' +
                            //     '<span>Reupload</span>' +
                            //     '</button>' : '') +
                            // (!index ? ' <button class="button button--red-thin fileDelete">' +
                            //     '<i class="glyphicon glyphicon-trash" style="margin-right:4px"></i>' +
                            //     '<span>Delete</span>' +
                            //     '</button>' : '') +
                            // (!index ? ' <button class="button button--red-thin cancel remove" style="visibility:hidden">' +
                            //     '</button>' : '') +
                            // '</td>' +
                            '</tr>'
                        );
                        row.find('.size').text(o.formatFileSize(data.files[0].size));
                        if (data.files[0].error) {
                            row.find('.name').text(data.files[0].name);
                            row.find('.error').text(data.files[0].error);
                        } else {
                            row.find('.name').append($('<a></a>').text(data.files[0].name));
                            if (data.files[0].thumbnailUrl) {
                                row.find('.preview').append(
                                    $('<a></a>').append(
                                        $('<img>').prop('src', data.files[0].thumbnailUrl)
                                    )
                                );
                            }

                            row.find('a')
                                .attr('data-gallery', '')
                                .prop('href', data.files[0].url);
                            row.find('button.delete')
                                .attr('data-type', data.files[0]['delete_type'])
                                .attr('data-url', data.files[0]['delete_url']);
                            $(row.find('.delete')).click(function() {
                                var idx = ctrl.newUploadedFile.indexOf(data.files[0].name.toUpperCase());
                                ctrl.newUploadedFile.splice(idx, 1);

                                for (var i = 0; i < ctrl.newUploadedFile.length; i++) {
                                    if (ctrl.newUploadedFile[i]['file_name'] === data.files[0].name) {
                                        ctrl.newUploadedFile.splice(i, 1);
                                        break;
                                    }
                                }
                            });
                        }

                        row.find('.name').text(data.files[0].name);
                        rows = rows.add(row);
                        $(row.find('.preFile')).click(function() {
                            $(document).trigger('previewFile', [data.files[0]]);
                        });

                        $(row.find('.fileDelete')).click(function() {
                            $(document).trigger('deleteFile', [row]);
                        });

                        $(row.find('.deleteUploaded')).click(function() {
                            $(document).trigger('deleteUploadedFile', [row]);
                        });

                        $(row.find('.retry')).click(function() {
                            data.submit();
                        });

                        $(row.find('.remove')).click(function() {

                            var name = $filter('uppercase')(data.files[0].name);
                            var j = ctrl.newUploadedFile.indexOf(name);
                            ctrl.newUploadedFile.splice(j, 1);
                            if (ctrl.newUploadedFile.length < 1) {
                                $('#uploadAll').prop('disabled', true);
                                $(document).trigger('userUploadButton', [true]);
                            }
                        });

                        row.find('.size').text(o.formatFileSize(data.files[0].size));
                    });

                    return rows;
                },

                done: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    var that = $(this).data('blueimp-fileupload') ||
                        $(this).data('fileupload'),
                        getFilesFromResponse = data.getFilesFromResponse ||
                        that.options.getFilesFromResponse,
                        files = getFilesFromResponse(data),
                        template,
                        deferred;
                    if (data.context) {
                        data.context.each(function() {
                            deferred = that._addFinishedDeferreds();
                            that._transition($(this)).done(
                                function() {
                                    var node = $(this);
                                    template = that._renderDownload([data])
                                        .replaceAll(node);
                                    that._forceReflow(template);
                                    that._transition(template).done(
                                        function() {
                                            data.context = $(this);
                                            that._trigger('completed', e, data);
                                            that._trigger('finished', e, data);
                                            deferred.resolve();
                                        }

                                    );
                                }

                            );
                        });
                    } else {
                        template = that._renderDownload(files)[
                            that.options.prependFiles ? 'prependTo' : 'appendTo'
                        ](that.options.filesContainer);
                        that._forceReflow(template);
                        deferred = that._addFinishedDeferreds();
                        that._transition(template).done(
                            function() {
                                data.context = $(this);
                                that._trigger('completed', e, data);
                                that._trigger('finished', e, data);
                                deferred.resolve();
                            }

                        );
                    }
                },

                fail: function(e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }

                    var that = $(this).data('blueimp-fileupload') ||
                        $(this).data('fileupload'),
                        template,
                        deferred;
                    if (data.context) {
                        data.context.each(function() {
                            if (data.errorThrown !== 'abort') {
                                deferred = that._addFinishedDeferreds();
                                that._transition($(this)).done(
                                    function() {
                                        var node = $(this);
                                        template = that._renderDownload([data])
                                            .replaceAll(node);
                                        that._forceReflow(template);
                                        that._transition(template).done(
                                            function() {
                                                data.context = $(this);
                                                that._trigger('failed', e, data);
                                                that._trigger('finished', e, data);
                                                deferred.resolve();
                                            }

                                        );
                                    }

                                );
                            } else {
                                deferred = that._addFinishedDeferreds();
                                that._transition($(this)).done(
                                    function() {
                                        $(this).remove();
                                        that._trigger('failed', e, data);
                                        that._trigger('finished', e, data);
                                        deferred.resolve();
                                    }

                                );
                            }
                        });
                    } else if (data.errorThrown !== 'abort') {
                        data.context = that._renderUpload(data.files)[
                                that.options.prependFiles ? 'prependTo' : 'appendTo'
                            ](that.options.filesContainer)
                            .data('data', data);
                        that._forceReflow(data.context);
                        deferred = that._addFinishedDeferreds();
                        that._transition(data.context).done(
                            function() {
                                data.context = $(this);
                                that._trigger('failed', e, data);
                                that._trigger('finished', e, data);
                                deferred.resolve();
                            }

                        );
                    } else {
                        that._trigger('failed', e, data);
                        that._trigger('finished', e, data);
                        that._addFinishedDeferreds().resolve();
                    }
                }
            });

            $('form')
                .bind('fileuploadadd', function(e, data) {
                    var fileName = $filter('uppercase')(data.files[0].name);
                    var ext = fileName.split('.').pop();
                    var size = data.files[0].size;

                    if (ctrl.validators && ctrl.validators.blockSameFileName) {
                        if (ctrl.newUploadedFile.indexOf(fileName) > -1) {
                            //alert(data.files[0].name + ctrl.validators.blockSameFileName.message);
                            return false;
                        }
                    }

                    if (ctrl.allowedType) {
                        if (ctrl.allowedType.fileExt.indexOf(ext) < 0) {
                            //alert(ctrl.allowedType.message);
                            return false;
                        }
                    }

                    if (ctrl.allowedSize) {
                        if (size > ctrl.allowedSize.value) {
                            //alert(ctrl.allowedSize.message);
                            return false;
                        }
                    }

                    $timeout(function() {
                        setFieldValid();
                    });

                    ctrl.newUploadedFile.push(fileName);

                    // for uploadAll
                    // $('#uploadAll').prop('disabled', false);
                    // $(document).trigger('userUploadButton', [false]);
                    //data.url = ctrl.fieldUuid ? getDFTCUploadURL() : hostUrl;
                })
                .bind('fileuploadfail', function(e, data) {

                    if (data.errorThrown === 'abort') {
                        if (data.files) {
                            var name = $filter('uppercase')(data.files[0].name);
                            var index = ctrl.newUploadedFile.indexOf(name);
                            ctrl.newUploadedFile.splice(index, 1);
                            if (ctrl.newUploadedFile.length < 1) {
                                $('#uploadAll').prop('disabled', true);
                                $(document).trigger('userUploadButton', [true]);
                            }
                        }
                    } else {
                        uploadSuccess = false;
                    }
                })
                .bind('fileuploadsend', function(e, data) {

                    data.url = getDFTCUploadURL();
                })
                .bind('fileuploaddone', function(e, data) {

                    var name = data.files[0].name;

                    var index = ctrl.newUploadedFile.indexOf(name);
                    ctrl.newUploadedFile.splice(index, 1);

                    var tempObj = {};
                    if (ctrl.formManager.getValue('claim_id')) {
                        tempObj = {
                            'file_name': name,
                            'file_download_link': COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + '/uipcfo/FileDownloadDBStream?ModuleCode=CLAIM_DOCUMENT&SysCode=SYS&DftcId=' +
                                tempCurrentUUID + '&contentDispositonType=inline&ReferenceId_1=' + ctrl.formManager.getValue('claim_id') +
                                '&ReferenceId_2=' + ctrl.formManager.getValue('policy_num') +
                                '&ReferenceId_3=' + ctrl.formManager.getValue('claim_type') +
                                '&ReferenceId_4=' + ctrl.formManager.getValue('member_cif_num') +
                                '&ReferenceId_5=' + ctrl.formManager.getValue('company_cif_num') +
                                '&ReferenceId_6=DONE',
                            'file_id': tempCurrentUUID,
                            'file_delete_link': COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + '/uipcfo/rest/claim/' + ctrl.formManager.getValue('claim_id') +
                                '/document/' + tempCurrentUUID
                        };
                    } else {
                        tempObj = {
                            'file_name': name,
                            'file_download_link': COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + '/uipcfo/FileDownloadDBStream?ModuleCode=CLAIM_DOCUMENT&SysCode=SYS&DftcId=' +
                                tempCurrentUUID + '&contentDispositonType=inline',
                            'file_id': tempCurrentUUID,
                            'file_delete_link': 'tempDeleteLink'
                        };
                    }

                    if (!ctrl.newUploadedFile) {
                        ctrl.newUploadedFile = [];
                    }

                    ctrl.newUploadedFile.push(tempObj);

                    // if (ctrl.newUploadedFile.length < 1) {
                    //     $('#uploadAll').prop('disabled', true);
                    //     $(document).trigger('userUploadButton', [true]);
                    // }
                })
                .bind('fileuploadstop', function() {
                    var upload = {
                        upload: uploadSuccess
                    };
                    $(document).trigger('redirectPage', [upload]);
                });

            $('form').fileupload('option', {
                redirect: window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                ),
                maxFileSize: 1000000000,
                acceptFileTypes: /(\.|\/).*$/i
            });

            // Load existing files:
            $('form').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('form').fileupload('option', 'url'),
                dataType: 'json',
                context: $('form')[0]
            }).always(function() {
                $(this).removeClass('fileupload-processing');
            }).done(function(result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {
                        result: result
                    });
            });

            // var files = [{
            //     'name': 'fileName.jpg',
            //     'size': 775702,
            //     'type': 'image/jpeg',
            //     'url': 'http://mydomain.com/files/fileName.jpg',
            //     'deleteUrl': 'http://mydomain.com/files/fileName.jpg',
            //     'deleteType': 'DELETE'
            // }, {
            //     'name': 'file2.jpg',
            //     'size': 68222,
            //     'type': 'image/jpeg',
            //     'url': 'http://mydomain.com/files/file2.jpg',
            //     'deleteUrl': 'http://mydomain.com/files/file2.jpg',
            //     'deleteType': 'DELETE'
            // }];

            // $('form').fileupload('option', 'done').call(this, $.Event('done'), {
            //     result: {
            //         files: files
            //     }
            // });
        }

        ctrl['delete'] = function(row) {
            // var template = '<file-delete></file-delete>';
            // var backdrop = 'static';
            // var size = 'lg';
            // var resolve = {};
            // var close = function() {
            //     row.find('.cancel').click();
            // };

            // var dismiss = function() {};

            // componentsModalService.openModalWithTemplate(template, backdrop, size, resolve, close, dismiss);
            row.find('.cancel').click();
        };

        ctrl.deleteUploaded = function(index, link) {

            if (link === 'tempDeleteLink') {
                ctrl.newUploadedFile.splice(index, 1);
                return;
            }

            // var template = '<file-delete></file-delete>';
            // var backdrop = 'static';
            // var size = 'lg';
            // var resolve = {};
            // var close = function() {
            //     function requestSuccessCallback() {
            //         ctrl.attachedFile.splice(index, 1);
            //         var remove = {
            //             remove: true
            //         };
            //         $(document).trigger('redirectPage', [remove]);
            //     }

            //     function requestFailedCallback() {
            //         var remove = {
            //             remove: false
            //         };
            //         $(document).trigger('redirectPage', [remove]);
            //     }

            //     $timeout(function() {
            //         $resource(link).delete({}, requestSuccessCallback, requestFailedCallback);
            //     });

            // };

            $timeout(function() {
                $resource(link)['delete']({}, requestSuccessCallback, requestFailedCallback);
            });

            // var dismiss = function() {};

            // componentsModalService.openModalWithTemplate(template, backdrop, size, resolve, close, dismiss);
            function requestSuccessCallback(response) {
                if (response.status === 'success') {
                    ctrl.attachedFile.splice(index, 1);
                } else if (response.status === 'failed') {
                    ctrl.errorMessage = response.message;
                }

                // var remove = {
                //     remove: true
                // };
                //$(document).trigger('redirectPage', [remove]);
            }

            function requestFailedCallback() {
                ctrl.errorMessage = 'Request fail';

                // var remove = {
                //     remove: false
                // };
                //$(document).trigger('redirectPage', [remove]);
            }

        };

        ctrl.getUploadedFile = function(link, uploadedFileName) {

            var authToken = storageService.get(ctrl.fieldKeyName);
            var header = {};
            header[ctrl.fieldRequestHeaderName] = authToken;

            $http.get(link, {
                headers: header,
                responseType: 'blob'
            }).success(function(data) {
                var ext = $filter('uppercase')(uploadedFileName.split('.').pop());

                var template = '<file-preview-modal></file-preview-modal>';
                var backdrop = 'static';
                var size = 'lg';

                componentsModalService.modalStorage.fileData = data;
                componentsModalService.modalStorage.ext = ext;
                componentsModalService.openModalWithTemplate(template, backdrop, size);
            });
        };

        function generateUUID() {
            var t = new Date().getTime();
            var uuid = 'xxxxxxxxxxxxxxxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (t + Math.random() * 16) % 16 | 0;
                t = Math.floor(t / 16);
                return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });

            return uuid;
        }

        function getDFTCUploadURL() {

            //var moduleCode = 'CLAIM_DOCUMENT';
            var moduleCode = ctrl.fieldModuleCode;
            var sysCode = 'SYS';

            tempCurrentUUID = '';
            tempCurrentUUID = generateUUID();

            var ref = '';
            if (ctrl.formManager.getValue('claim_id')) {
                ref = '&ReferenceId_1=' + ctrl.formManager.getValue('claim_id') +
                    '&ReferenceId_2=' + ctrl.formManager.getValue('policy_num') +
                    '&ReferenceId_3=' + ctrl.formManager.getValue('claim_type') +
                    '&ReferenceId_4=' + ctrl.formManager.getValue('member_cif_num') +
                    '&ReferenceId_5=' + ctrl.formManager.getValue('company_cif_num') +
                    '&ReferenceId_6=DONE';
            } else {
                ref = '&ReferenceId_2=' + ctrl.formManager.getValue('policy_num') +
                    '&ReferenceId_3=' + ctrl.formManager.getValue('claim_type') +
                    '&ReferenceId_4=' + ctrl.formManager.getValue('member_cif_num') +
                    '&ReferenceId_5=' + ctrl.formManager.getValue('company_cif_num') +
                    '&ReferenceId_6=PENDING';
            }

            var url = COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + '/' + hostUrl +
                '?ModuleCode=' + moduleCode + '&SysCode=' + sysCode + '&DftcId=' + tempCurrentUUID + ref;

            return url;
        }

        //---------------------- end of function ------------------------

        ctrl.checkFileType = function(fileName) {
            var attachedFileName = $filter('uppercase')(fileName);
            var ext = attachedFileName.split('.').pop();

            if (ext !== 'DOC' && ext !== 'DOCX') {
                return true;
            } else {
                return false;
            }
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        if (ctrl.fieldDependencies) {
            for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                var d = ctrl.fieldDependencies[i];

                if (ctrl.formManager.getValue(d.watch)) {
                    applyDependency(d);
                }

                watchValue(d.watch, d);
            }
        }

        function applyDependency(dependency) {
            var regexList = Object.keys(dependency.actionList);

            if (!regexList || regexList.length === 0) {
                $log.error('no matched regex defined');
                return;
            }

            var dependencyValue = ctrl.formManager.getValue(dependency.watch);
            var matchedRegex = null;
            regexList.some(function(regexText) {
                var regex = new RegExp(regexText);

                if (regex.test(dependencyValue)) {
                    matchedRegex = regexText;
                    return true;
                }

                return false;
            });

            if (!matchedRegex) {
                return;
            }

            var matchedActions = dependency.actionList[matchedRegex];

            if (!matchedActions || matchedActions.length === 0) {
                $log.error('no action defined for matched regex');
                return;
            }

            matchedActions.forEach(function(action) {
                if (action.actionType === 'disable') {
                    isDisabled = action.value;
                    reRunValidate(!action.value);
                } else if (action.actionType === 'visible') {
                    isVisible = action.value;
                    if (ctrl.validators && ctrl.formSectionMode !== 'view') {
                        $timeout(function() {
                            initValidation();
                        });
                    }

                    reRunValidate(action.value);
                } else if (action.actionType === 'update') {
                    $log.debug('action type: update');
                    ctrl.selectedValue = action.value;
                }
            });
        }

        function initErrorTooltip() {
            $log.debug('initialize error tooltip');
            $timeout(function() {
                if (ctrl.formSectionMode === 'edit') {
                    ctrl.tooltipElem = $('#' + ctrl.fieldName + '[data-toggle="tooltip"]').tooltip();
                }
            });
        }

        function initValidation() {
            var inputElem = $element.find('input');

            inputElem.bind('focus', function() {
                ctrl.isFocus = true;
                $timeout(function() {
                    if (!ctrl.isValid) {
                        showErrorTooltip();
                    }

                    setFieldValid();
                });
            });

            inputElem.bind('blur', function() {
                ctrl.isFocus = false;
                $timeout(function() {
                    hideErrorTooltip();
                    validate();
                }, 300);
            });
        }

        function showErrorTooltip() {
            if (ctrl.errorTooltipVisible) {
                return;
            }

            ctrl.tooltipElem.tooltip('show');
            ctrl.errorTooltipVisible = true;
        }

        function hideErrorTooltip() {
            if (!ctrl.errorTooltipVisible) {
                return;
            }

            ctrl.tooltipElem.tooltip('hide');
            ctrl.errorTooltipVisible = false;
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                    //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }

        function validate() {
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.isDisabled) {
                setFieldValid();

                return getValidateObj();
            }

            if (ctrl.validators.isRequired) {
                if (ctrl.newUploadedFile.length < 1) {
                    setFieldInvalid(ctrl.validators.isRequired.message);
                    return getValidateObj();
                }
            }

            setFieldValid();
            return getValidateObj();
        }

        function reRunValidate(actionValue) {
            setFieldValid();
            if (ctrl.formManager) {
                if (actionValue) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                }
            }

            initErrorTooltip();
        }

        function reset() {
            var resetValue = ctrl.defaultValue || '';
            ctrl.formModel[ctrl.fieldName] = resetValue;
            $('.cancel').click();
            setFieldValid();
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

        function destroy() {
            ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                deregisterFunc();
            });
        }

        var deregisterBeforeFormSubmission = $scope.$on('fileupload_update_attached_file', function() {

            var tempObj = ctrl.formManager.getValue('attachment') ? ctrl.formManager.getValue('attachment') : [];
            angular.forEach(ctrl.newUploadedFile, function(value) {
                tempObj.push(value);
            });

            ctrl.formManager.setValue('attachment', tempObj);

        });

        $scope.$on('$destroy', function() {
            $log.debug('destroy fileUpload');
            destroy();
            deregisterBeforeFormSubmission();
        });
    }

    angular.module('components.input').directive(directiveName, directive);
})();
