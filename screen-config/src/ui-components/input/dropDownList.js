(function() {
    'use strict';

    var directiveName = 'dropDownList';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                fieldId: '=',
                formModel: '=',
                defaultLabel: '=',
                ddlType: '=',
                formSectionMode: '=',
                formSectionName: '=',
                url: '=?',
                ddlItems: '=?',
                ignoreSort: '=?',
                formManager: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                formErrors: '=?',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                columnCssClass: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dropDownListCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function dropDownListCtrl($filter, $q, $compile, $http, $log, $element, $rootScope, $timeout, $scope, COMP_CONST, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.items = [];
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.cssClass = ctrl.fieldClass;
        ctrl.deregisterFuncs = [];
        ctrl.formSectionMode = ctrl.formSectionMode || 'edit';
        ctrl.isValid = true;
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;
        ctrl.isVisible = isVisible;
        ctrl.isDisabled = isDisabled;
        ctrl.isOpen = false;
        ctrl.isDirty = false;
        var others;

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            ctrl.apiPath = angular.copy(ctrl.url);

            $timeout(function() {
                $rootScope.$on('formModeChange:' + ctrl.formSectionName, function(eventName, mode) {
                    $log.debug('form mode change: ' + ctrl.formSectionName);

                    switch (mode) {
                        case 'edit':
                            initEditMode();
                            break;
                        case 'view':
                            initViewMode();
                            break;
                        default:
                            break;
                    }
                });

                function destroy() {
                    ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                        deregisterFunc();
                    });
                }

                $scope.$on('$destroy', function() {
                    $log.debug('destroy singleLineText');
                    destroy();
                });
            });
        }

        function initViewMode() {
            // if (ctrl.formSectionMode === 'view') {
            //     $log.debug('initViewMode:', ctrl.fieldName);

            //     if (ctrl.formManager) {
            //         ctrl.formManager.addComponent(ctrl.fieldName);
            //     }
            // }
        }

        function initEditMode() {
            /*  View mode is checked here is because when from claim-overview to preview page,
                ddl returned from server is not always label and value. Sometimes only return
                value. Thats why we need to map the value with the full list of ddl ourself to
                get the label to be displayed and this can only be done in Edit mode. */

            if (ctrl.formSectionMode === 'edit' || ctrl.formSectionMode === 'view') {
                ctrl.setSelectedValue = setSelectedValue;

                $log.debug('initEditMode:', ctrl.fieldName);
                if (ctrl.formManager) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                }

                var deregisterSetDisable = $rootScope.$on('setDisable_' + ctrl.fieldName, function(e, disable) {
                    ctrl.isDisabled = disable;
                });

                var deregisterHideDropDown = $rootScope.$on('hideDropDown_' + ctrl.fieldName, function() {
                    hideDropDownList();
                });

                ctrl.deregisterFuncs.push(deregisterSetDisable);
                ctrl.deregisterFuncs.push(deregisterHideDropDown);

                loadDDL().then(function() {
                        $log.debug('DDL Loaded:', ctrl.fieldName);

                        $(document).on('click', function(e) {
                            var dropdownlistContainer = $('#' + ctrl.fieldName);

                            if (!dropdownlistContainer.is(e.target) && dropdownlistContainer.has(e.target).length === 0) {
                                hideDropDownList();
                            }
                        });

                        ctrl.toggle = function toggle() {
                            $timeout(function() {
                                if (ctrl.isDisabled || !ctrl.ddlLoaded) {
                                    return;
                                }

                                ctrl.isOpen = !ctrl.isOpen;
                            });
                        };

                        ctrl.selectedValue = (ctrl.formManager.getValue(ctrl.fieldName)) ?
                            ctrl.formManager.getValue(ctrl.fieldName) :
                            ctrl.fieldDefaultValue;
                        $log.debug('fieldId:', ctrl.fieldId, ', fieldId value:', ctrl.formManager.getValue(ctrl.fieldId));
                        $log.debug('fieldname:', ctrl.fieldName, ', fieldName value:', ctrl.formManager.getValue(ctrl.fieldName));

                        if (ctrl.fieldDependencies) {
                            for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                                var dep = ctrl.fieldDependencies[i];

                                //use case in module: MY Profile
                                applyDependency(dep);

                                watchValue('valueChange_' + dep.watch, dep);
                            }
                        }

                        var deregisterSetValid = $rootScope.$on('setValid_' + ctrl.fieldName, function() {
                            setFieldValid();
                        });

                        ctrl.deregisterFuncs.push(deregisterSetValid);

                        if (ctrl.isVisible && !ctrl.isDisabled) {
                            setSelectedValue(ctrl.selectedValue, false);
                        }
                    },

                    function(reason) {
                        $log.error('failed to load DDL:', reason);
                    });
            }

            function hideDropDownList() {
                ctrl.isOpen = false;
            }

            function setSelectedValue(value, isDirty) {
                ctrl.selectedValue = value;
                ctrl.isDirty = (isDirty !== undefined) ? isDirty : true;
                var matched = false;
                $log.debug('set selected value:', ctrl.selectedValue);
                ctrl.formManager.setValue(ctrl.fieldId, ctrl.selectedValue);
                $log.debug(ctrl.items);
                ctrl.items.some(function(item) {
                    if (item.value === ctrl.selectedValue) {
                        ctrl.selectedItemLabel = item.label;
                        ctrl.formManager.setValue(ctrl.fieldName, item.value);
                        ctrl.formManager.setValue(ctrl.fieldName + '_label', item.label);
                        matched = true;
                        return true;
                    }

                    return false;
                });

                if (!matched) {
                    $log.debug('no matched item for field:', ctrl.fieldName);
                    ctrl.formManager.setValue(ctrl.fieldName, '');
                    ctrl.formManager.setValue(ctrl.fieldName + '_label', '');
                    ctrl.formManager.setValue(ctrl.fieldId, '');
                    ctrl.selectedValue = '';
                    ctrl.selectedItemLabel = ctrl.defaultItem;
                }

                $rootScope.$emit('valueChange_' + ctrl.fieldName, ctrl.isDirty);
            }

            function loadDDLFromAPI(url) {
                ctrl.defaultItem = 'loading...';
                var regexPattern = /\{(\w+)\}/g;
                var matchGroup = '';

                while ((matchGroup = regexPattern.exec(url)) != null) {
                    var fieldName = matchGroup[1];
                    var fieldValue = ctrl.formManager.getValue(fieldName);
                    url = url.replace('{' + fieldName + '}', fieldValue);
                }

                $log.debug('load ddl from api:', url);

                return $http({
                    method: 'GET',
                    url: url
                });
            }

            function loadDDL() {
                var loadDDLDeferred = $q.defer();

                function loadSuccess(response) {
                    $log.debug('DDL loaded ' + ctrl.fieldName);
                    var ddlData = response.data.data;

                    if (!ddlData) {
                        ctrl.defaultItem = '';
                        loadDDLDeferred.resolve();
                        return;
                    }

                    if (!ddlData.items || ddlData.items.length === 0) {
                        $log.debug(response);
                        $log.error('zero item for DDL: ' + ctrl.fieldName);
                        loadDDLDeferred.reject('zero item for DDL: ' + ctrl.fieldName);
                    }

                    $timeout(function() {
                        if (ctrl.ddlType === 'static') {
                            if (ddlData.items[0]) {
                                if (!ddlData.items[0].label) {
                                    $log.error('Label is empty for DDL: ' + ctrl.fieldName);
                                    loadDDLDeferred.reject('Label is empty for DDL: ' + ctrl.fieldName);
                                }

                                if (!ddlData.items[0].value) {
                                    $log.error('Value is empty for DDL: ' + ctrl.fieldName);
                                    loadDDLDeferred.reject('Value is empty for DDL: ' + ctrl.fieldName);
                                }
                            }

                            ctrl.items = ddlData.items;

                            /*
                                Default Sort is included is because for certain scenario.
                                For example, if we wanna manually sort a ddl, we will need to set
                                the ctrl.ignoreSort to true and sort it manually.
                                Otherwise the ddl items we sorted manually will be sorted again here.
                            */

                            if (ctrl.ignoreSort !== true) {
                                ctrl.items = $filter('orderBy')(ctrl.items, 'label');
                            }

                            for (var i = 0; i < ctrl.items.length; i++) {
                                if (ctrl.items[i].label === 'Others' || ctrl.items[i].label === 'Other') {
                                    others = ctrl.items.splice(i, 1);
                                    break;
                                }
                            }

                            if (others) {
                                ctrl.items.push(others[0]);
                            }
                        } else {
                            var items = [];
                            angular.forEach(ddlData.items, function(item) {
                                var ddlItem = {};

                                ddlItem.value = item[ddlData.value];
                                var label = ddlData.display;
                                angular.forEach(item, function(value, key) {
                                    label = label.replace('{' + key + '}', value);
                                });

                                ddlItem.label = label;
                                items.push(ddlItem);
                            });

                            ctrl.items = items;
                            if (ctrl.ignoreSort !== true) {
                                ctrl.items = $filter('orderBy')(ctrl.items, 'label');
                            }

                            for (var j = 0; j < ctrl.items.length; j++) {
                                if (ctrl.items[j].label === 'Others' || ctrl.items[j].label === 'Other') {
                                    others = ctrl.items.splice(j, 1);
                                    break;
                                }
                            }

                            if (others) {
                                ctrl.items.push(others[0]);
                            }
                        }

                        ctrl.ddlLoaded = true;
                        ctrl.selectedItemLabel = ctrl.defaultLabel;
                        ctrl.defaultItem = ctrl.defaultLabel;

                        loadDDLDeferred.resolve();
                    });
                }

                function loadFailed() {
                    $log.error('Fail to load DDL: ' + ctrl.fieldName);
                    loadDDLDeferred.reject('Failed to load DDL from API:', ctrl.url);
                }

                /*   if (ctrl.items && ctrl.items.length > 0) {
                       loadDDLDeferred.resolve();
                   } else {*/
                if (ctrl.url) {
                    var url = COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + COMP_CONST.API_BASE_PATH + ctrl.url;
                    loadDDLFromAPI(url).then(loadSuccess, loadFailed);
                } else {
                    ctrl.defaultItem = ctrl.defaultLabel;

                    if (ctrl.ddlType !== 'static') {
                        $log.error('no url is defined for DDL');
                        loadDDLDeferred.reject('no url is defined for DDL');
                    } else {
                        if (!ctrl.ddlItems || ctrl.ddlItems.length === 0) {
                            $log.error('no items loaded');
                            loadDDLDeferred.reject('no items loaded');
                        }

                        ctrl.items = ctrl.ddlItems;
                        if (ctrl.ignoreSort !== true) {
                            ctrl.items = $filter('orderBy')(ctrl.items, 'label');
                        }

                        for (var k = 0; k < ctrl.items.length; k++) {
                            if (ctrl.items[k].label === 'Others' || ctrl.items[k].label === 'Other') {
                                others = ctrl.items.splice(k, 1);
                                break;
                            }
                        }

                        if (others) {
                            ctrl.items.push(others[0]);
                        }

                        ctrl.ddlLoaded = true;
                        loadDDLDeferred.resolve();
                    }
                }
                /*}*/

                return loadDDLDeferred.promise;
            }

            function watchValue(eventName, dependency) {
                var deregisterValueChange = $rootScope.$on('valueChange_' + dependency.watch, function(e, isDirty) {
                    $log.debug('valueChange_' + dependency.watch);
                    applyDependency(dependency, isDirty);
                });

                if (!ctrl.deregisterFuncs) {
                    ctrl.deregisterFuncs = [];
                }

                ctrl.deregisterFuncs.push(deregisterValueChange);
            }

            function applyDependency(dependency, isDirty) {
                //var regexList = Object.keys(dependency.actionList);
                var regexList = dependency.actionList;

                if (!regexList || regexList.length === 0) {
                    $log.error('no matched regex defined');
                    return;
                }

                var dependencyValue = (ctrl.formManager.getValue(dependency.watch) !== undefined) ?
                    ctrl.formManager.getValue(dependency.watch) : '';

                if (ctrl.apiPath) {
                    ctrl.url = ctrl.apiPath.replace('{' + dependency.watch + '}', ctrl.formManager.getValue(dependency.watch));
                }

                /*var matchedRegex = null;
                regexList.some(function(regexText) {
                    var regex = new RegExp(regexText);

                    if (regex.test(dependencyValue)) {
                        matchedRegex = regexText;
                        return true;
                    }

                    return false;
                });

                if (!matchedRegex) {
                    return;
                }*/

                for (var regexText in regexList) {
                    var regex = new RegExp(regexText);
                    if (regex.test(dependencyValue)) {
                        var matchedActions = dependency.actionList[regexText];

                        if (!matchedActions || matchedActions.length === 0) {
                            $log.error('no action defined for matched regex');
                            return;
                        }

                        matchedActions.forEach(function(action) {
                            if (action.actionType === 'disable') {
                                $log.debug('action type: disable:', action.value);
                                ctrl.isDisabled = action.value;
                                reRunValidate(!action.value);
                                if (!action.value) {
                                    loadDDL().then(function() {
                                            $log.debug('************ ddl loaded ********');
                                            ctrl.selectedValue = (ctrl.formManager.getValue(ctrl.fieldName)) ?
                                                ctrl.formManager.getValue(ctrl.fieldName) :
                                                ctrl.fieldDefaultValue;

                                            $log.debug(ctrl.selectedValue);
                                            ctrl.setSelectedValue(ctrl.selectedValue, false);
                                        },

                                        function(reason) {
                                            $log.error('failed to load DDL:', reason);
                                        });
                                }
                            } else if (action.actionType === 'visible') {
                                $log.debug('action type: visible');
                                isVisible = action.value;
                                ctrl.isVisible = action.value;
                                if (ctrl.isVisible) {
                                    loadDDL().then(function() {
                                            ctrl.selectedValue = (ctrl.formManager.getValue(ctrl.fieldName)) ?
                                                ctrl.formManager.getValue(ctrl.fieldName) :
                                                ctrl.fieldDefaultValue;

                                            ctrl.setSelectedValue(ctrl.selectedValue, false);
                                        },

                                        function(reason) {
                                            $log.error('failed to load DDL:', reason);
                                        });
                                }

                                reRunValidate(action.value);
                            } else if (action.actionType === 'update') {
                                $log.debug('action type: update');
                                ctrl.selectedValue = action.value;
                                setSelectedValue(action.value, false);
                            } else if (action.actionType === 'reinitialize') {
                                $log.debug('action type: reinitialize, fieldName:', ctrl.fieldName);

                                // TODO: revisit this

                                loadDDL().then(function() {
                                        ctrl.selectedValue = (ctrl.formManager.getValue(ctrl.fieldName)) ?
                                            ctrl.formManager.getValue(ctrl.fieldName) :
                                            ctrl.fieldDefaultValue;

                                        ctrl.setSelectedValue(ctrl.selectedValue, false);
                                    },

                                    function(reason) {
                                        $log.error('failed to load DDL:', reason);
                                    });
                            } else if (action.actionType === 'reset') {
                                if (isDirty) {
                                    reset();
                                }
                            }
                        });
                    }
                }

            }

            function reRunValidate(actionValue) {
                setFieldValid();

                if (ctrl.formManager) {
                    if (actionValue) {
                        ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                    } else {
                        ctrl.formManager.removeComponent(ctrl.fieldName);
                    }
                }
            }

            function reset() {
                var resetValue = ctrl.defaultValue || '';
                ctrl.selectedValue = resetValue;
                setSelectedValue(resetValue, false);
                setFieldValid();
            }

            function getValidateObj() {
                if (isVisible) {
                    return {
                        isValid: ctrl.isValid,
                        message: ctrl.errorMessage
                        //label: ctrl.label.name
                    };
                } else {
                    return {
                        isValid: true
                    };
                }
            }

            function validate() {
                if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.isDisabled) {
                    setFieldValid();

                    return getValidateObj();
                }

                if (ctrl.validators.isRequired) {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        setFieldInvalid(ctrl.validators.isRequired.message);

                        return getValidateObj();
                    }
                }

                if (ctrl.validators.groupValidation) {
                    if (ctrl.validators.groupValidation.type === 'isRequired') {
                        if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                            var valid = true;
                            angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                                var pattern = member.pattern ? member.pattern : '^.+$';
                                var regex = new RegExp(pattern);
                                var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                                if (regex.test(memberValue)) {
                                    setFieldInvalid(ctrl.validators.groupValidation.message);
                                    valid = false;
                                    return;
                                }
                            });

                            if (!valid) {
                                return getValidateObj();
                            }

                            angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                                $rootScope.$emit('setValid_' + member.name);
                            });
                        }
                    }
                }

                //TODO: add more validation

                setFieldValid();
                return getValidateObj();
            }

            function setFieldValid() {
                ctrl.isValid = true;
                ctrl.errorMessage = '';
            }

            function setFieldInvalid(message) {
                ctrl.isValid = false;
                ctrl.errorMessage = message;
            }
        }

        $timeout(function() {
            init();
        });

    }

    angular.module('components.input').directive(directiveName, directive);
})();
