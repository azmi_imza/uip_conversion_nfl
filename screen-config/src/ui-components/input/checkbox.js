(function() {
    'use strict';

    var directiveName = 'checkbox';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formModel: '=',
                formSectionMode: '=',
                fieldItems: '=',
                fieldAlignment: '=',
                singleOrMultiValue: '=',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                fieldId: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                columnCssClass: '=?',
                formManager: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: checkboxCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function checkboxCtrl($timeout, $rootScope, $element, $log, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.fieldAlignment = ctrl.fieldAlignment || 'inline';
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.isValid = true;
        ctrl.deregisterFuncs = [];
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;

        // ctrl.value = function(newValue) {
        //     return arguments.length ? (ctrl.formManager.setValue(ctrl.fieldName, newValue)) : ctrl.formManager.getValue(ctrl.fieldName);
        // };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        ctrl.check = function(item) {
            if (ctrl.singleOrMultiValue === 'single') {
                var existingValue = ctrl.formManager.getValue(ctrl.fieldName);
                var value = existingValue === 'Y' ? '' : 'Y';

                $log.debug('set single checkbox value:', value);
                ctrl.formManager.setValue(ctrl.fieldName, value);
            } else {
                var fieldValue = ctrl.formManager.getValue(ctrl.fieldName);

                $log.debug('set multi checkbox value:', fieldValue);
                var selectedIndex = fieldValue.indexOf(item.value);
                if (selectedIndex < 0) {
                    fieldValue.push(item.value);
                } else {
                    fieldValue.splice(selectedIndex, 1);
                }
            }

            $rootScope.$emit('valueChange_' + ctrl.fieldName);
        };

        ctrl.isChecked = function(value) {
            var fieldValue = ctrl.formManager.getValue(ctrl.fieldName);
            return fieldValue.indexOf(value) > -1;
        };

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            $timeout(function() {
                if (ctrl.formManager) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                }

                if (!ctrl.formManager.getValue(ctrl.fieldName) && ctrl.fieldDefaultValue) {
                    if (ctrl.singleOrMultiValue === 'single') {
                        ctrl.formManager.setValue(ctrl.fieldName, defaultValue);
                    } else {
                        ctrl.fieldDefaultValue = ctrl.fieldDefaultValue.replace(/ /g, '');
                        var defaultValue = ctrl.fieldDefaultValue.split(',');
                        ctrl.formManager.setValue(ctrl.fieldName, defaultValue);
                    }
                }

                if (ctrl.fieldAlignment === 'inline') {
                    ctrl.checkboxStyle = 'display:inline-block; padding:0px 10px 5px 10px;';
                } else {
                    ctrl.checkboxStyle = 'display:block; padding-left:10px';
                }

                if (ctrl.formManager.getValue(ctrl.fieldName)) {
                    if (ctrl.singleOrMultiValue === 'single') {
                        var singleValue = ctrl.formManager.getValue(ctrl.fieldName);
                        for (var j = 0; j < ctrl.fieldItems.length; j++) {
                            if (singleValue === ctrl.fieldItems[j].value) {
                                ctrl.fieldItems[j].checked = true;
                            }
                        }
                    } else {
                        var multiValues = ctrl.formManager.getValue(ctrl.fieldName);
                        for (var i = 0; i < ctrl.fieldItems.length; i++) {
                            if (multiValues.indexOf(ctrl.fieldItems[i].value) > -1) {
                                ctrl.fieldItems[i].checked = true;
                            }
                        }
                    }

                } else {
                    if (ctrl.singleOrMultiValue === 'single') {
                        ctrl.formManager.setValue(ctrl.fieldName, '');
                    } else {
                        ctrl.formManager.setValue(ctrl.fieldName, []);
                    }
                }
            });
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);

                // TODO: add view mode initialization
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);

                // TODO: add edit mode initialization

                if (ctrl.fieldDependencies) {
                    for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                        var d = ctrl.fieldDependencies[i];

                        //if (ctrl.formManager.getValue(d.watch)) {
                            applyDependency(d);
                        //}

                        watchValue(d.watch, d);
                    }
                }

                initValidation();
            }
        }

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        function initValidation() {
            var inputElem = $element.find('input');

            inputElem.bind('change', function() {
                $timeout(function() {
                    setFieldValid();
                });
            });
        }

        function applyDependency(dependency) {
            var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];

            if (listAction) {
                for (var j = 0; j < listAction.length; j++) {
                    var action = listAction[j];
                    if (action.actionType === 'disable') {
                        isDisabled = action.value;
                        reRunValidate(!action.value);
                    } else if (action.actionType === 'visible') {
                        isVisible = action.value;
                        if (ctrl.formSectionMode === 'edit') {
                            initEditMode();
                        }

                        if (ctrl.formSectionMode === 'view') {
                            initViewMode();
                        }

                        reRunValidate(action.value);
                    } else if (action.actionType === 'reset') {
                        reset();
                    }
                }
            }
        }

        function reRunValidate(actionValue) {
            setFieldValid();
            if (ctrl.formManager) {
                if (actionValue) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                }
            }
        }

        function reset() {
            var resetValue;

            if (ctrl.singleOrMultiValue === 'single') {
                resetValue = ctrl.defaultValue || '';
                ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            } else {
                resetValue = ctrl.defaultValue || [];
                ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            }

            setFieldValid();
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

        function validate() {
            $log.debug('run validation');
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.disability()) {
                setFieldValid();
                return {
                    isValid: true
                };
            }

            if (ctrl.validators.isRequired) {

                if (ctrl.singleOrMultiValue === 'single') {
                    var singleValue = ctrl.formManager.getValue(ctrl.fieldName);
                    if (!singleValue || singleValue !== 'Y') {

                        setFieldInvalid(ctrl.validators.isRequired.message);

                        return getValidateObj();
                    }
                } else {
                    var multiValues = ctrl.formManager.getValue(ctrl.fieldName).length;
                    $log.debug('value: ' + multiValues);
                    if (multiValues < 1) {

                        setFieldInvalid(ctrl.validators.isRequired.message);

                        return getValidateObj();
                    }

                }
            }

            if (ctrl.validators.groupValidation) {
                if (ctrl.validators.groupValidation.type === 'isRequired') {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        var valid = true;
                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            var pattern = member.pattern ? member.pattern : '^.+$';
                            var regex = new RegExp(pattern);
                            var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                            if (regex.test(memberValue)) {
                                setFieldInvalid(ctrl.validators.groupValidation.message);
                                valid = false;
                                return;
                            }
                        });

                        if (!valid) {
                            return getValidateObj();
                        }

                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            $rootScope.$emit('setValid_' + member.name);
                        });
                    }
                }
            }

            setFieldValid();
            return getValidateObj();
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                    //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }

        init();
    }

    angular.module('components.input').directive(directiveName, directive);
})();
