(function() {
    'use strict';

    var directiveName = 'radioGroup';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formModel: '=',
                formSectionMode: '=',
                fieldItems: '=',
                fieldAlignment: '=',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                fieldId: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                columnCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                formManager: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: radioGroupCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function radioGroupCtrl($log, $timeout, $rootScope, $element, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.isValid = true;
        ctrl.deregisterFuncs = [];
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        ctrl.radioStyle = 'display:block; padding-left:10px';
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            $log.debug('initBase:', ctrl.fieldName);

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.radioStyle) {
                var radioStyle = ctrl.radioStyle.split(';');
                ctrl.radioStyleObj = {};
                _.forEach(radioStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.radioStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            // TODO: add base initialization
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);

                // TODO: add view mode initialization
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);

                // TODO: add edit mode initialization
            }
        }

        init();

        if (ctrl.formManager) {
            ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
        }

        if (ctrl.validators && ctrl.formSectionMode !== 'view') {
            $timeout(function() {
                initValidation();
            });
        }

        if (ctrl.formManager.getValue(ctrl.fieldName)) {
            angular.forEach(ctrl.fieldItems, function(item) {
                if (item.value === ctrl.formManager.getValue(ctrl.fieldName)) {
                    ctrl.formManager.setValue(ctrl.fieldName + '_label', item.label);
                    return;
                }
            });
        } else if (ctrl.fieldDefaultValue) {
            ctrl.formManager.setValue(ctrl.fieldName, ctrl.fieldDefaultValue);
            angular.forEach(ctrl.fieldItems, function(item) {
                if (item.value === ctrl.fieldDefaultValue) {
                    ctrl.formManager.setValue(ctrl.fieldName + '_label', item.label);
                    return;
                }
            });
        }

        if (ctrl.fieldAlignment === 'inline') {
            ctrl.radioStyleObj = {
                display: 'inline-block',
                padding: '0px 10px 0px 10px'
            };
        }

        ctrl.onChange = function(label) {
            ctrl.formManager.setValue(ctrl.fieldName + '_label', label);
            $rootScope.$emit('valueChange_' + ctrl.fieldName, ctrl.formManager.getValue(ctrl.fieldName));
        };

        ctrl.value = function(newValue) {
            return arguments.length ? (ctrl.formManager.setValue(ctrl.fieldName, newValue)) : ctrl.formManager.getValue(ctrl.fieldName);
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        if (ctrl.fieldDependencies) {
            for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                var d = ctrl.fieldDependencies[i];

                //if (ctrl.formManager.getValue(d.watch)) {
                    applyDependency(d);
                //}

                watchValue(d.watch, d);
            }
        }

        function initValidation() {
            $timeout(function() {
                var inputElem = $element.find('input');

                inputElem.bind('change', function() {
                    $log.debug('radio change');
                });
            });
        }

        function applyDependency(dependency) {
            var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];

            if (listAction) {
                for (var j = 0; j < listAction.length; j++) {
                    var action = listAction[j];
                    if (action.actionType === 'disable') {
                        isDisabled = action.value;
                        reRunValidate(!action.value);
                    } else if (action.actionType === 'visible') {
                        isVisible = action.value;
                        if (ctrl.validators && ctrl.formSectionMode !== 'view') {
                            initValidation();
                        }

                        reRunValidate(action.value);
                    } else if (action.actionType === 'reset') {
                        reset();
                    }
                }
            }
        }

        function reRunValidate(actionValue) {
            setFieldValid();
            if (ctrl.formManager) {
                if (actionValue) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                }
            }
        }

        function reset() {
            var resetValue = ctrl.defaultValue || '';
            ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            setFieldValid();
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

        function validate() {
            $log.debug('run validation');
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.disability()) {
                setFieldValid();
                return getValidateObj();
            }

            if (ctrl.validators.isRequired) {
                var value = ctrl.value();
                $log.debug('value: ' + value);
                if (!value) {
                    setFieldInvalid(ctrl.validators.isRequired.message);
                    return getValidateObj();
                }
            }

            if (ctrl.validators.groupValidation) {
                if (ctrl.validators.groupValidation.type === 'isRequired') {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        var valid = true;
                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            var pattern = member.pattern ? member.pattern : '^.+$';
                            var regex = new RegExp(pattern);
                            var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                            if (regex.test(memberValue)) {
                                setFieldInvalid(ctrl.validators.groupValidation.message);
                                valid = false;
                                return;
                            }
                        });

                        if (!valid) {
                            return getValidateObj();
                        }

                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            $rootScope.$emit('setValid_' + member.name);
                        });
                    }
                }
            }

            //TODO: add more validation

            setFieldValid();
            return getValidateObj();
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                    //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }
    }

    angular.module('components.input').directive(directiveName, directive);
})();
