(function() {
    'use strict';

    var directiveName = 'fileDelete';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: fileDeleteCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function fileDeleteCtrl(componentsModalService) {
        var ctrl = this;
        ctrl.templateName = templateName;

        ctrl.confirm = function() {
            componentsModalService.closeModal();
        };

        ctrl.cancel = function() {
            componentsModalService.dismissModal();
        };

        ctrl.back = function() {
            componentsModalService.dismissModal();
        };
    }

    angular.module('components.input').directive(directiveName, directive);
})();
