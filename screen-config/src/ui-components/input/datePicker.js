(function() {
    'use strict';

    var directiveName = 'datePicker';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formModel: '=',
                formSectionMode: '=',
                formManager: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                max: '=?',
                min: '=?',
                maxDependency: '=?',
                minDependency: '=?',
                format: '=?',
                columnCssClass: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: datePickerCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function datePickerCtrl($log, $element, $rootScope, $timeout, $compile, $scope, $filter, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.isValid = true;
        ctrl.deregisterFuncs = [];
        ctrl.format = ctrl.format || 'dd MMM yyyy';
        ctrl.dateString = ctrl.formManager.getValue(ctrl.fieldName) ?
            $filter('date')(new Date(ctrl.formManager.getValue(ctrl.fieldName)), ctrl.format) : '';
        ctrl.cssClass = 'form-control form-control-date ' + ctrl.fieldClass;
        ctrl.formSectionMode = ctrl.formSectionMode || 'edit';
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;

        ctrl.value = function(newValue) {
            if (!arguments || !arguments.length) {
                return ctrl.dateString;
            }

            if (!newValue) {
                ctrl.dateString = '';
                ctrl.formManager.setValue(ctrl.fieldName, null);
                ctrl.datePicker.pickadate('picker').set('select', null);
            } else {
                var date = new Date(newValue).getTime();
                ctrl.formManager.setValue(ctrl.fieldName, date);
                ctrl.dateString = newValue;
                ctrl.datePicker.pickadate('picker').set('select', date);
            }

            $rootScope.$emit('valueChange_' + ctrl.fieldName);
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        ctrl.visibility = function() {
            return isVisible;
        };

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            $scope.$on('$destroy', function() {
                $log.debug('destroy datePicker');
                destroy();
            });

            $timeout(function() {
                $rootScope.$on('formModeChange:' + ctrl.formSectionName, function(eventName, mode) {
                    $log.debug('form mode change: ' + ctrl.formSectionName);

                    switch (mode) {
                        case 'edit':
                            initViewMode();
                            break;
                        case 'view':
                            initEditMode();
                            break;
                        default:
                            break;
                    }
                });
            });
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $timeout(function() {
                    $log.debug('initViewMode:', ctrl.fieldName);

                    if (ctrl.formManager) {
                        ctrl.formManager.addComponent(ctrl.fieldName);
                    }

                });
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $timeout(function() {
                    $log.debug('initEditMode:', ctrl.fieldName);

                    if (ctrl.formManager) {
                        ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                    }

                    if (ctrl.fieldDependencies) {
                        for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                            var d = ctrl.fieldDependencies[i];

                            //if (ctrl.formManager.getValue(d.watch)) {
                                applyDependency(d);
                            //}

                            watchValue(d.watch, d);
                        }
                    }

                    ctrl.openDatePicker = openDatePicker;

                    ctrl.datePickerOptions = {
                        selectYears: 100,
                        selectMonths: true,
                        format: ctrl.format.toLowerCase()
                    };

                    ctrl.datePickerOptions.onClose = function() {
                        $log.debug('close datepicker');
                    };

                    ctrl.datePickerOptions.onSet = function(context) {
                        console.log('set datepicker:', context);

                        if (context.clear !== undefined) {
                            ctrl.value('');
                        }
                    };

                    ctrl.datePickerOptions.onOpen = function() {
                        $log.debug('open datepicker');
                    };

                    ctrl.datePicker = $element.find('input').pickadate(ctrl.datePickerOptions);

                    if (ctrl.max) {
                        ctrl.datePicker.pickadate('picker').set('max', ctrl.max);
                    }

                    if (ctrl.min) {
                        ctrl.datePicker.pickadate('picker').set('min', ctrl.min);
                    }

                    if (!ctrl.deregisterFuncs) {
                        ctrl.deregisterFuncs = [];
                    }

                    if (ctrl.maxDependency) {
                        var setMaxDate = function(item) {
                            var day = item.excludeSelected ? (1000 * 60 * 60 * 24) : 0;
                            var timestamp = parseInt(ctrl.formManager.getValue(item.name)) - day;
                            var date = new Date(timestamp);

                            if (isNaN(ctrl.datePicker.pickadate('picker').get('max').pick)) {
                                ctrl.datePicker.pickadate('picker').set('max', date);
                            } else {
                                if (timestamp < ctrl.datePicker.pickadate('picker').get('max').pick) {
                                    ctrl.datePicker.pickadate('picker').set('max', date);
                                }
                            }
                        };

                        var applyMaxDependency = function() {
                            if (ctrl.max) {
                                ctrl.datePicker.pickadate('picker').set('max', ctrl.max);
                            } else {
                                ctrl.datePicker.pickadate('picker').set('max', false);
                            }

                            _.forEach(ctrl.maxDependency, function(obj) {
                                if (ctrl.formManager.getValue(obj.name)) {
                                    setMaxDate(obj);
                                }
                            });
                        };

                        angular.forEach(ctrl.maxDependency, function(item) {

                            if (ctrl.formManager.getValue(item.name)) {
                                setMaxDate(item);
                            }

                            var maxDependency = $rootScope.$on('valueChange_' + item.name, function() {
                                applyMaxDependency();
                            });

                            ctrl.deregisterFuncs.push(maxDependency);
                        });
                    }

                    if (ctrl.minDependency) {
                        var setMinDate = function(item) {
                            var day = item.excludeSelected ? (1000 * 60 * 60 * 24) : 0;
                            var timestamp = parseInt(ctrl.formManager.getValue(item.name)) + day;
                            var date = new Date(timestamp);

                            if (isNaN(ctrl.datePicker.pickadate('picker').get('min').pick)) {
                                ctrl.datePicker.pickadate('picker').set('min', date);
                            } else {
                                if (timestamp > ctrl.datePicker.pickadate('picker').get('min').pick) {
                                    ctrl.datePicker.pickadate('picker').set('min', date);
                                }
                            }
                        };

                        var applyMinDependency = function() {
                            if (ctrl.min) {
                                ctrl.datePicker.pickadate('picker').set('min', ctrl.min);
                            } else {
                                ctrl.datePicker.pickadate('picker').set('min', false);
                            }

                            _.forEach(ctrl.minDependency, function(obj) {
                                if (ctrl.formManager.getValue(obj.name)) {
                                    setMinDate(obj);
                                }
                            });
                        };

                        angular.forEach(ctrl.minDependency, function(item) {

                            if (ctrl.formManager.getValue(item.name)) {
                                setMinDate(item);
                            }

                            var minDependency = $rootScope.$on('valueChange_' + item.name, function() {
                                applyMinDependency();
                            });

                            ctrl.deregisterFuncs.push(minDependency);
                        });
                    }
                });
            }
        }

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        function openDatePicker($event) {
            ctrl.datePicker.pickadate('picker').open();
            $event.stopPropagation();
            $event.preventDefault();
        }

        // function initializeElement() {
        //     $timeout(function() {
        //         var date = (ctrl.formManager.getValue(ctrl.fieldName)) ?
        //             ctrl.formManager.getValue(ctrl.fieldName) :
        //             ctrl.fieldDefaultValue;

        //         if (date) {
        //             ctrl.value(date);
        //         }
        //     });
        // }

        function validate() {
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || ctrl.disability()) {
                setFieldValid();
                return getValidateObj();
            }

            if (ctrl.validators.isRequired) {
                var value = ctrl.value();
                $log.debug('value: ' + value);
                if (!value) {

                    setFieldInvalid(ctrl.validators.isRequired.message);

                    return getValidateObj();
                }
            }

            if (ctrl.validators.groupValidation) {
                if (ctrl.validators.groupValidation.type === 'isRequired') {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        var valid = true;
                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            var pattern = member.pattern ? member.pattern : '^.+$';
                            var regex = new RegExp(pattern);
                            var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                            if (regex.test(memberValue)) {
                                setFieldInvalid(ctrl.validators.groupValidation.message);
                                valid = false;
                                return;
                            }
                        });

                        if (!valid) {
                            return getValidateObj();
                        }

                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            $rootScope.$emit('setValid_' + member.name);
                        });
                    }
                }
            }

            //TODO: add more validation

            setFieldValid();
            return getValidateObj();
        }

        function reset() {
            if (ctrl.min) {
                ctrl.datePicker.pickadate('picker').set('min', ctrl.min);
            } else {
                ctrl.datePicker.pickadate('picker').set('min', false);
            }

            if (ctrl.max) {
                ctrl.datePicker.pickadate('picker').set('max', ctrl.max);
            } else {
                ctrl.datePicker.pickadate('picker').set('max', false);
            }

            var resetValue = ctrl.defaultValue || '';
            ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            ctrl.dateString = resetValue;
            setFieldValid();
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                        //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }

        function applyDependency(dependency) {
            var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];

            if (listAction) {
                for (var j = 0; j < listAction.length; j++) {
                    var action = listAction[j];
                    if (action.actionType === 'disable') {
                        isDisabled = action.value;
                        reRunValidate(!action.value);
                    } else if (action.actionType === 'visible') {
                        isVisible = action.value;
                        initEditMode();

                        reRunValidate(action.value);
                    } else if (action.actionType === 'reset') {
                        reset();
                    }
                }
            }
        }

        function reRunValidate(actionValue) {
            setFieldValid();
            if (ctrl.formManager) {
                if (actionValue) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                    $rootScope.$emit('valueChange_' + ctrl.fieldName);
                }
            }
        }

        function destroy() {
            ctrl.deregisterFuncs.forEach(function(deregisterFunc) {
                deregisterFunc();
            });
        }

        init();
    }

    angular.module('components.input').directive(directiveName, directive);
})();
