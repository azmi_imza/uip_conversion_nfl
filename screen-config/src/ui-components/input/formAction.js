(function() {
    'use strict';

    var directiveName = 'formAction';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formModel: '=',
                formSectionMode: '=',
                fieldItems: '=',
                fieldAlignment: '=',
                formActions: '=',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                actionItems: '=?',
                fieldActionType: '=?',
                fieldId: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                labelCssClass: '=?',
                inputCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                columnCssClass: '=?',
                formManager: '=?',
                section: '=?',
                rowGroupIndex: '=?',
                rowGroupCount: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: formActionCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function formActionCtrl($log, $location, $http, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.triggerAction = triggerAction;

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            $log.debug('initBase:', ctrl.fieldName);

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            // TODO: add base initialization
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);

                // TODO: add view mode initialization
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);

                // TODO: add edit mode initialization
            }
        }

        init();

        var actionList = {};

        for (var i = 0; i < ctrl.formActions.length; i++) {
            actionList[ctrl.formActions[i].type] = ctrl.formActions[i];
        }

        function postData(action) {
            $http({
                method: 'POST',
                data: ctrl.formModel,
                url: action.apiUrl
            }).success(function(response) {
                $log.debug('submit success');
                $log.debug(response);

                // TODO: handle action response

            }).error(function(response) {
                $log.debug('dynamic form submit fail');
                $log.error(response);
            });
        }

        function triggerAction(action) {

            if (action.name === 'addPatient') {
                /*****************************************************************
                  revisit this. temporary coded for claim module add patient
                ******************************************************************/
                var addRows = 1;

                for (var count = 0; count < addRows; count++) {
                    var d = new Date().getTime() + count;
                    var rowGroup = angular.copy(ctrl.section.body.loopData.content);

                    for (var rIdx = 0; rIdx < rowGroup.rows.length; rIdx++) {
                        for (var cIdx = 0; cIdx < rowGroup.rows[rIdx].columns.length; cIdx++) {
                            rowGroup.rows[rIdx].columns[cIdx].id += '-' + d;
                            rowGroup.rows[rIdx].columns[cIdx].name += '-' + d;
                        }
                    }

                    ctrl.section.body.rowGroups.splice(ctrl.section.body.rowGroups.length - 1, 0, rowGroup);
                }

                ctrl.formManager.setValue('claim_details_group', ctrl.section.body.rowGroups);
                /****************************************************************/

            } else if (action.name === 'deletePatient') {
                var rGroup = ctrl.section.body.rowGroups[ctrl.rowGroupIndex];
                for (var row = 0; row < rGroup.rows.length; row++) {
                    for (var col = 0; col < rGroup.rows[row].columns.length; col++) {
                        ctrl.formManager.removeComponent(rGroup.rows[row].columns[col].name);
                    }
                }

                ctrl.section.body.rowGroups.splice(ctrl.rowGroupIndex, 1);
                ctrl.formManager.setValue('claim_details_group', ctrl.section.body.rowGroups);

            } else {
                /*****************************************************************
                  revisit this. temporary coded for claim module add patient
                ******************************************************************/
                if (ctrl.formManager.getValue('claim_details_group')) {
                    var claimDetailsGroup = ctrl.formManager.getValue('claim_details_group');
                    var claimDetails = [];
                    for (var groupIdx = 0; groupIdx < claimDetailsGroup.length - 1; groupIdx++) {
                        var detailsObj = {};
                        for (var rowIdx = 0; rowIdx < claimDetailsGroup[groupIdx].rows.length; rowIdx++) {
                            for (var colIdx = 0; colIdx < claimDetailsGroup[groupIdx].rows[rowIdx].columns.length; colIdx++) {
                                if (claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].type !== 'formAction') {
                                    var key = claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name.split('-')[0];
                                    detailsObj[key] = ctrl.formManager.getValue(claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name);
                                    detailsObj[key + '_label'] = ctrl.formManager.getValue(claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name + '_label');
                                }
                            }
                        }

                        claimDetails.push(detailsObj);
                    }

                    ctrl.formManager.setValue('out_patient_details', claimDetails);
                }
                /****************************************************************/

                if (!action.apiUrl) {
                    action.apiUrl = actionList[action.name].apiPath;
                }

                var url = action.apiUrl;
                var regexPattern = /\{(\w+)\}/g;
                var matchGroup = '';

                while ((matchGroup = regexPattern.exec(url)) != null) {
                    var fieldName = matchGroup[1];
                    var fieldValue = ctrl.formManager.getValue(fieldName);
                    url = url.replace('{' + fieldName + '}', fieldValue);
                }

                action.apiUrl = url;

                var validationSuccess = function validationSuccess() {
                    postData(action);
                };

                var validationFailed = function validationFailed(reason) {
                    $log.debug('form validation failed: ' + reason);
                    //postData(action);
                };

                ctrl.formManager.checkFormValidity().then(validationSuccess, validationFailed);
            }
        }

        ctrl.showAction = function(action) {
            if (action.name !== 'deletePatient') {
                return true;
            } else {
                if (ctrl.rowGroupCount > 2) {
                    return true;
                }
            }

            return false;
        };
    }

    angular.module('components.input').directive(directiveName, directive);
})();
