(function() {
    'use strict';

    var directiveName = 'multiLineText';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                label: '=',
                fieldName: '=',
                formManager: '=?',
                formSectionMode: '=',
                labelCssClass: '=?',
                inputCssClass: '=?',
                labelStyle: '=?',
                inputStyle: '=?',
                fieldId: '=?',
                formSectionName: '=?',
                validators: '=?',
                fieldClass: '=?',
                fieldStyle: '=?',
                fieldDefaultValue: '=?',
                fieldDefaultDisable: '=?',
                fieldDefaultVisible: '=?',
                fieldDependencies: '=?',
                fieldRows: '=?',
                columnCssClass: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: multiLineTextCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function multiLineTextCtrl($element, $compile, $rootScope, $timeout, $log, $q, $http, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.labelCssClass = ctrl.labelCssClass || '';
        ctrl.inputCssClass = ctrl.inputCssClass || '';
        ctrl.formSectionMode = ctrl.formSectionMode || 'edit';
        ctrl.isValid = true;
        ctrl.deregisterFuncs = [];
        ctrl.maxCharLength = 1000;
        ctrl.remainingCharLength = 1000;
        ctrl.fieldStyle = ctrl.fieldStyle || '';
        ctrl.style = 'width:100%; resize:none;' + ctrl.fieldStyle;
        ctrl.cssClass = 'form-control ' + ctrl.fieldClass;
        var isDisabled = (ctrl.fieldDefaultDisable !== undefined) ? ctrl.fieldDefaultDisable : false;
        var isVisible = (ctrl.fieldDefaultVisible !== undefined) ? ctrl.fieldDefaultVisible : true;

        function init() {
            initBase();
            initViewMode();
            initEditMode();
        }

        function initBase() {
            $log.debug('initBase:', ctrl.fieldName);

            if (ctrl.fieldStyle) {
                var fieldStyle = ctrl.fieldStyle.split(';');
                ctrl.fieldStyleObj = {};
                _.forEach(fieldStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.fieldStyleObj[pair[0].trim()] = pair[1];
                    }
                });

            }

            if (ctrl.labelStyle) {
                var labelStyle = ctrl.labelStyle.split(';');
                ctrl.labelStyleObj = {};
                _.forEach(labelStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.labelStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            if (ctrl.inputStyle) {
                var inputStyle = ctrl.inputStyle.split(';');
                ctrl.inputStyleObj = {};
                _.forEach(inputStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        ctrl.inputStyleObj[pair[0].trim()] = pair[1];
                    }
                });
            }

            // TODO: add base initialization
        }

        function initViewMode() {
            if (ctrl.formSectionMode === 'view') {
                $log.debug('initViewMode:', ctrl.fieldName);

                /*
                    to get the array returned by server and display in point form
                */
                if (_.isArray(ctrl.formManager.getValue(ctrl.fieldName))) {
                    var list = '<ul>';

                    _.forEach(ctrl.formManager.getValue(ctrl.fieldName), function(item) {
                        list += '<li>' + item + '</li>';
                    });

                    list += '</ul>';
                    ctrl.formManager.setValue(ctrl.fieldName, list);
                }

                // TODO: add view mode initialization
            }
        }

        function initEditMode() {
            if (ctrl.formSectionMode === 'edit') {
                $log.debug('initEditMode:', ctrl.fieldName);

                // TODO: add edit mode initialization
            }
        }

        init();

        if (!ctrl.fieldName) {
            $log.error('Field name is undefined');
        }

        if (ctrl.formManager) {
            ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
        }

        if (!ctrl.formManager.getValue(ctrl.fieldName) && ctrl.fieldDefaultValue) {
            ctrl.formManager.setValue(ctrl.fieldName, ctrl.fieldDefaultValue);
        }

        var fieldRows = parseInt(ctrl.fieldRows);
        if (isNaN(fieldRows)) {
            ctrl.fieldRows = '3';
        }

        if (ctrl.validators && ctrl.validators.maxLength) {
            var newMaxLength = parseInt(ctrl.validators.maxLength.value);
            if (!isNaN(newMaxLength)) {
                ctrl.maxCharLength = newMaxLength;
                ctrl.remainingCharLength = newMaxLength;
            }
        }

        if (ctrl.formManager.getValue(ctrl.fieldName)) {
            ctrl.remainingCharLength -= ctrl.formManager.getValue(ctrl.fieldName).length;
        }

        ctrl.value = function(newValue) {
            return arguments.length ? (ctrl.formManager.setValue(ctrl.fieldName, newValue)) :
                ctrl.formManager.getValue(ctrl.fieldName);
        };

        if (ctrl.validators && ctrl.formSectionMode !== 'view') {
            $timeout(function() {
                initValidation();
            });
        }

        function watchValue(eventName, dependency) {
            var deregisterFunc = $rootScope.$on('valueChange_' + dependency.watch, function() {
                applyDependency(dependency);
            });

            if (!ctrl.deregisterFuncs) {
                ctrl.deregisterFuncs = [];
            }

            ctrl.deregisterFuncs.push(deregisterFunc);
        }

        if (ctrl.fieldDependencies) {
            for (var i = 0; i < ctrl.fieldDependencies.length; i++) {
                var dep = ctrl.fieldDependencies[i];

                //if (ctrl.formManager.getValue(dep.watch)) {

                    applyDependency(dep);
                //}

                watchValue('valueChange_' + dep.watch, dep);
            }
        }

        ctrl.visibility = function() {
            return isVisible;
        };

        ctrl.disability = function() {
            return ctrl.formSectionMode === 'preview' || isDisabled;
        };

        function applyDependency(dependency) {
            var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];

            if (listAction) {
                for (var j = 0; j < listAction.length; j++) {
                    var action = listAction[j];
                    if (action.actionType === 'disable') {
                        isDisabled = action.value;
                        reRunValidate(!action.value);
                    } else if (action.actionType === 'visible') {
                        isVisible = action.value;
                        if (ctrl.validators && ctrl.formSectionMode !== 'view') {

                            initValidation();

                        }

                        reRunValidate(action.value);
                    } else if (action.actionType === 'reset') {
                        reset();
                    }
                }
            }

        }

        function initValidation() {
            $timeout(function() {
                var inputElem = $element.find('textarea');

                inputElem.bind('keyup', function() {
                    //not using model binding due to delay from digestion
                    $('#' + ctrl.fieldName + '_remaining span').html(ctrl.maxCharLength -
                        $(inputElem).val().length);
                });
            });
        }

        function reRunValidate(isRecognised) {
            setFieldValid();
            if (ctrl.formManager) {
                if (isRecognised) {
                    ctrl.formManager.addComponent(ctrl.fieldName, reset, validate);
                } else {
                    ctrl.formManager.removeComponent(ctrl.fieldName);
                }
            }
        }

        function reset() {
            var resetValue = ctrl.defaultValue || '';
            ctrl.formManager.setValue(ctrl.fieldName, resetValue);
            setFieldValid();
        }

        function validate() {
            if (!ctrl.validators || ctrl.formSectionMode !== 'edit' || isDisabled) {
                setFieldValid();
                return getValidateObj();
            }

            var value = ctrl.value();
            var validationFailedReason = 'Validation failed: ' + ctrl.fieldName;

            if (ctrl.validators.isRequired) {
                if (!value) {
                    setFieldInvalid(ctrl.validators.isRequired.message);
                    return getValidateObj();
                }
            }

            if (!validateMinLength(ctrl.validators, value)) {
                setFieldInvalid(ctrl.validators.minLength.message + ctrl.validators.minLength.value);
                return getValidateObj();
            }

            if (!validateMaxLength(ctrl.validators, value)) {
                setFieldInvalid(ctrl.validators.maxLength.message);
                return getValidateObj();
            }

            if (ctrl.validators.pattern) {
                var pattern = new RegExp(ctrl.validators.pattern.value);
                if (!pattern.test(value)) {
                    setFieldInvalid(ctrl.validators.pattern.message);
                    return getValidateObj();
                }
            }

            if (ctrl.validators.groupValidation) {
                if (ctrl.validators.groupValidation.type === 'isRequired') {
                    if (!ctrl.formManager.getValue(ctrl.fieldName)) {
                        var valid = true;
                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            var pattern = member.pattern ? member.pattern : '^.+$';
                            var regex = new RegExp(pattern);
                            var memberValue = ctrl.formManager.getValue(member.name) ? ctrl.formManager.getValue(member.name) : '';

                            if (regex.test(memberValue)) {
                                setFieldInvalid(ctrl.validators.groupValidation.message);
                                valid = false;
                                return;
                            }
                        });

                        if (!valid) {
                            return getValidateObj();
                        }

                        angular.forEach(ctrl.validators.groupValidation.members, function(member) {
                            $rootScope.$emit('setValid_' + member.name);
                        });
                    }
                }
            }

            // last validation
            if (ctrl.validators.ajax) {
                var deferred = $q.defer();

                $http({
                    method: 'GET',
                    url: ctrl.validators.ajax.url
                }).success(function() {
                    // if validation failed, reject promise
                    deferred.resolve(true);
                }).error(function() {
                    // if validation failed, reject promise
                    deferred.reject(validationFailedReason);
                });

                return deferred.promise;
            }

            setFieldValid();
            return getValidateObj();
        }

        function getValidateObj() {
            if (isVisible) {
                return {
                    isValid: ctrl.isValid,
                    message: ctrl.errorMessage
                    //label: ctrl.label.name
                };
            } else {
                return {
                    isValid: true
                };
            }
        }

        function validateMinLength(validators, value) {
            var minLength = validators.minLength;
            if (minLength !== undefined) {
                minLength = parseInt(minLength.value);
                if (isNaN(minLength)) {
                    $log.error('invalid min-length syntax: ' + minLength);
                } else {
                    if (value.length < minLength) {
                        return false;
                    }
                }
            }

            return true;
        }

        function validateMaxLength(validators, value) {
            var maxLength = validators.maxLength;
            if (maxLength !== undefined) {
                maxLength = parseInt(maxLength.value);
                if (isNaN(maxLength)) {
                    $log.error('invalid max-length syntax: ' + maxLength);
                } else {
                    if (value.length > maxLength) {
                        return false;
                    }
                }
            }

            return true;
        }

        function setFieldValid() {
            ctrl.isValid = true;
            ctrl.errorMessage = '';
        }

        function setFieldInvalid(message) {
            ctrl.isValid = false;
            ctrl.errorMessage = message;
        }

    }

    angular.module('components.input').directive(directiveName, directive);
})();
