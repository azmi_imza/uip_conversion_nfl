(function() {
    'use strict';

    var directiveName = 'labelTooltip';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function labelTooltip() {
        return {
            scope: {
                tooltipConfig: '=',
                fieldName: '=',
                showTooltip: '=?',
                icon: '=?'
            },
            bindToController: true,
            template: '<span ng-include="ctrl.templateName"></span>',
            controllerAs: 'ctrl',
            controller: labelTooltipCtrl
        };
    }

    /*@ngInject*/
    function labelTooltipCtrl($log, $timeout) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.showTooltip = ctrl.showTooltip || true;
        ctrl.icon = ctrl.icon || 'ui-components/images/ico-help.gif';

        function init() {
            $timeout(function() {
                if (ctrl.tooltipConfig) {
                    $('.label-tooltip.' + ctrl.fieldName + '[data-toggle="tooltip"]').tooltip();
                }
            });
        }

        init();
    }

    angular.module('components.input').directive(directiveName, labelTooltip);
})();
