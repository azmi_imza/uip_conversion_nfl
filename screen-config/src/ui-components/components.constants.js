(function() {
    'use strict';
    angular.module('hfits.components')
        .constant('COMP_CONST', {
            INPUT_TEXT: 'input-text',
            INPUT_TEXTAREA: 'input-textarea',
            INPUT_DATE: 'input-date',
            INPUT_NUMBER: 'input-number',
            INPUT_FILEUPLOAD: '',
            TABLE: 'servreqaction',
            PDFJS_SRC_PATH: 'js/pdf.worker.js',
            API_PROTOCOL: 'https',
            API_HOST: 'uip-sit-ext.ap.lifeisgreat.net:4445',
            API_BASE_PATH: '/uipcfo/rest/'
        })
        .constant('APP_CONST', {
            STORAGE_KEYS: {
                AUTH_TOKEN: 'authtoken'
            }
        });
})();
