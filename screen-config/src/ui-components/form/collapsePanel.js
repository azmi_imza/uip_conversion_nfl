(function() {
    'use strict';

    var directiveName = 'collapsePanel';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function collapsePanel() {
        return {
            scope: {
                collapsePanelLayout: '=',
                formSectionMode: '=',
                formName: '=',
                formModel: '=',
                formManager: '=',
                formActions: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: collapsePanelCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function collapsePanelCtrl() {
        var ctrl = this;
        ctrl.templateName = templateName;
    }

    angular.module('components.form').directive(directiveName, collapsePanel);
})();
