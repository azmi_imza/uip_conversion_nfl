(function() {
    'use strict';

    var directiveName = 'dynamicForm';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                formLayout: '=',
                formData: '=?',
                formDataSource: '=?',
                formMode: '=?',
                actions: '=?',
                formManager: '=?',
                formModel: '=?'
            },
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: dynamicFormCtrl
        };
    }

    /*@ngInject*/
    function dynamicFormCtrl($q, $rootScope, $timeout, $resource, $log, $http, FormManager, COMP_CONST) {
        $log.debug('dynamicFormCtrl');

        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.hasInitialized = false;
        ctrl.formModel = ctrl.formModel || {};
        ctrl.formMode = ctrl.formMode || 'edit';

        $log.debug('dynamicFormCtrl, formDataSource:', ctrl.formDataSource);
        (function init() {

            ctrl.formErrors = {
                client: {},
                server: undefined
            };

            if (!ctrl.formManager) {
                ctrl.formManager = new FormManager(ctrl.formLayout.name, ctrl.formLayout.mode, ctrl);
            }

            // if (!ctrl.formLayout.actions | ctrl.formLayout.actions.length === 0) {
            //     $log.error('no form actions defined');
            // }

            // ctrl.formModel = {};

            /*Remove this after testing*/
            /*test data for claimdetails*/
            /*if (!ctrl.formModel['out_patient_details']) {
                ctrl.formModel['out_patient_details'] = [{}];
            }*/
            /*******************************/

            if (ctrl.formData) {
                ctrl.formModel = ctrl.formData;
                ctrl.hasInitialized = true;
                return;
            }

            initializeFormModel().then(function(formData) {
                    ctrl.formManager.mapFields(ctrl.formLayout.srcMappings, formData);
                    $log.debug('>>>>>>>>>>>>>dynamicFormCtrl: set form data:', formData);
                    angular.forEach(formData, function(value, key) {
                        ctrl.formManager.setValue(key, value);
                    });

                    ctrl.hasInitialized = true;
                    $rootScope.$emit('initFormModel_' + ctrl.formLayout.name);
                },

                function(reason) {
                    $log.error('failed to initalize form model');
                    $log.error(reason);
                });

            function initializeFormModel() {
                var deferred = $q.defer();
                $log.debug('dynamicFormCtrl:ctrl.formDataSource>>>>>>>>>>>>>>>>>>>>>');
                $log.debug(ctrl.formDataSource);
                if (ctrl.formDataSource) {
                    var url = COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + COMP_CONST.API_BASE_PATH + ctrl.formDataSource;
                    $http({
                        method: 'GET',
                        url: url
                    }).success(function(response) {
                        // if validation failed, reject promise
                        $log.debug('>>>>>>>>>>>>>>>>retrieve form data success');
                        $log.debug(response);

                        ctrl.formData = ctrl.formData || {};
                        angular.extend(ctrl.formData, response.data);
                        deferred.resolve(ctrl.formData);
                    }).error(function(response) {
                        // if validation failed, reject promise
                        deferred.reject(response);
                    });
                } else {
                    if (ctrl.formData) {
                        deferred.resolve(ctrl.formData);
                    } else {
                        deferred.resolve({});
                    }
                }

                return deferred.promise;
            }

        })();
    }

    angular.module('components.form').directive(directiveName, directive);
})();
