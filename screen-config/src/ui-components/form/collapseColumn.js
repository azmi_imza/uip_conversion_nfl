(function() {
    'use strict';

    var directiveName = 'collapseColumn';
    var templateName = 'formColumn.html';

    /*@ngInject*/
    function collapseColumn() {
        return {
            scope: {
                columnLayout: '=',
                columnsCount: '=',
                formModel: '=',
                formName: '=',
                formSectionMode: '=',
                formManager: '=',
                formActions: '='
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controllerAs: 'ctrl',
            controller: collapseColumnCtrl
        };
    }

    /*@ngInject*/
    function collapseColumnCtrl() {
        var ctrl = this;
        ctrl.templateName = templateName;

        ctrl.cssClass = ctrl.inputCssClass = ctrl.labelCssClass = 'col-xs-12';
        var columnsCount = ctrl.columnsCount;
        if (columnsCount === 1) {
            ctrl.cssClass = 'col-xs-12';
            ctrl.labelCssClass = 'col-xs-12 col-sm-4 col-md-2';
            ctrl.inputCssClass = 'col-xs-12 col-sm-7 col-md-6';
        } else if (columnsCount === 2) {
            ctrl.cssClass = 'col-xs-12 col-md-6';
            ctrl.labelCssClass = 'col-xs-12 col-sm-4';
            ctrl.inputCssClass = 'col-xs-12 col-sm-8';
        } else if (columnsCount > 2) {
            ctrl.cssClass = 'col-xs-12 col-sm-6 col-md-4';
        }
    }

    angular.module('components.form').directive(directiveName, collapseColumn);
})();
