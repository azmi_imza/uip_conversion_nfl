(function() {
    'use strict';

    var directiveName = 'formColumn';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function formColumn() {
        return {
            scope: {
                columnLayout: '=',
                columnsCount: '=',
                formModel: '=',
                formName: '=',
                formSectionMode: '=',
                formManager: '=',
                formActions: '=',
                section: '=?',
                rowGroupIndex: '=?',
                rowGroupCount: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formColumnCtrl
        };
    }

    /*@ngInject*/
    function formColumnCtrl() {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.cssClass = ctrl.columnLayout.cssClass;
        ctrl.modelName = 'ctrl.formModel.' + ctrl.columnLayout.name;
        var columnsCount = ctrl.columnsCount;

        if (!ctrl.columnLayout.cssClass) {
            if (columnsCount === 1) {
                ctrl.cssClass = 'col-xs-12';
            } else if (columnsCount === 2) {
                ctrl.cssClass = 'col-xs-12 col-md-6';
            } else if (columnsCount === 3) {
                ctrl.cssClass = 'col-xs-12 col-sm-6 col-md-4';
            } else if (columnsCount > 3) {
                ctrl.cssClass = 'col-xs-12 col-sm-6 col-md-4 col-lg-3';
            } else {
                ctrl.cssClass = 'col-xs-12';
            }
        }
    }

    angular.module('components.form').directive(directiveName, formColumn);
})();
