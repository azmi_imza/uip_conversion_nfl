(function() {
    'use strict';
    var directiveName = 'formFooter';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                footerLayout: '=',
                formMode: '=',
                formManager: '=',
                formErrors: '=',
                formModel: '=',
                formActions: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: formFooterCtrl,
            template: '<div ng-include="ctrl.templateName"></div>'
        };
    }

    /*@ngInject*/
    function formFooterCtrl($log, $http, $state, $location, storageService, routeService, APP_CONST) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.submitForm = submitForm;
        ctrl.resetForm = resetForm;
        ctrl.triggerAction = triggerAction;

        if (!ctrl.formActions || ctrl.formActions.length === 0) {
            $log.error('no actions defined!');
        }

        function postData(action) {
            // var apiURL = 'http://10.128.1.181/uipcfo/rest/screenflow/execute';
            var data = {
                'action_code': action.code
                    // 'screen_code': storageService.get(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE)
            };

            angular.extend(data, ctrl.formModel);
            $log.debug('postData');
            $log.debug(data);
            $http({
                method: 'POST',
                url: action.apiUrl,
                data: data
            }).success(function(response) {
                $log.debug('form action success:', response);

                // var screenCode = response.data['additional_info']['current_screen_code'];
                // storageService.set(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE, screenCode);
                // storageService.set(APP_CONST.STORAGE_KEYS.DYNAMIC_LAYOUT, response.data);

                // storageService.set(APP_CONST.STORAGE_KEYS.DYNAMIC_LAYOUT_STATE_DATA, ctrl.formModel);
                routeService.reloadState();
            }).error(function(response) {
                $log.error(response);

                ctrl.formErrors = {};
                ctrl.formErrors.title = 'Your submission is fail';
                ctrl.formErrors.isSuccessAck = false;
                ctrl.formErrors.isVisibleAck = true;
            });
        }

        function triggerAction(action) {
            $log.debug('trigger action: ' + action.label);
            $log.debug('action api path: ' + action.apiPath);

            // ctrl.formManager.checkFormValidity().then(function() {
                    postData(action);
                // },

                // function(reason) {
                //     $log.debug('FORM INVALID: ' + reason);
                // });
        }

        function submitForm() {
            function submissionSuccess() {

            }

            function submissionFailed(errors) {
                ctrl.formErrors.server = errors;
            }

            ctrl.formManager.submit(ctrl.formModel).then(submissionSuccess, submissionFailed);
        }

        function resetForm() {
            ctrl.formManager.reset();

            ctrl.formErrors = {
                client: {},
                server: undefined
            };
        }
    }

    angular.module('components.form').directive(directiveName, directive);
})();
