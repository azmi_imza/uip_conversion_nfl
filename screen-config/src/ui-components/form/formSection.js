(function() {
    'use strict';

    var directiveName = 'formSection';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function formSection() {
        return {
            scope: {
                section: '=',
                formMode: '=',
                formName: '=',
                formModel: '=',
                formManager: '=',
                formActions: '=',
                showDivider: '=',
                sectionDefaultVisible: '=?',
                formDependencies: '=?'
            },
            bindToController: true,
            template: '<div ng-include="ctrl.templateName"></div>',
            controllerAs: 'ctrl',
            controller: formSectionCtrl
        };
    }

    /*@ngInject*/
    function formSectionCtrl($log, $rootScope, _) {
        var ctrl = this;
        ctrl.templateName = templateName;
        ctrl.toggleMode = toggleMode;
        ctrl.formSectionMode = ctrl.section.mode || ctrl.formMode;
        ctrl.defaultVisible = (ctrl.sectionDefaultVisible !== undefined) ? ctrl.sectionDefaultVisible : true;
        ctrl.visible = ctrl.defaultVisible;
        ctrl.switchMode = switchMode;

        if (ctrl.section.header) {
            ctrl.section.header.color = ctrl.section.header.color || '#f01616';
        }

        if (ctrl.section.style) {
            var sectionStyle = ctrl.section.style.split(';');
            ctrl.section.styleObj = {};
            _.forEach(sectionStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.section.styleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        if (ctrl.section.panelStyle) {
            var panelStyle = ctrl.section.panelStyle.split(';');
            ctrl.section.panelStyleObj = {};
            _.forEach(panelStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.section.panelStyleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        if (ctrl.section.header && ctrl.section.header.style) {
            var headerStyle = ctrl.section.header.style.split(';');
            ctrl.section.header.styleObj = {};
            _.forEach(headerStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.section.header.styleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        if (ctrl.section.body && ctrl.section.body.style) {
            var bodyStyle = ctrl.section.body.style.split(';');
            ctrl.section.body.styleObj = {};
            _.forEach(bodyStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.section.body.styleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        if (ctrl.section.body && ctrl.section.body.panelStyle) {
            var bodyPanelStyle = ctrl.section.body.panelStyle.split(';');
            ctrl.section.body.panelStyleObj = {};
            _.forEach(bodyPanelStyle, function(style) {
                var pair = style.split(':');
                if (pair.length === 2) {
                    ctrl.section.body.panelStyleObj[pair[0].trim()] = pair[1];
                }
            });

        }

        ctrl.constructRowStyle = function(row) {
            if (row.style) {
                var rowStyle = row.style.split(';');
                row.style = {};
                _.forEach(rowStyle, function(style) {
                    var pair = style.split(':');
                    if (pair.length === 2) {
                        row.style[pair[0]] = pair[1];
                    }
                });

            }
        };

        function toggleMode(mode) {
            $log.debug('toggle mode');
            ctrl.formSectionMode = mode;
            $log.debug('formModeChange:' + ctrl.section.name);
            $rootScope.$emit('formModeChange:' + ctrl.section.name, mode);
            return;
        }

        function switchMode(mode) {
            var inputs = [];

            ctrl.section.body.rows.forEach(function(row) {
                row.columns.forEach(function(column) {
                    inputs.push(column.name);
                });
            });

            ctrl.formManager.switchMode(mode, inputs);
        }

        function watchValue(eventName, dependency) {
            $rootScope.$on(eventName, function() {
                applyDependency(dependency);
            });
        }

        if (ctrl.formDependencies) {
            for (var i = 0; i < ctrl.formDependencies.length; i++) {
                var dep = ctrl.formDependencies[i];
                applyDependency(dep);

                watchValue('valueChange_' + dep.watch, dep);
            }
        }

        function applyDependency(dependency) {
            $log.debug('apply dependency');
            var regexList = Object.keys(dependency.actionList);

            if (!regexList || regexList.length === 0) {
                $log.error('no matched regex defined');
                return;
            }

            var dependencyValue = ctrl.formManager.getValue(dependency.watch);
            var matchedRegex = null;
            regexList.some(function(regexText) {
                var regex = new RegExp(regexText);

                if (regex.test(dependencyValue)) {
                    matchedRegex = regexText;
                    return true;
                }

                return false;
            });

            $log.debug(matchedRegex);

            if (!matchedRegex) {
                $log.debug('no matched regex');
                $log.debug(ctrl.defaultVisible);
                ctrl.visible = ctrl.defaultVisible;
                return;
            }

            var matchedActions = dependency.actionList[matchedRegex];

            if (!matchedActions || matchedActions.length === 0) {
                $log.error('no action defined for matched regex');
                return;
            }

            matchedActions.forEach(function(action) {
                if (action.actionType === 'visible') {
                    ctrl[action.actionType] = action.value;

                    if (!action.value) {
                        var rowCount = ctrl.section.body.rowGroups[0].rows.length;
                        var rows = ctrl.section.body.rowGroups[0].rows;
                        for (var k = 0; k < rowCount; k++) {
                            var columnCount = rows[k].columns.length;
                            var columns = rows[k].columns;

                            for (var m = 0; m < columnCount; m++) {
                                ctrl.formManager.removeComponent(columns[m].name);
                                if (columns[m].type === 'datePicker') {
                                    $rootScope.$emit('valueChange_' + columns[m].name);
                                }
                            }
                        }
                    }
                }
            });

            // var listAction = dependency.actionList[ctrl.formManager.getValue(dependency.watch)];
            // if (!listAction) {
            //     return;
            // }

            // for (var j = 0; j < listAction.length; j++) {
            //     var action = listAction[j];
            //     ctrl[action.actionType] = action.value;

            //     if (action.actionType !== 'visible') {
            //         continue;
            //     }

            //     if (action.value) {
            //         continue;
            //     }

            //     var rowCount = ctrl.section.body.rows.length;
            //     var rows = ctrl.section.body.rows;
            //     for (var k = 0; k < rowCount; k++) {
            //         var columnCount = rows[k].columns.length;
            //         var columns = rows[k].columns;

            //         for (var m = 0; m < columnCount; m++) {
            //             ctrl.formManager.removeComponent(columns[m].name);
            //         }
            //     }
            // }
        }

        /*****************************************************************
              revisit this. temporary coded for claim module add patient
          ******************************************************************/
        if (ctrl.section.body.loopData && ctrl.formManager.getValue(ctrl.section.body.loopData.name)) {
            var loopDataRef = ctrl.formManager.getValue(ctrl.section.body.loopData.name);
            for (var count = 0; count < loopDataRef.length; count++) {
                var d = new Date().getTime() + count;
                var rowGroup = angular.copy(ctrl.section.body.loopData.content);

                for (var rowIdx = 0; rowIdx < rowGroup.rows.length; rowIdx++) {
                    for (var colIdx = 0; colIdx < rowGroup.rows[rowIdx].columns.length; colIdx++) {
                        var colName = rowGroup.rows[rowIdx].columns[colIdx].name;
                        ctrl.formManager.setValue(colName + '-' + d, loopDataRef[count][colName]);
                        ctrl.formManager.setValue(colName + '-' + d + '_label', loopDataRef[count][colName + '_label']);
                        rowGroup.rows[rowIdx].columns[colIdx].id += '-' + d;
                        rowGroup.rows[rowIdx].columns[colIdx].name += '-' + d;
                    }
                }

                if (ctrl.section.mode === 'edit') {
                    ctrl.section.body.rowGroups.splice(ctrl.section.body.rowGroups.length - 1, 0, rowGroup);
                } else {
                    ctrl.section.body.rowGroups.push(rowGroup);
                }
            }

            ctrl.formManager.setValue('claim_details_group', ctrl.section.body.rowGroups);
        }

        /******************************************************************/
    }

    angular.module('components.form').directive(directiveName, formSection);
})();
