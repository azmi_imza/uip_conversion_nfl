(function() {
    'use strict';

    var factoryName = 'FormManager';

    /*@ngInject*/
    function FormManager($q, $timeout, $log) {
        return function(formName, formMode, controller) {
            // var _formName = formName;
            // var _formMode = formMode;
            var _formInputs = {};
            var _ctrl = controller;
            var manager = this;

            this.mapFields = function(fieldsMap, data) {
                if (!fieldsMap || fieldsMap.length === 0) {
                    $log.debug('fieldsMap is undefined, skip mapping');
                    return data;
                }

                angular.forEach(fieldsMap, function(item) {
                    if (data[item.from]) {
                        data[item.to] = data[item.from];
                    }
                });
            };

            this.checkFormValidity = function checkFormValidity() {
                var promises = [];
                manager.errors = [];
                angular.forEach(_formInputs, function(formInput, formInputName) {
                    if (!formInput.validate) {
                        return;
                    }

                    $log.debug('run validation: ' + formInputName);

                    var promise;
                    var validationResult = formInput.validate();

                    if (validationResult.isValid === undefined) {
                        $log.error('Undefined validation result: ' + formInputName);
                        return;
                    }

                    if (!validationResult.isValid) {
                        manager.errors.push(validationResult);
                    }

                    if (!validationResult.then) {

                        var p = $q.defer();

                        if (validationResult.isValid) {
                            p.resolve(validationResult.isValid);
                        } else {

                            p.reject({
                                message: 'Validation failed: ' + formInputName,
                                inputName: formInputName
                            });

                        }

                        promise = p.promise;
                    } else {
                        promise = validationResult.isValid;
                    }

                    promises.push(promise);
                });

                return $q.all(promises);
            };

            function removeValue(name) {
                if (name === undefined) {
                    $log.error('removeValue: name is undefined');
                    return;
                }

                var names = name.split('.');
                var lastName = names.pop();

                var obj = _ctrl.formModel;
                names.forEach(function(n) {
                    obj = obj[n];
                });

                delete obj[lastName];
            }

            this.getValue = function(name) {
                if (name === undefined) {
                    $log.error('getValue: name is undefined');
                    return null;
                }

                var names = name.split('.');
                var obj = _ctrl.formModel;
                names.forEach(function(n) {
                    obj = obj[n];
                });

                return obj;
            };

            this.setValue = function(name, value) {
                if (name === undefined) {
                    $log.error('formManager.setValue: name is undefined');
                    return;
                }

                if (value === undefined) {
                    $log.error('value is undefined');
                    return;
                }

                var names = name.split('.');
                var lastName = names.pop();

                var obj = _ctrl.formModel;
                names.forEach(function(n) {
                    obj = obj[n];
                });

                obj[lastName] = value;
            };

            this.getComponents = function() {
                return _formInputs;
            };

            this.getComponent = function(inputName) {
                return _formInputs[inputName];
            };

            this.addComponent = function(inputName, resetFunc, validateFunc, formatFunc, destroyFunc) {
                _formInputs[inputName] = {
                    validate: validateFunc,
                    reset: resetFunc,
                    format: formatFunc,
                    destroy: destroyFunc
                };
            };

            this.removeComponent = function(inputName) {
                delete _formInputs[inputName];
                removeValue(inputName);
                removeValue(inputName + '_label');
            };

            this.reset = function() {
                $log.debug('RESET FORM');
                angular.forEach(_formInputs, function(input) {
                    if (input.reset) {
                        input.reset();
                    }
                });
            };

            this.submit = function(formModel) {
                $log.debug('SUBMIT FORM');
                $log.debug(_ctrl.formModel);

                var deferred = $q.defer();

                function validationSuccess() {
                    $log.debug('FORM VALID');
                    $log.debug(angular.toJson(formModel));

                    // submit form to server
                    // run post submit functions
                    deferred.resolve(true);
                }

                function validationFailed(reason) {
                    $log.debug('FORM INVALID: ' + reason.message);
                    var errors = {
                        inputName: reason.inputName,
                        errors: [{
                            message: '1234'
                        }, {
                            message: '5678'
                        }]
                    };
                    deferred.reject(errors);
                }

                // perform client side validation
                manager.checkFormValidity().then(validationSuccess, validationFailed);

                return deferred.promise;
            };

            this.switchMode = function() {

            };

            this.setVisible = function() {

            };
        };
    }

    angular.module('components.form').factory(factoryName, FormManager);
})();
