/* eslint no-loop-func:0 */

(function() {
    'use strict';

    var directiveName = 'tableListing';
    var templateName = directiveName + '.html';

    /*@ngInject*/
    function directive() {
        return {
            scope: {
                tableLayout: '=',
                formManager: '=?',
                selectOnRowFn: '&',
                selectedValue: '=?',
                selectedObject: '=?'
            },
            template: '<div ng-include="ctrl.templateName"></div>',
            bindToController: true,
            controller: tableListingCtrl,
            controllerAs: 'ctrl'
        };
    }

    /*@ngInject*/
    function tableListingCtrl($rootScope, $resource, $log, $timeout, $http, $state, FormManager,
        storageService, routeService, APP_CONST, COMP_CONST, _) {

        var ctrl = this;
        ctrl.templateName = templateName;

        var arrOriTableListing = [];

        var buildUrl = function(url) {
            if (!url) {
                return '';
            }

            return COMP_CONST.API_PROTOCOL + '://' + COMP_CONST.API_HOST + COMP_CONST.API_BASE_PATH + url;
        };

        ctrl.clicked = function(recordIndex, link) {

            //{key: "claim_num", value: "CEA249DD2D5345E491814AD76012EDDD"}
            ctrl.selectedValue = link;

            ctrl.selectedObject = arrOriTableListing[recordIndex];
            ctrl.selectOnRowFn(link);
        };

        ctrl.formModel = {};
        ctrl.formErrors = {
            client: {},
            server: {}
        };

        ctrl.pageNumberFormat = {
            type: 'currency',
            attributes: {
                currencyFormat: false,
                currencyInteger: 3,
                currencyDecimal: 0
            }
        };

        if (!ctrl.formManager) {
            ctrl.formManager = new FormManager(ctrl.tableLayout.name, ctrl.tableLayout.mode, ctrl);
        }

        ctrl.formModel.size = ctrl.tableLayout.pageSize ? ctrl.tableLayout.pageSize : '';

        ctrl[ctrl.tableLayout.name + '_show'] = ctrl.tableLayout.retrieveOnLoad;
        ctrl[ctrl.tableLayout.name + '_pageCount'] = 1;
        ctrl[ctrl.tableLayout.name + '_formModel'] = angular.copy(ctrl.formModel);
        ctrl.tableClass = 'table ' + ctrl.tableLayout.name;
        ctrl.selectRow = selectRow;
        ctrl.executeAction = executeAction;

        ctrl.tableFields = {};
        _.forEach(ctrl.tableLayout.fields, function(field) {
            ctrl.tableFields[field.name] = '';
        });

        var renderTable = function(data) {
            ctrl[ctrl.tableLayout.name + '_data'] = data;
            ctrl[ctrl.tableLayout.name + '_show'] = true;

            $timeout(function() {

                $('.' + ctrl.tableLayout.name).footable({
                    calculateWidthOverride: function($table, info) {
                        info.width = info.viewportWidth;
                        return info;
                    }
                });

                $('.' + ctrl.tableLayout.name).trigger('footable_redraw');
            }, 0);

        };

        function executeAction(record, link) {

            $log.debug('executeAction');
            $log.debug(record);
            $log.debug(link);

            //     var apiURL = 'http://52.74.67.29/uipcfo/rest/screenflow/execute';
            //     var data = {
            //         'action_code': 'HYPERLINK',
            //         'screen_code': storageService.get(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE)
            //     };

            //     data[link.key] = link.value;

            //     angular.forEach(record, function(field) {
            //         if (field.value && field.type !== 'pageLink') {
            //             data[field.name] = field.value;
            //         }
            //     });

            //     $http({
            //         method: 'POST',
            //         url: apiURL,
            //         data: data
            //     }).success(function(response) {
            //         $log.debug('response' + apiURL);
            //         $log.debug(response);
            //         var screenCode = response.data['additional_info']['current_screen_code'];
            //         storageService.set(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE, screenCode);
            //         storageService.set(APP_CONST.STORAGE_KEYS.DYNAMIC_LAYOUT, response.data);

            //         var stateData = {};
            //         angular.forEach(record, function(field) {
            //             if (field.value && typeof field.value === 'string') {
            //                 stateData[field.name] = field.value;
            //             }
            //         });

            //         stateData[link.key] = link.value;
            //         storageService.set(APP_CONST.STORAGE_KEYS.DYNAMIC_LAYOUT_STATE_DATA, stateData);
            //         routeService.reloadState();
            //     }).error(function(response) {
            //         $log.error(response);
            //     });
        }

        function selectRow(item, radioValue) {
            if (!item) {
                return;
            }

            $log.debug('selectRow(item, radioValue)');
            $log.debug(item);
            angular.forEach(item, function(value, key) {
                ctrl.formManager.setValue(key, value);
            });

            ctrl.selectedRadioValue = radioValue;
        }

        function transformApiDataToTableFormat(apiData) {
            $log.debug('transformApiDataToTableFormat(apiData)');
            if (!apiData || !apiData.length) {
                return [];
            }

            var tableRecords = [];
            for (var i = 0; i < apiData.length; i++) {
                var record = angular.extend(angular.copy(ctrl.tableFields), apiData[i]);

                var tableRecord = constructTableRecord(record);

                tableRecords.push(tableRecord);
            }

            return tableRecords;
        }

        function constructTableRecord(record) {
            $log.debug('constructTableRecord(record) ');
            var tableRecord = [];
            var nonTableFields = [];

            angular.forEach(record, function(recordFieldValue, recordFieldKey) {
                var tableField = {};
                tableField.value = '';
                tableField.name = recordFieldKey;
                tableField.visible = false;

                var tableFieldIndex;

                var matchedRecordField = _.find(ctrl.tableLayout.fields, function(layoutField, index) {
                    if (layoutField.name === recordFieldKey) {
                        tableFieldIndex = index;
                        tableField.visible = true;
                        return true;
                    }
                });

                if (matchedRecordField) {
                    tableField.type = matchedRecordField.type;
                    tableField.format = matchedRecordField.format;
                    if (matchedRecordField.type === 'pageLink') {
                        tableField.links = [];
                        if (recordFieldValue instanceof Array) {
                            angular.forEach(recordFieldValue, function(link) {
                                var pageLink = {
                                    label: link.label,
                                    value: link.value,
                                    key: recordFieldKey
                                };

                                tableField.links.push(pageLink);
                            });
                        } else {
                            var singlePageLink = {
                                key: recordFieldKey,
                                value: recordFieldValue
                            };
                            tableField.links.push(singlePageLink);
                        }
                    } else {
                        if (recordFieldValue instanceof Array) {
                            // concatenate mutiple value with comma
                            // TODO: refactor this to support table cell which contains multiple values
                            tableField.value = recordFieldValue.join();
                        } else {
                            tableField.value = recordFieldValue;
                        }
                    }

                    if (!tableField.type) {
                        $log.error('type is undefined for table field:', matchedRecordField.name);
                    }

                    tableRecord[tableFieldIndex] = tableField;
                } else {
                    if (recordFieldValue instanceof Array) {
                        // concatenate mutiple value with comma
                        // TODO: refactor this to support table cell which contains multiple values
                        tableField.value = recordFieldValue.join();
                    } else {
                        tableField.value = recordFieldValue;
                    }

                    nonTableFields.push(tableField);
                }

                if (tableField.type === 'currency') {
                    ctrl.fieldClass = 'align-right';
                }
            });

            return tableRecord.concat(nonTableFields);
        }

        var paginationLoadSuccess = function paginationLoadSuccess(response) {
            $log.debug('table data loaded');
            arrOriTableListing = response.data;
            var tableData = transformApiDataToTableFormat(response.data);
            ctrl[ctrl.tableLayout.name + '_pageCount'] = response['page_count'];
            ctrl[ctrl.tableLayout.name + '_toPage'] = response['page_index'];
            ctrl[ctrl.tableLayout.name + '_pageIndex'] = response['page_index'];
            ctrl[ctrl.tableLayout.name + '_pageSize'] = response.total;
            renderTable(tableData);
        };

        var paginationLoadFailed = function paginationLoadFailed() {
            $log.error('failed to load table data');
        };

        ctrl.paginationKeyUp = function($event) {
            if ($event.keyCode === 13) {
                ctrl.toPage();
            }
        };

        ctrl.pageBack = function() {
            if (ctrl[ctrl.tableLayout.name + '_pageIndex'] <= 1) {
                return;
            } else {
                ctrl[ctrl.tableLayout.name + '_show'] = false;
                ctrl[ctrl.tableLayout.name + '_formModel'].page = ctrl[ctrl.tableLayout.name + '_pageIndex'] - 1;

                $http({
                    method: 'GET',
                    url: buildUrl(ctrl.tableLayout.filters.filterSource),
                    params: ctrl[ctrl.tableLayout.name + '_formModel']
                }).success(paginationLoadSuccess).error(paginationLoadFailed);
            }
        };

        ctrl.toPage = function() {
            if (ctrl[ctrl.tableLayout.name + '_toPage'] < 1) {
                ctrl[ctrl.tableLayout.name + '_toPage'] = 1;
            } else if (ctrl[ctrl.tableLayout.name + '_toPage'] > ctrl[ctrl.tableLayout.name + '_pageCount']) {
                ctrl[ctrl.tableLayout.name + '_toPage'] = ctrl[ctrl.tableLayout.name + '_pageCount'];
            }

            ctrl[ctrl.tableLayout.name + '_show'] = false;
            ctrl[ctrl.tableLayout.name + '_formModel'].page = ctrl[ctrl.tableLayout.name + '_toPage'];

            $http({
                method: 'GET',
                url: buildUrl(ctrl.tableLayout.filters.filterSource),
                params: ctrl[ctrl.tableLayout.name + '_formModel']
            }).success(paginationLoadSuccess).error(paginationLoadFailed);

        };

        ctrl.pageNext = function() {
            if (ctrl[ctrl.tableLayout.name + '_pageIndex'] >= ctrl[ctrl.tableLayout.name + '_pageCount']) {
                return;
            } else {

                ctrl[ctrl.tableLayout.name + '_show'] = false;
                ctrl[ctrl.tableLayout.name + '_formModel'].page = ctrl[ctrl.tableLayout.name + '_pageIndex'] + 1;

                $http({
                    method: 'GET',
                    url: buildUrl(ctrl.tableLayout.filters.filterSource),
                    params: ctrl[ctrl.tableLayout.name + '_formModel']
                }).success(paginationLoadSuccess).error(paginationLoadFailed);
            }
        };

        if (ctrl.tableLayout.filters) {
            ctrl.filterTable = function() {
                $log.debug('filter data');
                $log.debug(ctrl.formModel);

                ctrl.formManager.checkFormValidity().then(function() {
                    function loadSuccess(response) {
                        $log.debug('done filter');
                        arrOriTableListing = response.data;
                        var tableData = transformApiDataToTableFormat(response.data);
                        ctrl[ctrl.tableLayout.name + '_pageCount'] = response['page_count'];
                        ctrl[ctrl.tableLayout.name + '_toPage'] = response['page_index'];
                        ctrl[ctrl.tableLayout.name + '_pageIndex'] = response['page_index'];
                        ctrl[ctrl.tableLayout.name + '_pageSize'] = response['total'];
                        ctrl[ctrl.tableLayout.name + '_formModel'] = angular.copy(ctrl.formModel);
                        renderTable(tableData);
                    }

                    function loadFailed() {
                        $log.debug('fail to filter');
                    }

                    ctrl[ctrl.tableLayout.name + '_show'] = false;
                    ctrl.formModel.page = 1;

                    $http({
                        method: 'GET',
                        url: buildUrl(ctrl.tableLayout.filters.filterSource),
                        params: ctrl.formModel
                    }).success(loadSuccess).error(loadFailed);
                });
            };
        }

        if (ctrl.tableLayout.retrieveOnLoad) {
            var loadSuccess = function loadSuccess(response) {
                $log.debug('table data loaded');
                arrOriTableListing = response.data;
                var tableData = transformApiDataToTableFormat(response.data);
                $rootScope.$emit('getTableData', response.total);
                ctrl[ctrl.tableLayout.name + '_pageCount'] = response['page_count'];
                ctrl[ctrl.tableLayout.name + '_pageIndex'] = response['page_index'];
                ctrl[ctrl.tableLayout.name + '_pageSize'] = response['total'];
                ctrl[ctrl.tableLayout.name + '_toPage'] = 1;
                ctrl[ctrl.tableLayout.name + '_formModel'] = angular.copy(ctrl.formModel);
                renderTable(tableData);
            };

            var loadFailed = function loadFailed() {
                $log.error('failed to load table data');
            };

            $resource(buildUrl(ctrl.tableLayout.dataSource)).get(ctrl.formModel, loadSuccess, loadFailed);
        }
    }

    angular.module('components.table').directive(directiveName, directive);
})();
