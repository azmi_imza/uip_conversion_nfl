(function() {
    'use strict';
    var serviceName = 'uiComponentsStorageService';

    /*@ngInject*/
    var uiComponentsStorageService = function($cookies) {
        var storage = {};

        this.get = function(key) {
            if ($cookies[key]) {
                return angular.fromJson($cookies[key])[key];
            }

            if (storage[key]) {
                return angular.fromJson(storage[key])[key];
            }

            return null;
        };

        this.set = function(key, value, isPersistent) {
            var clonedValue = angular.copy(value);
            var obj = {};
            obj[key] = clonedValue;
            if (isPersistent) {
                $cookies[key] = angular.toJson(obj);
            } else {
                storage[key] = angular.toJson(obj);
            }
        };

        this.remove = function(key) {
            delete storage[key];
        };

        this.removeCookie = function(key) {
            delete $cookies[key];
        };

        this.clearAll = function() {
            storage = {};
            this.clearCookies();
        };

        this.clearCookies = function() {
            for (var c in $cookies) {
                if ($cookies.hasOwnProperty(c)) {
                    delete $cookies[c];
                }
            }
        };
    };

    angular.module('components.common')
        .service(serviceName, uiComponentsStorageService);
})();
