(function() {
    'use strict';

    var serviceName = 'componentsModalService';

    /*@ngInject*/
    function componentsModalService($modal, uiHelperService) {

        var service = this;
        this.modalInstance = undefined;
        this.modalStorage = {};

        this.openModal = function(url, ctrl, backdrop, size, resolve, close, dismiss, windowClass) {
            windowClass = windowClass || '';
            service.modalInstance = $modal.open({
                templateUrl: url,
                controller: ctrl,
                backdrop: backdrop,
                size: size,
                resolve: resolve,
                windowClass: windowClass
            });

            service.modalInstance.result.then(close, dismiss);
        };

        this.openModalWithTemplate = function(template, backdrop, size, resolve, close, dismiss, windowClass) {
            windowClass = windowClass || '';
            service.modalInstance = $modal.open({
                template: template,
                backdrop: backdrop,
                size: size,
                resolve: resolve,
                windowClass: windowClass
            });

            service.modalInstance.result.then(close, dismiss);
        };

        this.dismissModal = function(response) {
            if (service.modalInstance) {
                service.modalInstance.dismiss(response);
                uiHelperService.resetViewport();
            }
        };

        this.closeModal = function(response) {
            if (service.modalInstance) {
                service.modalInstance.close(response);
                uiHelperService.resetViewport();
            }
        };
    }

    angular.module('components.common').service(serviceName, componentsModalService);
})();
