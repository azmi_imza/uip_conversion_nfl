(function() {
    'use strict';

    var serviceName = 'componentsMappingService';

    /*@ngInject*/
    function componentsMappingService() {
        var srcMappings = {};
        var destMappings = {};

        this.initializeMappings = function(src, dest) {
            srcMappings = src ? src : {};
            destMappings = dest ? dest : {};
        };

        this.applySrcMappings = function(formManager, formData) {
            angular.forEach(formData, function(value, key) {
                if (srcMappings[key]) {
                    formManager.setValue(srcMappings[key], value);
                } else {
                    formManager.setValue(key, value);
                }
            });
        };

        this.applyDestMappings = function(formManager, formModel) {
            angular.forEach(formModel, function(value, key) {
                if (destMappings[key]) {
                    formManager.setValue(destMappings[key], value);
                }
            });
        };
    }

    angular.module('components.common').service(serviceName, componentsMappingService);
})();
