(function() {
    'use strict';

    var currencyFormat = function($timeout) {
        var Factory = function() {
            this.init = function(scope, iElement, iAttrs, ngModel) {

                var integerLength = (scope.inputFormat.attributes.currencyInteger !== undefined) ? scope.inputFormat.attributes.currencyInteger : '9';
                var decimalLength = (scope.inputFormat.attributes.currencyDecimal !== undefined) ? scope.inputFormat.attributes.currencyDecimal : '2';

                var regexStr = '';
                if (decimalLength > 0) {
                    regexStr = '^(.{0})$|^(\\d{1,' + integerLength + '})$|^(\\d{1,' + integerLength + '}\\.\\d{0,' + decimalLength + '})$';
                } else {
                    regexStr = '^(.{0})$|^(\\d{1,' + integerLength + '})$';
                }

                var regex = new RegExp(regexStr);

                iElement.bind('keypress', function(e) {
                    if (e.which !== 0 && e.which !== 8 && !e.ctrlKey && !e.metaKey && !e.altKey) {
                        var keyChar = String.fromCharCode(e.which);
                        var fieldValue = iElement.val();
                        var resultString = fieldValue.substring(0, e.target.selectionStart) + keyChar + fieldValue.substring(e.target.selectionEnd);

                        if (!regex.test(resultString)) {
                            e.preventDefault();
                        }
                    }
                });

                iElement.bind('paste', function(e) {
                    var fieldValue = iElement.val();
                    var clipboardData = '';

                    if (e.originalEvent.clipboardData) {
                        clipboardData = (e.originalEvent || e).clipboardData.getData('text/plain');
                    } else {
                        clipboardData = window.clipboardData.getData('text');
                    }

                    var resultString = fieldValue.substring(0, e.target.selectionStart) + clipboardData + fieldValue.substring(e.target.selectionEnd);
                    if (!regex.test(resultString)) {
                        e.preventDefault();
                    }

                });

                // TODO: Set text field to 0 while onblur, commented as to cater senario where this behaviour not applicable for text field which
                // other than amount field, e.g. tel no.
                // iElement.bind('blur', function() {
                //     if (!iElement.val()) {
                //         iElement.val('0');
                //         ngModel.$setViewValue('0');
                //         scope.$apply();
                //     } else if (isNaN(iElement.val())) {
                //         var filter = iElement.val().replace(/\D/g, '');
                //         filter = filter ? filter : '0';
                //         iElement.val(filter);
                //         ngModel.$setViewValue(filter);
                //         scope.$apply();
                //     }
                // });

                if (scope.inputFormat.attributes.currencyFormat) {
                    var format = function format(viewValue) {

                        if (!viewValue || isNaN(viewValue)) {
                            return;
                        } else {
                            viewValue = parseFloat(viewValue).toFixed(decimalLength);
                            viewValue = viewValue.toString();
                        }

                        var wholeNumber = viewValue.split('.')[0];
                        var decimal = viewValue.split('.')[1] ? viewValue.split('.')[1] : '';
                        var formattedValue = '';

                        var count = 0;

                        for (var i = wholeNumber.length - 1; i >= 0; i--) {

                            if (count % 3 === 0 && count !== 0) {
                                formattedValue = viewValue[i] + ',' + formattedValue;
                            } else {
                                formattedValue = viewValue[i] + formattedValue;
                            }

                            count++;

                        }

                        if (decimal) {
                            formattedValue = formattedValue + '.' + decimal;
                        }

                        $timeout(function() {
                            iElement.val(formattedValue);
                        });

                    };

                    var unFormat = function unFormat(viewValue) {

                        viewValue = viewValue.replace(/\,/g, '');
                        if (isNaN(viewValue)) {
                            return;
                        }

                        iElement.val(viewValue);
                    };

                    iElement.bind('blur', function() {
                        format(iElement.val());
                    });

                    iElement.bind('focus', function() {
                        unFormat(iElement.val());
                    });

                    ngModel.$formatters.push(function(viewValue) {
                        if (!viewValue || viewValue.length === 0) {
                            return viewValue;
                        }

                        var formattedValue = format(viewValue);
                        return formattedValue;
                    });
                }

            };
        };

        return new Factory();
    };

    angular.module('components.formatter')
        .factory('currencyFormat', currencyFormat);
})();
