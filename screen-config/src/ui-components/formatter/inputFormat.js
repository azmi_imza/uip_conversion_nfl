(function() {
    'use strict';

    var directiveName = 'inputFormat';

    /*@ngInject*/
    function inputFormat($log, currencyFormat) {
        return {
            scope: {
                inputFormat: '='
            },
            restrict: 'A', // restrict to use directive by attribute only
            require: 'ngModel',
            bindToController: true,
            link: function(scope, iElement, iAttrs, ngModel) {
                if (!scope.inputFormat) {
                    $log.debug('Input format not provided');
                    return;
                }

                if (scope.inputFormat.type === 'currency') {
                    currencyFormat.init(scope, iElement, iAttrs, ngModel);
                }
            }

        };
    }

    angular.module('components.formatter').directive(directiveName, inputFormat);
})();
