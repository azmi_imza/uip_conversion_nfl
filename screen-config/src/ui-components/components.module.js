(function() {
    'use strict';

    angular.module('hfits.components', [
        'ngSanitize',
        'components.common',
        'components.formatter',
        'components.form',
        'components.layout',
        'components.input',
        'components.view',
        'components.table'
    ]);
})();
