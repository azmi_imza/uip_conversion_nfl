/* eslint no-console:0 */

(function() {
    'use strict';

    var gulp = require('gulp');
    var ngAnnotate = require('gulp-ng-annotate');
    var concat = require('gulp-concat');
    var uglifyJs = require('gulp-uglify');
    var rename = require('gulp-rename');
    var minifyCss = require('gulp-minify-css');
    var minifyHTML = require('gulp-minify-html');
    var imagemin = require('gulp-imagemin');
    var pngquant = require('imagemin-pngquant');
    var del = require('del');
    var path = require('path');
    var fs = require('fs');
    var argv = require('yargs').argv;
    var jscs = require('gulp-jscs');
    var eslint = require('gulp-eslint');
    var stylish = require('gulp-jscs-stylish');
    var autoprefixer = require('gulp-autoprefixer');
    var runSequence = require('run-sequence');
    var jsonlint = require('gulp-jsonlint');
    var compress = require('compression');
    var gulpif = require('gulp-if');
    var _ = require('underscore');

    var distComponentsJs = 'screen-config/dist/ui-components/js/*.js';
    var distComponentsCss = 'screen-config/dist/ui-components/css/*.css';
    var distComponentsHtml = 'screen-config/dist/ui-components/*.html';
    var distComponentsImages = 'screen-config/dist/ui-components/images/*';

    var uipConfig = require('./src/config.json');

    if (fs.existsSync('src/config.local.json')) {
        var uipLocalConfig = require('./src/config.local.json');
        _.extend(uipConfig, uipLocalConfig);
    }

    var dist = 'dist';
    var distJs = path.join(dist, 'js');
    var distCss = path.join(dist, 'css');
    var distImages = path.join(dist, 'images');
    var distDocuments = path.join(dist, 'documents');
    var distFonts = path.join(dist, 'fonts');
    var distwar = 'src/main/webapp';

    var uipJsFileName = 'uip.js';
    var uipVendorJsFileName = 'vendor.js';
    var uipCssFileName = 'uip.min.css';
    var uipVendorCssFileName = 'vendor.min.css';

    var noop = function() {};

    var isProduction = false;

    var src = {
        scss: [],
        css: {
            vendor: [
                'bower_components/bootstrap/dist/css/bootstrap.min.css',
                '!bower_components/fontawesome/css/font-awesome.min.css',
                '!bower_components/footable/css/footable.core.css',
                '!bower_components/jquery-ui/themes/smoothness/jquery-ui.css',
                '!bower_components/pickadate/lib/themes/classic.css',
                '!bower_components/pickadate/lib/themes/classic.date.css',
                '!bower_components/blueimp-file-upload/css/jquery.fileupload.css',
                '!bower_components/blueimp-file-upload/css/jquery.fileupload-ui.css',
                '!bower_components/ResponsiveMultiLevelMenu/css/component.css',
                '!bower_components/tablesaw/dist/tablesaw.css',
                '!libs/hfits_modules/hfits.modules.css'
            ],
            uip: [
                'css/global.css',
                'css/forms.css',
                'css/main.css',
                'css/custom.responsive.css',
                /**'css/common.css',**/
                'css/font.css'
            ]
        },
        images: ['images/**/*'],
        documents: ['documents/**/*'],
        fonts: [
            'bower_components/bootstrap/dist/fonts/*',
            'bower_components/fontawesome/fonts/*',
            'bower_components/footable/css/fonts/*',
            '!bower_components/ResponsiveMultiLevelMenu/fonts/icomoon.dev.svg',
            'bower_components/ResponsiveMultiLevelMenu/fonts/*'
        ],
        html: {
            uip: ['index.html', 'src/modules/**/*.html']
        },
        js: {
            uip: [
                /********************************************************************************************
                 - order is important here
                 - separate app constants into independent module, so that modules can refer to constants
                   during initialization
                 - then followed by independent *.module.js
                 - app.constants.js > all other application modules > app.module.js
                 - then load the rest of the modules code
                *********************************************************************************************/
                'src/app.constants.js',
                'src/modules/**/*.module.js',
                'src/app.module.js',
                'src/app.config.js',
                'src/app.js',
                'src/modules/**/*.js'
            ],
            vendor: [
                'bower_components/lodash-compat/lodash.js',
                'bower_components/jquery/dist/jquery.js',
                'bower_components/jquery-ui/jquery-ui.js',
                'bower_components/angular/angular.js',
                'bower_components/angular-resource/angular-resource.js',
                'bower_components/angular-messages/angular-messages.js',
                'bower_components/angular-cookies/angular-cookies.js',
                'bower_components/angular-ui-router/release/angular-ui-router.js',
                'bower_components/angular-bootstrap/ui-bootstrap.js',
                'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                'bower_components/angular-translate/angular-translate.js',
                'bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js',
                'bower_components/Chart.js/Chart.js',
                'bower_components/pdfjs-dist/web/compatibility.js',
                'bower_components/pdfobject/pdfobject.js',
                'bower_components/pdfjs-dist/build/pdf.js',
                'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
                'bower_components/blueimp-file-upload/js/jquery.iframe-transport.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-process.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-validate.js',
                'bower_components/blueimp-file-upload/js/jquery.fileupload-ui.js',
                'bower_components/fastclick/lib/fastclick.js',
                'bower_components/pickadate/lib/picker.js',
                'bower_components/pickadate/lib/picker.date.js',
                'bower_components/bootstrap/dist/js/bootstrap.min.js',
                'bower_components/angular-sanitize/angular-sanitize.min.js',
                'bower_components/ResponsiveMultiLevelMenu/js/modernizr.custom.js',
                'bower_components/ResponsiveMultiLevelMenu/js/jquery.dlmenu.js',
                'bower_components/tablesaw/dist/tablesaw.js',
                'bower_components/FileSaver/FileSaver.js',
                'bower_components/jquery-placeholder/jquery.placeholder.js',
                'libs/hfits_modules/hfits.modules.js',
                'libs/encryption/e2eejslib.js',
                'js/modernizr.js',
                'libs/raphael.js',
                'libs/footable/footable.js',
                'libs/footable/footable.sort.js',
                'libs/footable/footable.paginate.js'
            ],
            pdfWorker: [
                'bower_components/pdfjs-dist/build/pdf.worker.js'
            ]
        },
        resources: {
            json: ['i18n/**/*.json', 'src/config*.json', 'data/**/*.json'],
            html: ['data/**/*.html'],
            contentManaged: ['data/contentManaged/**/*']
        }
    };

    var minifyHTMLOptions = {
        empty: true,
        quotes: true,
        spare: true
    };

    var imageminOptions = {
        progressive: true,
        svgoPlugins: [{
            removeViewBox: false
        }],
        use: [pngquant()]
    };

    var browserSync = require('browser-sync');
    var reload = browserSync.reload;

    function runBrowserSync(baseDir, isProd) {
        isProd = isProd || false;
        console.log('Is Production: ' + isProd);
        var options = {
            server: {
                baseDir: baseDir,
                middleware: function(req, res, next) {
                    var gzip = compress();
                    gzip(req, res, next);
                }
            },
            https: uipConfig.SETTINGS.API_PROTOCOL === 'https' ? true : false,
            port: argv.port || 8080,
            reloadOnRestart: !isProd,
            ghostMode: false,
            notify: false,
            open: false
        };

        browserSync(options);
    }

    gulp.task('reload-browser', function() {
        reload();
    });

    /********************************** serve **************************************/

    gulp.task('run-prod', function() {
        runBrowserSync('dist', true);
    });

    gulp.task('serve-prod', function(cb) {
        runSequence('build-prod', 'run-prod', cb);
    });

    gulp.task('serve', function() {
        runBrowserSync('dist');
    });

    /********************************** clean **************************************/

    gulp.task('clean', function(cb) {
        del(['dist'], cb);
    });
    
    gulp.task('clean-war', function(cb) {
    	del(['src/main/webapp/**/*', '!src/main/webapp/WEB-INF', '!src/main/webapp/WEB-INF/web.xml', '!src/main/webapp/WEB-INF/weblogic.xml'], cb);
    });

    /********************************** build-uip **************************************/
    gulp.task('build-uip-js', function() {
        return gulp.src(src.js.uip)
            .pipe(jscs())
            .on('error', noop)
            .pipe(stylish())
            .pipe(eslint())
            .pipe(eslint.format())
            .pipe(concat(uipJsFileName))
            .pipe(ngAnnotate())
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('build-uip-vendor-js', function() {
        return gulp.src(src.js.vendor)
            .pipe(concat(uipVendorJsFileName))
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('build-uip-html', function() {
        return gulp.src(src.html.uip)
            .pipe(rename(function(p) {
                if (['index', 'test'].indexOf(p.basename) === -1) {
                    p.dirname = path.join('templates', p.dirname);
                }
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('build-uip-css', function() {
        return gulp.src(src.css.uip)
            .pipe(autoprefixer())
            .pipe(concat(uipCssFileName))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(distCss));
    });

    gulp.task('build-uip-vendor-css', function() {
        return gulp.src(src.css.vendor)
            .pipe(concat(uipVendorCssFileName))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(distCss));
    });

    gulp.task('copy-pdfworker-js', function() {
        return gulp.src(src.js.pdfWorker)
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(distJs));
    });

    gulp.task('build-uip-fonts', function() {
        return gulp.src(src.fonts)
            .pipe(gulp.dest(distFonts))
            .pipe(gulp.dest(path.join(dist, 'css', 'fonts')));
    });

    gulp.task('build-uip-images', function() {
        return gulp.src(src.images)
            .pipe(gulpif(isProduction, imagemin(imageminOptions)))
            .pipe(gulp.dest(distImages));
    });

    gulp.task('build-uip-documents', function() {
        return gulp.src(src.documents)
            .pipe(gulp.dest(distDocuments));
    });

    gulp.task('copy-resources-json', function() {
        return gulp.src(src.resources.json)
            .pipe(rename(function(p) {
                if (p.dirname !== '.' && p.basename.indexOf('config') !== 0) {
                    p.dirname = path.join('data', p.dirname);
                }
            }))
            .pipe(jsonlint())
            .pipe(jsonlint.reporter())
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-resources-html', function() {
        return gulp.src(src.resources.html)
            .pipe(rename(function(p) {
                p.dirname = path.join('data', p.dirname);
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-resources-content-managed', function() {
        return gulp.src(src.resources.contentManaged)
            .pipe(rename(function(p) {
                p.dirname = path.join('data', path.join('contentManaged', p.dirname));
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-components-js', function() {
        return gulp.src(distComponentsJs)
            .pipe(rename(function(p) {
                p.dirname = path.join('ui-components', 'js', p.dirname);
            }))
            .pipe(gulpif(isProduction, uglifyJs()))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-components-css', function() {
        return gulp.src(distComponentsCss)
            .pipe(rename(function(p) {
                p.dirname = path.join('ui-components', 'css', p.dirname);
            }))
            .pipe(gulpif(isProduction, minifyCss()))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-components-html', function() {
        return gulp.src(distComponentsHtml)
            .pipe(rename(function(p) {
                p.dirname = path.join('ui-components', p.dirname);
            }))
            .pipe(gulpif(isProduction, minifyHTML(minifyHTMLOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-components-images', function() {
        return gulp.src(distComponentsImages)
            .pipe(rename(function(p) {
                p.dirname = path.join('ui-components', 'images', p.dirname);
            }))
            .pipe(gulpif(isProduction, imagemin(imageminOptions)))
            .pipe(gulp.dest(dist));
    });

    gulp.task('copy-dist', function() {
        return gulp.src(path.join(dist, '/**/*'))
            .pipe(gulp.dest(distwar));
    });

    gulp.task('copy-components', [
        'copy-components-js',
        'copy-components-css',
        'copy-components-html',
        'copy-components-images'
    ]);

    gulp.task('build-uip', [
        'build-uip-js',
        'build-uip-vendor-js',
        'build-uip-html',
        'build-uip-fonts',
        'build-uip-images',
        'build-uip-documents',
        'build-uip-css',
        'build-uip-vendor-css',
        'copy-resources-content-managed',
        'copy-resources-html',
        'copy-resources-json',
        'copy-components',
        'copy-pdfworker-js'
    ]);

    /********************************** watch **************************************/

    function watchAndReload(watchSrc, watchOptions, tasks) {
        watchOptions = watchOptions || {
            debounceDelay: 1000
        };

        tasks.push('reload-browser');
        gulp.watch(watchSrc, watchOptions, function() {
            runSequence.apply(this, tasks);
        });
    }

    gulp.task('watch', function() {
        watchAndReload(src.js.uip, null, ['build-uip-js']);
        watchAndReload(src.html.uip, null, ['build-uip-html']);
        watchAndReload(src.css.uip, null, ['build-uip-css']);
        watchAndReload(src.fonts, null, ['build-uip-fonts']);
        watchAndReload(src.resources.json, null, ['copy-resources-json']);
        watchAndReload(src.resources.html, null, ['copy-resources-html']);
        watchAndReload(src.resources.contentManaged, null, ['copy-resources-content-managed']);
        watchAndReload(distComponentsJs, null, ['copy-components-js']);
        watchAndReload(distComponentsCss, null, ['copy-components-css']);
        watchAndReload(distComponentsHtml, null, ['copy-components-html']);
        watchAndReload(distComponentsImages, null, ['copy-components-images']);
    });

    gulp.task('build', function(cb) {
        runSequence('clean', 'build-uip', cb);
    });

    gulp.task('build-prod', function(cb) {
        isProduction = true;
        runSequence('build', cb);
    });
    
    gulp.task('build-war', function(cb) {
    	runSequence('clean', 'build-uip', 'clean-war', 'copy-dist', cb);
    });

    gulp.task('default', function(cb) {
        runSequence('build', 'serve', 'watch', cb);
    });
})();
