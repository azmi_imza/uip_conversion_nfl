var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var postLogin = require('./login').postLogin;
var getFund = require('./fund').getFund;

var app = express();
var apiPaths = {
    'LOGIN': '/login',
    'POLICY': '/policy',
    'BENEFIT': '/benefit',
    'FUND': '/fund',
};

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, X-Username, X-Password, X-Auth-Token');
    res.header('Access-Control-Expose-Headers', 'X-Auth-Token');
    res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS, DELETE');
    res.header('Access-Control-Max-Age', '3600');
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
});

// app.use(logger('dev'));
// app.use(bodyParser.json());

// app.get(apiPaths.POLICY, function(req, res) {

// });

// app.post(apiPaths.POLICY, function(req, res) {

// });

// app.put(apiPaths.POLICY, function(req, res) {

// });

// app.delete(apiPaths.POLICY, function(req, res) {

// });


app.post(apiPaths.LOGIN, postLogin);
app.get(apiPaths.FUND, getFund);

// app.post(apiPaths.LOGIN, function(req, res) {
//     console.log(req.header);
//     var response = {};

//     res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');

//     res.status(200);
//     if (req.query.username === 'userA' && req.query.password === 'a123') {
//         response = {
//             'status': 'success',
//             'data': {
//                 'subscriber': {
//                     'username': req.query.username,
//                     'token': 'T0001',
//                     'ID': 'UID0001'
//                 },
//                 'page': 'Dashboard',
//                 'link': '/dashboard',
//                 'menu': [{
//                     'name': 'Portfolio',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu2',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu3',
//                         'link': '#'
//                     }]
//                 }, {
//                     'name': 'Profile',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }]
//                 }, {
//                     'name': 'Service Request',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu2',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu3',
//                         'link': '#'
//                     }]
//                 }, {
//                     'name': 'Claim',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu2',
//                         'link': '#'
//                     }]
//                 }, {
//                     'name': 'e-Documents',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu2',
//                         'link': '#'
//                     }]
//                 }, {
//                     'name': 'Reports',
//                     'link': '#',
//                     'submenu': [{
//                         'name': 'Sub-Menu1',
//                         'link': '#'
//                     }, {
//                         'name': 'Sub-Menu2',
//                         'link': '#'
//                     }]
//                 }]
//             }
//         };

//     } else {
//         response = {
//             'status': 'fail'
//         };
//         if (req.query.username === 'error') {
//             res.status(404);
//         }
//     }

//     res.send(response);
// });

/*app.all('/user/userA', function(req, res, next) {
    // CORS headers
    res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'GET') {
        console.log(req);
        res.json({
            'status': 200,
            'username': 'userA',
            'pasword': 'a123',
        });
        res.status(200).end();
    } else {
        next();
    }
});

app.all('/user/userB', function(req, res, next) {
    // CORS headers
    res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'GET') {
        console.log('before json');
        var data = {
            'status': 404,
            'username': 'userB',
            'pasword': 'b1234'
        };
        //res.json(data);

        console.log('before send');
        //res.status(404).end();
        res.status(304);
        res.send(data);
    } else {
        next();
    }
});*/

// If no route is matched by now, it must be a 404
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// Start the server
app.set('port', process.env.PORT || 7001);

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});
