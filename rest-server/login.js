var uuid = require('node-uuid');

var loginResponse = require('./data/login');
var validUsers = [{
    username: 'user001',
    is_cka_pass: 'Y'
}, {
    username: 'user002',
    is_cka_pass: 'N'
}];

function postLogin (req, res) {
	var username = req.get('X-Username');
	var password = req.get('X-Password');
	var isValidLogin = false;
	var isCKAPass = '';
	res.status(200);
	
	for (var i in validUsers) {
		if (validUsers[i].username === username && password === 'password') {
			isValidLogin = true;
			isCKAPass = validUsers[i].is_cka_pass;
		}
	}

	if (!isValidLogin) {
		var errorResponse = { status: 'failed', error: 'Invalid Credentials'};
		res.send(errorResponse);
		return;
	}

    var response = {};
    var token = uuid.v4().replace(/-/g, '');
    res.header('X-Auth-Token', token);

    loginResponse.data.subscriber.username = username;
    loginResponse.data.subscriber.is_cka_pass = isCKAPass;

    res.json(loginResponse);
}

module.exports.postLogin = postLogin;
