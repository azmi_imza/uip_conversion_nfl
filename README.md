This repository contains the source code for two applications:
 
 * **UIP**
 * **Screen configurator** (in **screen-config** directory)

* * *
# UIP  #
* * *
## Setup ##
1. Install nodejs: https://nodejs.org/download/
2. Below are the global node modules being used in UIP and their usage:

      * **bower** - package manager for Javascript libraries
      * **gulp** - task/build runner
      * **browser-sync** - development web server that provides live reload feature
      * **eslint** - javascript code analysis tool (prevent bad coding practices)
      * **jscs** - javascript code style checker (standardize code format and style)

3. Install the global node modules listed in **step 2** by using this command: 

      * **npm install -g <list of modules to install with space in between each module>**
      * For e.g.: **npm install -g bower gulp browser-sync eslint jscs**

4. Install all local npm modules, run this command at the root level of the repository (where **package.json** file is located): 

      * **npm install**

5. Install all bower components, run this command at the root level of the repository (where **bower.json** file is located: 

      * **bower install**

## Gulp Build Flow (gulpfile.js) ##
In high level, the build consists of following steps:

* clean all files in **dist** folder
* **build js files**
     * all js files specified in **src.js.uip** array will go through the following code quality check, any violation will be reported in the console:
         * jscs
         * eslint
     * all js files specified in **src.js.uip** array will be bundled into single js file (**uip.js**) and copied to **dist** directory
     * Use **ng-annotate** to inject dependency explicitly to all functions that is decorated by **/*@ngInject*/**. To read more about this : [ng-annotate](https://github.com/olov/ng-annotate)
     * all js files specified in **src.js.vendor** array will be bundled into single js file (**vendor.js**) and copied to **dist** directory
* **build html files**
     * all html files specified in **src.html.uip** will be minified and copied to **dist/templates** directory following the same directory path in **src/modules**
* **build css files** 
     * all css files specified in **src.css.uip** will be bundled into single css file (**uip.min.css**), minified and copied to **dist** directory
     * all css files specified in **src.css.vendor** will be bundled into single css file (**vendor.min.css**) and copied to **dist** directory
* **copy fonts, images & resources**
     * all font files specified in **src.fonts** will be copied to **dist/fonts** directory
     * all image files specified in **src.images** will be copied to **dist/images** directory
     * all files specified in **src.resources.json** & **src.resources.html** will be copied to **dist/data** directory
* **copy ui-components directory in screen-config/dist**
     * there is a separate build process for screen-config, please refer to build flow for **Screen Configurator**.
* **run browser-sync web server**
* **watch all source files for changes**

*When any changes made to the files specified above, the build task for affected file will be triggered and if UIP app is opened in any web browser, it will also reload the page to reflect latest changes.*

## Gulp Commands ##
1. By running gulp defaul command: **gulp**, the **default** task in gulpfile.js will be executed
2. To build package for production, run this command: **gulp build-prod**, this will perform additional steps to further optimize the build size (will take longer time to build, which is why these steps are excluded in default build task)
     * *bundled js file will be minified*
     * *images will be compressed*
     * *files will be packaged into **dist** folder*
3. To build & serve production package: **gulp serve-prod** 


## Development procedures ##
1. Create new git branch for every jira task by naming the branch (all lower case) prefix with jira task id and followed by short description of task. (e.g. **umt-23-login-client-logic**)
2. Once development is done, create a **pull request** and assign a reviewer to the **pull request**.
    * 2.1 Reviewer should review the code as soon as possible and request the engineer to apply fixes based on code review comments.
    * 2.2 Engineer should review the comments by reviewer and discuss further with reviewer if needed.
    * 2.3 Engineer should commit and push code review fixes to same branch and inform reviewer to review.
    * 2.4 Repeat steps 2.1 - 2.3 until both parties are satisfied with the code.
    * 2.5 Reviewer should merge the pull request into master (or request someone who has the permission to do so). **DO NOT** delete the branch after merge for auditing purpose.
3. File location convention:
    * For **new module** development in UIP, create new folder in **src/<module name>**.
       * For eg. if module name is **moduleA**, module folder path will be **src/moduleA**.
       * 

## Git branch management ##
1. Please always ensure your pull request has merged with latest master branch by using the following command (might need to resolve conflict):
    * **git merge origin/master**
2. Always keep current working branches up to date by merging with master branch (using the same command in Step 1) before pushing to Bitbucket to resolve any conflicts upfront.

* * *
# Screen Configurator #
* * *

## Setup ##

Change current directory to: **screen-config** and run the same commands in **steps 4 & 5** in UIP SETUP

## Gulp Build Flow (screen-config/gulpfile.js) ##

In high level, the build consists of following steps:

* clean all files in **dist** folder
* **build js files (ui-components)**

     * all js files specified in **src.js.components** array will go through the following code quality check, any violation will be reported in the console:
         * jscs
         * eslint
     * all js files specified in **src.js.components** array will be bundled into single js file **components.min.js** and copied to **screen-config/dist/ui-components** directory
     * Use **ng-annotate** to inject dependency explicitly to all functions that is decorated by **/*@ngInject*/**. To read more about this : [ng-annotate](https://github.com/olov/ng-annotate)

* **build js files (screen-config)**
     * all js files specified in **src.js.screenconfig** array will go through the following code quality check, any violation will be reported in the console:
         * jscs
         * eslint
     * all js files specified in **src.js.screenconfig** array will be bundled into single js file **screenconfig.js** and copied to **screen-config/dist** directory
     * Use **ng-annotate** to inject dependency explicitly to all functions that is decorated by **/*@ngInject*/**. To read more about this : [ng-annotate](https://github.com/olov/ng-annotate)

* **build html files (ui-components)**
      * all html files specified in **src.html.components** will be bundled into single html file (**template.html**) and copied to **screen-config/dist/ui-components** directory

* **build css files**

* **copy fonts, images & resources**

* **watch all source files for changes**


## Gulp Commands ##



## Development procedures ##

Follow UIP standard

## Git branch management ##

Follow UIP standard

* * *
# AngularJS Best Practices #
* * *
1. use $log.debug instead console.log [Read more here](https://docs.angularjs.org/api/ng/service/$log)
     * can easily switch off all debug message with $logProvider [Details](https://docs.angularjs.org/api/ng/provider/$logProvider)
2. For more best practices, refer to [here](https://github.com/johnpapa/angularjs-styleguide)