(function() {
    'use strict';
    angular.module('common', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var appConstants = $injector.get('APP_CONST');
                var BASE_STATE_PREFIX = appConstants.BASE_STATE_PREFIX;
                var COMMON_STATE_PREFIX = appConstants.COMMON_STATE_PREFIX;
                var AUTH_STATE_PREFIX = appConstants.AUTH_STATE_PREFIX;
                var ERROR_STATE_PREFIX = appConstants.ERROR_STATE_PREFIX;

                /**************************************\
                    Base statess
                \**************************************/

                $stateProvider.state(BASE_STATE_PREFIX.substring(0, BASE_STATE_PREFIX.length - 1), {
                    abstract: true,
                    template: '<div base-view></div>',
                    resolve: {
                        authentication: function($state, $http, $log, $q, routeService, authService, storageService, APP_CONFIG, APP_CONST) {
                            $log.debug('*********** resolve authentication **************');

                            var deferred = $q.defer();

                            if ($state.toState.name.indexOf(APP_CONST.AUTH_STATE_PREFIX) > -1) {
                                if (authService.getAuthToken()) {
                                    $http({
                                        method: 'PUT',
                                        url: APP_CONFIG.API_PATHS.SESSION_RENEW
                                    }).then(function() {
                                            storageService.set(APP_CONST.STORAGE_KEYS.SESSION_REFERENCE, new Date().getTime(), true);
                                            $log.debug('AUTHENTICATED:', $state.toState.name);
                                            deferred.resolve('AUTHENTICATED');
                                        },

                                        function() {
                                            $log.error('UNABLE TO RENEW SESSION:', $state.toState.name);
                                            storageService.clearAll();
                                            deferred.reject('UNABLE TO RENEW SESSION');
                                        });
                                } else {
                                    deferred.reject('NOT AUTHENTICATED, no auth token');
                                }
                            } else {
                                $log.debug('*********** no auth required, resolve **************');
                                $log.debug('AUTHENTICATED NOT REQUIRED:', $state.toState.name);
                                deferred.resolve('AUTHENTICATED NOT REQUIRED');
                            }

                            return deferred.promise;
                        },

                        resources: function($state, $log, languageService, storageService, uiHelperService, APP_CONST, authentication) {
                            $log.debug('*********** resolve resources **************');
                            $log.debug('AUTHENTICATION:', authentication);

                            uiHelperService.showLoader();

                            return languageService.refreshLangugeResource();
                        }
                    },

                    onEnter: function($timeout, $log, uiHelperService) {
                        $log.debug('********** onEnter *************');
                        uiHelperService.resetViewport();
                        uiHelperService.hideLoader();
                    }
                });

                /**************************************\
                    Common states
                \**************************************/

                $stateProvider.state(COMMON_STATE_PREFIX.substring(0, COMMON_STATE_PREFIX.length - 1), {
                    abstract: true,
                    template: '<div common-view></div>',
                    data: {
                        screenCode: 'SCRNCOMMON'
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('session-expired'), {
                    templateUrl: 'templates/common/sessionexpired.html',
                    url: '/session-expired',
                    bindToController: true,
                    controllerAs: 'ctrl',
                    params: {
                        response: null
                    },
                    controller: function($log, storageService, routeService, authService, APP_CONST, uiHelperService, $stateParams) {
                        var ctrl = this;
                        var isSessionExpired = storageService.get(APP_CONST.STORAGE_KEYS.IS_SESSION_EXPIRED);
                        $log.debug('clear session expired');
                        storageService.removeCookie(APP_CONST.STORAGE_KEYS.IS_SESSION_EXPIRED);

                        if (!isSessionExpired) {
                            if (authService.getAuthToken()) {
                                authService.isAuthenticated().then(function() {
                                        routeService.goToPageNotFound();
                                    },

                                    function() {
                                        routeService.goToLogin();
                                    });
                            } else {
                                routeService.goToLogin();
                            }
                        }

                        if ($stateParams.response) {
                            ctrl.message = $stateParams.response.message;
                            if ($stateParams.response['error_code'] === '801099062') {
                                ctrl.header = 'cfo.header.inactiveSessionHdr';
                            } else if ($stateParams.response['error_code'] === '801099063') {
                                ctrl.header = 'cfo.header.multipleUserAccessHdr';
                            } else if ($stateParams.response['error_code'] === '801099064') {
                                ctrl.header = 'cfo.header.absoluteSessionTimeoutHdr';
                            }
                        }

                        this.goToLogin = function() {
                            routeService.goToLogin();
                        };
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('css-template'), {
                    template: '<div css-template></div>',
                    url: '/css-template',
                    data: {}
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('css-template-22'), {
                    template: '<div css-template-2></div>',
                    url: '/css-template-2',
                    data: {}
                });

                /**************************************\
                    Auth states
                \**************************************/

                $stateProvider.state(AUTH_STATE_PREFIX.substring(0, AUTH_STATE_PREFIX.length - 1), {
                    abstract: true,
                    template: '<div main></div>',
                    data: {
                        screenCode: ''
                    }
                });

                /**************************************\
                    Error states
                \**************************************/

                $stateProvider.state(ERROR_STATE_PREFIX.substring(0, ERROR_STATE_PREFIX.length - 1), {
                    abstract: true,
                    template: '<div ui-view></div>',
                    data: {
                        screenCode: 'SCRNCOMMON'
                    }
                });

                var notFoundStateName = ERROR_STATE_PREFIX.concat('not-found');
                $stateProvider.state(notFoundStateName, {
                    templateUrl: 'templates/common/error/notfound.html',
                    url: '/not-found'
                });

            });
})();
