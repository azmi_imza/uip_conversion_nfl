(function() {
    'use strict';
    var serviceName = 'authService';

    /*@ngInject*/
    var authService = function($http, $log, $q, storageService, APP_CONST, APP_CONFIG) {
        var service = this;

        this.isAuthenticated = function() {
            var authToken = service.getAuthToken();
            var deferred = $q.defer();

            if (!authToken) {
                deferred.reject();
            } else {
                $http({
                    method: 'PUT',
                    url: APP_CONFIG.API_PATHS.SESSION_RENEW
                }).then(function() {
                        $log.debug('authService, renew session successful');
                        storageService.set(APP_CONST.STORAGE_KEYS.SESSION_REFERENCE, new Date().getTime(), true);
                        deferred.resolve();
                    },

                    function(response) {
                        $log.error('authService, Unable to renew session:', response);
                        deferred.reject();
                    });
            }

            return deferred.promise;
        };

        this.getAuthToken = function() {
            return storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
        };

        this.setAuthToken = function(token) {
            storageService.set(APP_CONST.STORAGE_KEYS.AUTH_TOKEN, token, true);
        };
    };

    angular.module('common')
        .service(serviceName, authService);
})();
