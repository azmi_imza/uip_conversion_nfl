(function() {
    'use strict';

    var directiveName = 'nflFooter';

    /*@ngInject*/
    function nflFooter() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nflFooterCtrl,
            templateUrl: 'templates/common/nflFooter.html'
        };
    }

    /*@ngInject*/
    function nflFooterCtrl($state, $log, rootService, routeService) {
        var ctrl = this;
    }

    angular.module('common').directive(directiveName, nflFooter);
})();