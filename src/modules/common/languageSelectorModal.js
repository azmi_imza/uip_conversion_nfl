(function() {
    'use strict';

    var directiveName = 'languageSelectorModal';

    /*@ngInject*/
    function languageSelectorModal() {
        return {
            bindToController: true,
            controller: languageSelectorModalCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/common/languageSelectorModal.html'
        };
    }

    /*@ngInject*/
    function languageSelectorModalCtrl($timeout, generalModalService, storageService, languageService, APP_CONST) {
        var ctrl = this;
        ctrl.close = close;
        ctrl.selectLang = selectLang;
        ctrl.countries = languageService.loadCountriesAndLanguages();

        function selectLang(selectedLang, iconUrl) {
            storageService.set(APP_CONST.STORAGE_KEYS.SELECTED_LANG, selectedLang.code, true);
            $timeout(function() {
                languageService.refreshLangugeResource();
                selectedLang.iconUrl = iconUrl;
                generalModalService.closeModal(selectedLang);
            });
        }

        function close() {
            generalModalService.closeModal();
        }
    }

    angular.module('common').directive(directiveName, languageSelectorModal);
})();
