(function() {
    'use strict';
    var factoryName = 'authInterceptor';

    /*@ngInject*/
    var authInterceptor = function($log, storageService, $q, $rootScope, APP_CONST, APP_CONFIG) {
        var authTokenHeader = APP_CONST.HTTP_HEADERS.AUTH_TOKEN;
        var contentTypeHeader = APP_CONST.HTTP_HEADERS.CONTENT_TYPE;

        return {
            request: function(config) {
                storageService.set('COOKIES', document.cookie);

                var authToken = storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
                var selectedlang = storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG) ||
                    APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE;

                document.cookie = '';
                config.timeout = 120000;

                /* To add in language for those screen that does not have token. */
                if (config.url.indexOf('uipcfo/rest') > -1) {
                    if (!config.params) {
                        config.params = {};
                    }

                    if (!config.params.lang) {
                        config.params.lang = selectedlang;
                    }

                    if (authToken) {
                        config.headers[authTokenHeader] = authToken;

                        if (config.method === 'POST' || config.method === 'PUT') {
                            config.headers[contentTypeHeader] = 'application/json';
                        }
                    }
                }

                document.cookie = storageService.get('COOKIES');
                return config;
            },

            response: function(response) {
                storageService.set(APP_CONST.STORAGE_KEYS.SESSION_LAST_ACTIVE_TIME,
                    new Date().getTime(), true);
                return response;
            },

            responseError: function(rejection) {
                var ignorePaths = [APP_CONFIG.API_PATHS.LOGIN];
                var error = {
                    status: rejection.status,
                    statusText: rejection.statusText,
                    url: rejection.config.url,
                    data: rejection.data
                };

                $log.error('response error:', error);

                if (rejection.status === 401 && ignorePaths.indexOf(rejection.config.url) === -1) {
                    $rootScope.$emit('logoutClient');
                }

                return $q.reject(rejection);
            }
        };
    };

    angular.module('common')
        .factory(factoryName, authInterceptor);
})();
