(function() {
    'use strict';

    var serviceName = 'uiHelperService';

    /*@ngInject*/
    function uiHelperService($log, $timeout) {
        var previousBackgroundTopPosition = 0;

        this.resetViewport = function resetViewport() {
            $log.debug('*** resetViewport ***');
            $timeout(function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                $('#viewport').attr('content',
                    'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1');

                $timeout(function() {
                    $('#viewport').attr('content', 'width=device-width, initial-scale=1');
                }, 100);
            });
        };

        this.disableBackgroundScroll = function disableBackgroundScroll() {
            previousBackgroundTopPosition = document.querySelector('body').getBoundingClientRect().top;
            $('#maincontainer').addClass('disable-scroll');
        };

        this.resetScrollPosition = function resetScrollPosition() {
            $timeout(function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                if (previousBackgroundTopPosition) {
                    window.scrollTo(0, previousBackgroundTopPosition * -1);
                    previousBackgroundTopPosition = 0;
                } else {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    window.scrollTo(0, 0);
                }
            });
        };

        this.showLoader = function showLoader() {
            $timeout(function() {
                $log.debug('*** show loader ***');
                $('.uip-loader').removeClass('hide');

                // hide maincontainer to hide scrollbar, when loader is shown
                $('#maincontainer').addClass('hide');
            });

        };

        this.hideLoader = function hideLoader(position) {
            $timeout(function() {
                $log.debug('*** hide loader ***');
                $('#maincontainer').removeClass('hide');

                if (position) {
                    $('html, body').scrollTop(position);
                }

                $('.uip-loader').addClass('hide');
            });
        };

        this.scrollTo = function scrollTo(position, duration) {
            var d = duration ? duration : 1000;
            var page = $('html, body');

            page.on('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function() {
                page.stop();
            });

            page.animate({
                scrollTop: position - 30 // 30px above input
            }, d, 'easeOutExpo', function() {
                page.off('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove');
            });
        };
    }

    angular.module('common').service(serviceName, uiHelperService);
})();
