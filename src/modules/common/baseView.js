(function() {
    'use strict';
    var directiveName = 'baseView';

    /*@ngInject*/
    var baseView = function() {
        return {
            restrict: 'EA',
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: baseViewCtrl,
            templateUrl: 'templates/common/baseView.html'
        };
    };

    /*@ngInject*/
    var baseViewCtrl = function() {
        /*var ctrl = this;*/
    };

    angular.module('common')
        .directive(directiveName, baseView);
})();
