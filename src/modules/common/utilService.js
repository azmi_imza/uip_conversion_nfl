(function() {
    'use strict';

    var serviceName = 'utilService';

    /*@ngInject*/
    function utilService($log) {

        this.floorValue = function floorValue(value) {
            $log.debug('*** floorValue ***');

            value = Math.floor(value);
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        };

    }

    angular.module('common').service(serviceName, utilService);
})();
