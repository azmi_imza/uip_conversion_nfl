(function() {
    'use strict';

    var serviceName = 'routeService';

    /*@ngInject*/
    function routeService($resource, $rootScope, $log, $state, $location, languageService, uiHelperService,
        storageService, authService, generalModalService, APP_CONFIG, APP_CONST, _) {

        var service = this;
        var nonAbstractStates = [];
        var authStates = [];
        var commonStates = [];
        var errorStates = [];

        // var statesHistory = [];

        function init() {
            var registeredStates = $state.get();
            var statesCount = registeredStates.length;

            for (var i = 0; i < statesCount; i++) {
                var state = registeredStates[i];

                // skip abstract states
                if (state.abstract) {
                    continue;
                }
                console.log('test');
                var statePrefix = state.name.substring(0, _.lastIndexOf(state.name, '.') + 1);

                // KIV : to be remove
                // raise error if screen code is not defined not non-dynamic states
                if (statePrefix !== APP_CONST.DYNAMIC_STATE_PREFIX && (!state.data || !state.data.screenCode)) {
                    $log.error('screen code not defined for state:', state.name);
                    continue;
                }

                switch (statePrefix) {
                    case APP_CONST.COMMON_STATE_PREFIX:
                        commonStates.push(state);
                        break;
                    case APP_CONST.AUTH_STATE_PREFIX:
                        authStates.push(state);
                        break;
                    case APP_CONST.ERROR_STATE_PREFIX:
                        errorStates.push(state);
                        break;
                    default:
                        $log.error('state not categorized:', state.name);
                        break;
                }

                nonAbstractStates.push(state);
            }

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                event.preventDefault();
                $log.error('$stateChangeError:', error);
                storageService.clearAll();
                service.goToLogin();
            });

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
                $log.debug('$stateChangeStart transition:', $state.transition);
                $log.debug('$stateChangeStart retryInProgress:', toState.retryInProgress);

                // prevent infinite loop - do not remove
                if (toState.retryInProgress) {
                    toState.retryInProgress = false;
                    return;
                }

                $log.debug('$stateChangeStart toState:', toState.name);
                $log.debug('$stateChangeStart fromState:', fromState.name);

                toState.retryInProgress = true;
                event.preventDefault();

                if (toState.name.indexOf(APP_CONST.AUTH_STATE_PREFIX) > -1 && !authService.getAuthToken()) {
                    $log.debug('$stateChangeStart, redirect to login');
                    var loginState = service.getStateFromPath(APP_CONFIG.SETTINGS.NOT_AUTHENTICATED_HOME.URL);
                    $state.transitionTo(loginState.name, null, {
                        reload: true
                    });

                    return;
                }

                // set current screen code to retrieve resources for non-dynamic states
                if (toState.name.indexOf(APP_CONST.DYNAMIC_STATE_PREFIX) === -1) {
                    storageService.set(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE, toState.data.screenCode);
                }

                $log.debug('$stateChangeStart, transitionTo:', toState.name);

                // use transitionTo to reload parent state, which will trigger base state resolve
                $state.transitionTo(toState.name, toParams, {
                    reload: true
                });
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $log.debug('$stateChangeSuccess');
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                $rootScope.backgroundTopPosition = 0;
                generalModalService.dismissModal();

                $rootScope.showOverlay = false;
                $rootScope.showLoading = false;
                $rootScope.$emit('toggleMenuMobile');

                var path = $state.href(toState.name);
                $log.debug('$stateChangeSuccess, location: ' + path);
            });

            $rootScope.$on('navigateToState', function(event, stateName, params) {
                $log.debug('navigateToState:', stateName);
                $log.debug(params);
            });

            $rootScope.$on('navigateToPath', function(event, path, params) {
                $log.debug('navigateToPath:', path);
                $log.debug(params);
            });

            $rootScope.$on('reloadState', function(event, params) {
                $log.debug('reloadState');
                $log.debug(params);
            });

            $rootScope.$on('backToPreviousState', function(event, params) {
                $log.debug('backToPreviousState');
                $log.debug(params);
            });

            $rootScope.$on('$locationChangeStart', function(event, newurl, oldurl) {
                $log.debug('$locationChangeStart:', event);
                $log.debug('$locationChangeStart newurl:', newurl);
                $log.debug('$locationChangeStart oldurl:', oldurl);

                var path = $location.path();
                $log.debug('$locationChangeStart path:', path);

                // handle root path, redirect to home page
                if (!path || path === '/') {
                    $log.debug('$locationChangeStart path empty, go to home url', service.getHomeUrl());
                    event.preventDefault();
                    $location.replace().path(service.getHomeUrl());
                    return;
                } else {
                    // handle invalid path, redirect to page not found
                    var aState = service.getStateFromPath(path);
                    if (!aState) {
                        event.preventDefault();
                        $location.path(service.getPageNotFoundUrl());
                        return;
                    }
                }
            });
        }

        init();

        // TODO: KIV
        // this.backToPreviousState = function() {
        //     var previousState = statesHistory.pop();

        //     if (!previousState) {
        //         service.goToHome();
        //         return;
        //     }

        //     service.goToState(service.getStateFromPath(previousState.path).name);
        // };

        this.goToScreenCode = function(screenCode) {
            $log.debug('go to screencode:', screenCode);
        };

        //TODO: revisit to clear which originally for dynamic screen
        this.goToPath = function(path, screenCode, params) {
            $log.debug('go to path:', path);

            if (!path) {
                service.goToHome();
                return;
            } else {
                var state = service.getStateFromPath(path);

                if (!state) {
                    $log.error('state not found for path:', path);
                    return;
                }

                $log.debug('go to static state:', state.name);
                service.goToState(state.name, params);

            }
        };

        this.getStateFromPath = function(path) {
            for (var i = 0; i < nonAbstractStates.length; i++) {
                var state = nonAbstractStates[i];

                if (state.url.toLowerCase() === path.toLowerCase()) {
                    return state;
                }
            }

            $log.debug('state not found for path:', path);
            return null;
        };

        this.reloadState = function(stateParams) {
            service.goToPath($location.path(), null, stateParams);
        };

        this.goToState = function(stateName, stateParams, forceReload) {
            $log.debug('go to state:', stateName);
            $log.debug('go to state:', stateParams);
            if (!stateName) {
                return;
            }

            stateParams = stateParams || {};
            var options = {
                reload: forceReload || true
            };

            $state.go(stateName, stateParams, options);
        };

        this.goToLogin = function() {
            $log.debug('go to login');

            // $state.go(service.getStateFromPath(APP_CONFIG.SETTINGS.NOT_AUTHENTICATED_HOME.URL));
            $location.replace().path(APP_CONFIG.SETTINGS.NOT_AUTHENTICATED_HOME.URL);
        };

        this.getHomeUrl = function() {
            $log.debug('getHomeUrl');
            //var authToken = authService.getAuthToken();

            /**if (authToken) {
                $log.debug('getHomeUrl, has authToken:', APP_CONFIG.SETTINGS.AUTHENTICATED_HOME.URL);
                return APP_CONFIG.SETTINGS.AUTHENTICATED_HOME.URL;
            }**/

            $log.debug('getHomeUrl, no authToken:', APP_CONFIG.SETTINGS.NOT_AUTHENTICATED_HOME.URL);
            return APP_CONFIG.SETTINGS.NOT_AUTHENTICATED_HOME.URL;
        };

        this.getPageNotFoundUrl = function() {
            return '/not-found';
        };

        this.logout = function(sumActivity) {
            $log.debug('log out');
            if (sumActivity) {
                $log.debug('go to summary of activities');
                storageService.set(APP_CONST.STORAGE_KEYS.SUM_ACTIVITY, sumActivity);
                return $location.replace().path('/summary-activity');
            }

            service.goToLogin();

            // $location.replace().path(service.getHomeUrl());
        };

        this.goToHome = function() {
            $log.debug('go to home');
            $location.replace().path(service.getHomeUrl());
        };

        this.goToPageNotFound = function() {
            $log.debug('go to page not found');
            $location.replace().path(service.getPageNotFoundUrl());
        };

        this.goToSessionExpired = function(response) {
            $log.debug('go to session expired');
            service.goToPath('/session-expired', null, {
                response: response
            });
        };

        this.getCurrentState = function() {
            return $state.current.name;
        };

    }

    angular.module('common').service(serviceName, routeService);
})();
