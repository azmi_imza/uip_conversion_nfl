(function() {
    'use strict';
    var serviceName = 'rootService';

    /*@ngInject*/
    var rootService = function($cookies, $timeout, $rootScope, $window, $log, $state, $location,
        $translate, routeService, storageService, sessionService, generalModalService, authService, uiHelperService,
        APP_CONFIG, APP_CONST) {

        $rootScope.$on('sessionexpired', function(e, response) {
            $log.debug('on sessionexpired');
            storageService.clearAll();
            storageService.set(APP_CONST.STORAGE_KEYS.IS_SESSION_EXPIRED, true, true);
            routeService.goToSessionExpired(response);
        });

        $rootScope.$on('logoutClient', function(event, sumActivity) {
            $log.debug('on logout client');

            if (!sumActivity) {
                storageService.clearAll();
            }

            routeService.logout(sumActivity);
        });

        $rootScope.$on('toggleMenuMobile', function(event, args) {
            if ($('.dl-menu').hasClass('menu-open')) {
                $('.dl-menuwrapper').css('-webkit-transform', 'translate3d(-' + $('.dl-menuwrapper').css('width') + ', 0, 0)');
                $('.dl-menuwrapper').css('-ms-transform', 'translate(-' + $('.dl-menuwrapper').css('width') + ', 0)');
                $('.dl-menuwrapper').css('transform', 'translate3d(-' + $('.dl-menuwrapper').css('width') + ', 0, 0)');
                $('#menu-overlay').removeClass('show');
                $('.dl-menu').removeClass('menu-open');

                $('#menu-overlay').css('height', '100%');
                $('.dl-menuwrapper').css('height', '100%');
            } else {
                if (args && args.toggle) {
                    var height = $('#uipContent').height() + $('#uipFooter').height();

                    //footer margin top 40px
                    $('#menu-overlay').css('height', height + 40 + 'px');

                    //footer margin top 40px, dl-menu offset 50px
                    $('.dl-menuwrapper').css('height', height + 90 + 'px');

                    $('.dl-menuwrapper').css('-webkit-transform', 'translate3d(0, 0, 0)');
                    $('.dl-menuwrapper').css('-ms-transform', 'translate(0, 0)');
                    $('.dl-menuwrapper').css('transform', 'translate3d(0, 0, 0)');
                    $('#menu-overlay').addClass('show');
                    $('.dl-menu').addClass('menu-open');
                }
            }
        });

        $rootScope.$watch(function() {
                return $('body').attr('class');
            },

            function(newVal) {
                if (newVal.indexOf('modal-open') > -1) {
                    $rootScope.showOverlay = true;
                    $rootScope.backgroundTopPosition = document.querySelector('body').getBoundingClientRect().top;
                    $('#maincontainer').addClass('disable-scroll');
                } else {
                    $rootScope.showOverlay = false;
                    $('#maincontainer').removeClass('disable-scroll');
                    window.scrollTo(0, $rootScope.backgroundTopPosition * -1);
                }
            });

        $rootScope.$on('startTimeoutAlertTimer', function() {
            sessionService.startTimeoutAlertTimer();
        });
    };

    angular.module('common')
        .service(serviceName, rootService);
})();
