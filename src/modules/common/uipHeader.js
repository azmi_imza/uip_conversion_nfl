(function() {
    'use strict';

    var directiveName = 'uipHeader';

    /*@ngInject*/
    function uipHeader() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: uipHeaderCtrl,
            templateUrl: 'templates/common/uipHeader.html'
        };
    }

    /*@ngInject*/
    function uipHeaderCtrl($state, $log, $rootScope, storageService, authService, rootService, routeService,
        generalModalService, APP_CONFIG, APP_CONST) {
        var ctrl = this;
        ctrl.goToLogin = goToLogin;
        ctrl.goToHome = goToHome;
        ctrl.logout = logout;
        ctrl.changePassword = changePassword;
        ctrl.selectedLang = APP_CONFIG.SETTINGS.DEFAULT_LANG.LABEL;
        ctrl.showLanguageSelector = showLanguageSelector;
        ctrl.countryFlagIconUrl = APP_CONFIG.SETTINGS.DEFAULT_LANG.ICON;
        ctrl.hasAuthToken = hasAuthToken;
        ctrl.isValid1FA = isValid1FA;

        $('html').click(function() {
            if ($('.red-man-menu').hasClass('expanded')) {
                $('.red-man-menu').removeClass('expanded');
                $('.red-man-sub-menu').removeClass('expanded');
            }
        });

        ctrl.toggleRedMan = function($event) {
            $event.stopPropagation();

            if ($('.uip-header__item .red-man-menu').hasClass('expanded')) {
                $('.uip-header__item .red-man-menu').removeClass('expanded');
                $('.uip-header__item .red-man-sub-menu').removeClass('expanded');
            } else {
                $('.uip-header__item .red-man-menu').addClass('expanded');
                $('.uip-header__item .red-man-sub-menu').addClass('expanded');
            }
        };

        function hasAuthToken() {
            return storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
        }

        function isValid1FA() {
            return storageService.get(APP_CONST.STORAGE_KEYS.VALID_1FA);
        }

        function goToHome() {
            $log.debug('go to home');
            routeService.goToHome();
        }

        function goToLogin() {
            if (hasAuthToken()) {
                logout();
                return;
            }

            routeService.goToLogin();
        }

        function logout() {
            $rootScope.$emit('logoutSession');
        }

        function changePassword() {
            routeService.goToState('base.auth.change-password');
        }

        function showLanguageSelector() {
            var template = '<div language-selector-modal></div>';
            var backdrop = 'static';
            var size = '';
            var resolve = {};

            var close = function(selectedLang) {
                if (selectedLang) {
                    $log.debug('Selected Language: ' + selectedLang.label);
                    ctrl.selectedLang = selectedLang.label;
                    ctrl.countryFlagIconUrl = selectedLang.iconUrl;
                }
            };

            generalModalService.openModalWithTemplate(template, backdrop, size, resolve, close);
        }
    }

    angular.module('common').directive(directiveName, uipHeader);
})();
