(function() {
    'use strict';
    var factoryName = 'resourceLoader';

    /*@ngInject*/
    var resourceLoader = function($log, storageService, languageService, $q, $http, $state, APP_CONFIG, APP_CONST) {

        return function() {
            var deferred = $q.defer();
            var commonScreenCode = 'SCRNCOMMON';
            var stateName = $state.current.name;

            function commonResourceErrorHandler(response) {
                $log.error('*** commonResourceErrorHandler ***');
                $log.error('failed to load language resources for state:', stateName);
                $log.error('error response:', response);
                deferred.reject(response);
            }

            function loadCommonResources(selectedlang, screenCode) {
                var commonResourceDeferred = $q.defer();

                var commonResourcePath = APP_CONFIG.API_PATHS.RESOURCE + '/common';
                loadResourceFromAPI(selectedlang, screenCode, commonResourcePath, commonResourceDeferred);

                return commonResourceDeferred.promise;
            }

            function loadModuleResources(selectedlang, screenCode) {
                var moduleResourceDeferred = $q.defer();

                if (screenCode === commonScreenCode) {
                    $log.debug('*** is common screen code, skip loading modules resources ***');
                    moduleResourceDeferred.resolve();
                } else {
                    var resourceApiPath = APP_CONFIG.API_PATHS.RESOURCE + '/' + screenCode;
                    loadResourceFromAPI(selectedlang, screenCode, resourceApiPath, moduleResourceDeferred);
                }

                return moduleResourceDeferred.promise;
            }

            function loadResourceFromAPI(selectedlang, screenCode, apiPath, resourcesDeferred) {
                var existingResources = languageService.getExistingResources(screenCode);

                if (existingResources[screenCode][selectedlang]) {
                    $log.debug('******* resoures exists for screencode:', screenCode, ', load from cache *******');
                    resourcesDeferred.resolve(existingResources[screenCode][selectedlang]);
                } else {
                    $log.debug('*** load resources from API for screencode:', screenCode, '***');
                    $http({
                        method: 'GET',
                        url: apiPath,
                        params: {
                            lang: selectedlang
                        },
                        timeout: 120000
                    }).success(function(response) {
                        var resources = languageService.parseJSONToResourceKey(response.data);
                        existingResources[screenCode][selectedlang] = resources;
                        languageService.setExistingResources(existingResources);

                        resourcesDeferred.resolve(resources);
                    }).error(function(reason) {
                        resourcesDeferred.reject(reason);
                    });
                }
            }

            function load() {
                var selectedlang = storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG) ||
                    APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE;

                loadCommonResources(selectedlang, commonScreenCode).then(function(commonResources) {
                    var screenCode = storageService.get(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE);

                    if (!screenCode) {
                        $log.debug('*** no module specific screencode, resolve common resources ***');
                        deferred.resolve(commonResources);
                        return;
                    }

                    loadModuleResources(selectedlang, screenCode, commonResources).then(function(moduleResources) {
                            if (moduleResources) {
                                deferred.resolve(angular.extend(moduleResources, commonResources));
                            } else {
                                $log.debug('*** no module resources, resolve common resources ***');
                                deferred.resolve(commonResources);
                            }
                        },

                        function moduleResourceErrorHandler(response) {
                            $log.error('*** moduleResourceErrorHandler ***');
                            var latestStateName = $state.current.name;
                            if (stateName !== latestStateName) {
                                $log.debug('latest state changed, reload resources');
                                $log.debug('previous state:', stateName);
                                $log.debug('latest state:', latestStateName);
                                stateName = latestStateName;

                                load();
                                return;
                            }

                            if (commonResources) {
                                deferred.resolve(commonResources);
                            } else {
                                commonResourceErrorHandler(response);
                            }
                        });
                }, commonResourceErrorHandler);
            }

            load();
            return deferred.promise;
        };

    };

    angular.module('common')
        .factory(factoryName, resourceLoader);
})();
