/* global confirm */
/* eslint no-alert:0 */

(function() {
    'use strict';
    var serviceName = 'sessionService';

    /*@ngInject*/
    var sessionService = function($http, $timeout, $rootScope, $log, routeService, storageService, authService, APP_CONST, APP_CONFIG) {
        var service = this;
        var timeoutTimer;
        var debounceTimer = null;
        var sessionRenewing = false;

        this.renewSession = function() {
            if (!sessionRenewing) {
                $log.debug('renew session');
                sessionRenewing = true;
                $http({
                    method: 'PUT',
                    url: APP_CONFIG.API_PATHS.SESSION_RENEW
                }).success(function() {
                    $log.debug('sessionService, renew session successful');

                }).error(function(response) {
                        $log.error('Unable to renew session:', response);

                        //TODO: Alert handling in proper way
                        storageService.clearAll();
                        $rootScope.$emit('sessionexpired', response);
                    }

                )['finally'](function() {
                    sessionRenewing = false;
                });
            }
        };

        this.logoutSession = function() {
            $log.debug('logout session');

            $timeout.cancel(timeoutTimer);
            $timeout.cancel(debounceTimer);

            if (!authService.getAuthToken()) {
                $rootScope.$emit('logoutClient');
            } else {
                authService.isAuthenticated().then(function() {
                        $http({
                            method: 'POST',
                            url: APP_CONFIG.API_PATHS.SESSION_LOGOUT
                        }).then(function(response) {
                                $log.debug('logout session success');
                                $rootScope.$emit('logoutClient', response.data);
                            },

                            function(response) {
                                $log.error('logout session failed:', response);

                                // if (response.status !== 401) {
                                //     $log.debug('status: ' + response.status);
                                //     $rootScope.$emit('logoutClient');
                                //     return;
                                // }

                                $rootScope.$emit('logoutClient');
                            });
                    },

                    function() {
                        routeService.goToLogin();
                    });
            }
        };

        this.onScopeDestroy = function() {
            $log.debug('session service, on scope destroy');
            $timeout.cancel(timeoutTimer);
            $timeout.cancel(debounceTimer);
        };

        this.startTimeoutAlertTimer = function() {
            timeoutTimer = $timeout(function() {
                $log.debug('session timer timeout');
                var authToken = storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
                if (authToken) {
                    alert('Your session will expire in ' + storageService.get(APP_CONST.STORAGE_KEYS.SESSION_REMAINING_TIME) + ' minutes');
                }
            }, storageService.get(APP_CONST.STORAGE_KEYS.SESSION_REMINDER_TIME));
        };

        var startDebounceTimer = function() {
            var authToken = storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
            if (debounceTimer === null && authToken) {

                service.renewSession();

                $log.debug('start debounce timer');
                debounceTimer = $timeout(function() {
                    //do nothing other than countdown
                }, APP_CONFIG.SETTINGS.DEBOUNCE_TIME);

                debounceTimer.then(function() {
                        debounceTimer = null;
                    },

                    function() {
                        debounceTimer = null;
                    });
            }

        };

        $(document).on('keyup', function() {
            $log.debug('document keyup');
            startDebounceTimer();
        });

        $(document).on('click', function() {
            $log.debug('document tap');
            startDebounceTimer();
        });

        $rootScope.$on('logoutSession', function() {
            service.logoutSession();
        });
    };

    angular.module('common')
        .service(serviceName, sessionService);
})();
