(function() {
    'use strict';

    var serviceName = 'languageService';

    /*@ngInject*/
    function languageService($q, $rootScope, $log, $translate, storageService, uiHelperService, APP_CONST, APP_CONFIG) {
        var service = this;

        // TODO: load countries and languages from REST API
        this.loadCountriesAndLanguages = function loadCountriesAndLanguages() {
            // TODO: To enable this section of code in the future for more selection. Now just stick with only SG English
            // return [{
            //     country: 'Singapore',
            //     iconUrl: 'images/icon_sg.png',
            //     languages: [{
            //         code: 'en_GB',
            //         label: 'English'
            //     }, {
            //         code: 'ms_MY',
            //         label: 'Malay'
            //     }, {
            //         code: 'cn_ZH',
            //         label: '中文'
            //     }]
            // }, {
            //     country: 'Malaysia',
            //     iconUrl: 'images/icon_msia.png',
            //     languages: [{
            //         code: 'ms_MY',
            //         label: 'Malay'
            //     }, {
            //         code: 'en_GB',
            //         label: 'English'
            //     }]
            // }];

            return [{
                country: 'Singapore',
                iconUrl: 'images/icon_sg.png',
                languages: [{
                    code: 'en_GB',
                    label: 'English'
                }]
            }];
        };

        this.getExistingResources = function getExistingResources(screenCode) {
            var existingResources = storageService.get(APP_CONST.STORAGE_KEYS.LANG_RESOURCES);

            if (!existingResources) {
                existingResources = {};
            }

            if (!existingResources[screenCode]) {
                existingResources[screenCode] = {};
            }

            return existingResources;
        };

        this.setExistingResources = function setExistingResources(existingResources) {
            storageService.set(APP_CONST.STORAGE_KEYS.LANG_RESOURCES, existingResources);
        };

        this.refreshLangugeResource = function refreshLangugeResource(resourceData) {
            var deferred = $q.defer();
            var languageCode = storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG);

            var deregisterFn = $rootScope.$on('$translateLoadingEnd', function() {
                $log.debug('****** $translateLoadingEnd ******');
                uiHelperService.resetViewport();
                uiHelperService.hideLoader();
                deferred.resolve();
                deregisterFn();
            });

            if (!languageCode) {
                languageCode = APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE;
                storageService.set(APP_CONST.STORAGE_KEYS.SELECTED_LANG, languageCode);
            }

            if (resourceData) {
                var formattedResourceData = service.parseJSONToResourceKey(resourceData);
                var currentScreenCode = storageService.get(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE);
                var existingResources = service.getExistingResources(currentScreenCode);

                existingResources[currentScreenCode][languageCode] = formattedResourceData;
                service.setExistingResources(existingResources);
            }

            $translate.use(languageCode);
            $translate.refresh();

            return deferred.promise;
        };

        this.parseJSONToResourceKey = function parseJSONToResourceKey(resourceData) {
            /************************************\
                Resouce Data Format
            ======================================
            {
              data: [{
                rkey: "cfo.label.firstname",
                value: "First Name"
              }, {
                rkey: "cfo.message.helloworld",
                value: "Hello World"
              }]
            }
            \************************************/

            var tempObj = {};
            if (resourceData.length > 0) {
                for (var i = 0; i < resourceData.length; i++) {
                    var tempItem = resourceData[i];
                    tempObj[tempItem.rkey] = tempItem.value;
                }
            }

            return tempObj;
        };
    }

    angular.module('common').service(serviceName, languageService);
})();
