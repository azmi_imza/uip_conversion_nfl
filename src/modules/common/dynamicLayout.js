(function() {
    'use strict';

    var directiveName = 'dynamicLayout';

    /*@ngInject*/
    function dynamicLayout() {
        return {
            bindToController: true,
            controller: dynamicLayoutCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/common/dynamicLayout.html'
        };
    }

    /*@ngInject*/
    function dynamicLayoutCtrl() {
        //$element, $compile, $scope, $log, $timeout,
        //  storageService, routeService, languageService, APP_CONST, $resource) {
        // $log.debug('******************dynamic layout*****************');
        // var ctrl = this;

        // var url = storageService.get(APP_CONST.STORAGE_KEYS.DYNAMIC_LAYOUT);

        // // $resource(url).get({}, loadSuccess, loadFailed);

        // function loadSuccess(data) {
        //     var dynamicScreenData = data;

        //     ctrl.additionalInfo = dynamicScreenData['additional_info'];
        //     ctrl.layoutData = angular.fromJson(data);

        //     ctrl.initialized = true;
        // }

        // function loadFailed() {
        //     $log.debug('fail to load layout');
        //     routeService.goToHome();
        // }

    }

    angular.module('common').directive(directiveName, dynamicLayout);
})();
