(function() {
    'use strict';
    var serviceName = 'generalModalService';

    /*@ngInject*/
    function generalModalService($modal, uiHelperService) {

        var service = this;
        this.modalStorage = {};
        this.modalInstance = undefined;

        function openModal(modalOptions) {
            service.modalInstance = $modal.open(modalOptions);
        }

        this.openModal = function(templateUrl, ctrl, backdrop, size, resolve, close, dismiss, windowClass) {
            windowClass = windowClass || '';
            openModal({
                templateUrl: templateUrl,
                controller: ctrl,
                backdrop: backdrop,
                size: size,
                resolve: resolve,
                windowClass: windowClass
            });

            service.modalInstance.result.then(close, dismiss);
        };

        this.openModalWithTemplate = function(template, backdrop, size, resolve, close, dismiss, windowClass) {
            windowClass = windowClass || '';
            service.modalInstance = $modal.open({
                template: template,
                backdrop: backdrop,
                size: size,
                resolve: resolve,
                windowClass: windowClass
            });

            service.modalInstance.result.then(close, dismiss);
        };

        this.dismissModal = function(response) {
            if (service.modalInstance) {
                service.modalInstance.dismiss(response);
                uiHelperService.resetViewport();
            }
        };

        this.closeModal = function(response) {
            if (service.modalInstance) {
                service.modalInstance.close(response);
                uiHelperService.resetViewport();
            }
        };
    }

    angular.module('common')
        .service(serviceName, generalModalService);
})();
