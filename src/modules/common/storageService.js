(function() {
    'use strict';
    var serviceName = 'storageService';

    /*@ngInject*/
    var storageService = function($cookies, APP_CONST) {
        var storage = {};
        var service = this;

        this.get = function(key) {
            if ($cookies[key]) {
                return angular.fromJson($cookies[key])[key];
            }

            if (storage[key]) {
                return angular.fromJson(storage[key])[key];
            }

            return null;
        };

        this.set = function(key, value, isPersistent) {
            var clonedValue = angular.copy(value);
            var obj = {};
            obj[key] = clonedValue;
            if (isPersistent) {
                $cookies[key] = angular.toJson(obj);
            } else {
                storage[key] = angular.toJson(obj);
            }
        };

        this.remove = function(key) {
            delete storage[key];
        };

        this.removeCookie = function(key) {
            delete $cookies[key];
        };

        this.clearAll = function() {
            var langResources = service.get(APP_CONST.STORAGE_KEYS.LANG_RESOURCES);

            storage = {};
            this.clearCookies();

            service.set(APP_CONST.STORAGE_KEYS.LANG_RESOURCES, langResources);
        };

        this.clearCookies = function() {
            for (var c in $cookies) {
                if ($cookies.hasOwnProperty(c)) {
                    delete $cookies[c];
                }
            }
        };
    };

    angular.module('common')
        .service(serviceName, storageService);
})();
