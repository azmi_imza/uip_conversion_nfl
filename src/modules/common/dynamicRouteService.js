(function() {
    'use strict';

    var serviceName = 'dynamicRouteService';

    /*@ngInject*/
    function dynamicRouteService($log) {
        $log.debug('dynamic route service');
    }

    angular.module('common').service(serviceName, dynamicRouteService);
})();
