(function() {
    'use strict';

    var directiveName = 'cssTemplate';

    /*@ngInject*/
    function cssTemplate() {
        return {
            bindToController: true,
            controller: cssTemplateCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/common/css-template.html'
        };
    }

    function cssTemplateCtrl(FormManager, $timeout, $resource, $window, $scope) {
        var ctrl = this;
        var passMessage = {};
        var passMessage2 = {};
        var failedMessage = {};
        var failedMessage2 = {};

        ctrl.formModel = {};
        ctrl.config = {};

        ctrl.config.policy = {
            name: 'ddl1',
            mode: 'edit',
            label: {
                name: 'DDL 1  (with component)',
                tooltip: '',
                tooltipPlacement: ''
            },
            ddlItems: [{
                label: '1',
                value: '1'
            }, {
                label: '2',
                value: '2'
            }, {
                label: '3',
                value: '3'
            }]

        };

        ctrl.config.requestType = {
            name: 'ddl2',
            mode: 'edit',
            label: {
                name: 'DDL 2  (with component)',
                tooltip: '',
                tooltipPlacement: ''
            },
            ddlItems: [{
                label: 'one two three',
                value: '1'
            }, {
                label: 'one two three one two three one two three',
                value: '2'
            }, {
                label: 'one two three one two three one two three one two three one two three one two three',
                value: '3'
            }]
        };

        ctrl.config.testing = {
            name: 'testing',
            mode: 'edit',
            label: {
                name: 'Field 1 (with component)',
                tooltip: '',
                tooltipPlacement: ''
            }
        };

        ctrl.config.testing2 = {
            name: 'testing2',
            mode: 'edit',
            label: {
                name: 'Field 2 (with component)',
                tooltip: '',
                tooltipPlacement: ''
            }
        };

        ctrl.config.testing3 = {
            name: 'testing3',
            mode: 'view',
            label: {
                name: 'Field 3 (with component)',
                tooltip: '',
                tooltipPlacement: ''
            }
        };

        ctrl.config.testing4 = {
            name: 'testing4',
            mode: 'view',
            label: {
                name: 'Field 4 (with component)',
                tooltip: '',
                tooltipPlacement: ''
            }
        };

        ctrl.config.testing5 = {
            name: 'testing',
            mode: 'edit',
            label: {
                name: 'Date Field',
                tooltip: '',
                tooltipPlacement: ''
            },
            format: 'dd MMM yyyy'
        };

        ctrl.config.areaCode = {
            name: 'areaCode',
            mode: 'edit',
            label: {
                name: 'Personal Contact Mobile',
                tooltip: ''
            },
            ddlType: 'static',
            ddlItems: [{
                label: '01',
                value: '1'
            }, {
                label: '02',
                value: '2'
            }, {
                label: '03',
                value: '3'
            }],
            columnCssClass: 'col-xs-3 personal-details__input',
            labelCssClass: 'personal-details-input-label',
            inputCssClass: 'col-xs-12'
        };

        ctrl.config.number = {
            name: 'number',
            mode: 'edit',
            label: {
                name: 'Number',
                tooltip: ''
            },
            columnCssClass: 'col-xs-9 personal-details__input',
            labelCssClass: 'personal-details-input-label',
            inputCssClass: 'two-fields-padding'
        };

        ctrl.passContentDetails = [];
        passMessage.message = 'Your request to (insert service request type) has been successfully submitted.';
        passMessage2.message = 'Reference no.: xxxxxxxxxxxxx';
        ctrl.passContentDetails.push(passMessage);
        ctrl.passContentDetails.push(passMessage2);

        ctrl.failedContentDetails = [];
        failedMessage.message = 'Your request for (insert transaction type) has not been successfully processed.';
        failedMessage2.message = 'Please contact our Customer Service at xxxxxxxxxxxxxx.';
        ctrl.failedContentDetails.push(failedMessage);
        ctrl.failedContentDetails.push(failedMessage2);

        ctrl.config.policy.formManager = new FormManager(ctrl.config.policy.name, ctrl.config.policy.mode, ctrl);
        ctrl.config.requestType.formManager = new FormManager(ctrl.config.requestType.name, ctrl.config.requestType.mode, ctrl);
        ctrl.config.testing.formManager = new FormManager(ctrl.config.testing.name, ctrl.config.testing.mode, ctrl);
        ctrl.config.testing2.formManager = new FormManager(ctrl.config.testing2.name, ctrl.config.testing2.mode, ctrl);
        ctrl.config.testing3.formManager = new FormManager(ctrl.config.testing3.name, ctrl.config.testing3.mode, ctrl);
        ctrl.config.testing4.formManager = new FormManager(ctrl.config.testing4.name, ctrl.config.testing4.mode, ctrl);
        ctrl.config.testing5.formManager = new FormManager(ctrl.config.testing5.name, ctrl.config.testing5.mode, ctrl);
        ctrl.config.areaCode.formManager = new FormManager(ctrl.config.areaCode.name, ctrl.config.areaCode.mode, ctrl);
        ctrl.config.number.formManager = new FormManager(ctrl.config.number.name, ctrl.config.number.mode, ctrl);

        ctrl.testingList = [{
            name: 'abc',
            date: '17-Dec-2001',
            amount: 200.00
        }, {
            name: 'def',
            date: '6-Jan-2012',
            amount: 350.00
        }];

        ctrl.pageNumberFormat = {
            type: 'currency',
            attributes: {
                currencyFormat: false,
                currencyInteger: 3,
                currencyDecimal: 0
            }
        };

        ctrl.initTable = function(id) {
            $timeout(function() {
                $('#' + id).footable();
            }, 0);
        };

        ctrl.config.testing3.formManager.setValue('testing3', 'hello i\'m from component');
        ctrl.config.testing4.formManager.setValue('testing4', 'hello i\'m from component');

        //---- variable and functions to be use by collpasible table UI -------------------//
        ctrl.policyTableTitle = {
            health: 'Health Protection',
            PA: 'Personal Accident Protection',
            wealth: 'Wealth Accumulation',
            retirement: 'Retirement Planning',
            lifestyle: 'LifeStyle Protection',
            life: 'Life Protection'
        };
        ctrl.policyTableHeader = {
            planName: 'Plan Name',
            policyNo: 'Policy No',
            premium: 'Premium (SGD)',
            frequency: 'Frequency',
            paymentMethod: 'Payment Method',
            policyStatus: 'Policy Status',
            sumAssured: 'Sum Assured (SGD)',
            surrenderValue: 'Surrender Value(SGD)',
            lifeAssured: 'Life Assured',
            policyStartDate: 'Policy Start Date',
            coverageEndDate: 'Coverage End Date',
            nextPremiumDueDate: 'Next Premium Due Date',
            nominationExists: 'Nomination Exists',
            nominateEffectiveDate: 'N.Effective Date'
        };

        ctrl.policyTable = [{
            planName: 'Supreme Health Plan',
            policyNo: '101010765',
            premium: '353.00',
            frequency: 'Yearly',
            paymentMethod: 'CPF',
            policyStatus: 'inforce',
            sumAssured: '-',
            surrenderValue: '-',
            lifeAssured: '-',
            startDate: '-',
            endDate: '30 Sep 2015',
            nextPremiumDueDate: '01 Oct 2015',
            nomination: '-',
            nextEffectiveDate: '-'
        }, {
            planName: 'TotalShield',
            policyNo: '101010563',
            premium: '448.00',
            frequency: 'Yearly',
            paymentMethod: 'GIRO',
            policyStatus: 'inforce',
            sumAssured: '-',
            surrenderValue: '-',
            lifeAssured: 'Judy Tan',
            startDate: '01 Oct 2014',
            endDate: '30 Sep 2015',
            nextPremiumDueDate: '01 Oct 2015',
            nomination: '-',
            nextEffectiveDate: '-'
        }];

        $window.onresize = function() {
            changeTextTemplate();
            $scope.$apply();
        };

        changeTextTemplate();

        function changeTextTemplate() {
            var screenWidth = $window.innerWidth;
            if (screenWidth < 546) {
                ctrl.policyTableHeader.paymentMethod = 'Payment Method:';
            } else {
                ctrl.policyTableHeader.paymentMethod = 'Payment Method';
            }
        }

        // ------------------------------------------------------------------------------//
        /*************************************
         Methods to load layout
        *************************************/

        var url = 'data/claim_layout/sampleLayout.json';

        $resource(url).get({}, loadSuccess, loadFailed);

        function loadSuccess(data) {

            ctrl.layoutData = angular.fromJson(data);

            ctrl.initialized = true;
        }

        function loadFailed() {
            // $log.debug('fail to load layout');
            // routeService.goToHome();
        }

    }

    angular.module('common').directive(directiveName, cssTemplate);
})();
