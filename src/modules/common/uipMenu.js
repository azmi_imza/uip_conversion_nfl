(function() {
    'use strict';
    var directiveName = 'uipMenu';

    /*@ngInject*/
    var uipMenu = function() {
        return {
            restrict: 'EA', // restrict to use directive by attribute only
            scope: {
                menuSource: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: menuCtrl,
            templateUrl: 'templates/common/uipMenu.html'
        };
    };

    /*@ngInject*/
    var menuCtrl = function($rootScope, $log, $timeout, $http, routeService, storageService, APP_CONST) {

        var ctrl = this;
        var menus = [];
        menus = storageService.get(APP_CONST.STORAGE_KEYS.MENU_DATA);
        ctrl.notificationLimit = 3;
        ctrl.notificationShow = angular.copy(ctrl.notificationLimit);
        ctrl.showUipMenu = true;

        ctrl.goToPath = function(path, screenCode) {
            if (path) {
                routeService.goToPath(path, screenCode);
            }
        };

        ctrl.toggleNotification = function($event, length) {
            $event.stopPropagation();
            ctrl.notificationShow = length;
        };

        var hideSubMenu = function() {
            /*need to use hide class to control due to css rendering issue in safari*/
            $('.menu-bar .sub-menu').addClass('hide');
            $('.menu-bar .sub-menu').removeClass('expanded');
            $('.menu-bar .main-menu').removeClass('expanded');
            $('.menu-bar .divider').removeClass('hide');
        };

        $timeout(function() {
            $('.uip-menu__red-man-wrapper .red-man-menu').on('click', function(event) {
                event.stopPropagation();

                if ($('.uip-menu__red-man-wrapper .red-man-menu').hasClass('expanded')) {
                    $('.uip-menu__red-man-wrapper .red-man-menu').removeClass('expanded');
                    $('.uip-menu__red-man-wrapper .red-man-sub-menu').removeClass('expanded');
                } else {
                    $('.uip-menu__red-man-wrapper .red-man-menu').addClass('expanded');
                    $('.uip-menu__red-man-wrapper .red-man-sub-menu').addClass('expanded');
                }
            });

            $('.hamburger').on('click', function() {
                $rootScope.$emit('toggleMenuMobile', {
                    toggle: true
                });
            });

            $('html').click(function() {
                hideSubMenu();
            });

            $('.menu-bar .main-menu').on('click', function(event) {
                event.stopPropagation();
                hideSubMenu();

                var subMenu = $(this).parent().find('.sub-menu');
                var prevDivider = $(this).parent().parent().prev().find('.divider');
                var nextDivider = $(this).parent().next();

                if (subMenu[0]) {
                    subMenu.removeClass('hide');
                    subMenu.addClass('expanded');
                    $(this).addClass('expanded');
                    prevDivider.addClass('hide');
                    nextDivider.addClass('hide');
                }
            });

            $('.menu-bar .main-menu').on('mouseover', function() {
                var subMenu = $(this).parent().find('.sub-menu');
                var prevDivider = $(this).parent().parent().prev().find('.divider');
                var nextDivider = $(this).parent().next();

                if (subMenu[0]) {
                    subMenu.removeClass('hide');
                    subMenu.addClass('expanded');
                    $(this).addClass('expanded');
                    prevDivider.addClass('hide');
                    nextDivider.addClass('hide');
                }
            });

            $('.menu-bar .menu-block').on('mouseleave', function() {
                hideSubMenu();
            });

            /* $('.menu-bar .sub-menu').each(function() {
                 var subMenu = $(this);

                 var subMenuPaddingWidth = parseInt(subMenu.css('padding-left')) + parseInt(subMenu.css('padding-right'));
                 var subMenuContentWidth = 0;
                 subMenu.children().each(function() {
                     subMenuContentWidth += $(this).width();
                 });

                 var subMenuWidth = subMenuPaddingWidth + subMenuContentWidth + 10; //10px to add safety measure

                 subMenu.css('width', subMenuWidth);
             });*/
        }, 0);

        //sample notification for coming notification development use
        /*dummy notification [s]*/
        /*var notification = {};
        notification.rkey = 'Notification';
        notification.submenu = [];

        for (var n = 0; n < 5; n++) {
            var item = {
                title: 'title ' + n,
                date: 'date ' + n,
                message: 'message ' + n,
                action: 'View'
            };
            notification.submenu.push(item);
        }

        menus.push(notification);*/

        /*dummy notification [e]*/

        if (routeService.getCurrentState().indexOf(APP_CONST.COMMON_STATE_PREFIX) > -1) {
            ctrl.showUipMenu = false;
        }

        if (menus && menus.length > 0) {
            for (var i = 0; i < menus.length; i++) {
                if (menus[i]['menu_cd'] === 'CFO_00035' || menus[i]['menu_cd'] === 'CFO_00002') {
                    menus.splice(i, 1);
                    i--;
                }
            }
        }

        ctrl.menus = menus;

    };

    angular.module('common')
        .directive(directiveName, uipMenu);
})();
