(function() {
    'use strict';
    var factoryName = 'localResourceLoader';

    /*@ngInject*/
    var localResourceLoader = function($rootScope, $log, $q, $http, $state, storageService, APP_CONST) {
        return function() {
            $log.debug('*load language resources*');
            $rootScope.showOverlay = true;
            $rootScope.showLoading = true;
            var deferred = $q.defer();

            var selectedlang = storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG) || 'en_gb';
            selectedlang = selectedlang.toLowerCase();

            var commonResourcePath = 'data/resources/common/' + selectedlang + '.json';

            var resources;
            var stateName = $state.current.name.replace(APP_CONST.AUTH_STATE_PREFIX, '');

            function commonResourceErrorHandler(response) {
                $log.error('failed to load language resources for module:', stateName);
                $log.error('error response:', response);
                $rootScope.showOverlay = false;
                $rootScope.showLoading = false;
            }

            function moduleResourceErrorHandler(response) {
                var latestStateName = $state.current.name.replace(APP_CONST.AUTH_STATE_PREFIX, '');
                if (stateName !== latestStateName) {
                    $log.debug('latest state changed, reload resources');
                    stateName = latestStateName;
                    load();
                    return;
                }

                if (resources) {
                    deferred.resolve(resources);
                }

                commonResourceErrorHandler(response);
            }

            function load() {
                $http({
                    method: 'GET',
                    url: commonResourcePath
                }).success(function(commonResourceData) {
                    resources = commonResourceData;
                    var moduleResourcePath = 'data/resources/' + stateName + '/' + selectedlang + '.json';

                    $http({
                        method: 'GET',
                        url: moduleResourcePath
                    }).success(function(moduleResourceData) {
                        angular.extend(resources, moduleResourceData);
                        deferred.resolve(resources);
                        $rootScope.showOverlay = false;
                        $rootScope.showLoading = false;
                    }).error(moduleResourceErrorHandler);
                }).error(commonResourceErrorHandler);
            }

            load();
            return deferred.promise;
        };
    };

    angular.module('common').factory(factoryName, localResourceLoader);
})();
