(function() {
    'use strict';
    var directiveName = 'commonView';

    /*@ngInject*/
    var commonView = function() {
        return {
            restrict: 'EA',
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: commonViewCtrl,
            templateUrl: 'templates/common/commonView.html'
        };
    };

    /*@ngInject*/
    var commonViewCtrl = function($scope, $timeout, uiHelperService, $state) {

        var ctrl = this;
        ctrl.menuSource = 'data/menu/menu.json';

        ctrl.backToTop = function() {
            uiHelperService.scrollTo(0);
        };

        function checkScroll() {

            if ($state.is('base.common.login')) {
                ctrl.showBackToTop = false;
                return;
            }

            if ($('body').height() > $(window).height()) {
                ctrl.showBackToTop = true;
            } else {
                ctrl.showBackToTop = false;
            }
        }

        function onResize() {
            checkScroll();
            $scope.$apply();
        }

        function onScroll() {

            if ($state.is('base.common.login')) {
                ctrl.showBackToTop = false;
                return;
            }

            if ($(window).width() > 500) {
                ctrl.backToTopFixed = false;
            } else {
                if ($(window).scrollTop() + ($(window).height()) + $('#uipFooter').height() >= $('body').height()) {
                    ctrl.backToTopFixed = false;
                } else {
                    if ($(window).scrollTop() < 200) {
                        ctrl.backToTopFixed = false;
                    } else {
                        ctrl.backToTopFixed = true;
                    }
                }
            }

            $scope.$apply();
        }

        //check on content change
        var watchScroll = $scope.$watch(function() {
                return $('body').height();
            },

            function() {
                checkScroll();
            }

        );

        //check on resize (need this due to delay in scope watch)
        $(window).on('resize', onResize);

        $(window).on('scroll', onScroll);

        //check on init
        $timeout(function() {
            checkScroll();
        });

        $scope.$on('$destroy', function() {
            watchScroll();
            $(window).off('resize', onResize);
            $(window).off('scroll', onScroll);
        });
    };

    angular.module('common')
        .directive(directiveName, commonView);
})();
