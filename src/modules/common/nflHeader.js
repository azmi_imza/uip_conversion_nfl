(function() {
    'use strict';

    var directiveName = 'nflHeader';

    /*@ngInject*/
    function nflHeader() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nflHeaderCtrl,
            templateUrl: 'templates/common/nflHeader.html'
        };
    }

    /*@ngInject*/
    function nflHeaderCtrl($state, $log,rootService, routeService){
        var ctrl = this;
        ctrl.goToHome = function () {
            routeService.goToHome();
        }
    }

    angular.module('common').directive(directiveName, nflHeader);
})();