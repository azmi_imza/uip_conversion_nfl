(function() {
    'use strict';
    var directiveName = 'menu';

    /*@ngInject*/
    var menu = function() {
        return {
            restrict: 'EA',
            scope: {
                menuSource: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: menuCtrl,
            templateUrl: 'templates/common/menu.html'
        };
    };

    /*@ngInject*/
    var menuCtrl = function($rootScope, routeService, storageService, APP_CONST) {

        var ctrl = this;
        ctrl.logoutSession = function() {
            $rootScope.$emit('logoutSession');
        };

        ctrl.changePassword = function() {
            routeService.goToState('base.auth.change-password');
        };

        ctrl.hasAuthToken = function() {
            return storageService.get(APP_CONST.STORAGE_KEYS.AUTH_TOKEN);
        };

        ctrl.isValid1FA = function() {
            return storageService.get(APP_CONST.STORAGE_KEYS.VALID_1FA);
        };
    };

    angular.module('common')
        .directive(directiveName, menu);
})();
