/* global PDFObject */
/* eslint new-cap:0 */

(function() {
    'use strict';
    var directiveName = 'pdfModal';

    /*@ngInject*/
    function pdfModal() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: pdfModalCtrl,
            templateUrl: 'templates/common/pdfModal.html'
        };
    }

    /*@ngInject*/
    function pdfModalCtrl($attrs, $timeout, generalModalService) {
        var ctrl = this;
        ctrl.modalTitle = $attrs.modalTitle;

        ctrl.close = function() {
            generalModalService.closeModal();
        };

        ctrl.redirect = function() {
            window.open($attrs.url, '_blank');
        };

        $timeout(function() {
            PDFObject({
                url: $attrs.url
            }).embed('pdfFrame');
        });
    }

    angular.module('common').directive(directiveName, pdfModal);
})();
