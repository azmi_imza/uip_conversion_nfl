(function() {
    'use strict';
    var directiveName = 'uipMenuMobile';

    /*@ngInject*/
    var uipMenuMobile = function() {
        return {
            restrict: 'EA', // restrict to use directive by attribute only
            scope: {
                menuSource: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: menuMobileCtrl,
            templateUrl: 'templates/common/uipMenuMobile.html'
        };
    };

    /*@ngInject*/
    var menuMobileCtrl = function($rootScope, $log, $timeout, $http, routeService, storageService, APP_CONST) {

        var ctrl = this;
        var menus = [];
        menus = storageService.get(APP_CONST.STORAGE_KEYS.MENU_DATA);

        ctrl.notificationShow = 3;
        ctrl.showUipMenu = true;

        ctrl.goToPath = function(path, screenCode) {
            if (path) {
                routeService.goToPath(path, screenCode);
            }
        };

        ctrl.readMore = function($event, length) {
            $event.stopPropagation();
            ctrl.notificationShow = length;
        };

        /* Collapse menu at initial (ie) */
        function initialSubMenu() {
            $('.dl-menuwrapper').css('-webkit-transform', 'translate3d(-' + $('.dl-menuwrapper').css('width') + ', 0, 0)');
            $('.dl-menuwrapper').css('-ms-transform', 'translate(-' + $('.dl-menuwrapper').css('width') + ', 0)');
            $('.dl-menuwrapper').css('transform', 'translate3d(-' + $('.dl-menuwrapper').css('width') + ', 0, 0)');
        }

        initialSubMenu();

        $timeout(function() {
            $('#dl-menu').dlmenu({
                animationClasses: {
                    classin: 'dl-animate-in-2',
                    classout: 'dl-animate-out-2'
                }
            });

            $('#menu-overlay').on('click', function() {
                $rootScope.$emit('toggleMenuMobile');
            });

        }, 0);

        //sample notification for coming notification development use
        /*dummy notification [s]*/
        /* var notification = {};
         notification.rkey = 'Notification';
         notification.submenu = [];

         for (var n = 0; n < 5; n++) {
             var item = {
                 title: 'title ' + n,
                 date: 'date ' + n,
                 message: 'message ' + n,
                 action: 'View'
             };
             notification.submenu.push(item);
         }

         menus.push(notification);*/

        /*dummy notification [e]*/

        if (routeService.getCurrentState().indexOf(APP_CONST.COMMON_STATE_PREFIX) > -1) {
            ctrl.showUipMenu = false;
        }

        if (menus && menus.length > 0) {
            for (var i = 0; i < menus.length; i++) {
                if (menus[i]['menu_cd'] === 'CFO_00035' || menus[i]['menu_cd'] === 'CFO_00002') {
                    menus.splice(i, 1);
                    i--;
                }
            }
        }

        ctrl.menus = menus;
    };

    angular.module('common')
        .directive(directiveName, uipMenuMobile);
})();
