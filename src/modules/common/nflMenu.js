(function() {
    'use strict';
    var directiveName = 'nflMenu';

    /*@ngInject*/
    var nflMenu = function() {
        return {
            templateUrl: 'templates/common/nflMenu.html'
        };
    };


    angular.module('common').directive(directiveName, nflMenu);
})();
