(function() {
    'use strict';

    var directiveName = 'cssTemplate2';

    /*@ngInject*/
    function cssTemplate2() {
        return {
            bindToController: true,
            controller: cssTemplate2Ctrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/common/css-template-2.html'
        };
    }

    function cssTemplate2Ctrl($log, FormManager) {
        var ctrl = this;
        ctrl.collapsed = true;
        ctrl.test = 'Test 2';
        ctrl.draftIcon = 'images/claim_draft_icon.png';

        ctrl.iconActive = [{
            state: true,
            iconImage: 'images/claim_draft_icon.png',
            count: 99,
            text: 'Draft'
        }, {
            state: false,
            iconImage: 'images/claim_pending_icon.png',
            count: 99,
            text: 'Pending'
        }, {
            state: false,
            iconImage: 'images/claim_approved_icon.png',
            count: 99,
            text: 'Approved'
        }, {
            state: false,
            iconImage: 'images/claim_rejected_icon.png',
            count: 99,
            text: 'Rejected'
        }];

        ctrl.setBubbleIconStyle = function(index) {
            if (index === 0 || index === 1 || index === 2 || index === 3) {
                return {
                    background: '#eeeeee url(' + ctrl.iconActive[index].iconImage + ') center center no-repeat'
                };
            }
        };

        ctrl.iconClickEvent = function(index) {
            for (var i = 0; i < ctrl.iconActive.length; i++) {
                if (ctrl.iconActive[i].state === true) {
                    ctrl.iconActive[i].state = !ctrl.iconActive[i].state;
                }
            }

            ctrl.iconActive[index].state = !ctrl.iconActive[index].state;
        };

        ctrl.testString = 'Cras quis: nulla commodo <br/>' +
            'Aliquam: lectus sed, blandit augue. <br/>' +
            'Cras ullamcorper: bibendum bibendum. <br/>' +
            'Cras: quis nulla commodo Aliquam lectus sed, blandit augue.';

        ctrl.testArray = [{
            text: ctrl.testString
        }, {
            text: ctrl.testString
        }, {
            text: ctrl.testString
        }];

        ctrl.arrowIcon = 'images/arrow_up.png';

        ctrl.formModel = {};
        ctrl.config = {};

        ctrl.config.search1 = {
            name: 'ddl1',
            mode: 'edit',
            label: {
                name: 'DDL 1  (with component)',
                tooltip: '',
                tooltipPlacement: ''
            },
            ddlItems: [{
                label: '1',
                value: '1'
            }, {
                label: '2',
                value: '2'
            }, {
                label: '3',
                value: '3'
            }]

        };
        ctrl.changeArrowIcon = function() {
            ctrl.collapsed = !ctrl.collapsed;

            if (ctrl.collapsed === true) {
                ctrl.arrowIcon = 'images/arrow_up.png';
            } else if (ctrl.collapsed === false) {
                ctrl.arrowIcon = 'images/arrow_down.png';
            }
        };

        ctrl.changeArrowIcon();

        ctrl.config.search2 = {
            name: 'ddl2',
            mode: 'edit',
            label: {
                name: 'DDL 2  (with component)',
                tooltip: '',
                tooltipPlacement: ''
            },
            ddlItems: [{
                label: '1',
                value: '1'
            }, {
                label: '2',
                value: '2'
            }, {
                label: '3',
                value: '3'
            }]

        };

        ctrl.config.datePicker1 = {
            name: 'datePicker1',
            mode: 'edit',
            label: {
                name: 'Date Field 1',
                tooltip: '',
                tooltipPlacement: ''
            },
            format: 'dd MMM yyyy'
        };

        ctrl.config.datePicker2 = {
            name: 'datePicker2',
            mode: 'edit',
            label: {
                name: 'Date Field 2',
                tooltip: '',
                tooltipPlacement: ''
            },
            format: 'dd MMM yyyy'
        };

        ctrl.config.searchDdl = {
            name: 'searchDdl',
            mode: 'edit',
            label: {
                name: 'DDL (Component)',
                tooltip: '',
                tooltipPlacement: ''
            },
            ddlItems: [{
                label: '1',
                value: '1'
            }, {
                label: '2',
                value: '2'
            }, {
                label: '3',
                value: '3'
            }]

        };

        ctrl.testingTableTitle = {
            column1: 'Column Title',
            column2: 'Column Title',
            column3: 'Column Title',
            column4: 'Column Title',
            column5: 'Column Title'
        };

        ctrl.testingTabledata = [{
            column1: 'Neque Porro',
            column2: 'Neque Porro',
            column3: 'Neque Porro',
            column4: 'Neque Porro',
            column5: 'Neque Porro'
        }, {
            column1: 'Neque Porro',
            column2: 'Neque Porro',
            column3: 'Neque Porro',
            column4: 'Neque Porro',
            column5: 'Neque Porro'
        }, {
            column1: 'Neque Porro',
            column2: 'Neque Porro',
            column3: 'Neque Porro',
            column4: 'Neque Porro',
            column5: 'Neque Porro'
        }, {
            column1: 'Neque Porro',
            column2: 'Neque Porro',
            column3: 'Neque Porro',
            column4: 'Neque Porro',
            column5: 'Neque Porro'
        }];

        ctrl.config.search1.formManager = new FormManager(ctrl.config.search1.name, ctrl.config.search1.mode, ctrl);
        ctrl.config.search2.formManager = new FormManager(ctrl.config.search2.name, ctrl.config.search2.mode, ctrl);
        ctrl.config.datePicker1.formManager = new FormManager(ctrl.config.datePicker1.name, ctrl.config.datePicker1.mode, ctrl);
        ctrl.config.datePicker2.formManager = new FormManager(ctrl.config.datePicker2.name, ctrl.config.datePicker2.mode, ctrl);
        ctrl.config.searchDdl.formManager = new FormManager(ctrl.config.searchDdl.name, ctrl.config.searchDdl.mode, ctrl);

        ctrl.action1 = function() {
            $log.debug('Action 1');
        };

        ctrl.action2 = function() {
            $log.debug('Action 2');
        };

        ctrl.loadMore = function() {
            if (ctrl.testArray.length <= 20) {
                for (var i = 0; i < 3; i++) {
                    ctrl.testArray.push({
                        text: ctrl.testString
                    });
                }
            }
        };
    }

    angular.module('common').directive(directiveName, cssTemplate2);
})();
