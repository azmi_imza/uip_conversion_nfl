(function() {
    'use strict';

    var directiveName = 'uipFooter';

    /*@ngInject*/
    function uipFooter() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: uipFooterCtrl,
            templateUrl: 'templates/common/uipFooter.html'
        };
    }

    /*@ngInject*/
    function uipFooterCtrl($log) {
        $log.debug('uipFooterCtrl');
    }

    angular.module('common').directive(directiveName, uipFooter);
})();
