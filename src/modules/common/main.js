(function() {
    'use strict';
    var directiveName = 'main';

    /*@ngInject*/
    var main = function() {
        return {
            restrict: 'EA',
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: mainCtrl,
            templateUrl: 'templates/common/main.html'
        };
    };

    /*@ngInject*/
    var mainCtrl = function($scope, $timeout, uiHelperService, storageService, APP_CONST) {

        var ctrl = this;
        ctrl.menuSource = 'data/menu/menu.json';

        ctrl.backToTop = function() {
            uiHelperService.scrollTo(0);
        };

        function checkScroll() {
            if ($('body').height() > $(window).height()) {
                ctrl.showBackToTop = true;
            } else {
                ctrl.showBackToTop = false;
            }
        }

        function onResize() {
            checkScroll();
            $scope.$apply();
        }

        function onScroll() {
            if ($(window).width() > 500) {
                ctrl.backToTopFixed = false;
            } else {
                if ($(window).scrollTop() + ($(window).height()) + $('#uipFooter').height() >= $('body').height()) {
                    ctrl.backToTopFixed = false;
                } else {
                    if ($(window).scrollTop() < 200) {
                        ctrl.backToTopFixed = false;
                    } else {
                        ctrl.backToTopFixed = true;
                    }
                }
            }

            $scope.$apply();
        }

        //**** Load Content Managed *******//

        if (storageService.get(APP_CONST.STORAGE_KEYS.CURRENT_SCREEN_CODE) === '801024SCRNPERSONALPROFILEIN') {
            ctrl.contentPath = 'data/contentManaged/myprofile_pagetitle.html';
        } else {
            ctrl.contentPath = 'data/contentManaged/policy_pagetitle.html';
        }

        // KIV remove in future
        // function resizeFrame() {
        //     $('iframe').each(function(index, iframe) {
        //         var id = $(iframe).attr('id');
        //         var wrapper = id + '_wrapper';

        //         if (!document.getElementById(id).contentWindow.document.getElementById(wrapper)) {
        //             document.getElementById(id).contentWindow.document.body.innerHTML =
        //                 '<div id="' + wrapper + '">' +
        //                 document.getElementById(id).contentWindow.document.body.innerHTML + '</div>';
        //         }

        //         var newHeight = document.getElementById(id).contentWindow.document.getElementById(wrapper).scrollHeight;

        //         document.getElementById(id).height = newHeight + 'px';
        //     });
        // }

        //check on content change
        var watchScroll = $scope.$watch(function() {
                return $('body').height();
            },

            function() {
                checkScroll();
            }

        );

        //check on resize (need this due to delay in scope watch)
        $(window).on('resize', onResize);

        $(window).on('scroll', onScroll);

        //responsive content managed
        // $('iframe').on('load', resizeFrame);
        // $(window).on('resize', resizeFrame);

        //check on init
        $timeout(function() {
            checkScroll();
        });

        $scope.$on('$destroy', function() {
            watchScroll();
            $(window).off('resize', onResize);
            $(window).off('scroll', onScroll);
        });
    };

    angular.module('common')
        .directive(directiveName, main);
})();
