(function() {
    'use strict';
    var directiveName = 'tableHeaderSwitch';

    /*@ngInject*/
    var directive = function() {
        return {
            compile: function(tElem) {
                var template =
                    '<div class="tablesaw-visible-non-mobile">{0}</div>' +
                    '<div class="tablesaw-visible-mobile">{1}</div>';

                var headers = tElem.find('th');
                var temp = '';
                for (var i = 0; i <= headers.length; i++) {
                    var header = angular.element(headers[i]);
                    var headerChild = header.contents();
                    var headerChildHtml = headerChild.wrap('<p></p>').parent().html();
                    var headerChildText = header.text();

                    temp = template.replace('{0}', headerChildHtml);
                    temp = temp.replace('{1}', headerChildText);

                    for (var k = 0; k <= header.contents().length; k++) {
                        var childnode = header.contents()[k];
                        if (childnode) {
                            childnode.remove();
                        }
                    }

                    header.append(temp);
                }
            }
        };
    };

    angular.module('common')
        .directive(directiveName, directive);

})();
