(function() {
    'use strict';

    var directiveName = 'ckaPage';

    /*@ngInject*/
    function ckaPage() {
        return {
            scope: {},
            bindToController: true,
            controller: ckaCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/cka/cka.html'
        };
    }

    /*@ngInject*/
    function ckaCtrl($log, $stateParams, $state, ckaFactory, $timeout, routeService, uiHelperService) {
        var ctrl = this;

        ctrl.model = {};
        ctrl.model.selection = {};
        ctrl.model.detail = {};
        ctrl.validate = {};
        ctrl.maxCharLength = 50;

        if (!$stateParams.params) {
            $stateParams.params = {};
        }

        if (!$stateParams.params.link) {
            $stateParams.params.link = '/cka';
        }

        ctrl.validateSelection = function(id) {
            if (!ctrl.model.selection[id]) {
                var message = 'cfo.label.fieldIsRequired';
                ctrl.setFieldInvalid('selection', id, message);
            } else {
                ctrl.setFieldValid('selection', id);
                if (ctrl.model.selection[id] === 'N') {
                    ctrl.setFieldValid('detail', id);
                }
            }
        };

        ctrl.updateRemainings = function(id) {
            $('#remainingChar_' + id + ' span').html(ctrl.maxCharLength -
                $('#detail_' + id).val().length);
        };

        ctrl.validateDetail = function(id) {
            if (ctrl.model.selection[id] === 'Y' && !ctrl.model.detail[id]) {
                var message = 'cfo.label.fieldIsRequired';
                ctrl.setFieldInvalid('detail', id, message);
            } else {
                ctrl.setFieldValid('detail', id);
            }
        };

        ctrl.setFieldInvalid = function(field, id, message) {
            ctrl.validate[field + '_' + id].isValid = false;
            ctrl.validate[field + '_' + id].message = message;
        };

        ctrl.setFieldValid = function(field, id) {
            ctrl.validate[field + '_' + id].isValid = true;
            ctrl.validate[field + '_' + id].messsage = '';
        };

        ctrl.submit = function() {

            for (var i = 0; i < ctrl.ckaQuestions.length; i++) {
                for (var j = 0; j < ctrl.ckaQuestions[i].details.length; j++) {
                    ctrl.validateSelection(ctrl.ckaQuestions[i].details[j].id);
                    ctrl.validateDetail(ctrl.ckaQuestions[i].details[j].id);
                }
            }

            var validationResult = true;
            for (var key in ctrl.validate) {
                if (!ctrl.validate[key].isValid) {
                    validationResult = false;
                    uiHelperService.scrollTo($('#' + key).offset().top);
                    break;
                }
            }

            var success = function(response) {
                $stateParams.params.result = response.data.message;
                routeService.goToPath('/cka-ack', null, {
                    params: $stateParams.params
                });
            };

            var fail = function(response) {
                if (response.status !== 401) {
                    //TODO: Alert handling in proper way
                    $log.error('Submit failed :\n' + response.statusText);
                }
            };

            if (validationResult) {
                ckaFactory.submitCka(ctrl, success, fail);
            }
        };

        ctrl.initValidation = function() {
            for (var i = 0; i < ctrl.ckaQuestions.length; i++) {
                for (var j = 0; j < ctrl.ckaQuestions[i].details.length; j++) {
                    ctrl.validate['selection_' + ctrl.ckaQuestions[i].details[j].id] = {
                        isValid: true,
                        message: ''
                    };
                    ctrl.validate['detail_' + ctrl.ckaQuestions[i].details[j].id] = {
                        isValid: true,
                        message: ''
                    };
                }
            }
        };

        ckaFactory.getCkaQuestions(ctrl);
    }

    angular.module('cka').directive(directiveName, ckaPage);
})();
