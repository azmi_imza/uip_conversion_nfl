(function() {
    'use strict';
    angular.module('cka', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                var ckaState = statePrefix.concat('cka');
                $stateProvider.state(ckaState, {
                    url: '/cka',
                    template: '<div cka-page></div>',
                    data: {
                        screenCode: '801026SCRNCKAIN'
                    },
                    params: {
                        params: null
                    }
                });

                var ckaAckState = statePrefix.concat('cka-ack');
                $stateProvider.state(ckaAckState, {
                    url: '/cka-ack',
                    template: '<div cka-ack-page></div>',
                    data: {
                        screenCode: '801026SCRNCKAIN'
                    },
                    params: {
                        params: null
                    }
                });
            });
})();
