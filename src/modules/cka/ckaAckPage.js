(function() {
    'use strict';

    var directiveName = 'ckaAckPage';

    /*@ngInject*/
    function ckaAckPage() {
        return {
            scope: {},
            bindToController: true,
            controller: ckaAckCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/cka/ckaAck.html'
        };
    }

    /*@ngInject*/
    function ckaAckCtrl($log, $stateParams, storageService, routeService, APP_CONST) {
        var ctrl = this;
        ctrl.ckaResult = $stateParams.params.result;

        ctrl.ok = function() {
            storageService.set(APP_CONST.STORAGE_KEYS.CKA_STATUS, ctrl.ckaResult.indexOf('Passed') > -1, true);

            routeService.goToPath($stateParams.params.link, null, {
                params: $stateParams.params
            });
        };
    }

    angular.module('cka').directive(directiveName, ckaAckPage);
})();
