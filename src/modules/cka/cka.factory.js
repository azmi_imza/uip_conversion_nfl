(function() {
    'use strict';

    var factoryName = 'ckaFactory';

    /*@ngInject*/
    var factory = function($log, $http, $resource, $timeout, APP_CONFIG) {
        var CkaFactory = function() {

            var sortBySeqNum = function(a, b) {
                if (a['seq_num'] < b['seq_num']) {
                    return -1;
                }

                if (a['seq_num'] > b['seq_num']) {
                    return 1;
                }

                return 0;
            };

            this.getCkaQuestions = function(ctrl) {
                $resource(APP_CONFIG.API_PATHS.GET_CKA_QUESTIONS).get({},
                    function(response) {
                        $log.debug('Cka questions loaded');
                        ctrl.ckaQuestions = response.data.sort(sortBySeqNum);
                        $timeout(function() {
                            $('.cka div[ng-messages*="error"]').addClass('col-xs-12 no-padding');
                        }, 100);
                    },

                    function() {
                        $log.debug('Fail to get cka questions');
                    }

                );
            };

            this.submitCka = function(ctrl, success, fail) {

                var data = [];
                for (var id in ctrl.model.selection) {
                    var dataObj = {};
                    dataObj.id = id;
                    dataObj.answer = ctrl.model.selection[id];

                    if (ctrl.model.detail[id]) {
                        dataObj.remarks = ctrl.model.detail[id];
                    } else {
                        dataObj.remarks = '';
                    }

                    data.push(dataObj);
                }

                $http({
                    method: 'POST',
                    url: APP_CONFIG.API_PATHS.SUBMIT_CKA,
                    data: data
                }).then(success, fail);
            };
        };

        return new CkaFactory();
    };

    angular.module('cka').factory(factoryName, factory);
})();
