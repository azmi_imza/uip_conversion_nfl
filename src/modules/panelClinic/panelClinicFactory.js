(function() {
    'use strict';

    var factoryName = 'panelClinicFactory';

    /*@ngInject*/

    var factory = function($resource, $log, APP_CONFIG) {

        var PanelClinic = function() {

            this.getPanelClinicList = function(success, error) {
                $resource(APP_CONFIG.API_PATHS.PANEL_CLINICS).get({}, success, error);
            };

            this.getDropListProgram = function(success, error) {
                $resource(APP_CONFIG.API_PATHS.DROPLIST_PROGRAM).get({}, success, error);

            };

            this.getPanelClinicLatLng = function(success, error) {
                $resource(APP_CONFIG.API_PATHS.PANEL_CLINICS).get({
                    size: -1
                }, success, error);
            };

            this.getList = function(model, success, error) {
                // this hardcoded value variable is just for testing purposes

                $resource(APP_CONFIG.API_PATHS.PANEL_CLINICS).get({
                    program: model.program || '',
                    clinicName: model.clinicName || '',
                    address: model.address || '',
                    page: model.page || '',
                    size: 20
                }, success, error);
            };
        };

        return new PanelClinic();
    };

    angular.module('panelClinic')
        .factory(factoryName, factory);
})();
