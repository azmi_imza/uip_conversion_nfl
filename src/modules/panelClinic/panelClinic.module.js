(function() {
    'use strict';
    angular.module('panelClinic', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                var panelClinicIN = statePrefix.concat('panel-clinic-in');
                $stateProvider.state(panelClinicIN, {
                    url: '/panel-clinic-in',
                    template: '<div panel-clinic-page></div>',
                    data: {
                        screenCode: 'SCRNPANELCLINICIN'
                    }
                });

                var panelClinicBE = statePrefix.concat('panel-clinic-be');
                $stateProvider.state(panelClinicBE, {
                    url: '/panel-clinic-be',
                    template: '<div panel-clinic-page></div>',
                    data: {
                        screenCode: 'SCRNPANELCLINICBE'
                    }
                });
            });
})();
