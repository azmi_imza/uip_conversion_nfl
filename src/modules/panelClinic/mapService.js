/* global navigator, google */
/* eslint no-unused-vars:0 */

(function() {
    'use strict';

    var serviceName = 'mapService';

    /*@ngInject*/
    var mapService = function($log, APP_CONST, storageService, $window, $q, $http) {

        this.generateMap = function generateMap(ctrl) {

            function loadGoogleAPI() {
                var deferred = $q.defer();

                $window.initializeGoogleMap = function() {

                    var mapOptions = {
                        zoom: 16,
                        mapTypeId: google.maps.MapTypeId.TERRAIN
                    };
                    ctrl.mapElem = new google.maps.Map(document.getElementById('panel-clinic__map'), mapOptions);

                    for (var j = 0; j < ctrl.markerData.length; j++) {
                        var latlng = new google.maps.LatLng(ctrl.markerData[j].lat, ctrl.markerData[j].lng);
                        var markers = new google.maps.Marker({
                            position: latlng,
                            map: ctrl.mapElem
                        });
                    }

                    deferred.resolve();
                };

                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
                    '&signed_in=true&libraries=places&callback=initializeGoogleMap';
                document.body.appendChild(script);

                return deferred.promise;
            }

            function loadGoogleAPISuccess() {
                if ($window.google && $window.google.maps) {
                    createSearchBox(ctrl.mapElem);
                    loadAllBranchesMarker(ctrl.clinics, ctrl.mapElem);
                    getCurrentLocation(ctrl.mapElem);
                } else {
                    $log.debug('google map not loaded');
                }
            }

            function loadGoogleAPIFailed() {
                $log.debug('promise rejected');
            }

            loadGoogleAPI().then(loadGoogleAPISuccess, loadGoogleAPIFailed);
        };

        //******************* function called ***************************************//
        //google api called autocomplete, it bind with included the correct google path

        function createSearchBox(mapUI) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');

            var options = { //options for autocomplete object
                types: ['geocode']
            };
            var autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.bindTo('bounds', mapUI);
            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: mapUI,
                anchorPoint: new google.maps.Point(0, -29)
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    //TODO: Alert handling in proper way
                    $log.info('Autocomplete\'s returned place contains no geometry');
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    mapUI.fitBounds(place.geometry.viewport);
                } else {
                    mapUI.setCenter(place.geometry.location);
                    mapUI.setZoom(18); // Why 17? Because it looks good.
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';

                var addressComponents = place['address_components'];
                var shortNames = [];
                if (addressComponents && addressComponents.length > 0) {
                    for (var i = 0; i <= 2; i++) {
                        if (!addressComponents[i] || !addressComponents[i]['short_name']) {
                            continue;
                        }

                        shortNames.push(addressComponents[i]['short_name']);
                    }

                    address = shortNames.join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(mapUI, marker);
            });

            var selectFirstResult = function() {
                infowindow.close();
                marker.setVisible(false);

                var firstResult = $('.pac-container .pac-item:first').text();
                var stringMatched = $('.pac-container .pac-item:first').find('.pac-item-query').text();
                firstResult = firstResult.replace(stringMatched, stringMatched + ' ');

                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'address': firstResult
                }, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var place = results[0];

                        $('#pac-input').val(firstResult);

                        if (!place.geometry) {
                            //TODO: Alert handling in proper way
                            $log.info('First selection returned place contains no geometry');
                            return;
                        }

                        if (place.geometry.viewport) {
                            mapUI.fitBounds(place.geometry.viewport);
                        } else {
                            mapUI.setCenter(place.geometry.location);
                            mapUI.setZoom(18); // Why 17? Because it looks good.
                        }

                        // If the place has a geometry, then present it on a map.
                        marker.setPosition(place.geometry.location);
                        marker.setVisible(true);

                        var address = '';

                        var addressComponents = place['address_components'];
                        var shortNames = [];
                        if (addressComponents && addressComponents.length > 0) {
                            for (var i = 0; i <= 2; i++) {
                                if (!addressComponents[i] || !addressComponents[i]['short_name']) {
                                    continue;
                                }

                                shortNames.push(addressComponents[i]['short_name']);
                            }

                            address = shortNames.join(' ');
                        }

                        infowindow.setContent('<div><strong>' + place['address_components'][0]['long_name'] + '</strong><br>' + address);
                        infowindow.open(mapUI, marker);
                    }
                });
            };

            $('#pac-input').keypress(function(e) {
                if (e.which === 13) {
                    selectFirstResult();
                }
            });

        }

        //****************************************************************************************
        // HTML5 geolocation get current location
        // brower safari need to make sure using WIFI instead of LAN to access internet

        function getCurrentLocation(mapUI) {
            if (navigator.geolocation) {
                var getCurrentPositionSuccess = function(position) {
                    var pos = new google.maps.LatLng(position.coords.latitude,
                        position.coords.longitude);

                    var infoWindow = new google.maps.InfoWindow({
                        map: mapUI,
                        position: pos,
                        content: 'Current location.'
                    });

                    mapUI.setCenter(pos);
                    mapUI.setZoom(15);
                    $log.debug('get current location:end position');
                };

                var getCurrentPositionFailed = function() {
                    handleNoGeolocation(true);
                };

                navigator.geolocation.getCurrentPosition(getCurrentPositionSuccess, getCurrentPositionFailed);
            } else {
                $log.debug('navigator.geolocation: false');

                var pos1 = new google.maps.LatLng(storageService.get(APP_CONST.STORAGE_KEYS.HQ_LATITUDE),
                    storageService.get(APP_CONST.STORAGE_KEYS.HQ_LONGITUDE));

                //set a default center if couldn't get the current location
                var infoWindow1 = new google.maps.InfoWindow({
                    map: mapUI,
                    position: pos1,
                    content: 'Great Eastern HQ'
                });

                mapUI.setCenter(pos1);
                mapUI.setZoom(20);
                $log.debug('get current location:end position :' + pos1);
            }

            function handleNoGeolocation(errorFlag) {
                if (errorFlag === true) {
                    //TODO: Alert handling in proper way
                    $log.error('Geolocation service failed.');
                    $log.debug('navigator.geolocation: false');

                    var pos1a = new google.maps.LatLng(storageService.get(APP_CONST.STORAGE_KEYS.HQ_LATITUDE),
                        storageService.get(APP_CONST.STORAGE_KEYS.HQ_LONGITUDE));

                    //set a default center if couldn't get the current location
                    var infoWindow1a = new google.maps.InfoWindow({
                        map: mapUI,
                        position: pos1a,
                        content: 'Great Eastern HQ'
                    });

                    mapUI.setCenter(pos1a);
                    mapUI.setZoom(18);
                    $log.debug('get current location:end position :' + pos1a);

                } else {
                    //TODO: Alert handling in proper way
                    $log.error('Your browser doesn\'t support geolocation.');

                }
            }
        }

        // Load all branches marker
        function loadAllBranchesMarker(clinics, mapUI) {

            var infoWindow = new google.maps.InfoWindow();

            var createMarker = function(info) {
                var marker = new google.maps.Marker({
                    map: mapUI,
                    position: new google.maps.LatLng(info.latitude, info.longitude),
                    title: info.name,
                    icon: 'images/map_indicator.png',
                    zoom: 5
                });
                marker.content = '<div class="infoWindowContent">' + info.address + '</div><a href="http://www.google.com/maps?saddr=MY+Location&daddr=' + info.latitude + ',' + info.longitude + '" target="_blank">Driving Directions</a>';

                google.maps.event.addListener(marker, 'click', function() {
                    infoWindow.close();
                    infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                    infoWindow.open(mapUI, marker);
                });
            };

            var promises = [];

            //SG area not provide lat and lng, so retrieve google api to get location
            angular.forEach(clinics, function(value, key) {
                //checking if latitute is null, retrieve location from google api
                if (!value.latitude || value.latitude === '') {
                    promises.push(getLatLng(value));
                }
            });

            $q.all(promises)
                .then(function() {
                    angular.forEach(clinics, function(value, key) {
                        createMarker(value);
                    });
                });

            function getLatLng(obj) {
                var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + obj['postal_cd'] + '&components=country:SG';

                return $http({
                    method: 'GET',
                    url: url
                }).
                success(function(data, status) {
                    var location = data.results[0].geometry.location;
                    obj.latitude = location.lat;
                    obj.longitude = location.lng;
                }).
                error(function(data, status) {
                    $log.debug('fail to retrieve clinics location from google api ' + obj['postal_cd']);
                });
            }
        }

    };

    angular.module('panelClinic')
        .service(serviceName, mapService);
})();
