(function() {
    'use strict';

    var serviceName = 'stringParserService';

    /*@ngInject*/
    var serviceFunction = function() {
        this.formattingString = function(string) {
            if (!string || !string.length) {
                return '-';
            }

            var tempArray2 = splitStringInArray(string.split(';'));
            if (tempArray2) {
                return tempArray2.join('<br>');
            } else {
                return string;
            }

        };

        function splitStringInArray(tempArray) {
            if (!tempArray) {
                return undefined;
            }

            var tempArray2 = [];
            angular.forEach(tempArray, function(value) {
                if (value.length === 0) {
                    return;
                }

                // console.log('PanelClinic >>>>>>>>');
                // console.log(value);

                // if (value.match(/\d{4}/)) {
                //     var temp = value.replace('Start:', '');
                //     temp = temp.replace('End:', '');

                //     //temp=0900-1200
                //     var regTime = /(\d{4})-(\d{4})/;
                //     var match = regTime.exec(temp);
                //     var tempString;

                //     if (match && match[1] && match[2]) {
                //         tempString = getFormattedTime(match[1]) + ' - ' + getFormattedTime(match[2]);
                //     } else {
                //         tempString = value;
                //     }
                // } else {
                //     tempString = value;
                // }

                tempArray2.push(value);

            });

            return tempArray2;

        }

        // function getFormattedTime(str) {
        //     var array = str.split('');
        //     return array[0] + array[1] + ':' + array[2] + array[3];
        // }
    };

    angular.module('panelClinic')
        .service(serviceName, serviceFunction);
})();
