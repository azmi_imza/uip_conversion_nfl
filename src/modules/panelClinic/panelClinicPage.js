(function() {
    'use strict';
    var directiveName = 'panelClinicPage';

    /*@ngInject*/
    function panelClinicPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: panelClinicCtrl,
            templateUrl: 'templates/panelClinic/panelClinic.html'
        };
    }

    /*@ngInject*/
    function panelClinicCtrl(mapService, $timeout, $log, panelClinicFactory,
        $window, APP_CONST, storageService, stringParserService, FormManager, uiHelperService) {
        $log.debug('panel clinic -');

        var ctrl = this;

        ctrl.config = {};
        ctrl.hasData = true;

        ctrl.formManager = new FormManager('panelClinicForm', 'edit', ctrl);

        ctrl.formModel = {};
        ctrl.formModel.page = 0;
        ctrl.formModel.size = 0;

        ctrl.showPagination = true;
        ctrl.isListing = true;
        ctrl.dataShow = true;
        ctrl.totalPages = 0;
        ctrl.isUserGroupIndividual = (storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE) === 'IN') ? true : false;

        if (ctrl.isUserGroupIndividual) {
            ctrl.selectedProgramForInd = '-';
        }

        if (ctrl.isUserGroupIndividual) {
            uiHelperService.showLoader();
            panelClinicFactory.getDropListProgram(function(objectReturn) {
                    if (objectReturn.data < 1) {
                        return;
                    }

                    $log.debug('>>>>>>>>>>panel clinics droplist program', objectReturn.data);

                    ctrl.programs = objectReturn.data.items;

                    if (ctrl.isUserGroupIndividual) {
                        ctrl.selectedProgramForInd = ctrl.programs[0].name;
                        ctrl.formManager.setValue('selectedProgramForInd', ctrl.programs[0].name);
                    }

                    uiHelperService.hideLoader();

                },

                function(error) {
                    //TODO: Alert handling in proper way
                    $log.error('Fail to load program droplist.');
                    $log.debug(error);
                    uiHelperService.hideLoader();
                });
        }

        function addNodeAddress(obj) {
            var arrayTemp = [
                'Block ',
                obj.block,
                ' ',
                obj.street,
                ' #',
                obj['storey_num'],
                '-',
                obj['unit_num'],
                ', ',
                obj.building,
                ' ',
                obj.country,
                ' ',
                obj['postal_cd']
            ];
            obj.address = arrayTemp.join('');
        }

        var oriClinics;

        panelClinicFactory.getPanelClinicList(function(objectReturn) {
                if (objectReturn.data < 1) {
                    return;
                }

                var tempObj = objectReturn.data;

                $log.debug('>>>>>>>>>>panel clinics listing', tempObj);

                angular.forEach(tempObj, function(obj) {
                    addNodeAddress(obj);
                    obj.formatedWeekday = stringParserService.formattingString(obj.weekday);

                    obj.formatedSaturday = stringParserService.formattingString(obj.sat);
                    obj.formatedSunday = stringParserService.formattingString(obj.sun);

                    // obj.formatedRemark = stringParserService.formattingString(obj.remarks);
                });

                oriClinics = tempObj;

                ctrl.clinics = oriClinics;

                //TODO: Commented for testing footable Sort error on console// $timeout(function() {
                //     $('.footable').footable();
                // }, 0);
            },

            function(error) {

                //TODO: Alert handling in proper way
                $log.error('Fail to load clinic data.');
                $log.debug(error);
            });

        var filteredClinics;

        ctrl.searchClinics = function() {
            filteredClinics = oriClinics;

            if (ctrl.formManager.getValue('selectedProgram')) {

                filteredClinics = filteredClinics.filter(searchProgramme);
            }

            if (ctrl.formManager.getValue('searchClinic')) {
                filteredClinics = filteredClinics.filter(searchClinic);

            }

            if (ctrl.formManager.getValue('searchAddress')) {
                filteredClinics = filteredClinics.filter(searchAddress);

            }

            ctrl.clinics = filteredClinics;

            //TODO: Commented for testing footable Sort error on console
            // $timeout(function() {
            //     $('.footable').trigger('footable_redraw');
            // }, 0);

        };

        function searchAddress(clinic) {
            return clinic.address.toLowerCase().indexOf(ctrl.formManager.getValue('searchAddress').toLowerCase()) !== -1;
        }

        function searchClinic(clinic) {
            return clinic.name.toLowerCase().indexOf(ctrl.formManager.getValue('searchClinic').toLowerCase()) !== -1;
        }

        function searchProgramme(clinic) {
            return clinic.program.indexOf(ctrl.formManager.getValue('selectedProgram')) !== -1;
        }

        ctrl.onListingClicked = function() {
            ctrl.isListing = true;
            ctrl.mapElem = null;
        };

        ctrl.mapRender = function() {
            ctrl.isListing = false;

            if (!ctrl.mapElem && ctrl.clinics && ctrl.clinics.length > 0) {
                $timeout(function() {

                    ctrl.markerData = [];
                    panelClinicFactory.getPanelClinicLatLng(function(objectReturn) {
                            if (objectReturn.data < 1) {
                                return;
                            }

                            $log.debug('>>>>>>>>>>panel clinics map marker data', objectReturn.data);

                            for (var i = 0; i < objectReturn.data.length; i++) {
                                var obj = {};
                                obj.lat = objectReturn.data[i].latitude;
                                obj.lng = objectReturn.data[i].longitude;

                                ctrl.markerData.push(obj);
                            }
                        },

                        function(error) {
                            //TODO: Alert handling in proper way
                            $log.error('Fail to load data from API');
                            $log.debug(error);
                        });

                    mapService.generateMap(ctrl);

                    $(document).on({
                        'DOMNodeInserted': function() {
                            $('.pac-item, .pac-item span', this).addClass('needsclick');
                        }
                    }, '.pac-container');

                }, 100);
            }

        };

        ctrl.getPageNumber = function(pageNumber) {
            if (pageNumber > ctrl.totalPages) {
                ctrl.formModel.page = ctrl.totalPages;
            }

            if (pageNumber <= 1) {
                ctrl.formModel.page = 1;
            }

            ctrl.getListofClinics();
        };

        ctrl.previousPageNumber = function() {
            if (ctrl.formModel.page <= ctrl.totalPages && ctrl.formModel.page >= 0) {
                ctrl.formModel.page--;
            }

            if (ctrl.formModel.page < 1) {
                ctrl.formModel.page = 1;
            }

            ctrl.getListofClinics();
        };

        ctrl.nextPageNumber = function() {
            if (ctrl.formModel.page < ctrl.totalPages && ctrl.formModel.page >= 0) {
                ctrl.formModel.page++;
            }

            if (ctrl.formModel.page > ctrl.totalPages) {
                ctrl.formModel.page = ctrl.totalPages;
            }

            ctrl.getListofClinics();
        };

        ctrl.getListofClinics = function() {
            ctrl.formModel.program = ctrl.formManager.getValue('selectedProgram');
            ctrl.formModel.clinicName = ctrl.formManager.getValue('searchClinic');
            ctrl.formModel.address = ctrl.formManager.getValue('searchAddress');
            ctrl.showTable = false;
            panelClinicFactory.getList(ctrl.formModel, function(objectReturn) {

                if (objectReturn.data < 1) {
                    ctrl.hasData = false;
                    return;
                }

                var tempObj = objectReturn.data;
                ctrl.hasData = true;
                ctrl.showTable = true;
                $log.debug('Get panel clinics listing', tempObj);

                angular.forEach(tempObj, function(obj) {
                    addNodeAddress(obj);
                    obj.formatedWeekday = stringParserService.formattingString(obj.weekday);
                    obj.formatedSaturday = stringParserService.formattingString(obj.sat);
                    obj.formatedSunday = stringParserService.formattingString(obj.sun);
                    obj.formatedRemark = stringParserService.formattingString(obj.remarks);
                });

                oriClinics = tempObj;

                ctrl.filterResults = oriClinics;

                ctrl.totalPages = objectReturn['page_count'];
                if (objectReturn['page_count'] < 2) {
                    ctrl.showPagination = false;
                } else {
                    ctrl.showPagination = true;
                }

                if (ctrl.formModel.page === 0) {
                    ctrl.formModel.page = objectReturn['page_index'];
                }

                $timeout(function() {
                    $('.footable').footable();
                    $('.footable').trigger('footable_redraw');
                }, 0);

            }, function(error) {
                // TODO: Alert handling in proper way
                $log.error('Fail to load clinic data.');
                $log.debug(error);

            });
        };

        ctrl.clearSearch = function() {
            ctrl.searcValue = '';
            $('#pac-input').val('');
            $('#pac-input').focus();
        };

        ctrl.getListofClinics();

        ctrl.pageNumberFormat = {
            type: 'currency',
            attributes: {
                currencyFormat: false,
                currencyInteger: 3,
                currencyDecimal: 0
            }
        };

    }

    angular.module('panelClinic').directive(directiveName, panelClinicPage);
})();
