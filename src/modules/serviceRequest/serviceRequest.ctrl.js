/* eslint no-alert:0 */

(function() {
    'use strict';
    var ctrlName = 'serviceRequestCtrl';

    /*@ngInject*/
    var controller = function($state, storageService, $log, $scope, $rootScope, $filter, loginFactory,
        formManagerService, serviceRequestFactory, generalModalService, APP_CONST, SERV_REQ_ACTION) {
        $log.debug('service request controller');
        formManagerService.clearValidators();
        $rootScope.sortState = {};
        $scope.formData = {};
        $scope.OTP = {};

        $scope.initSortState = function(name, data, sort, order) {
            $rootScope.sortState[name] = {};
            $rootScope.sortState[name].sort = {};
            $rootScope.sortState[name].data = data;
            $rootScope.sortState[name].sort.sortBy = sort;
            $rootScope.sortState[name].sort.orderBy = order;

            $log.debug('sort state');
            $log.debug($rootScope.sortState);
        };

        $scope.model = {};
        $scope.selectedPolicy = {};
        $scope.topupAmount = '';
        $scope.policyListLoaded = false;
        $scope.policyRequestListLoaded = false;

        $scope.changePolicy = function() {
            $scope.selectedPolicyRequest = '';
            $scope.showServiceReqDetails = false;

            if ($scope.selectedPolicy && $scope.selectedPolicy.id) {
                serviceRequestFactory.getPolicyRequests($scope);
            } else {
                $scope.policyRequestListLoaded = false;
            }
        };

        $scope.changePolicyRequest = function() {
            if (!$scope.selectedPolicyRequest || $scope.showServiceReqDetails) {
                $scope.showServiceReqDetails = false;
            }
        };

        $scope.search = function() {
            $log.debug('search');
            serviceRequestFactory.getPolicyRequestDetails($scope);
        };

        $scope.selectServiceRequestAction = function(serviceRequestType) {
            $scope.servRequestAction = serviceRequestType;
            formManagerService.clearAllValidateStates();
        };

        $scope.selectPaymentType = function(paymentType) {
            $scope.formData.paymentType = paymentType;
        };

        $scope.proceed = function() {
            $log.debug('$scope.proceed');

            if ($scope.CKAStatus && !$scope.servRequestAction) {
                //TODO: Alert handling in proper way
                alert('Please select service request action');
                return;
            }

            if (!$scope.CKAStatus) {
                $scope.servRequestAction = SERV_REQ_ACTION.SEEKADVICE;
            }

            storageService.set(APP_CONST.STORAGE_KEYS.SERV_REQ_ACTION, $scope.servRequestAction);
            var servReq = {};
            servReq['policy_id'] = $scope.selectedPolicy.id;
            servReq.code = $scope.selectedPolicyRequest.code;
            servReq.action = {
                name: $scope.selectedPolicyRequest.code
            };

            //temp hardcoded value for prod_cd
            servReq['prod_cd'] = 'PREM_TOP_UP';

            if (SERV_REQ_ACTION.TOPUP === $scope.servRequestAction) {
                if (!$scope.formData.topupAmount) {
                    //TODO: Alert handling in proper way
                    alert('Please enter top up amount.');
                    return;
                }

                var regex = /[^0-9]/;
                if (regex.test($scope.formData.topupAmount)) {
                    //TODO: Alert handling in proper way
                    alert('Please enter valid amount.');
                    return;
                }

                if (!$scope.formData.paymentType) {
                    //TODO: Alert handling in proper way
                    alert('Please select payment type');
                    return;
                }

                var url = 'templates/serviceRequest/serviceRequestOTP.html';
                var ctrl = 'serviceRequestOTPCtrl';
                var backdrop = 'static';
                var size = 'md';
                var resolve = {
                    policyId: function() {
                        return $scope.selectedPolicy.id;
                    },

                    policyRequest: function() {
                        return $scope.selectedPolicyRequest.code;
                    }
                };

                var close = function(responseData) {
                    //open payment gateway
                    $log.debug('Response Data: ' + responseData.status);

                    servReq.action['is_advice'] = 'N';
                    servReq.premium = $scope.formData.topupAmount;
                    servReq['pymt_type'] = $scope.formData.paymentType;
                    servReq.cur = 'SGD';

                    servReq.policyNo = $scope.selectedPolicy.number;
                    storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ, servReq);

                    var urlPG = 'templates/serviceRequest/paymentGateway.html';
                    var ctrlPG = 'paymentGatewayCtrl';
                    var backdropPG = 'static';
                    var sizePG = 'md';
                    var resolvePG = {};
                    var closePG = function() {
                        //go to success page
                        $log.info('Payment gateway Closed at: ' + new Date());
                    };

                    var dismissPG = function() {
                        $log.info('Payment Gateway Modal dismissed at: ' + new Date());
                    };

                    generalModalService.openModal(urlPG, ctrlPG, backdropPG, sizePG, resolvePG, closePG, dismissPG);
                };

                var dismiss = function() {
                    $log.info('OTP Modal dismissed at: ' + new Date());
                };

                generalModalService.openModal(url, ctrl, backdrop, size, resolve, close, dismiss);

            } else {
                if (!$scope.formData.msgToAgent) {

                    //TODO: Alert handling in proper way
                    alert('Please enter message.');
                    return;
                }

                servReq.action['is_advice'] = 'Y';
                servReq.msg = $scope.formData.msgToAgent;
                servReq.policyNo = $scope.selectedPolicy.number;

                storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ, servReq);

                serviceRequestFactory.submitServiceRequest(function() {
                    servReq.agentName = $scope.plannerNameObject.name;
                    storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ, servReq);
                    $state.go('main.servicerequestsummary');
                });
            }
        };

        $scope.sortTable = function(name, predicate, reverse) {
            if (!reverse || reverse === 'false') {
                reverse = false;
            } else {
                reverse = true;
            }

            $scope.sortState[name].data = $filter('orderBy')($scope.sortState[name].data, predicate, reverse);
            $scope.sortState[name].sort.sortBy = predicate;
            if (reverse) {
                reverse = 'true';
            } else {
                reverse = 'false';
            }

            $scope.sortState[name].sort.orderBy = reverse;
            $log.debug(predicate + '- ' + reverse);
        };

        serviceRequestFactory.getPolicies($scope);
        serviceRequestFactory.getCKAStatus($scope);

    };

    angular.module('serviceRequest').controller(ctrlName, controller);
})();
