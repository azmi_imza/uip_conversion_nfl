(function() {
    'use strict';
    var ctrlName = 'serviceRequestOTPCtrl';

    /*@ngInject*/
    var controller = function($scope, $log, formManagerService, serviceRequestFactory, policyId,
        policyRequest, generalModalService) {
        formManagerService.clearValidators();
        $scope.model = {};
        $scope.model['policy_Id'] = policyId;
        $scope.model.code = policyRequest;

        $scope.submitOTP = function() {
            var validationResult = formManagerService.runAllValidatorsOnSubmit();

            if (validationResult) {
                $log.debug($scope.model.otp);

                //send to server
                serviceRequestFactory.sendOTP($scope.model).then(function(response) {

                    $log.debug(response);
                    $log.debug('Response status: ' + response.statusText);

                    //proceed to payment gateway
                    if (response.data.status === 'failed') //obtain reponse status from 'response.data.status'
                    {
                        //TODO: handle error
                        $log.error(response.data.message);
                    } else { //proceed if OTP is valid
                        generalModalService.closeModal(response);
                    }
                },

                function(response) {
                        $log.error(response.data);
                        $log.error(response.status);
                    });

            }
        };

        $scope.cancel = function() {
            generalModalService.dismissModal();
        };
    };

    angular.module('serviceRequest').controller(ctrlName, controller);
})();
