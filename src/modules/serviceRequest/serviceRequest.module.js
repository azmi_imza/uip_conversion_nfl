(function() {
    'use strict';
    angular.module('serviceRequest', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var AUTH_STATE_PREFIX = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(AUTH_STATE_PREFIX.concat('servicerequest'), {
                    url: '/serviceRequest',
                    templateUrl: 'templates/serviceRequest/serviceRequest.html',
                    controller: 'serviceRequestCtrl',
                    data: {
                        screenCode: 'DUMMY'
                    }
                });

                $stateProvider.state(AUTH_STATE_PREFIX.concat('servicerequestsummary'), {
                    url: '/serviceRequest/summary',
                    templateUrl: 'templates/serviceRequest/serviceRequestSummary.html',
                    controller: 'serviceRequestSummaryCtrl',
                    data: {
                        screenCode: 'DUMMY'
                    }
                });
            });
})();
