(function() {
    'use strict';
    var ctrlName = 'paymentGatewayCtrl';

    /*@ngInject*/
    var controller = function($scope, $state, serviceRequestFactory, storageService, generalModalService, $log) {
        $scope.continueClick = function() {
            serviceRequestFactory.submitServiceRequest(function(response) {
                generalModalService.closeModal();
                if (response.status === 'failed') {

                    //TODO: Alert handling in proper way
                    $log.error('Request failed due to:\n' + response.message);
                } else {
                    storageService.set('serviceRequestAvailable', response.data.service['is_available']);
                    storageService.set('serviceRequestMsg', response.data.service.msg);
                    $state.go('main.servicerequestsummary');
                }
            });
        };

        $scope.cancel = function() {
            generalModalService.dismissModal();
        };
    };

    angular.module('serviceRequest').controller(ctrlName, controller);
})();
