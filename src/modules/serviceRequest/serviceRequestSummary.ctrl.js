(function() {
    'use strict';
    var ctrlName = 'serviceRequestSummaryCtrl';

    /*@ngInject*/
    var controller = function($timeout, $log, $state, $scope, $modal, storageService, SERV_REQ_ACTION, APP_CONST) {
        $scope.serviceRequestAction = storageService.get(APP_CONST.STORAGE_KEYS.SERV_REQ_ACTION);

        if (!$scope.serviceRequestAction) {
            $state.go('error.notfound');
        }

        $scope.CKAStatus = storageService.get(APP_CONST.STORAGE_KEYS.CKA_STATUS);

        $scope.model = {};
        $scope.model.status = 'In Progress';
        $scope.model.referenceNo = 'ref128';

        $log.debug('CKASTATUS: ' + $scope.CKAStatus);
        $log.debug('serviceRequestAction: ' + $scope.serviceRequestAction);

        if ($scope.CKAStatus === false) {
            $scope.msg = 'SERV_REQ_CKA_FAILED_MSG';
        } else {
            if ($scope.serviceRequestAction === SERV_REQ_ACTION.TOPUP) {
                if (storageService.get('serviceRequestAvailable') === 'Y') {
                    $scope.msg = 'SERV_REQ_CKA_PASS_MSG';
                    $scope.serviceAvailable = true;

                    var submittedServReq = storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ);
                    $log.debug('service request summary');
                    $log.debug(submittedServReq);
                    $scope.model.topUp = submittedServReq.premium;
                    $scope.model.policyNo = submittedServReq.policyNo;
                    $timeout(function() {
                        $(document).trigger('enhance.tablesaw');
                    }, 10);
                } else {
                    $scope.msg = storageService.get('serviceRequestMsg');
                    $scope.serviceAvailable = false;
                }
            } else {
                $scope.servReq = storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ);
                $scope.msg = 'SERV_REQ_MSG_SENT';
            }
        }
    };

    angular.module('serviceRequest').controller(ctrlName, controller);
})();
