/* eslint no-alert:0 */

(function() {
    'use strict';

    var factoryName = 'serviceRequestFactory';

    /*@ngInject*/
    var factory = function($http, $log, $resource, $timeout, storageService, APP_CONST, APP_CONFIG,
        SERV_REQ_ACTION) {

        var ServiceRequest = function() {
            var requestFailedCallback = function(response) {
                $log.debug('Service request failed: ');
                $log.debug('Response: ');
                $log.debug(response);

                //TODO: Alert handling in proper way
                alert('Request failed. Please try again.');
            };

            this.getPolicies = function(scope) {
                scope.policyListLoaded = false;
                $log.debug('Get policies request success');

                var requestSuccessCallback = function(response) {
                    $log.debug('Service Request success');
                    scope.model.policies = response.data.filter(function(obj) {
                        return obj.category !== 'THIRD_PARTY' && obj['prod_typ'] === 'ILP';
                    });

                    $log.debug('Policies: ');
                    $log.debug(response.data);
                    scope.policyListLoaded = true;
                };

                $resource(APP_CONFIG.API_PATHS.POLICY).get({}, requestSuccessCallback, requestFailedCallback);
            };

            this.getPolicyRequests = function(scope) {
                scope.policyRequestListLoaded = false;
                $log.debug('Service Request, get policy request');

                if (!scope.selectedPolicy || !scope.selectedPolicy.id) {
                    //TODO: Alert handling in proper way
                    alert('Please select policy');
                    return;
                }

                var requestSuccessCallback = function(response) {
                    $log.debug('Get Policy request success');
                    scope.model.policyRequests = response.data;
                    $log.debug('Policy requests: ');
                    $log.debug(response.data);
                    if (response.data.length === 1) {
                        scope.selectedPolicyRequest = response.data[0];
                    }

                    scope.policyRequestListLoaded = true;
                };

                $resource(APP_CONFIG.API_PATHS.POLICYREQUEST).get({
                    policyId: scope.selectedPolicy.id
                }, requestSuccessCallback, requestFailedCallback);
            };

            this.getPolicyRequestDetails = function(scope) {
                if (!scope.selectedPolicy || !scope.selectedPolicy.id) {
                    //TODO: Alert handling in proper way
                    alert('Please select policy');
                    return;
                }

                if (!scope.selectedPolicyRequest) {
                    //TODO: Alert handling in proper way
                    alert('Please select policy request type');
                    return;
                }

                var requestSuccessCallback = function(response) {
                    $log.debug('Get Policy request details success');
                    if (!response.data) {
                        //TODO: Alert handling in proper way
                        $log.error('Service request not available for the selected policy requested');
                        scope.showServiceReqDetails = false;
                        return;
                    }

                    scope.showServiceReqDetails = true;
                    scope.funds = response.data.fund;
                    scope.initSortState('fund', scope.funds, 'name', false);
                    scope.plannerNameObject = {};
                    scope.plannerNameObject.name = response.data['sales_force_nm'];
                    $timeout(function() {
                        $(document).trigger('enhance.tablesaw');
                        scope.sortTable('fund', 'name', false);
                    }, 10);
                };

                $resource(APP_CONFIG.API_PATHS.POLICYREQDETAILS).get({
                    policyId: scope.selectedPolicy.id,
                    requestCode: scope.selectedPolicyRequest.code
                }, requestSuccessCallback, requestFailedCallback);
            };

            this.getCKAStatus = function(scope) {
                scope.CKAStatus = storageService.get(APP_CONST.STORAGE_KEYS.CKA_STATUS);
            };

            this.submitServiceRequest = function(callback, errorCallback) {
                var serviceReqAction = storageService.get(APP_CONST.STORAGE_KEYS.SERV_REQ_ACTION);
                var submittedServReq = angular.copy(storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_SERV_REQ));

                if (serviceReqAction === SERV_REQ_ACTION.TOPUP) {
                    delete submittedServReq.policyNo;
                }

                $log.debug('submit service request: ');
                $log.debug(submittedServReq);
                $http({
                    method: 'POST',
                    data: submittedServReq,
                    url: APP_CONFIG.API_PATHS.SERVICEREQUEST
                }).success(callback).error(errorCallback);

            };

            this.sendOTP = function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: APP_CONFIG.API_PATHS.OTP
                });
            };
        };

        return new ServiceRequest();
    };

    angular.module('serviceRequest')
        .factory(factoryName, factory);
})();
