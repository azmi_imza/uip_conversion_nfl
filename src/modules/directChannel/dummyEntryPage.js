(function() {
    'use strict';

    var directiveName = 'dummyEntryPage';

    /*@ngInject*/
    function dummyEntryPage() {
        return {
            scope: {},
            bindToController: true,
            controller: dummyEntryCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/directChannel/dummyEntry.html'
        };
    }

    /*@ngInject*/
    function dummyEntryCtrl($http, APP_CONFIG, uiHelperService, routeService) {
        var ctrl = this;

        ctrl.getQuotation = function(code) {

            var success = function(response) {
                uiHelperService.hideLoader();

                var responseData = response.data;
                responseData.code = code;

                routeService.goToState('base.auth.direct-channel', {
                    params: responseData
                });
            };

            var error = function() {};

            uiHelperService.showLoader();

            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.GET_QUOTATION.replace('{basePlanCode}', code)
            }).success(success).error(error);
        };

        ctrl.gotoAppDetails = function() {
            routeService.goToState('base.auth.direct-channel', {
                params: {
                    progressState: 'APP_DETAILS'
                }
            });
        };
    }

    angular.module('directChannel').directive(directiveName, dummyEntryPage);
})();
