(function() {
    'use strict';

    var directiveName = 'applicationDetailsPage';

    /*@ngInject*/
    function applicationDetailsPage() {
        return {
            scope: {
                progressInfo: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: applicationDetailsCtrl,
            templateUrl: 'templates/directChannel/applicationDetails.html'
        };
    }

    /*@ngInject*/
    function applicationDetailsCtrl() {
        var ctrl = this;
    }

    angular.module('directChannel').directive(directiveName, applicationDetailsPage);
})();
