(function() {
    'use strict';

    var directiveName = 'stepIndicator';

    /*@ngInject*/
    function stepIndicator() {
        return {
            scope: {
                totalSteps: '=',
                currentStep: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: stepIndicatorCtrl,
            templateUrl: 'templates/directChannel/stepIndicator.html'
        };
    }

    /*@ngInject*/
    function stepIndicatorCtrl() {
        var ctrl = this;

        ctrl.getArray = function(total) {
            return new Array(total);
        };
    }

    angular.module('directChannel').directive(directiveName, stepIndicator);
})();
