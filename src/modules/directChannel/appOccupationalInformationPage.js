(function() {
    'use strict';

    var directiveName = 'appOccupationalInformationPage';

    /*@ngInject*/
    function appOccupationalInformationPage() {
        return {
            scope: {
                progressInfo: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: appOccupationalInformationCtrl,
            templateUrl: 'templates/directChannel/appOccupationalInformation.html'
        };
    }

    /*@ngInject*/
    function appOccupationalInformationCtrl(appOccupationalInformationConfig, FormManager, $timeout, uiHelperService, routeService, storageService, APP_CONST, APP_CONFIG, $http, $rootScope, $scope, $anchorScroll) {
        var ctrl = this;
        ctrl.progressInfo.currentStep = 3;
        ctrl.progressInfo.totalSteps = 8;

        ctrl.formModel = {};
        ctrl.config = appOccupationalInformationConfig;
        ctrl.formManager = new FormManager(ctrl.config.occupationalInformationForm.name, ctrl.config.occupationalInformationForm.mode, ctrl);
        var saveAndContinue = false;

        ctrl.saveAndContinue = function() {
            var success = function(response) {
                /*routeService.goToState('base.auth.change-password-ack', {
                    params: response
                });*/

            };

            var error = function(response) {
                /*routeService.goToState('base.auth.change-password-ack', {
                    params: response
                });*/
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    saveAndContinue = true;
                    //send request to server

                    /*step update will be moved to success callback*/
                    //step 4 is not ready
                    //ctrl.progressInfo.appDetailsStep = '4';
                    $anchorScroll();
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );

        };

        ctrl.back = function() {
            //if (confirm('All information entered will be lost. Do you wish to leave this site?')) {
            ctrl.progressInfo.appDetailsStep = '2';
            $anchorScroll();
            //}
        };

        ctrl.exit = function() {
            /*to be confirmed. not mentioned in fsd*/
        };

        ctrl.showCompanyName = function() {
            if (ctrl.formManager.getValue('occupation') !== 'unemployed' &&
                ctrl.formManager.getValue('occupation') !== 'housewife' &&
                ctrl.formManager.getValue('occupation') !== 'retiree' &&
                ctrl.formManager.getValue('occupation') !== 'student' &&
                ctrl.formManager.getValue('occupation') !== 'child') {

                return true;
            }

            return false;
        };

        ctrl.showSchoolName = function() {
            if (ctrl.formManager.getValue('occupation') === 'student' ||
                ctrl.formManager.getValue('occupation') === 'child') {

                return true;
            }

            return false;
        };

        ctrl.showBusinessNature = function() {
            if (ctrl.formManager.getValue('occupation') !== 'unemployed' &&
                ctrl.formManager.getValue('occupation') !== 'housewife' &&
                ctrl.formManager.getValue('occupation') !== 'retiree' &&
                ctrl.formManager.getValue('occupation') !== 'student' &&
                ctrl.formManager.getValue('occupation') !== 'child') {

                return true;
            }

            return false;
        };

        ctrl.showOthers = function() {
            if (ctrl.formManager.getValue('businessNature') === 'others') {
                return true;
            }

            ctrl.formManager.removeComponent('others');
            return false;
        };

        ctrl.showDutiesInvolved = function() {
            if (ctrl.formManager.getValue('occupation') !== 'unemployed' &&
                ctrl.formManager.getValue('occupation') !== 'housewife' &&
                ctrl.formManager.getValue('occupation') !== 'retiree' &&
                ctrl.formManager.getValue('occupation') !== 'student' &&
                ctrl.formManager.getValue('occupation') !== 'child') {

                return true;
            }

            return false;
        };

        ctrl.showAnnualIncome = function() {
            return true;
        };

        ctrl.showSourceOfWealth = function() {
            return true;
        };

        ctrl.showSourceOfWealthDetails = function() {
            if (ctrl.formManager.getValue('sourceOfWealth') === 'others') {
                return true;
            }

            ctrl.formManager.removeComponent('sourceOfWealthDetails');
            return false;
        };

        ctrl.showTaxResidency = function() {
            if (ctrl.formManager.getValue('occupation') !== 'unemployed' &&
                ctrl.formManager.getValue('occupation') !== 'housewife' &&
                ctrl.formManager.getValue('occupation') !== 'retiree' &&
                ctrl.formManager.getValue('occupation') !== 'student' &&
                ctrl.formManager.getValue('occupation') !== 'child') {

                return true;
            }

            return false;
        };

        var occupationChange = $rootScope.$on('valueChange_occupation', function() {
            if (ctrl.formManager.getValue('occupation') === 'unemployed' ||
                ctrl.formManager.getValue('occupation') === 'housewife' ||
                ctrl.formManager.getValue('occupation') === 'retiree') {

                ctrl.formManager.removeComponent('companyName');
                ctrl.formManager.removeComponent('schoolName');
                ctrl.formManager.removeComponent('businessNature');
                ctrl.formManager.removeComponent('dutiesInvolved');
                ctrl.formManager.removeComponent('taxResidency');
            } else if (ctrl.formManager.getValue('occupation') === 'student' ||
                ctrl.formManager.getValue('occupation') === 'child') {

                ctrl.formManager.removeComponent('companyName');
                ctrl.formManager.removeComponent('businessNature');
                ctrl.formManager.removeComponent('dutiesInvolved');
                ctrl.formManager.removeComponent('taxResidency');
            } else {
                ctrl.formManager.removeComponent('schoolName');
            }
        });

        $(window).on('beforeunload.step3', function(e) {
            /*KIV for this checking since it is single page navigation*/
            if (saveAndContinue) {
                return undefined;
            }

            var confirmationMessage = 'All information entered will be lost';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });

        $scope.$on('$destroy', function() {
            occupationChange();
            $(window).off('beforeunload.step3');
        });

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);
    }

    angular.module('directChannel').directive(directiveName, appOccupationalInformationPage);
})();
