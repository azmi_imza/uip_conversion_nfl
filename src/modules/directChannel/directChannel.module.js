(function() {
    'use strict';
    angular.module('directChannel', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(statePrefix.concat('dummy-entry'), {
                    url: '/dummy-entry',
                    template: '<dummy-entry-page></dummy-entry-page>',
                    data: {
                        screenCode: '-'
                    },
                    params: {
                        params: null
                    }
                });

                $stateProvider.state(statePrefix.concat('direct-channel'), {
                    url: '/direct-channel',
                    template: '<direct-channel-page></direct-channel-page>',
                    data: {
                        screenCode: '-'
                    },
                    params: {
                        params: null
                    }
                });
            });
})();
