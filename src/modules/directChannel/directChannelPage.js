(function() {
    'use strict';

    var directiveName = 'directChannelPage';

    /*@ngInject*/
    function directChannelPage() {
        return {
            scope: {},
            bindToController: true,
            controller: directChannelCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/directChannel/directChannel.html'
        };
    }

    /*@ngInject*/
    function directChannelCtrl($stateParams) {
        var ctrl = this;

        ctrl.progressInfo = {};

        if ($stateParams.params) {
            ctrl.progressInfo.quotation = {};
            angular.extend(ctrl.progressInfo.quotation, $stateParams.params);
            ctrl.progressInfo.state = $stateParams.params.progressState ? $stateParams.params.progressState : 'QUOTATION';
        }
    }

    angular.module('directChannel').directive(directiveName, directChannelPage);
})();
