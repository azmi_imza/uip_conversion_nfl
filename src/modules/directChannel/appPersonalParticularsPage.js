(function() {
    'use strict';

    var directiveName = 'appPersonalParticularsPage';

    /*@ngInject*/
    function appPersonalParticularsPage() {
        return {
            scope: {
                progressInfo: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: appPersonalParticularsCtrl,
            templateUrl: 'templates/directChannel/appPersonalParticulars.html'
        };
    }

    /*@ngInject*/
    function appPersonalParticularsCtrl(appPersonalParticularsConfig, FormManager, $timeout, uiHelperService, routeService, storageService, APP_CONST, APP_CONFIG, $http, $rootScope, $scope, _, $anchorScroll) {
        var ctrl = this;
        ctrl.progressInfo.currentStep = 2;
        ctrl.progressInfo.totalSteps = 8;

        ctrl.formModel = {};
        ctrl.config = appPersonalParticularsConfig;
        ctrl.formManager = new FormManager(ctrl.config.personalParticularsForm.name, ctrl.config.personalParticularsForm.mode, ctrl);
        ctrl.collapsePanel = {};
        var saveAndContinue = false;

        ctrl.saveAndContinue = function() {
            var success = function(response) {
                /*routeService.goToState('base.auth.change-password-ack', {
                    params: response
                });*/

            };

            var error = function(response) {
                /*routeService.goToState('base.auth.change-password-ack', {
                    params: response
                });*/
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    saveAndContinue = true;
                    //send request to server

                    /*step update will be moved to success callback*/
                    ctrl.progressInfo.appDetailsStep = '3';
                    $anchorScroll();
                },

                function(response) {
                    $timeout(function() {
                        updateComponentHeight();
                        uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                    });
                }

            );

        };

        ctrl.back = function() {
            /*step 1 is not ready*/
            //ctrl.progressInfo.appDetailsStep = '1';
            $anchorScroll();
        };

        ctrl.exit = function() {
            /*to be confirmed. not mentioned in fsd*/
        };

        ctrl.initCollapse = function(id) {
            ctrl.collapsePanel[id] = true;
            $('#' + id).on('hidden.bs.collapse shown.bs.collapse', function() {
                ctrl.collapsePanel[id] = $('#' + id).hasClass('in');
                $scope.$apply();
            });
        };

        ctrl.showNationality = function() {
            if (ctrl.formManager.getValue('idType') !== 'singapore' &&
                ctrl.formManager.getValue('idType') !== 'malaysia' &&
                ctrl.formManager.getValue('idType') !== 'brunei') {
                return true;
            }

            ctrl.formManager.removeComponent('nationality');
            return false;
        };

        ctrl.showSingaporePr = function() {
            if (ctrl.formManager.getValue('idType') === 'singapore') {
                return true;
            }

            ctrl.formManager.removeComponent('singaporePr');
            return false;
        };

        ctrl.showValidPass = function() {
            if (ctrl.formManager.getValue('idType') === 'malaysia' ||
                ctrl.formManager.getValue('idType') === 'brunei' ||
                ctrl.formManager.getValue('idType') === 'passport') {
                return true;
            }

            ctrl.formManager.removeComponent('validPass');
            return false;
        };

        var updateComponentHeight = function() {
            if (window.innerWidth < 768) {
                return;
            }

            var minHeight = '65px';
            $('.app-details-particulars').each(function(index) {
                if (index % 2 === 0) {
                    minHeight = $(this).css('height');
                } else {
                    $(this).css('min-height', minHeight);
                }
            });
        };

        var checkScreenSize = function() {
            if (window.innerWidth < 768) {
                ctrl.isDesktop = false;
            } else {
                ctrl.isDesktop = true;
                _.forEach(ctrl.collapsePanel, function(value, key) {
                    if (!$('#' + key).hasClass('in')) {
                        $('#' + key + 'Control').click();
                    }
                });

                updateComponentHeight();
            }
        };

        $(window).on('resize', checkScreenSize);

        var dobChange = $rootScope.$on('valueChange_dob', function() {
            var today = new Date();
            var todayMonth = today.getMonth() > 9 ? today.getMonth() : '0' + today.getMonth();
            var todayDate = today.getDate() > 9 ? today.getDate() : '0' + today.getDate();
            var todayDateString = '' + today.getFullYear() + todayMonth + todayDate;

            var dob = new Date(ctrl.formManager.getValue('dob'));
            var dobMonth = dob.getMonth() > 9 ? dob.getMonth() : '0' + dob.getMonth();
            var dobDate = dob.getDate() > 9 ? dob.getDate() : '0' + dob.getDate();
            var dobDateString = '' + dob.getFullYear() + dobMonth + dobDate;

            var age = Math.floor((todayDateString - dobDateString) / 10000) + 1;
            ctrl.formManager.setValue('age', age);
        });

        var idTypeChange = $rootScope.$on('valueChange_idType', function() {
            $timeout(function() {
                updateComponentHeight();
            });
        });

        var residentialTypeChange = $rootScope.$on('valueChange_residentialAddress', function() {
            if (ctrl.formManager.getValue('residentialAddress') === 'local') {
                ctrl.formManager.removeComponent('residentialAddress1');
                ctrl.formManager.removeComponent('residentialAddress2');
                ctrl.formManager.removeComponent('residentialAddress3');
                ctrl.formManager.removeComponent('residentialAddress4');
                ctrl.formManager.removeComponent('residentialCountry');
            } else {
                ctrl.formManager.removeComponent('residentialPostalCode');
                ctrl.formManager.removeComponent('residentialBlock');
                ctrl.formManager.removeComponent('residentialStreetName');
                ctrl.formManager.removeComponent('residentialUnitNo');
                ctrl.formManager.removeComponent('residentialBuildingName');
            }
        });

        var mailingTypeChange = $rootScope.$on('valueChange_mailingAddress', function() {
            if (ctrl.formManager.getValue('mailingAddress') === 'local') {
                ctrl.formManager.removeComponent('mailingAddress1');
                ctrl.formManager.removeComponent('mailingAddress2');
                ctrl.formManager.removeComponent('mailingAddress3');
                ctrl.formManager.removeComponent('mailingAddress4');
                ctrl.formManager.removeComponent('mailingCountry');
            } else {
                ctrl.formManager.removeComponent('mailingPostalCode');
                ctrl.formManager.removeComponent('mailingBlock');
                ctrl.formManager.removeComponent('mailingStreetName');
                ctrl.formManager.removeComponent('mailingUnitNo');
                ctrl.formManager.removeComponent('mailingBuildingName');
            }
        });

        var differentMailingChange = $rootScope.$on('valueChange_differentMailingAddress', function() {
            if (ctrl.formManager.getValue('differentMailingAddress') !== 'Y') {
                ctrl.formManager.removeComponent('mailingPostalCode');
                ctrl.formManager.removeComponent('mailingBlock');
                ctrl.formManager.removeComponent('mailingStreetName');
                ctrl.formManager.removeComponent('mailingUnitNo');
                ctrl.formManager.removeComponent('mailingBuildingName');
                ctrl.formManager.removeComponent('mailingAddress1');
                ctrl.formManager.removeComponent('mailingAddress2');
                ctrl.formManager.removeComponent('mailingAddress3');
                ctrl.formManager.removeComponent('mailingAddress4');
                ctrl.formManager.removeComponent('mailingCountry');
                ctrl.formManager.removeComponent('reasonDifferentMailing');
            }
        });

        $(window).on('beforeunload.step2', function(e) {
            /*KIV for this checking since it is single page navigation*/
            if (saveAndContinue) {
                return undefined;
            }

            var confirmationMessage = 'All information entered will be lost';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });

        $scope.$on('$destroy', function() {
            dobChange();
            idTypeChange();
            residentialTypeChange();
            mailingTypeChange();
            differentMailingChange();
            $(window).off('resize', checkScreenSize);
            $(window).off('beforeunload.step2');
            _.forEach(ctrl.collapsePanel, function(value, key) {
                $('#' + key).off('.collapse');
            });
        });

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);
    }

    angular.module('directChannel').directive(directiveName, appPersonalParticularsPage);
})();
