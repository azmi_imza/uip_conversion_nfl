(function() {
    'use strict';

    angular.module('directChannel')
        .value('quotationConfig', {
            gender: {
                name: 'gender',
                mode: 'edit',
                label: {
                    name: 'cfo.label.gender',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 bottom-padding',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-9 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                items: [{
                    label: 'cfo.label.male',
                    value: 'M',
                    activeImagePath: 'ui-components/images/male_white.png',
                    inactiveImagePath: 'ui-components/images/male_red.png',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }, {
                    label: 'cfo.label.female',
                    value: 'F',
                    activeImagePath: 'ui-components/images/female_white.png',
                    inactiveImagePath: 'ui-components/images/female_red.png',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }],
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                buttonSize: 'big',
                defaultValue: 'M'
            },
            smokerStatus: {
                name: 'is_smoker',
                mode: 'edit',
                label: {
                    name: 'cfo.label.doYouSmoke',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 bottom-padding margin-top-15',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-9 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                items: [{
                    label: 'cfo.label.yes',
                    value: 'Y',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }, {
                    label: 'cfo.label.no',
                    value: 'N',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }],
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                buttonSize: 'big',
                defaultValue: 'Y'
            },
            dateOfBirth: {
                name: 'date_of_birth',
                mode: 'edit',
                label: {
                    name: 'cfo.label.dob',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding: 10px 15px;',
                columnCssClass: 'col-xs-12 col-sm-6 bottom-padding',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-9 no-padding',
                fieldClass: 'lightBlue-date field-size__large',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                max: 'today',
                format: 'dd MMM yyyy',
                minDependency: [],
                maxDependency: []
            },
            residenceCountry: {
                name: 'country_code',
                mode: 'edit',
                label: {
                    name: 'cfo.label.countryOfResidence',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                columnCssClass: 'col-xs-12 col-sm-6 bottom-padding margin-top-15',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-9 no-padding',
                fieldClass: 'lightBlue field-size__large__ddl',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                ddlType: 'static',
                url: 'ddl/MaintCountry',
                ddlItems: []
            },
            isForeigner: {
                name: 'is_foreigner',
                mode: 'edit',
                label: {
                    name: 'cfo.label.foreigner',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 bottom-padding margin-top-15 margin-top-20',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-5 no-padding residence-field-width',
                fieldStyle: 'padding: 10px 15px;',
                items: [{
                    label: 'cfo.label.yes',
                    value: 'Y',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }, {
                    label: 'cfo.label.no',
                    value: 'N',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }],
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                buttonSize: 'big',
                defaultValue: 'Y'
            },
            coverageTerm: {
                name: 'coverage_term',
                mode: 'edit',
                label: {
                    name: 'cfo.label.wishToBeCovered',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                columnCssClass: 'bottom-padding',
                labelCssClass: 'font-size-20 padding-bottom-20',
                inputCssClass: 'col-xs-12 col-sm-9 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large__ddl',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                ddlType: 'static',
                ddlItems: []
            },
            coveredSumAssured: {
                name: 'covered_sum_assured',
                mode: 'edit',
                label: {
                    name: 'cfo.label.sumAssured',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'bottom-padding',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: true,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                },
                dependencies: [],
                defaultDisable: false
            },
            coveredPremiumAmount: {
                name: 'covered_premium_amount',
                mode: 'edit',
                label: {
                    name: 'cfo.label.amount',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 bottom-padding margin-top-15',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: true,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                },
                dependencies: [],
                defaultDisable: true
            },
            coveredPaymentFrequency: {
                name: 'covered_frequency',
                mode: 'edit',
                label: {
                    name: 'cfo.label.frequency',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                columnCssClass: 'col-xs-12 bottom-padding margin-top-15',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large__ddl',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                ddlType: 'static',
                url: '',
                ddlItems: [],
                dependencies: [],
                defaultDisable: true
            },
            budgetSumAssured: {
                name: 'budget_sum_assured',
                mode: 'edit',
                label: {
                    name: 'cfo.label.sumAssured',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 bottom-padding',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: true,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                },
                dependencies: [],
                defaultDisable: true
            },
            budgetPremiumAmount: {
                name: 'budget_premium_amount',
                mode: 'edit',
                label: {
                    name: 'cfo.label.amount',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 bottom-padding margin-top-15',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: true,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                },
                dependencies: [],
                defaultDisable: false
            },
            budgetPaymentFrequency: {
                name: 'budget_frequency',
                mode: 'edit',
                label: {
                    name: 'cfo.label.frequency',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                columnCssClass: 'col-xs-12 bottom-padding margin-top-15',
                labelCssClass: 'col-sm-5 font-size-23 no-padding padding-bottom-20',
                inputCssClass: 'col-sm-4 no-padding',
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'lightBlue field-size__large__ddl',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                ddlType: 'static',
                url: '',
                ddlItems: [],
                dependencies: [],
                defaultDisable: false
            },
            riderSelection: {
                name: 'rider',
                mode: 'edit',
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 bottom-padding',
                inputCssClass: 'col-sm-5',
                fieldStyle: 'padding: 0',
                labelStyle: 'display: none',
                items: [{
                    label: 'cfo.label.yes',
                    value: 'Y',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }, {
                    label: 'cfo.label.no',
                    value: 'N',
                    activeImagePath: '',
                    inactiveImagePath: '',
                    tooltip: {
                        tooltip: '',
                        tooltipPlacement: ''
                    }
                }],
                buttonSize: 'small',
                defaultValue: 'Y'
            },
            benefitTooltip: {
                name: '',
                tooltip: 'cfo.label.benefitTooltip',
                tooltipPlacement: 'bottom'
            },
            policyIllustrationTooltip: {
                name: '',
                tooltip: 'cfo.label.policyIllustrationTooltip',
                tooltipPlacement: 'top'
            },
            coveredTooltip: {
                name: '',
                tooltip: 'cfo.label.beCoveredTooltip',
                tooltipPlacement: 'top'
            },
            budgetTooltip: {
                name: '',
                tooltip: 'cfo.label.setAsideForPremiumsTooltip',
                tooltipPlacement: 'top'
            },
            enjoyBenefitTooltip: {
                name: '',
                tooltip: 'cfo.label.enjoyBenefitTooltip',
                tooltipPlacement: 'top'
            },
            deathBenefitTooltip: {
                name: '',
                tooltip: 'cfo.label.deathBenefitTooltip',
                tooltipPlacement: 'top'
            },
            surrenderValueTooltip: {
                name: '',
                tooltip: 'cfo.label.surrenderValueBenefitTooltip',
                tooltipPlacement: 'top'
            }
        })
        .value('appPersonalParticularsConfig', {
            personalParticularsForm: {
                name: 'personalParticularsForm',
                mode: 'edit'
            },
            title: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.title',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'title 1',
                    value: 'title1'
                }, {
                    label: 'title 2',
                    value: 'title2'
                }]
            },
            name: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.name',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                defaultDisable: true
            },
            dob: {
                label: {
                    name: 'cfo.label.dob',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                format: 'dd MMM yyyy',
                columnCssClass: 'col-xs-12 col-sm-6',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                min: '',
                max: 'today',
                minDependency: [],
                maxDependency: [],
                defaultDisable: true
            },
            age: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.age',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-5 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '2',
                        currencyDecimal: '0'
                    }
                }
            },
            idNumber: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 9',
                        value: 9
                    }
                },
                label: {
                    name: 'cfo.label.idNumber',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                defaultDisable: true
            },
            idType: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.idType',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'idType 1',
                    value: 'idType1'
                }, {
                    label: 'idType 2',
                    value: 'idType2'
                }, {
                    label: 'singapore',
                    value: 'singapore'
                }, {
                    label: 'malaysia',
                    value: 'malaysia'
                }, {
                    label: 'brunei',
                    value: 'brunei'
                }, {
                    label: 'passport',
                    value: 'passport'
                }],
                defaultDisable: true
            },
            nationality: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.nationality',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'nationality 1',
                    value: 'nationality1'
                }, {
                    label: 'nationality 2',
                    value: 'nationality2'
                }]
            },
            countryOfBirth: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.countryOfBirth',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'countryOfBirth 1',
                    value: 'countryOfBirth1'
                }, {
                    label: 'countryOfBirth 2',
                    value: 'countryOfBirth2'
                }],
                defaultDisable: true
            },
            singaporePr: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.singaporePr',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-5 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Yes',
                    value: 'Y'
                }, {
                    label: 'No',
                    value: 'N'
                }],
                alignment: 'inline'
            },
            validPass: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.validPass',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-5 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Yes',
                    value: 'Y'
                }, {
                    label: 'No',
                    value: 'N'
                }],
                alignment: 'inline'
            },
            gender: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.gender',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Male',
                    value: 'male'
                }, {
                    label: 'Female',
                    value: 'female'
                }],
                alignment: 'inline',
                defaultDisable: true
            },
            height: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.height',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '2',
                        currencyDecimal: '2'
                    }
                }
            },
            weight: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.weight',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '2',
                        currencyDecimal: '1'
                    }
                }
            },
            race: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.race',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'race 1',
                    value: 'race1'
                }, {
                    label: 'race 2',
                    value: 'race2'
                }],
                defaultDisable: true
            },
            maritalStatus: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.maritalStatus',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'maritalStatus 1',
                    value: 'maritalStatus1'
                }, {
                    label: 'maritalStatus 2',
                    value: 'maritalStatus2'
                }]
            },
            email1: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.email1',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;'
            },
            email2: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.email2',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6 app-details-particulars',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px; min-height:65px;'
            },
            residentialAddress: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.residentialAddress',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-3 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Local',
                    value: 'local'
                }, {
                    label: 'Foreign',
                    value: 'foreign'
                }],
                alignment: 'inline',
                defaultValue: 'local'
            },
            residentialPostalCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.residentialPostalCode',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '6',
                        currencyDecimal: '0'
                    }
                }
            },
            residentialBlock: {
                validators: {},
                label: {
                    name: 'cfo.label.residentialBlock',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialStreetName: {
                validators: {},
                label: {
                    name: 'cfo.label.residentialStreetName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialUnitNo: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 5',
                        value: 5
                    }
                },
                label: {
                    name: 'cfo.label.residentialUnitNo',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialBuildingName: {
                validators: {},
                label: {
                    name: 'cfo.label.residentialBuildingName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialAddress1: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.residentialAddress1',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialAddress2: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.residentialAddress2',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialAddress3: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.residentialAddress3',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialAddress4: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.residentialAddress4',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            residentialCountry: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.residentialCountry',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'residentialCountry 1',
                    value: 'residentialCountry1'
                }, {
                    label: 'residentialCountry 2',
                    value: 'residentialCountry2'
                }]
            },
            differentMailingAddress: {
                validators: {},
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: '',
                labelCssClass: '',
                inputCssClass: '',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Tick id mailing address is different from residential address',
                    value: 'Y',
                    name: 'differentMailingAddress'
                }],
                fieldStyle: 'padding-left: 15px;',
                alignment: 'inline',
                labelStyle: 'float:left;',
                inputStyle: 'float:left;',
                singleOrMultiValue: 'single'
            },
            mailingAddress: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.mailingAddress',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-3 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'Local',
                    value: 'local'
                }, {
                    label: 'Foreign',
                    value: 'foreign'
                }],
                alignment: 'inline',
                defaultValue: 'local'
            },
            mailingPostalCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.mailingPostalCode',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '6',
                        currencyDecimal: '0'
                    }
                }
            },
            mailingBlock: {
                validators: {},
                label: {
                    name: 'cfo.label.mailingBlock',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingStreetName: {
                validators: {},
                label: {
                    name: 'cfo.label.mailingStreetName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingUnitNo: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 5',
                        value: 5
                    }
                },
                label: {
                    name: 'cfo.label.mailingUnitNo',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingBuildingName: {
                validators: {},
                label: {
                    name: 'cfo.label.mailingBuildingName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingAddress1: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.mailingAddress1',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingAddress2: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.mailingAddress2',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingAddress3: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.mailingAddress3',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingAddress4: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.mailingAddress4',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            mailingCountry: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.mailingCountry',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-2 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'mailingCountry 1',
                    value: 'mailingCountry1'
                }, {
                    label: 'mailingCountry 2',
                    value: 'mailingCountry2'
                }]
            },
            reasonDifferentMailing: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                label: {
                    name: 'cfo.label.reasonDifferentMailing',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                displayRows: 2
            },
            homeCountryCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-6',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12',
                columnStyle: 'padding: 5px 5px 0 0',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'homeCountryCode 1',
                    value: 'homeCountryCode1'
                }, {
                    label: 'homeCountryCode 2',
                    value: 'homeCountryCode2'
                }, {
                    label: 'singapore (65)',
                    value: 'sg'
                }],
                defaultValue: 'sg'
            },
            homeNumber: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                columnStyle: 'padding:5px 0 0 0;',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12 no-padding',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                }
            },
            officeCountryCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-6',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12',
                columnStyle: 'padding: 5px 5px 0 0',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'officeCountryCode 1',
                    value: 'officeCountryCode1'
                }, {
                    label: 'officeCountryCode 2',
                    value: 'officeCountryCode2'
                }, {
                    label: 'singapore (65)',
                    value: 'sg'
                }],
                defaultValue: 'sg'
            },
            officeNumber: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                columnStyle: 'padding:5px 0 0 0;',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12 no-padding',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                }
            },
            officeExt: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.ext',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                placeHolder: '',
                columnCssClass: 'col-xs-12',
                columnStyle: 'padding: 5px 15px;',
                labelCssClass: 'col-xs-12 col-sm-4 no-padding',
                inputCssClass: 'col-xs-8 col-sm-4 no-padding',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '5',
                        currencyDecimal: '0'
                    }
                }
            },
            mobileCountryCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-6',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12',
                columnStyle: 'padding: 5px 5px 0 0',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'mobileCountryCode 1',
                    value: 'mobileCountryCode1'
                }, {
                    label: 'mobileCountryCode 2',
                    value: 'mobileCountryCode2'
                }, {
                    label: 'singapore (65)',
                    value: 'sg'
                }],
                defaultValue: 'sg'
            },
            mobileNumber: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                columnStyle: 'padding:5px 0 0 0;',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12 no-padding',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                }
            },
            faxCountryCode: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-6',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12',
                columnStyle: 'padding: 5px 5px 0 0',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'faxCountryCode 1',
                    value: 'faxCountryCode1'
                }, {
                    label: 'faxCountryCode 2',
                    value: 'faxCountryCode2'
                }, {
                    label: 'singapore (65)',
                    value: 'sg'
                }],
                defaultValue: 'sg'
            },
            faxNumber: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12 col-sm-6',
                columnStyle: 'padding:5px 0 0 0;',
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12 no-padding',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '8',
                        currencyDecimal: '0'
                    }
                }
            }
        })
        .value('appOccupationalInformationConfig', {
            occupationalInformationForm: {
                name: 'occupationalInformationForm',
                mode: 'edit'
            },
            occupation: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.occupation',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'Unemployed',
                    value: 'unemployed'
                }, {
                    label: 'Housewife',
                    value: 'housewife'
                }, {
                    label: 'Student',
                    value: 'student'
                }, {
                    label: 'Child',
                    value: 'child'
                }, {
                    label: 'Retiree',
                    value: 'retiree'
                }, {
                    label: 'Others',
                    value: 'others'
                }]
            },
            companyName: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: 40
                    }
                },
                label: {
                    name: 'cfo.label.companyName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            schoolName: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: 40
                    }
                },
                label: {
                    name: 'cfo.label.schoolName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            businessNature: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.businessNature',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'nature 1',
                    value: 'nature1'
                }, {
                    label: 'nature 2',
                    value: 'nature2'
                }, {
                    label: 'others',
                    value: 'others'
                }]
            },
            others: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: 40
                    }
                },
                label: {
                    name: 'cfo.label.others',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            dutiesInvolved: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: 40
                    }
                },
                label: {
                    name: 'cfo.label.dutiesInvolved',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;'
            },
            annualIncome: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.annualIncome',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '8',
                        currencyDecimal: '2'
                    }
                }
            },
            sourceOfWealth: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.sourceOfWealth',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-4 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                ddlItems: [{
                    label: 'sourceOfWealth 1',
                    value: 'sourceOfWealth1'
                }, {
                    label: 'sourceOfWealth 2',
                    value: 'sourceOfWealth2'
                }, {
                    label: 'others',
                    value: 'others'
                }]
            },
            sourceOfWealthDetails: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: 40
                    }
                },
                label: {
                    name: 'cfo.label.sourceOfWealthDetails',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-6 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                displayRows: '2'
            },
            taxResidency: {
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                label: {
                    name: 'cfo.label.taxResidency',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                columnCssClass: 'col-xs-12',
                labelCssClass: 'col-xs-12 col-sm-3 no-padding',
                inputCssClass: 'col-xs-12 col-sm-8 no-padding',
                fieldStyle: 'padding: 8px 15px;',
                url: '',
                ddlType: 'static',
                items: [{
                    label: 'I am Not U.S Tax Resident',
                    value: 'N'
                }, {
                    label: 'I am U.S Tax Resident',
                    value: 'Y'
                }],
                alignment: 'inline'
            }
        });
})();
