(function() {
    'use strict';

    var directiveName = 'quotationPage';

    /*@ngInject*/
    function quotationPage() {
        return {
            scope: {
                progressInfo: '='
            },
            bindToController: true,
            controller: quotationCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/directChannel/quotation.html'
        };
    }

    /*@ngInject*/
    function quotationCtrl(_, $timeout, quotationConfig, FormManager, $rootScope, $scope, $http, APP_CONFIG, uiHelperService) {
        var ctrl = this;

        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.acknowledgement = {};
        ctrl.formManager = new FormManager('quotationForm', 'edit', ctrl);
        ctrl.formModel.rider = [];
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.collapsePanel = {};

        ctrl.config.gender = quotationConfig.gender;
        ctrl.config.smokerStatus = quotationConfig.smokerStatus;
        ctrl.config.dateOfBirth = quotationConfig.dateOfBirth;
        ctrl.config.residenceCountry = quotationConfig.residenceCountry;
        ctrl.config.isForeigner = quotationConfig.isForeigner;
        ctrl.config.coverageTerm = quotationConfig.coverageTerm;
        ctrl.config.coverageOption = quotationConfig.coverageOption;
        ctrl.config.coveredSumAssured = quotationConfig.coveredSumAssured;
        ctrl.config.coveredPremiumAmount = quotationConfig.coveredPremiumAmount;
        ctrl.config.coveredPaymentFrequency = quotationConfig.coveredPaymentFrequency;
        ctrl.config.budgetSumAssured = quotationConfig.budgetSumAssured;
        ctrl.config.budgetPremiumAmount = quotationConfig.budgetPremiumAmount;
        ctrl.config.budgetPaymentFrequency = quotationConfig.budgetPaymentFrequency;
        ctrl.config.riderSelection = quotationConfig.riderSelection;
        ctrl.config.benefitTooltip = quotationConfig.benefitTooltip;
        ctrl.config.coveredTooltip = quotationConfig.coveredTooltip;
        ctrl.config.budgetTooltip = quotationConfig.budgetTooltip;
        ctrl.config.enjoyBenefitTooltip = quotationConfig.enjoyBenefitTooltip;
        ctrl.config.policyIllustrationTooltip = quotationConfig.policyIllustrationTooltip;
        ctrl.config.deathBenefitTooltip = quotationConfig.deathBenefitTooltip;
        ctrl.config.surrenderValueTooltip = quotationConfig.surrenderValueTooltip;

        ctrl.frequencyList = [];
        ctrl.coverageTermList = [];
        ctrl.riderList = ctrl.progressInfo.quotation.rider;

        var dobChange = $rootScope.$on('valueChange_date_of_birth', function() {
            var today = new Date();
            var todayMonth = today.getMonth() > 9 ? today.getMonth() : '0' + today.getMonth();
            var todayDate = today.getDate() > 9 ? today.getDate() : '0' + today.getDate();
            var todayDateString = '' + today.getFullYear() + todayMonth + todayDate;

            var dob = new Date(ctrl.formManager.getValue('date_of_birth'));
            var dobMonth = dob.getMonth() > 9 ? dob.getMonth() : '0' + dob.getMonth();
            var dobDate = dob.getDate() > 9 ? dob.getDate() : '0' + dob.getDate();
            var dobDateString = '' + dob.getFullYear() + dobMonth + dobDate;

            ctrl.age = Math.ceil((todayDateString - dobDateString) / 10000) + 1;
        });

        for (var i = 0; i < ctrl.progressInfo.quotation['frequency_list'].length; i++) {
            var frequencyListObject = {
                label: ctrl.progressInfo.quotation['frequency_list'][i].description,
                value: ctrl.progressInfo.quotation['frequency_list'][i].id,
                code: ctrl.progressInfo.quotation['frequency_list'][i].code
            };
            ctrl.frequencyList.push(frequencyListObject);
        }

        ctrl.config.coveredPaymentFrequency.ddlItems = ctrl.frequencyList;
        ctrl.config.budgetPaymentFrequency.ddlItems = ctrl.frequencyList;

        for (var j = 0; j < ctrl.progressInfo.quotation['coverage_term_list'].length; j++) {
            var coverageTermListObject = {
                label: ctrl.progressInfo.quotation['coverage_term_list'][j].description,
                value: ctrl.progressInfo.quotation['coverage_term_list'][j].id,
                code: ctrl.progressInfo.quotation['coverage_term_list'][j].code,
                maxAge: ctrl.progressInfo.quotation['coverage_term_list'][j]['maximum_life_assured_age'],
                minAge: ctrl.progressInfo.quotation['coverage_term_list'][j]['minimum_life_assured_age']
            };
            ctrl.coverageTermList.push(coverageTermListObject);
        }

        ctrl.config.coverageTerm.ddlItems = ctrl.coverageTermList;

        if (ctrl.formManager.getValue('coverage_option') === undefined) {
            ctrl.formManager.setValue('coverage_option', 'COVERED');
            ctrl.showCoverage = true;
        }

        $timeout(function() {
            if (ctrl.formManager.getValue('coverage_option') === 'BUDGET') {
                ctrl.formManager.removeComponent(ctrl.config.coveredSumAssured.name);
                ctrl.formManager.removeComponent(ctrl.config.coveredPremiumAmount.name);
                ctrl.formManager.removeComponent(ctrl.config.coveredPaymentFrequency.name);
            } else if (ctrl.formManager.getValue('coverage_option') === 'COVERED') {
                ctrl.formManager.removeComponent(ctrl.config.budgetSumAssured.name);
                ctrl.formManager.removeComponent(ctrl.config.budgetPremiumAmount.name);
                ctrl.formManager.removeComponent(ctrl.config.budgetPaymentFrequency.name);
            }
        });

        ctrl.calculateBtnClick = function() {
            var success = function(response) {
                if (response.status === 'failed') {
                    ctrl.showPlanBenefit = false;
                    ctrl.acknowledgement.details = [];
                    ctrl.acknowledgement.isSuccessAck = false;
                    ctrl.acknowledgement.isVisibleAck = true;
                    ctrl.acknowledgement.title = 'cfo.label.failed';

                    if (response.message) {
                        ctrl.acknowledgement.details.push({
                            message: response.message
                        });
                    }

                    uiHelperService.scrollTo($('#errorMessage').offset().top);
                } else {
                    ctrl.responseData = response.data;
                    ctrl.showPlanBenefit = true;
                    if (ctrl.formManager.getValue('coverage_option') === 'COVERED') {
                        ctrl.formManager.setValue('covered_sum_assured', response.data['sum_assured']);
                        ctrl.formManager.setValue('covered_premium_amount', response.data['premium_amount']);
                        ctrl.formManager.setValue('covered_frequency', response.data.frequency.id);
                        ctrl.formManager.setValue('covered_frequency_label', response.data.frequency.description);
                    } else if (ctrl.formManager.getValue('coverage_option') === 'BUDGET') {
                        ctrl.formManager.setValue('budget_sum_assured', response.data['sum_assured']);
                        ctrl.formManager.setValue('budget_premium_amount', response.data['premium_amount']);
                        ctrl.formManager.setValue('budget_frequency', response.data.frequency.id);
                        ctrl.formManager.setValue('budget_frequency_label', response.data.frequency.description);
                    }
                }
            };

            var error = function() {};

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    var validation = {
                        isValid: true
                    };

                    ctrl.coverageTermErrorMessage = '';
                    ctrl.coveredSumAssuredErrorMessage = '';
                    ctrl.budgetSumAssuredErrorMessage = '';

                    if (!ctrl.formManager.getValue('coverage_option')) {
                        ctrl.coverageOptionNotValid = true;
                        uiHelperService.scrollTo($('#coverageOptionValidation').offset().top - 30);
                        return;
                    } else {
                        ctrl.coverageOptionNotValid = false;
                    }

                    var formModel = angular.copy(ctrl.formModel);

                    if (ctrl.formManager.getValue('coverage_term')) {
                        for (var k = 0; k < ctrl.config.coverageTerm.ddlItems.length; k++) {
                            if (ctrl.formManager.getValue('coverage_term') === ctrl.config.coverageTerm.ddlItems[k].value) {
                                if (ctrl.age <= ctrl.config.coverageTerm.ddlItems[k].maxAge && ctrl.age >= ctrl.config.coverageTerm.ddlItems[k].minAge) {
                                    var selectedCoverageTerm = {
                                        id: ctrl.config.coverageTerm.ddlItems[k].value,
                                        code: ctrl.config.coverageTerm.ddlItems[k].code,
                                        description: ctrl.config.coverageTerm.ddlItems[k].label
                                    };

                                    formModel['coverage_term'] = selectedCoverageTerm;
                                    break;
                                } else {
                                    if (ctrl.age > ctrl.config.coverageTerm.ddlItems[k].maxAge) {
                                        ctrl.ageLimit = {
                                            ageLimit: ctrl.config.coverageTerm.ddlItems[k].maxAge
                                        };
                                        ctrl.coverageTermErrorMessage = 'cfo.label.allowedMaxAge';
                                    } else if (ctrl.age < ctrl.config.coverageTerm.ddlItems[k].minAge) {
                                        ctrl.ageLimit = {
                                            ageLimit: ctrl.config.coverageTerm.ddlItems[k].minAge
                                        };
                                        ctrl.coverageTermErrorMessage = 'cfo.label.allowedMinAge';
                                    }

                                    validation.isValid = false;
                                    validation.id = 'coverage_term';
                                }
                            }
                        }
                    }

                    for (var m = 0; m < ctrl.riderList.length; m++) {
                        if (ctrl.formManager.getValue(ctrl.riderList[m].id) === 'Y') {
                            var selectedRider = {
                                id: ctrl.riderList[m].id,
                                'plan_code': ctrl.riderList[m]['plan_code']
                            };
                            formModel.rider.push(selectedRider);
                        }
                    }

                    var selectedPaymentFrequency = {};

                    if (ctrl.formManager.getValue('coverage_option') === 'COVERED') {
                        if (ctrl.formManager.getValue('covered_frequency')) {
                            for (var n = 0; n < ctrl.config.coveredPaymentFrequency.ddlItems.length; n++) {
                                if (ctrl.formManager.getValue('covered_frequency') === ctrl.config.coveredPaymentFrequency.ddlItems[n].value) {
                                    selectedPaymentFrequency = {
                                        id: ctrl.config.coveredPaymentFrequency.ddlItems[n].value,
                                        code: ctrl.config.coveredPaymentFrequency.ddlItems[n].code,
                                        description: ctrl.config.coveredPaymentFrequency.ddlItems[n].label
                                    };

                                    formModel.frequency = selectedPaymentFrequency;
                                    break;
                                }
                            }
                        }

                        if (ctrl.formManager.getValue('covered_sum_assured')) {
                            ctrl.formManager.setValue('covered_sum_assured', parseFloat(ctrl.formManager.getValue('covered_sum_assured')));

                            if (ctrl.formManager.getValue('covered_sum_assured') >= ctrl.progressInfo.quotation['minimum_sum_assured_amount'] &&
                                ctrl.formManager.getValue('covered_sum_assured') <= ctrl.progressInfo.quotation['maximum_sum_assured_amount']) {
                                formModel['sum_assured'] = ctrl.formManager.getValue('covered_sum_assured');
                            } else {
                                ctrl.sumAssuredRange = {
                                    maxAmount: ctrl.progressInfo.quotation['maximum_sum_assured_amount'],
                                    minAmount: ctrl.progressInfo.quotation['minimum_sum_assured_amount']
                                };
                                ctrl.coveredSumAssuredErrorMessage = 'cfo.label.sumAssuredAmountRange';

                                validation.isValid = false;
                                validation.id = 'covered_sum_assured';
                            }
                        }

                        if (ctrl.formManager.getValue('covered_premium_amount')) {
                            formModel['premium_amount'] = ctrl.formManager.getValue('covered_premium_amount');
                        }

                    }

                    if (ctrl.formManager.getValue('coverage_option') === 'BUDGET') {
                        if (ctrl.formManager.getValue('budget_frequency')) {
                            for (var p = 0; p < ctrl.config.budgetPaymentFrequency.ddlItems.length; p++) {
                                if (ctrl.formManager.getValue('budget_frequency') === ctrl.config.budgetPaymentFrequency.ddlItems[p].value) {
                                    selectedPaymentFrequency = {
                                        id: ctrl.config.budgetPaymentFrequency.ddlItems[p].value,
                                        code: ctrl.config.budgetPaymentFrequency.ddlItems[p].code,
                                        description: ctrl.config.budgetPaymentFrequency.ddlItems[p].label
                                    };

                                    formModel.frequency = selectedPaymentFrequency;
                                    break;
                                }
                            }
                        }

                        if (ctrl.formManager.getValue('budget_sum_assured')) {
                            if (ctrl.formManager.getValue('budget_sum_assured') >= ctrl.progressInfo.quotation['minimum_sum_assured_amount'] &&
                                ctrl.formManager.getValue('budget_sum_assured') <= ctrl.progressInfo.quotation['maximum_sum_assured_amount']) {
                                formModel['sum_assured'] = ctrl.formManager.getValue('budget_sum_assured');
                            } else {
                                ctrl.sumAssuredRange = {
                                    maxAmount: ctrl.progressInfo.quotation['maximum_sum_assured_amount'],
                                    minAmount: ctrl.progressInfo.quotation['minimum_sum_assured_amount']
                                };
                                ctrl.budgetSumAssuredErrorMessage = 'cfo.label.sumAssuredAmountRange';

                                validation.isValid = false;
                                validation.id = 'budget_sum_assured';
                            }
                        }

                        if (ctrl.formManager.getValue('budget_premium_amount')) {
                            formModel['premium_amount'] = ctrl.formManager.getValue('budget_premium_amount');
                        }
                    }

                    if (!validation.isValid) {
                        uiHelperService.scrollTo($('#' + validation.id).offset().top);
                        return;
                    }

                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.COMPUTE_QUOTATION.replace('{basePlanCode}', ctrl.progressInfo.quotation.code),
                        data: formModel
                    }).success(success).error(error);
                },

                function(response) {
                    ctrl.coverageTermErrorMessage = '';
                    ctrl.coveredSumAssuredErrorMessage = '';
                    ctrl.budgetSumAssuredErrorMessage = '';

                    if (!ctrl.formManager.getValue('coverage_option')) {
                        ctrl.coverageOptionNotValid = true;
                    } else {
                        ctrl.coverageOptionNotValid = false;
                    }

                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.initCollapse = function(id, expand) {
            ctrl.collapsePanel[id] = expand;
            $('#' + id).on('hidden.bs.collapse shown.bs.collapse', function() {
                ctrl.collapsePanel[id] = $('#' + id).hasClass('in');
                $scope.$apply();
            });
        };

        ctrl.selectTooltip = function(event) {
            event.stopPropagation();
        };

        ctrl.selectCoverageOption = function(value) {
            ctrl.formManager.setValue('coverage_option', value);
            ctrl.showCoverage = false;
            $timeout(function() {
                ctrl.showCoverage = true;
            });

            if (ctrl.formManager.getValue('coverage_option') === 'BUDGET') {
                ctrl.formManager.removeComponent(ctrl.config.coveredSumAssured.name);
                ctrl.formManager.removeComponent(ctrl.config.coveredPremiumAmount.name);
                ctrl.formManager.removeComponent(ctrl.config.coveredPaymentFrequency.name);
            } else if (ctrl.formManager.getValue('coverage_option') === 'COVERED') {
                ctrl.formManager.removeComponent(ctrl.config.budgetSumAssured.name);
                ctrl.formManager.removeComponent(ctrl.config.budgetPremiumAmount.name);
                ctrl.formManager.removeComponent(ctrl.config.budgetPaymentFrequency.name);
            }
        };

        $scope.$on('$destroy', function() {
            dobChange();

            _.forEach(ctrl.collapsePanel, function(value, key) {
                $('#' + key).off('.collapse');
            });
        });
    }

    angular.module('directChannel').directive(directiveName, quotationPage);
})();
