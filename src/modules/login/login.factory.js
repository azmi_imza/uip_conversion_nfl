/* global RsaEncrypt */
/* eslint new-cap:0 */

(function() {
    'use strict';

    var loginFactory = function($location, $http, $state, $log, routeService, storageService, APP_CONST, APP_CONFIG, $rootScope) {
        var Login = function() {
            var authTokenHeader = APP_CONST.HTTP_HEADERS.AUTH_TOKEN;

            /*20160204 do not remove temp-random-key, force pass in keep changes value to force API triggering (IE issue) */
            this.getEnCryptKey = function() {
                return $http({
                    method: 'GET',
                    url: APP_CONFIG.API_PATHS.GET_ENCRYPT_KEY,
                    params: {
                        'temp-random-key': new Date().getTime()
                    }
                });
            };

            this.login = function(ctrl, keyData) {

                var publicKey = keyData['public_key'];
                var randomKey = keyData['random_key'];
                var migrationState = keyData['migration_state'];
                var hashedPassword = RsaEncrypt(publicKey, ctrl.config.signInForm.formManager.getValue('password'), randomKey, migrationState);

                $http({
                    method: 'POST',
                    url: APP_CONFIG.API_PATHS.LOGIN,
                    headers: {
                        'X-Username': ctrl.config.signInForm.formManager.getValue('userId').toUpperCase(),
                        'X-Password': hashedPassword,
                        APF_CD: APP_CONFIG.SETTINGS.APF_CD
                    }
                }).then(function(response) {
                        if (response.data.status === 'success') {

                            var token = response.headers(authTokenHeader);
                            var menuData = response.data.data.menu;

                            $log.debug('Response:' + response.data.status);

                            if (!response.data.data.subscriber) {
                                $log.error('MISSING SUBSCRIBER INFO');
                            }

                            var subscriber = response.data.data.subscriber;

                            var sessionReminder = parseInt(response.data.data.timeout) - parseInt(response.data.data.reminder);
                            var sessionRemaining = parseInt(response.data.data.reminder) / 60000;

                            storageService.set(APP_CONST.STORAGE_KEYS.SESSION_REMINDER_TIME, sessionReminder, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.SESSION_REMAINING_TIME, sessionRemaining, true);

                            storageService.set(APP_CONST.STORAGE_KEYS.PUBLICKEY, publicKey, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.RANDOMKEY, randomKey, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.MIGRATIONSTATE, migrationState, true);

                            storageService.set(APP_CONST.STORAGE_KEYS.USERNAME, subscriber.username, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.SUBSCRIBERNAME, subscriber.name, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.USERID, subscriber.ID, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.USERTYPE, subscriber['user_type'], true);
                            storageService.set(APP_CONST.STORAGE_KEYS.CKA_STATUS,
                                subscriber['is_cka_pass'] === 'Y', true);
                            storageService.set(APP_CONST.STORAGE_KEYS.SECURITYPOLICY, response.data.data['security_policy'], true);

                            //token to be set in after otp passed
                            //storageService.set(APP_CONST.STORAGE_KEYS.AUTH_TOKEN, token, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.MENU_DATA, menuData, true);

                            //location setting : panel clinic
                            storageService.set(APP_CONST.STORAGE_KEYS.HQ_LONGITUDE,
                                response.data.data['client_config']['hq_addr'].longitude, true);
                            storageService.set(APP_CONST.STORAGE_KEYS.HQ_LATITUDE,
                                response.data.data['client_config']['hq_addr'].latitude, true);

                            storageService.set(APP_CONST.STORAGE_KEYS.VALID_1FA, true, true);

                            ctrl.authenticated = true;
                            ctrl.unauthorized = false;

                            $rootScope.$emit('startTimeoutAlertTimer');

                            if (subscriber['is_first_time_login'] === 'Y') {
                                routeService.goToState('base.common.force-change-password', {
                                    token: token,
                                    subscriber: subscriber,
                                    keyData: keyData
                                });
                            } else if (subscriber['has_otp_mobile'] === 'N') {
                                routeService.goToState('base.common.register-otp', {
                                    token: token
                                });
                            } else {
                                routeService.goToState('base.common.login-otp', {
                                    token: token
                                });
                            }

                        }

                        ctrl.loginInProgress = false;
                    },

                    function(response) {
                        ctrl.loginInProgress = false;
                        $log.error('LOGIN FAILED, status: ' + response.status);
                        if (response.status >= 400 && response.status < 500) {
                            ctrl.loginErrorMsg = response.data.message;
                            ctrl.authenticated = false;
                            ctrl.unauthorized = true;
                            ctrl.loginBtnText = 'LOGIN_LOG_IN';
                            return;
                        }

                        $log.debug('UNABLE TO LOGIN');
                        ctrl.loginInProgress = false;
                        ctrl.authenticated = false;
                        ctrl.unauthorized = false;
                        ctrl.loginBtnText = 'LOGIN_LOG_IN';

                        //TODO: Alert handling in proper way
                        $log.error('Server request failure. Login API not available');
                        $state.go('login');
                    });

            };
        };

        return new Login();
    };

    angular.module('login')
        .factory('loginFactory', loginFactory);
})();
