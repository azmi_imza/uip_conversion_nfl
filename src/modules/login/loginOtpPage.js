(function() {
    'use strict';

    var directiveName = 'loginOtpPage';

    /*@ngInject*/
    function loginOtpPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: loginOtpCtrl,
            templateUrl: 'templates/login/loginOtp.html'
        };
    }

    /*@ngInject*/
    function loginOtpCtrl($timeout, FormManager, loginOtpConfig, uiHelperService, $http, APP_CONST, APP_CONFIG, routeService, storageService, $stateParams) {
        var ctrl = this;
        ctrl.config = loginOtpConfig;
        ctrl.formModel = {};
        ctrl.formManager = new FormManager(ctrl.config.loginOtpForm.name, ctrl.config.loginOtpForm.mode, ctrl);
        ctrl.resendPadding = '22px';

        if (!$stateParams.token) {
            routeService.goToHome();
        }

        ctrl.cancel = function() {
            routeService.goToHome();
        };

        ctrl.submit = function() {
            ctrl.acknowledgement = {};
            ctrl.acknowledgement.details = [];
            ctrl.acknowledgement.isSuccessAck = true;
            ctrl.acknowledgement.isVisibleAck = false;
            ctrl.acknowledgement.title = '';

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();

                    var success = function(response) {
                        if (response.status === 'success') {
                            storageService.set(APP_CONST.STORAGE_KEYS.AUTH_TOKEN, $stateParams.token, true);
                            var userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
                            if (userType === 'IN') {
                                routeService.goToState('base.auth.dashboard-in');
                            } else if (userType === 'CA') {
                                routeService.goToState('base.auth.dashboard-be');
                            }
                        } else {
                            if (response['error_code'] === '801099020') {
                                routeService.goToState('base.common.login-ack', {
                                    params: response
                                });
                            } else {

                                ctrl.acknowledgement.isSuccessAck = false;
                                ctrl.acknowledgement.isVisibleAck = true;

                                if (response.message) {
                                    ctrl.acknowledgement.details.push({
                                        message: response.message
                                    });
                                }

                                ctrl.formManager.setValue('oneTimePassword', '');

                                uiHelperService.scrollTo($('#errorMessage').offset().top);
                            }
                        }

                    };

                    var fail = function(response) {
                        ctrl.acknowledgement.isSuccessAck = false;
                        ctrl.acknowledgement.isVisibleAck = true;

                        if (response.message) {
                            ctrl.acknowledgement.details.push({
                                message: response.message
                            });
                        }
                    };

                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.OTP,
                        headers: {
                            'X-Auth-Token': $stateParams.token,
                            'Content-Type': 'application/json'
                        },
                        data: {
                            otp: ctrl.formManager.getValue('oneTimePassword')
                        }
                    }).success(success).error(fail)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    // TODO: form invalid handling
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.resend = function() {
            ctrl.acknowledgement = {};
            ctrl.acknowledgement.details = [];
            ctrl.acknowledgement.isSuccessAck = true;
            ctrl.acknowledgement.isVisibleAck = false;
            ctrl.acknowledgement.title = '';

            var success = function() {

            };

            var fail = function(response) {
                ctrl.acknowledgement.isSuccessAck = false;
                ctrl.acknowledgement.isVisibleAck = true;

                if (response.message) {
                    ctrl.acknowledgement.details.push({
                        message: response.message
                    });
                }
            };

            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.OTP,
                headers: {
                    'X-Auth-Token': $stateParams.token
                }
            }).success(success).error(fail);
        };

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);

        ctrl.resend();

    }

    angular.module('login').directive(directiveName, loginOtpPage);
})();
