(function() {
    'use strict';

    var directiveName = 'registerOtpPage';

    /*@ngInject*/
    function registerOtpPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: registerOtpCtrl,
            templateUrl: 'templates/login/registerOtp.html'
        };
    }

    /*@ngInject*/
    function registerOtpCtrl($timeout, FormManager, registerOtpConfig, uiHelperService, $http, APP_CONFIG, routeService, $stateParams) {
        var ctrl = this;
        ctrl.config = registerOtpConfig;
        ctrl.formModel = {};
        ctrl.formManager = new FormManager(ctrl.config.registerOtpForm.name, ctrl.config.registerOtpForm.mode, ctrl);

        if (!$stateParams.token) {
            routeService.goToHome();
        }

        ctrl.submit = function() {
            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    var success = function(response) {
                        response.toState = 'base.common.login-otp';
                        routeService.goToState('base.common.login-ack', {
                            token: $stateParams.token,
                            params: response
                        });
                    };

                    var fail = function(response) {
                        routeService.goToState('base.common.login-ack', {
                            token: $stateParams.token,
                            params: response
                        });
                    };

                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.REGISTER_OTP,
                        headers: {
                            'X-Auth-Token': $stateParams.token,
                            'Content-Type': 'application/json'
                        },
                        data: {
                            mobile: ctrl.formManager.getValue('phoneNumber'),
                            email: ctrl.formManager.getValue('email')
                        }
                    }).success(success).error(fail)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    // TODO: form invalid handling
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);
    }

    angular.module('login').directive(directiveName, registerOtpPage);
})();
