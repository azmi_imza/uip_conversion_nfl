/* global RsaEncrypt */
/* global getNewEncryptBlockChangePassword */
/* eslint new-cap:0 */

(function() {
    'use strict';

    var directiveName = 'forceChangePasswordPage';

    /*@ngInject*/
    function forceChangePasswordPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: forceChangePasswordCtrl,
            templateUrl: 'templates/login/forceChangePassword.html'
        };
    }

    /*@ngInject*/
    function forceChangePasswordCtrl(loginFactory, $timeout, FormManager, forceChangePasswordConfig, uiHelperService, $http, routeService, $stateParams, APP_CONFIG, storageService, APP_CONST) {
        var ctrl = this;
        var config = forceChangePasswordConfig;
        ctrl.formModel = {};

        if (!$stateParams.token) {
            routeService.goToHome();
        }

        ctrl.subscriber = $stateParams.subscriber;

        var securityPolicy = storageService.get(APP_CONST.STORAGE_KEYS.SECURITYPOLICY);
        securityPolicy['contain_lowercase'] = securityPolicy['contain_lowercase'] ? securityPolicy['contain_lowercase'] : '0';
        securityPolicy['contain_uppercase'] = securityPolicy['contain_uppercase'] ? securityPolicy['contain_uppercase'] : '0';
        securityPolicy['contain_number'] = securityPolicy['contain_number'] ? securityPolicy['contain_number'] : '0';
        securityPolicy['contain_special_char'] = securityPolicy['contain_special_char'] ? securityPolicy['contain_special_char'] : '0';
        var passwordRegex = '^' + '(?=(.*[a-z]){' + securityPolicy['contain_lowercase'] + '})(?=(.*[A-Z]){' + securityPolicy['contain_uppercase'] + '})(?=(.*\\d){' + securityPolicy['contain_number'] + '})(?=(.*[!@#$%^&*]){' + securityPolicy['contain_special_char'] + '})[a-zA-Z0-9!@#$%^&*]{' + securityPolicy['min_pswd_length'] + ',' + securityPolicy['max_pswd_length'] + '}$';
        config.newPassword.validators.patterns[0].value = passwordRegex;
        config.confirmPassword.validators.patterns[0].value = passwordRegex;
        var passwordMaxLength = securityPolicy['max_pswd_length'];
        config.currentPassword.validators.maxLength.value = passwordMaxLength;
        config.newPassword.validators.maxLength.value = passwordMaxLength;
        config.confirmPassword.validators.maxLength.value = passwordMaxLength;
        
        ctrl.config = config;
        ctrl.formManager = new FormManager(ctrl.config.forceChangePasswordForm.name, ctrl.config.forceChangePasswordForm.mode, ctrl);
        ctrl.securityPolicy = securityPolicy;
        ctrl.submit = function() {
            ctrl.acknowledgement = {};
            ctrl.acknowledgement.details = [];
            ctrl.acknowledgement.isSuccessAck = true;
            ctrl.acknowledgement.isVisibleAck = false;
            ctrl.acknowledgement.title = 'cfo.label.failed';

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    var success = function(response) {
                        if (response.status !== 'failed' && response.status !== 'success') {
                            ctrl.acknowledgement.isSuccessAck = false;
                            ctrl.acknowledgement.isVisibleAck = true;

                            if (response.message) {
                                ctrl.acknowledgement.details.push({
                                    message: response.message
                                });
                            }

                            uiHelperService.scrollTo($('#errorMessage').offset().top);
                        } else {

                            if ($stateParams.subscriber['has_otp_mobile'] === 'N') {
                                response.toState = 'base.common.register-otp';
                            } else {
                                response.toState = 'base.common.login-otp';
                            }

                            routeService.goToState('base.common.login-ack', {
                                token: $stateParams.token,
                                params: response
                            });
                        }
                    };

                    var fail = function(response) {
                        routeService.goToState('base.common.login-ack', {
                            token: $stateParams.token,
                            params: response
                        });
                    };

                    if (ctrl.formManager.getValue('newPassword') !== ctrl.formManager.getValue('confirmPassword')) {
                        ctrl.acknowledgement.isSuccessAck = false;
                        ctrl.acknowledgement.isVisibleAck = true;

                        ctrl.acknowledgement.details.push({
                            message: 'cfo.label.passwordMustMatch'
                        });

                        uiHelperService.scrollTo($('#errorMessage').offset().top);
                    } else {
                        uiHelperService.showLoader();

                        var successGetKey = function(response) {
                            ctrl.encryptData = response.data.data;

                            var publicKey = ctrl.encryptData['public_key'];
                            var randomKey = ctrl.encryptData['random_key'];
                            var migrationState = ctrl.encryptData['migration_state'];

                            $http({
                                method: 'PUT',
                                headers: {
                                    'X-Auth-Token': $stateParams.token,
                                    'Content-Type': 'application/json'
                                },
                                data: {
                                    'cur_pwd': RsaEncrypt(publicKey,
                                        ctrl.formManager.getValue('currentPassword'),
                                        randomKey, migrationState),
                                    'new_pwd': getNewEncryptBlockChangePassword(ctrl.formManager.getValue('currentPassword'),
                                        ctrl.formManager.getValue('newPassword'), publicKey,
                                        randomKey, migrationState),
                                    'confirm_new_pwd': getNewEncryptBlockChangePassword(ctrl.formManager.getValue('currentPassword'),
                                        ctrl.formManager.getValue('confirmPassword'), publicKey,
                                        randomKey, migrationState),
                                    'random_key': randomKey
                                },
                                url: APP_CONFIG.API_PATHS.CHANGE_PASSWORD
                            }).success(success).error(fail)['finally'](function() {
                                uiHelperService.hideLoader();
                            });
                        };

                        var failGetKey = function() {
                            uiHelperService.hideLoader();
                        };

                        loginFactory.getEnCryptKey()
                            .then(successGetKey, failGetKey);

                    }
                },

                function(response) {
                    // TODO: form invalid handling
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);

    }

    angular.module('login').directive(directiveName, forceChangePasswordPage);
})();
