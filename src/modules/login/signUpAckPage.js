(function() {
    'use strict';

    var directiveName = 'signUpAckPage';

    /*@ngInject*/
    function signUpAckPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: signUpAckCtrl,
            templateUrl: 'templates/login/signUpAck.html'
        };
    }

    /*@ngInject*/
    function signUpAckCtrl(loginFactory, authService, routeService, $stateParams) {
        var ctrl = this;

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if ($stateParams.params) {
            ctrl.acknowledgement.title = 'cfo.label.success';
            ctrl.acknowledgement.isVisibleAck = true;
            ctrl.acknowledgement.isSuccessAck = true;

            if ($stateParams.params.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.params.message
                });
            }
        }

        ctrl.closeBtnClick = function() {
            routeService.goToState('base.common.login');
        };
    }

    angular.module('login').directive(directiveName, signUpAckPage);
})();
