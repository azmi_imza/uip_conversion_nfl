(function() {
    'use strict';

    var directiveName = 'signUpPage';

    /*@ngInject*/
    function signUpPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: signUpCtrl,
            templateUrl: 'templates/login/signUp.html'
        };
    }

    /*@ngInject*/
    function signUpCtrl(APP_CONFIG, signUpConfig, loginFactory, authService, routeService, $timeout, FormManager, $http, uiHelperService, $filter) {
        var ctrl = this;

        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.formManager = new FormManager('signUpForm', 'edit', ctrl);
        ctrl.showTerms = true;
        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';

        ctrl.config.identityType = signUpConfig.identityType;
        ctrl.config.identityNumber = signUpConfig.identityNumber;
        ctrl.config.countryIssue = signUpConfig.countryIssue;

        /****** Manually get url for issue country and sort the list *****/

        function pass(response) {
            ctrl.sortList = $filter('orderBy')(response.data.items, 'label');

            var spliceItem = [];
            for (var i = 0; i < ctrl.sortList.length; i++) {
                if (ctrl.sortList[i].value === 'SG') {
                    spliceItem = ctrl.sortList.splice(i, 1);
                    break;
                }
            }

            if (spliceItem[0]) {
                ctrl.sortList.splice(0, 0, spliceItem[0]);
            }

            ctrl.config.countryIssue.ddlItems = ctrl.sortList;
        }

        function fail() {}

        $http({
            method: 'GET',
            url: APP_CONFIG.API_PATHS.COUNTRY_ISSUE
        }).success(pass).error(fail);

        /*************************** END ******************************/

        ctrl.acceptBtnClick = function() {
            ctrl.showTerms = false;
        };

        ctrl.submitBtnClick = function() {
            var success = function(response) {
                if (response.status === 'success') {
                    routeService.goToState('base.common.sign-up-ack', {
                        params: response
                    });
                } else {
                    ctrl.acknowledgement.details = [];
                    ctrl.acknowledgement.isSuccessAck = false;
                    ctrl.acknowledgement.isVisibleAck = true;
                    ctrl.acknowledgement.title = 'cfo.label.failed';

                    if (response.message) {
                        ctrl.acknowledgement.details.push({
                            message: response.message
                        });
                    }
                }
            };

            var error = function(response) {
                ctrl.acknowledgement.isSuccessAck = false;
                ctrl.acknowledgement.isVisibleAck = true;
                ctrl.acknowledgement.title = 'cfo.label.failed';
                ctrl.acknowledgement.details = [];

                if (response.message) {
                    ctrl.acknowledgement.details.push({
                        message: response.message
                    });
                }
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    ctrl.formManager.setValue('identity_num', ctrl.formManager.getValue('identity_num').toUpperCase());
                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.SIGN_UP,
                        data: ctrl.formModel,
                        headers: {
                            'APF_CD': APP_CONFIG.SETTINGS.APF_CD
                        }
                    }).success(success).error(error)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.cancelBtnClick = function() {
            routeService.goToState('base.common.login');
        };
    }

    angular.module('login').directive(directiveName, signUpPage);
})();
