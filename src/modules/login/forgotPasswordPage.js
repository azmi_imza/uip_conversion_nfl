(function() {
    'use strict';

    var directiveName = 'forgotPasswordPage';

    /*@ngInject*/
    function forgotPasswordPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: forgotPasswordCtrl,
            templateUrl: 'templates/login/forgotPassword.html'
        };
    }

    /*@ngInject*/
    function forgotPasswordCtrl(APP_CONFIG, forgotPasswordConfig, $scope, $rootScope, $filter, loginFactory, authService, routeService, $timeout, FormManager, uiHelperService, $http) {
        var ctrl = this;

        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.formManager = new FormManager('forgotPasswordForm', 'edit', ctrl);
        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';

        ctrl.config.userType = forgotPasswordConfig.userType;
        ctrl.config.identityType = forgotPasswordConfig.identityType;
        ctrl.config.identityNumber = forgotPasswordConfig.identityNumber;
        ctrl.config.countryIssue = forgotPasswordConfig.countryIssue;
        ctrl.config.userId = forgotPasswordConfig.userId;

        var userTypeChange = $rootScope.$on('valueChange_user_type_code', function() {
            if (ctrl.formModel['user_type_code'] === 'IN') {
                ctrl.formManager.removeComponent(ctrl.config.userId.name);
            } else {
                ctrl.formManager.removeComponent(ctrl.config.identityType.name);
                ctrl.formManager.removeComponent(ctrl.config.identityNumber.name);
                ctrl.formManager.removeComponent(ctrl.config.countryIssue.name);
            }
        });

        $scope.$on('$destroy', function() {
            userTypeChange();
        });

        /****** Manually get url for country list and sort the list *****/

        function pass(response) {
            ctrl.sortList = $filter('orderBy')(response.data.items, 'label');

            var spliceItem = [];
            for (var i = 0; i < ctrl.sortList.length; i++) {
                if (ctrl.sortList[i].value === 'SG') {
                    spliceItem = ctrl.sortList.splice(i, 1);
                    break;
                }
            }

            if (spliceItem[0]) {
                ctrl.sortList.splice(0, 0, spliceItem[0]);
            }

            ctrl.config.countryIssue.ddlItems = ctrl.sortList;
        }

        function fail() {}

        $http({
            method: 'GET',
            url: APP_CONFIG.API_PATHS.COUNTRY_ISSUE
        }).success(pass).error(fail);

        /*********** END **********/

        ctrl.submitBtnClick = function() {
            var success = function(response) {
                if (response.status === 'success') {
                    routeService.goToState('base.common.forgot-password-ack', {
                        params: response
                    });
                } else {
                    ctrl.acknowledgement.details = [];
                    ctrl.acknowledgement.isSuccessAck = false;
                    ctrl.acknowledgement.isVisibleAck = true;
                    ctrl.acknowledgement.title = 'cfo.label.failed';

                    if (response.message) {
                        ctrl.acknowledgement.details.push({
                            message: response.message
                        });
                    }
                }
            };

            var error = function(response) {
                ctrl.acknowledgement.details = [];
                ctrl.acknowledgement.isSuccessAck = false;
                ctrl.acknowledgement.isVisibleAck = true;
                ctrl.acknowledgement.title = 'cfo.label.failed';

                if (response.message) {
                    ctrl.acknowledgement.details.push({
                        message: response.message
                    });
                }
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    if (ctrl.formManager.getValue('identity_num')) {
                        ctrl.formManager.setValue('identity_num', ctrl.formManager.getValue('identity_num').toUpperCase());
                    }

                    if (ctrl.formManager.getValue('user_id')) {
                        ctrl.formManager.setValue('user_id', ctrl.formManager.getValue('user_id').toUpperCase());
                    }

                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.RESET_PASSWORD,
                        data: ctrl.formModel,
                        headers: {
                            'APF_CD': APP_CONFIG.SETTINGS.APF_CD
                        }
                    }).success(success).error(error)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.cancelBtnClick = function() {
            routeService.goToState('base.common.login');
        };

    }

    angular.module('login').directive(directiveName, forgotPasswordPage);
})();
