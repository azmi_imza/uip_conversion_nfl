(function() {
    'use strict';

    var directiveName = 'loginAckPage';

    /*@ngInject*/
    function loginAckPage() {
        return {
            scope: {},
            bindToController: true,
            controller: loginAckCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/login/loginAck.html'
        };
    }

    /*@ngInject*/
    function loginAckCtrl($log, routeService, $stateParams) {

        $log.debug('loginAckCtrl');
        var ctrl = this;

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = 'cfo.label.failed';
        ctrl.acknowledgement.details = [];

        if (!$stateParams.params) {
            routeService.goToHome();
        }

        if ($stateParams.params) {
            ctrl.acknowledgement.isVisibleAck = true;

            if ($stateParams.params.status === 'success') {
                ctrl.acknowledgement.isSuccessAck = true;
                ctrl.acknowledgement.title = 'cfo.label.success';
            }

            if ($stateParams.params.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.params.message
                });
            }
        }

        ctrl.ok = function() {
            if ($stateParams.params.status === 'success') {
                routeService.goToState($stateParams.params.toState, {
                    token: $stateParams.token
                });
            } else {
                routeService.goToHome();
            }

        };

    }

    angular.module('login').directive(directiveName, loginAckPage);
})();
