(function() {
    'use strict';

    var directiveName = 'loginPage';

    /*@ngInject*/
    function loginPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: loginCtrl,
            templateUrl: 'templates/login/login.html'
        };
    }

    /*@ngInject*/
    function loginCtrl(loginFactory, authService, routeService, $timeout, FormManager, loginConfig) {
        var ctrl = this;

        ctrl.login = login;
        ctrl.signUp = signUp;
        ctrl.forgotPassword = forgotPassword;

        ctrl.formModel = {};
        ctrl.config = {};

        ctrl.config.signInForm = loginConfig.signInForm;
        ctrl.config.userId = loginConfig.userId;
        ctrl.config.password = loginConfig.password;
        ctrl.config.signInForm.formManager = new FormManager(ctrl.config.signInForm.name, ctrl.config.signInForm.mode, ctrl);

        function login() {
            ctrl.unauthorized = false;

            ctrl.loginInProgress = true;
            ctrl.config.signInForm.formManager.submit(ctrl.formModel).then(function() {
                    loginFactory.getEnCryptKey()
                        .then(function(response) {

                            if (response.data.status === 'success') {
                                loginFactory.login(ctrl, response.data.data);
                            }
                        }, function() {
                            //failed to get encryption key
                            ctrl.loginInProgress = false;
                        });

                },

                function() {
                    // TODO: form invalid handling
                    ctrl.loginInProgress = false;
                }

            );
        }

        function signUp() {
            routeService.goToState('base.common.sign-up');
        }

        function forgotPassword() {
            routeService.goToState('base.common.forgot-password');
        }

        ctrl.toggleRedMan = function($event) {
            $event.stopPropagation();

            if ($('.signin-banner-wrapper .red-man-menu').hasClass('expanded')) {
                $('.signin-banner-wrapper .red-man-menu').removeClass('expanded');
                $('.signin-banner-wrapper .red-man-sub-menu').removeClass('expanded');
            } else {
                $('.signin-banner-wrapper .red-man-menu').addClass('expanded');
                $('.signin-banner-wrapper .red-man-sub-menu').addClass('expanded');
            }
        };

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);

        $timeout(function() {
            $('input').placeholder({
                customClass: 'customPlaceholder'
            });
            $('.signin-body input').on('keydown', function(event) {
                if (event.keyCode === 13 && ctrl.loginInProgress !== true) {
                    event.preventDefault();
                    login();
                }
            });
        }, 500);
    }

    angular.module('login').directive(directiveName, loginPage);
})();
