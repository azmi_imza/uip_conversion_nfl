/* global RsaEncrypt */
/* global getNewEncryptBlockChangePassword */
/* eslint new-cap:0 */

(function() {
    'use strict';

    var directiveName = 'changePasswordPage';

    /*@ngInject*/
    function changePasswordPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: changePasswordCtrl,
            templateUrl: 'templates/login/changePassword.html'
        };
    }

    /*@ngInject*/
    function changePasswordCtrl(loginFactory, changePasswordConfig, FormManager, $timeout, uiHelperService, routeService, storageService, APP_CONST, APP_CONFIG, $http) {
        var ctrl = this;

        ctrl.formModel = {};
        var config = changePasswordConfig;
        var securityPolicy = storageService.get(APP_CONST.STORAGE_KEYS.SECURITYPOLICY);
        securityPolicy['contain_lowercase'] = securityPolicy['contain_lowercase'] ? securityPolicy['contain_lowercase'] : '0';
        securityPolicy['contain_uppercase'] = securityPolicy['contain_uppercase'] ? securityPolicy['contain_uppercase'] : '0';
        securityPolicy['contain_number'] = securityPolicy['contain_number'] ? securityPolicy['contain_number'] : '0';
        securityPolicy['contain_special_char'] = securityPolicy['contain_special_char'] ? securityPolicy['contain_special_char'] : '0';
        var passwordRegex = '^' + '(?=(.*[a-z]){' + securityPolicy['contain_lowercase'] + '})(?=(.*[A-Z]){' + securityPolicy['contain_uppercase'] + '})(?=(.*\\d){' + securityPolicy['contain_number'] + '})(?=(.*[!@#$%^&*]){' + securityPolicy['contain_special_char'] + '})[a-zA-Z0-9!@#$%^&*]{' + securityPolicy['min_pswd_length'] + ',' + securityPolicy['max_pswd_length'] + '}$';
        config.newPassword.validators.patterns[0].value = passwordRegex;
        config.confirmNewPassword.validators.patterns[0].value = passwordRegex;
        var passwordMaxLength = securityPolicy['max_pswd_length'];
        config.currentPassword.validators.maxLength.value = passwordMaxLength;
        config.newPassword.validators.maxLength.value = passwordMaxLength;
        config.confirmNewPassword.validators.maxLength.value = passwordMaxLength;

        ctrl.config = config;
        ctrl.formManager = new FormManager(ctrl.config.changePasswordForm.name, ctrl.config.changePasswordForm.mode, ctrl);
        ctrl.securityPolicy = securityPolicy;

        ctrl.submit = function() {
            ctrl.acknowledgement = {};
            ctrl.acknowledgement.details = [];
            ctrl.acknowledgement.isSuccessAck = true;
            ctrl.acknowledgement.isVisibleAck = false;
            ctrl.acknowledgement.title = 'cfo.label.failed';

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    if (ctrl.formManager.getValue('new_pwd') !== ctrl.formManager.getValue('confirm_new_pwd')) {
                        ctrl.acknowledgement.isSuccessAck = false;
                        ctrl.acknowledgement.isVisibleAck = true;

                        ctrl.acknowledgement.details.push({
                            message: 'cfo.label.passwordMustMatch'
                        });

                        uiHelperService.scrollTo($('#errorMessage').offset().top);
                    } else {
                        uiHelperService.showLoader();

                        var successGetKey = function(key) {
                            ctrl.encryptData = key.data.data;

                            var success = function(response) {
                                if (response.status !== 'failed' && response.status !== 'success') {

                                    ctrl.acknowledgement.isSuccessAck = false;
                                    ctrl.acknowledgement.isVisibleAck = true;

                                    if (response.message) {
                                        ctrl.acknowledgement.details.push({
                                            message: response.message
                                        });
                                    }

                                    uiHelperService.scrollTo($('#errorMessage').offset().top);
                                } else {
                                    routeService.goToState('base.auth.change-password-ack', {
                                        params: response
                                    });
                                }

                            };

                            var error = function(response) {
                                routeService.goToState('base.auth.change-password-ack', {
                                    params: response
                                });
                            };

                            var publicKey = ctrl.encryptData['public_key'];
                            var randomKey = ctrl.encryptData['random_key'];
                            var migrationState = ctrl.encryptData['migration_state'];

                            $http({
                                method: 'PUT',
                                data: {
                                    'cur_pwd': RsaEncrypt(publicKey,
                                        ctrl.formManager.getValue('cur_pwd'),
                                        randomKey, migrationState),
                                    'new_pwd': getNewEncryptBlockChangePassword(ctrl.formManager.getValue('cur_pwd'),
                                        ctrl.formManager.getValue('new_pwd'), publicKey,
                                        randomKey, migrationState),
                                    'confirm_new_pwd': getNewEncryptBlockChangePassword(ctrl.formManager.getValue('cur_pwd'),
                                        ctrl.formManager.getValue('confirm_new_pwd'), publicKey,
                                        randomKey, migrationState),
                                    'random_key': randomKey
                                },
                                url: APP_CONFIG.API_PATHS.CHANGE_PASSWORD
                            }).success(success).error(error)['finally'](function() {
                                uiHelperService.hideLoader();
                            });
                        };

                        var failGetKey = function() {
                            uiHelperService.hideLoader();
                        };

                        loginFactory.getEnCryptKey()
                            .then(successGetKey, failGetKey);
                    }
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.cancel = function() {
            var userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
            var state = '';

            if (userType === 'IN') {
                state = 'base.auth.dashboard-in';

            } else if (userType === 'CA') {
                state = 'base.auth.dashboard-be';

            }

            routeService.goToState(state);
        };

        $timeout(function() {
            ctrl.compileElement = true;
        }, 100);
    }

    angular.module('login').directive(directiveName, changePasswordPage);
})();
