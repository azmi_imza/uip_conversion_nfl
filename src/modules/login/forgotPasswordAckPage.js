(function() {
    'use strict';

    var directiveName = 'forgotPasswordAckPage';

    /*@ngInject*/
    function forgotPasswordAckPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: forgotPasswordAckCtrl,
            templateUrl: 'templates/login/forgotPasswordAck.html'
        };
    }

    /*@ngInject*/
    function forgotPasswordAckCtrl(loginFactory, authService, routeService, $stateParams) {
        var ctrl = this;
        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if ($stateParams.params) {
            ctrl.acknowledgement.isVisibleAck = true;
            ctrl.acknowledgement.isSuccessAck = true;
            ctrl.acknowledgement.title = 'cfo.label.success';

            if ($stateParams.params.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.params.message
                });
            }
        }

        ctrl.closeBtnClick = function() {
            routeService.goToState('base.common.login');
        };
    }

    angular.module('login').directive(directiveName, forgotPasswordAckPage);
})();
