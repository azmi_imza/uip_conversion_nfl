(function() {
    'use strict';

    var directiveName = 'changePasswordAckPage';

    /*@ngInject*/
    function changePasswordAckPage() {
        return {
            scope: {},
            bindToController: true,
            controller: changePasswordAckCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/login/changePasswordAck.html'
        };
    }

    /*@ngInject*/
    function changePasswordAckCtrl($log, routeService, $stateParams, storageService, APP_CONST) {

        $log.debug('changePasswordAckCtrl');
        var ctrl = this;

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = true;
        ctrl.acknowledgement.isSuccessAck = true;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if ($stateParams.params) {
            ctrl.acknowledgement.isVisibleAck = true;
            if ($stateParams.params.status !== 'success') {
                ctrl.acknowledgement.isSuccessAck = false;

            }

            if ($stateParams.params.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.params.message
                });
            }

        }

        ctrl.ok = function() {
            var userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
            var state = '';

            if (userType === 'IN') {
                state = 'base.auth.dashboard-in';

            } else if (userType === 'CA') {
                state = 'base.auth.dashboard-be';

            }

            routeService.goToState(state);
        };

    }

    angular.module('login').directive(directiveName, changePasswordAckPage);
})();
