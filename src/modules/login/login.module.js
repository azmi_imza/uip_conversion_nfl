(function() {
    'use strict';

    angular.module('login', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var COMMON_STATE_PREFIX = $injector.get('APP_CONST').COMMON_STATE_PREFIX;
                var authPrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(COMMON_STATE_PREFIX.concat('login'), {
                    url: '/login',
                    template: '<div login-page></div>'
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('login-ack'), {
                    url: '/login-ack',
                    template: '<div login-ack-page></div>',
                    data: {
                        screenCode: 'SCRNACK'
                    },
                    params: {
                        title: null,
                        token: null,
                        params: null
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('forgot-password'), {
                    url: '/forgot-password',
                    template: '<div forgot-password-page></div>',
                    data: {
                        screenCode: 'SCRNFORGOTPASSWORD'
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('sign-up'), {
                    url: '/sign-up',
                    template: '<div sign-up-page></div>',
                    data: {
                        screenCode: 'SCRNSIGNUP'
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('sign-up-ack'), {
                    url: '/sign-up-ack',
                    template: '<div sign-up-ack-page></div>',
                    data: {
                        screenCode: 'SCRNSIGNUP'
                    },
                    params: {
                        params: null
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('forgot-password-ack'), {
                    url: '/forgot-password-ack',
                    template: '<div forgot-password-ack-page></div>',
                    data: {
                        screenCode: 'SCRNFORGOTPASSWORD'
                    },
                    params: {
                        params: null
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('force-change-password'), {
                    url: '/force-change-password',
                    template: '<div force-change-password-page></div>',
                    data: {
                        screenCode: 'SCRNFORCECHANGEPASSWORD'
                    },
                    params: {
                        token: null,
                        subscriber: null,
                        keyData: null
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('register-otp'), {
                    url: '/register-otp',
                    template: '<div register-otp-page></div>',
                    data: {
                        screenCode: 'SCRNREGISTEROTP'
                    },
                    params: {
                        token: null
                    }
                });

                $stateProvider.state(COMMON_STATE_PREFIX.concat('login-otp'), {
                    url: '/login-otp',
                    template: '<div login-otp-page></div>',
                    data: {
                        screenCode: 'SCRNLOGINOTP'
                    },
                    params: {
                        token: null
                    }
                });

                $stateProvider.state(authPrefix.concat('change-password'), {
                    url: '/change-password',
                    template: '<div change-password-page></div>',
                    data: {
                        screenCode: '801032SCRNCHANGEPASSWORD'
                    }
                });

                $stateProvider.state(authPrefix.concat('change-password-ack'), {
                    url: '/change-password-ack',
                    template: '<div change-password-ack-page></div>',
                    data: {
                        screenCode: '801032SCRNCHANGEPASSWORD'
                    },
                    params: {
                        params: null
                    }
                });
            });
})();
