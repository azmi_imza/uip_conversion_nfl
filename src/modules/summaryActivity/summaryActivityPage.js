(function() {
    'use strict';

    var directiveName = 'summaryActivityPage';

    /*@ngInject*/
    function summaryActivityPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: summaryActivityCtrl,
            templateUrl: 'templates/summaryActivity/summaryActivity.html'
        };
    }

    /*@ngInject*/
    function summaryActivityCtrl($scope, $timeout, $log, authService, storageService, routeService, APP_CONST) {
        $log.debug('******* summary activity controller ************');

        var sumActivities = storageService.get(APP_CONST.STORAGE_KEYS.SUM_ACTIVITY);

        if (!sumActivities) {
            if (authService.getAuthToken()) {
                routeService.goToPageNotFound();
                return;
            }

            routeService.goToLogin();
            return;
        }

        storageService.clearAll();
        var ctrl = this;
        ctrl.goToLogin = goToLogin;
        ctrl.model = {};
        ctrl.model.message = sumActivities.message;
        ctrl.model.login = sumActivities.data.subscriber['login_dt'];
        ctrl.model.logout = sumActivities.data.subscriber['logout_dt'];

        var hours = parseInt(sumActivities.data.subscriber.duration / 3600) % 24;
        var minutes = parseInt(sumActivities.data.subscriber.duration / 60) % 60;
        var seconds = sumActivities.data.subscriber.duration % 60;
        ctrl.model.hours = (hours < 10 ? '0' + hours : hours);
        ctrl.model.minutes = (minutes < 10 ? '0' + minutes : minutes);
        ctrl.model.seconds = (seconds < 10 ? '0' + seconds : seconds);

        ctrl.model.sumActivity = sumActivities.data.transaction;

        function goToLogin() {
            routeService.goToLogin();
        }

        $timeout(function() {
            $('.footable_sum_activity').footable();
        }, 0);
    }

    angular.module('summaryActivity').directive(directiveName, summaryActivityPage);
})();
