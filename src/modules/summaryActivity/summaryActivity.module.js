(function() {
    'use strict';
    angular.module('summaryActivity', [])
        .config(
            /*@ngInject*/
            function($injector, $stateProvider) {
                var COMMON_STATE_PREFIX = $injector.get('APP_CONST').COMMON_STATE_PREFIX;

                $stateProvider.state(COMMON_STATE_PREFIX.concat('summary-activity'), {
                    url: '/summary-activity',
                    template: '<div summary-activity-page></div>'
                });
            });
})();
