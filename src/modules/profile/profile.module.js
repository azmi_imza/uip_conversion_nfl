(function() {
    'use strict';

    angular.module('profileDetails', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var AUTH_STATE_PREFIX = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                var profileStateName = AUTH_STATE_PREFIX.concat('profile');
                $stateProvider.state(profileStateName, {
                    url: '/profile',
                    template: '<div profile-page></div>',
                    data: {
                        screenCode: '801024SCRNPERSONALPROFILEIN'
                    }
                });

                var profileUpdateStateName = AUTH_STATE_PREFIX.concat('profile-update');
                $stateProvider.state(profileUpdateStateName, {
                    url: '/profile-update',
                    template: '<div profile-update-page></div>',
                    data: {
                        screenCode: '801024SCRNPERSONALPROFILEIN'
                    }

                });

                var profileConfirmationStateName = AUTH_STATE_PREFIX.concat('profile-confirmation');
                $stateProvider.state(profileConfirmationStateName, {
                    url: '/profile-confirmation',
                    template: '<div profile-confirmation-page></div>',
                    data: {
                        screenCode: '801024SCRNPERSONALPROFILEIN'
                    },
                    params: {
                        newData: null,
                        oldData: null
                    }
                });

                var profileUpdateAckSuccessStateName = AUTH_STATE_PREFIX.concat('profile-update-ack-success');
                $stateProvider.state(profileUpdateAckSuccessStateName, {
                    url: '/profile-update/ack-success',
                    template: '<div profile-acknowledgement-page></div>',
                    data: {
                        screenCode: 'SCRNACKSUCCESS'
                    },
                    params: {
                        response: null,
                        message: null,
                        transactionRef: null
                    }
                });

                var profileUpdateAckFailedStateName = AUTH_STATE_PREFIX.concat('profile-update-ack-failed');
                $stateProvider.state(profileUpdateAckFailedStateName, {
                    url: '/profile-update/ack-failed',
                    template: '<div profile-acknowledgement-page></div>',
                    data: {
                        screenCode: 'SCRNACKFAILED'
                    },
                    params: {
                        response: null,
                        message: null
                    }
                });
            });
})();
