(function() {
    'use strict';

    var directiveName = 'profileAcknowledgementPage';

    /*@ngInject*/
    function profileAcknowledgementPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: profileAcknowledgementCtrl,
            templateUrl: 'templates/profile/profileAcknowledgement.html'
        };
    }

    /*@ngInject*/
    function profileAcknowledgementCtrl($log, $location, $state, $stateParams, routeService) {
        var ctrl = this;
        ctrl.model = {};

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = true;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if (!$stateParams || !$stateParams.response) {
            routeService.goToPageNotFound();
        }

        if ($stateParams.response === 'success') {
            ctrl.acknowledgement.isSuccessAck = true;
            if ($stateParams.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.message
                });
            }
        } else {
            ctrl.acknowledgement.isSuccessAck = false;
            if ($stateParams.message) {
                ctrl.acknowledgement.details.push({
                    message: $stateParams.message
                });
            }
        }

        $log.debug(ctrl.viewSuccess);
        ctrl.backButton = function() {
            $log.debug('Back to My profile view page');
            routeService.goToPath('/profile');
        };
    }

    angular.module('profileDetails').directive(directiveName, profileAcknowledgementPage);
})();
