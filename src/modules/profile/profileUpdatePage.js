(function() {
    'use strict';

    var directiveName = 'profileUpdatePage';

    /*@ngInject*/
    function profileUpdatePage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: profileUpdateCtrl,
            templateUrl: 'templates/profile/profileupdate.html'
        };
    }

    /*@ngInject*/
    function profileUpdateCtrl($translate, $scope, $log, ProfileFactory, FormManager, $timeout, routeService, $stateParams, $rootScope, profileConfig, $location, $anchorScroll) {
        var ctrl = this;
        var factory = new ProfileFactory(ctrl);
        $anchorScroll.yOffset = 50;
        ctrl.model = {};

        ctrl.policyAddressTable = {
            isValid: true,
            message: ''
        };

        /*form component config to be moved to separate file[s]*/
        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.testTextField = true;
        ctrl.testObj = {};

        /*FORM*/
        ctrl.config.personalDetailsForm = profileConfig.personalDetailsForm;
        ctrl.config.localAddressForm = profileConfig.localAddressForm;
        ctrl.config.overseasAddressForm = profileConfig.overseasAddressForm;

        /*PERSONAL DETAILS*/
        ctrl.config.personalDetails = {};
        ctrl.config.personalDetails.mobile = profileConfig.personalDetails.mobile;
        ctrl.config.personalDetails.house = profileConfig.personalDetails.house;
        ctrl.config.personalDetails.office = profileConfig.personalDetails.office;
        ctrl.config.personalDetails.others = profileConfig.personalDetails.others;
        ctrl.config.personalDetails.email = profileConfig.personalDetails.email;

        /*LOCAL ADDRESS*/
        ctrl.config.localAddress = {};
        ctrl.config.localAddress.block = profileConfig.localAddress.block;
        ctrl.config.localAddress.street = profileConfig.localAddress.street;
        ctrl.config.localAddress.storeyAndUnitNum = profileConfig.localAddress.storeyAndUnitNum;
        ctrl.config.localAddress.building = profileConfig.localAddress.building;
        ctrl.config.localAddress.postalCode = profileConfig.localAddress.postalCode;

        /*OVERSEAS ADDRESS*/
        ctrl.config.overseasAddress = {};
        ctrl.config.overseasAddress.addr1 = profileConfig.overseasAddress.addr1;
        ctrl.config.overseasAddress.addr2 = profileConfig.overseasAddress.addr2;
        ctrl.config.overseasAddress.addr3 = profileConfig.overseasAddress.addr3;
        ctrl.config.overseasAddress.addr4 = profileConfig.overseasAddress.addr4;
        ctrl.config.overseasAddress.addr5 = profileConfig.overseasAddress.addr5;
        ctrl.config.overseasAddress.addr6 = profileConfig.overseasAddress.addr6;
        ctrl.config.overseasAddress.countryName = profileConfig.overseasAddress.countryName;

        /*POLICY ADDRESS TABLE*/
        ctrl.config.policyAddressTable = {};
        ctrl.config.policyAddressTable.localAddress = profileConfig.policyAddressTable.localAddress;
        ctrl.config.policyAddressTable.overseasAddress = profileConfig.policyAddressTable.overseasAddress;

        /*INIT FORMMANAGER*/
        ctrl.config.personalDetailsForm.formManager = new FormManager(ctrl.config.personalDetailsForm.name, ctrl.config.personalDetailsForm.mode, ctrl);
        ctrl.config.localAddressForm.formManager = new FormManager(ctrl.config.localAddressForm.name, ctrl.config.localAddressForm.mode, ctrl);
        ctrl.config.overseasAddressForm.formManager = new FormManager(ctrl.config.overseasAddressForm.name, ctrl.config.overseasAddressForm.mode, ctrl);

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        var deregisterFn = $rootScope.$on('setValid_policyAddressTable', function() {
            setPolicyAddressTableValid();
        });

        $scope.$on('$destroy', function() {
            deregisterFn();
        });

        var setPolicyAddressTableInvalid = function() {
            ctrl.policyAddressTable.isValid = false;
            ctrl.policyAddressTable.message = 'cfo.label.fieldIsRequired';
        };

        var setPolicyAddressTableValid = function() {
            ctrl.policyAddressTable.isValid = true;
            ctrl.policyAddressTable.message = '';
        };

        var validatePolicyAddressTable = function() {
            var memberList = [];
            var errorMessage = '';
            var formManager;

            if (ctrl.isOverseaAddr === 'Y') {
                memberList = ctrl.config.policyAddressTable.overseasAddress.validators.groupValidation.members;
                formManager = ctrl.config.overseasAddressForm.formManager;
                errorMessage = ctrl.config.policyAddressTable.overseasAddress.validators.groupValidation.message;
            } else {
                memberList = ctrl.config.policyAddressTable.localAddress.validators.groupValidation.members;
                formManager = ctrl.config.localAddressForm.formManager;
                errorMessage = ctrl.config.policyAddressTable.localAddress.validators.groupValidation.message;
            }

            if (!formManager.getValue('policyAddressTable')) {
                var valid = true;
                angular.forEach(memberList, function(member) {
                    if (formManager.getValue(member.name) > 0) {
                        setPolicyAddressTableInvalid(errorMessage);
                        valid = false;
                        return;
                    }
                });

                if (!valid) {
                    return false;
                }

                angular.forEach(memberList, function(member) {
                    $rootScope.$emit('setValid_' + member.name);
                });
            }

            setPolicyAddressTableValid();
            return true;
        };

        ctrl.addNewMobileNumber = function() {
            var newItemNo = ctrl.model.mobile.length + 1;
            ctrl.model.mobile.push({
                'id': newItemNo
            });
        };

        ctrl.back = function() {
            routeService.goToPath('/profile');
        };

        //Remark: function for $anchorScroll
        ctrl.gotoField = function(fieldName) {
            $location.hash(fieldName);
            $anchorScroll();
        };

        ctrl.submit = function() {
            var validationResult = true;
            var personalDetailsIsValidate = true;
            var validateAndSubmit = function() {
                validatePersonalDetails();
            };

            var validatePersonalDetails = function() {
                ctrl.config.personalDetailsForm.formManager.submit(ctrl.formModel).then(function() {
                        validatePolicyAddress();
                    },

                    function(response) {
                        ctrl.gotoField(response.inputName);
                        personalDetailsIsValidate = false;
                        validationResult = false;
                        validatePolicyAddress();
                    }

                );
            };

            var validatePolicyAddress = function() {
                if (ctrl.isOverseaAddr === 'Y') {
                    ctrl.config.overseasAddressForm.formManager.submit(ctrl.formModel).then(function() {
                            proceedSubmit();
                        },

                        function(response) {
                            $log.debug(response.inputName);
                            if (personalDetailsIsValidate === true) {
                                ctrl.gotoField(response.inputName);
                            }

                            validationResult = false;
                        }

                    );
                } else {
                    ctrl.config.localAddressForm.formManager.submit(ctrl.formModel).then(function() {
                            proceedSubmit();
                        },

                        function(response) {
                            $log.debug(response.inputName);

                            if (personalDetailsIsValidate === true) {
                                ctrl.gotoField(response.inputName);
                            }

                            validationResult = false;
                        }

                    );
                }
            };

            var proceedSubmit = function() {
                ctrl.response = false;
                if (validationResult) {

                    compareData();
                    if (ctrl.hasChanges) {
                        factory.toConfirmationPage();
                        $log.debug('Proceed Submit');
                    }

                    ctrl.address = {};
                }
            };

            validationResult = validatePolicyAddressTable();
            validateAndSubmit();
        };

        function compareData() {
            var sameMobileCode = angular.equals(ctrl.modelCopy.mobile['country_cd'], ctrl.formModel.mobileAreaCode);
            var sameMobileNumber = angular.equals(ctrl.modelCopy.mobile.number, ctrl.formModel.mobileNumber);
            var sameHouseCode = angular.equals(ctrl.modelCopy.house['country_cd'], ctrl.formModel.houseAreaCode);
            var sameHouseNumber = angular.equals(ctrl.modelCopy.house.number, ctrl.formModel.houseNumber);
            var sameOfficeCode = angular.equals(ctrl.modelCopy.office['country_cd'], ctrl.formModel.officeAreaCode);
            var sameOfficeNumber = angular.equals(ctrl.modelCopy.office.number, ctrl.formModel.officeNumber);
            var sameEmail = angular.equals(ctrl.modelCopy.data.email, ctrl.formModel.email);
            var houseContactChanges = false;
            var officeContactChanges = false;

            if (ctrl.formModel.houseAreaCode !== '' && ctrl.formModel.houseNumber !== '') {
                //no changes on office contact
                houseContactChanges = true;
                $log.debug('Area code and number has value');
            } else {
                houseContactChanges = false;
                $log.debug('Area code and number has no value');
            }

            if (ctrl.formModel.officeAreaCode !== '' && ctrl.formModel.officeNumber !== '') {
                //no changes on office contact
                officeContactChanges = true;
                $log.debug('Area code and number has value');
            } else {
                officeContactChanges = false;
                $log.debug('Area code and number has no value');
            }

            if (sameMobileCode === false || sameMobileNumber === false || ((sameHouseCode === false || sameHouseNumber === false && houseContactChanges === true)) || sameEmail === false ||
                ((sameOfficeCode === false || sameOfficeNumber === false) && officeContactChanges === true)) {
                ctrl.hasChanges = true;
            } else {
                ctrl.hasChanges = false;

                //TODO: Revisit in future for better handling method
                alert('No Changes has been made!');
            }

        }

        ctrl.changeAddressType = function(countryName, isOverseaAddr) {
            ctrl.isOverseaAddr = isOverseaAddr || 'N';

            ctrl.model.country.name = countryName;
            $log.debug(ctrl.address);
        };

        ctrl.clear = function() {
            if (ctrl.isOverseaAddr === 'Y') {
                ctrl.config.overseasAddressForm.formManager.reset();
            } else {
                ctrl.config.localAddressForm.formManager.reset();
            }
        };

        ctrl.selectAllPolicy = function() {
            factory.setSelectAllPolicy();
            ctrl.config.localAddressForm.formManager.setValue('policyAddressTable', ctrl.selection.length);
            ctrl.config.overseasAddressForm.formManager.setValue('policyAddressTable', ctrl.selection.length);
            $log.debug('Select All Policy ' + ctrl.selection.length);
            validatePolicyAddressTable();
        };

        $scope.$watchCollection('ctrl.selection', function(newValue) {
            $log.debug('selection watch');
            $log.debug(newValue);

            if (newValue && newValue.length > 0) {
                ctrl.selectionCount = newValue.length.toString();
            } else {
                ctrl.selectionCount = '';
            }

            $log.debug('selection count: ' + ctrl.selectionCount);
        });

        ctrl.changeSelected = function(seqNumber, policyNumber) {
            factory.setSelectedPolicy(seqNumber, policyNumber);
            ctrl.config.localAddressForm.formManager.setValue('policyAddressTable', ctrl.selection.length);
            ctrl.config.overseasAddressForm.formManager.setValue('policyAddressTable', ctrl.selection.length);

            validatePolicyAddressTable();
        };

        ctrl.isOverSea = function(item) {
            return factory.isOverSeaAddress(item);
        };

        ctrl.getAddress = function() {
            if (ctrl.config.localAddressForm.formManager.getValue('postalCode')) {
                var postCode = ctrl.config.localAddressForm.formManager.getValue('postalCode');
                factory.getLocalAddress(postCode);
            }
        };

        factory.getDetails();

    }

    angular.module('profileDetails').directive(directiveName, profileUpdatePage);
})();
