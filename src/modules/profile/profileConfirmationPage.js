(function() {
    'use strict';

    var directiveName = 'profileConfirmationPage';

    /*@ngInject*/
    function profileConfirmationPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: profileConfirmationCtrl,
            templateUrl: 'templates/profile/profileConfirmation.html'
        };
    }

    /*@ngInject*/
    function profileConfirmationCtrl($log, ProfileFactory, $stateParams, routeService) {
        $log.debug('************* profileConfirmationCtrl ********************');

        var ctrl = this;
        var factory = new ProfileFactory(ctrl);
        ctrl.newData = {};
        ctrl.oldData = {};
        ctrl.newData = $stateParams.newData;
        ctrl.oldData = $stateParams.oldData;

        ctrl.newMobile = ctrl.newData.contact[0];
        ctrl.newHouse = ctrl.newData.contact[1];
        ctrl.newOffice = ctrl.newData.contact[2];
        ctrl.newEmail = ctrl.newData.email;
        ctrl.newPolicyAddress = ctrl.newData['policy_addr'][0];

        ctrl.mobileChanges = angular.equals(ctrl.oldData.mobile, ctrl.newMobile);
        ctrl.houseChanges = angular.equals(ctrl.oldData.house, ctrl.newHouse);
        ctrl.officeChanges = angular.equals(ctrl.oldData.office, ctrl.newOffice);
        ctrl.emailChanges = angular.equals(ctrl.oldData.data.email, ctrl.newEmail);

        ctrl.isOverSea = function(item) {
            return factory.isOverSeaAddress(item);
        };

        ctrl.confirm = function() {
            factory.updateProfileDetails();
        };

        ctrl.back = function() {
            routeService.goToPath('/profile-update');
        };

        factory.getDetails();

    }

    angular.module('profileDetails').directive(directiveName, profileConfirmationPage);
})();
