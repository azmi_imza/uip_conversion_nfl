(function() {
    'use strict';

    var directiveName = 'profilePage';

    /*@ngInject*/
    function profilePage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: profileCtrl,
            templateUrl: 'templates/profile/profile.html'
        };
    }

    /*@ngInject*/
    function profileCtrl($translate, $log, ProfileFactory, routeService) {
        $log.debug('************* profileCtrl ********************');
        // formManagerService.clearValidators();
        var ctrl = this;
        var factory = new ProfileFactory(ctrl);

        ctrl.model = {};

        ctrl.update = function() {
            routeService.goToPath('/profile-update');
        };

        ctrl.isOverSea = function(item) {
            return factory.isOverSeaAddress(item);
        };

        factory.getDetails();

    }

    angular.module('profileDetails').directive(directiveName, profilePage);
})();
