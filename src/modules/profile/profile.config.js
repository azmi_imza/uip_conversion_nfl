(function() {
    'use strict';

    angular.module('profileDetails').value('profileConfig', {
        personalDetailsForm: {
            name: 'personalDetailsForm',
            mode: 'edit'
        },
        localAddressForm: {
            name: 'localAddressForm',
            mode: 'edit'
        },
        overseasAddressForm: {
            name: 'overseasAddressForm',
            mode: 'edit'
        },
        personalDetails: {
            mobile: {
                areaCode: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {
                        isRequired: {
                            message: 'cfo.label.fieldIsRequired'
                        }
                    },
                    ddlType: 'static',
                    defaultLabel: 'cfo.label.areaCode',
                    defaultValue: '',
                    columnCssClass: 'col-xs-5 col-sm-5 col-md-4 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'col-xs-12'
                },
                number: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {
                        isRequired: {
                            message: 'cfo.label.fieldIsRequired'
                        },
                        maxLength: {
                            message: 'Maximum field length is 8',
                            value: 8
                        }
                    },
                    inputFormat: {
                        type: 'currency',
                        attributes: {
                            currencyFormat: false,
                            currencyInteger: '8',
                            currencyDecimal: '0'
                        }
                    },
                    columnCssClass: 'col-xs-7 col-sm-7 col-md-8 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'two-fields-padding'
                }
            },
            house: {
                areaCode: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {},
                    ddlType: 'static',
                    defaultLabel: 'cfo.label.areaCode',
                    defaultValue: '',
                    columnCssClass: 'col-xs-5 col-sm-5 col-md-4 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'col-xs-12'
                },
                number: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {
                        maxLength: {
                            message: 'Maximum field length is 10',
                            value: 10
                        }
                    },
                    inputFormat: {
                        type: 'currency',
                        attributes: {
                            currencyFormat: false,
                            currencyInteger: '10',
                            currencyDecimal: '0'
                        }
                    },
                    columnCssClass: 'col-xs-7 col-sm-7 col-md-8 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'two-fields-padding'
                }
            },
            office: {
                areaCode: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {},
                    ddlType: 'static',
                    defaultLabel: 'cfo.label.areaCode',
                    defaultValue: '',
                    columnCssClass: 'col-xs-5 col-sm-5 col-md-4 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'col-xs-12'
                },
                number: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {
                        maxLength: {
                            message: 'Maximum field length is 10',
                            value: 10
                        }
                    },
                    inputFormat: {
                        type: 'currency',
                        attributes: {
                            currencyFormat: false,
                            currencyInteger: '10',
                            currencyDecimal: '0'
                        }
                    },
                    columnCssClass: 'col-xs-7 col-sm-7 col-md-8 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'two-fields-padding'
                }
            },
            others: {
                areaCode: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {},
                    ddlType: 'static',
                    defaultLabel: 'cfo.label.areacode',
                    defaultValue: '',
                    columnCssClass: 'col-xs-3 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: 'col-xs-12'
                },
                number: {
                    label: {
                        name: '',
                        tooltip: ''
                    },
                    validators: {},
                    columnCssClass: 'col-xs-9 personal-details__input',
                    labelCssClass: 'personal-details-input-label',
                    inputCssClass: ''
                }
            },
            email: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 50',
                        value: 50
                    }
                },
                columnCssClass: 'col-xs-12 personal-details__input',
                labelCssClass: 'personal-details-input-label',
                inputCssClass: ''
            }
        },
        localAddress: {
            block: {
                label: {
                    name: 'cfo.label.personalAddressBlock',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 7',
                        value: 7
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            },
            street: {
                label: {
                    name: 'cfo.label.personalAddressStreetName',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 32',
                        value: 32
                    },
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'storeyAndUnitNum',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'postalCode',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'policyAddressTable',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            },
            storeyAndUnitNum: {
                label: {
                    name: 'cfo.label.personalAddressStoreyUnitNumber',
                    tooltip: ''
                },
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'street',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'postalCode',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'policyAddressTable',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    },
                    maxLength: {
                        message: 'Maximum field length is 15',
                        value: 15
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            },
            building: {
                label: {
                    name: 'cfo.label.personalAddressBuildingName',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 45',
                        value: 45
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            },
            postalCode: {
                label: {
                    name: 'cfo.label.postalCode',
                    tooltip: ''
                },
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'street',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'storeyAndUnitNum',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'policyAddressTable',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    },
                    maxLength: {
                        message: 'Maximum field length is 9',
                        value: 9
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: '10',
                        currencyDecimal: '0'
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            }
        },
        overseasAddress: {
            addr1: {
                label: {
                    name: 'cfo.label.personalAddressOverseas',
                    tooltip: ''
                },
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }
                },
                labelCssClass: 'col-xs-12',
                inputCssClass: 'col-xs-12'
            },
            addr2: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }
                },
                labelCssClass: 'policy-address-overseas',
                inputCssClass: 'col-xs-12'
            },
            addr3: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }
                },
                labelCssClass: 'policy-address-overseas',
                inputCssClass: 'col-xs-12'
            },
            addr4: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }
                },
                labelCssClass: 'policy-address-overseas',
                inputCssClass: 'col-xs-12'
            },
            addr5: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }

                },
                labelCssClass: 'policy-address-overseas',
                inputCssClass: 'col-xs-12'
            },
            addr6: {
                label: {
                    name: '',
                    tooltip: ''
                },
                validators: {
                    maxLength: {
                        message: 'Maximum field length is 40',
                        value: '40'
                    }
                },
                labelCssClass: 'policy-address-overseas',
                inputCssClass: 'col-xs-12'
            },
            countryName: {
                label: {
                    name: 'Country',
                    tooltip: ''
                },
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'addr1',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr2',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr3',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr4',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr5',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr6',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'policyAddressTable',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    }
                },
                ddlType: 'static',
                defaultLabel: 'cfo.label.pleaseSelect',
                columnCssClass: 'col-xs-12',
                labelCssClass: '',
                inputCssClass: 'col-xs-12'
            }
        },
        policyAddressTable: {
            localAddress: {
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'street',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'storeyAndUnitNum',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'postalCode',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    }
                }
            },
            overseasAddress: {
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'addr1',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr2',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr3',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr4',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr5',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'addr6',
                            message: 'cfo.label.fieldIsRequired'
                        }, {
                            name: 'countryName',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    }
                }
            }
        }
    });
})();
