(function() {
    'use strict';

    var factoryName = 'ProfileFactory';

    /*@ngInject*/
    var factory = function($log, $resource, $http, $state, APP_CONFIG, $filter, $timeout, routeService, uiHelperService) {
        return function(ctrl) {

            ctrl.selection = [];
            ctrl.localAddress = {
                block: '',
                street: '',
                'storey_and_unit_num': '',
                building: '',
                'postal_cd': ''

            };
            ctrl.isOverseaAddr = 'N';
            ctrl.oversea = {
                'addr_1': '',
                'addr_2': '',
                'addr_3': '',
                'addr_4': '',
                'addr_5': '',
                'addr_6': '',
                'country_name': ''
            };
            ctrl.factoryModel = {};
            ctrl.factoryModel['policy_addr'] = [];
            ctrl.factoryModel.contact = [];
            ctrl.factoryModel.email = '';
            ctrl.modelCopy = {};

            var requestFailedCallback = function(response) {
                $log.debug('Service data failed: ');
                $log.debug('Response: ');
                $log.debug(response);

                // TODO: handle error
                $log.error('Request failed. Please try again.');
                uiHelperService.hideLoader();
            };

            var contactFilter = function(contactModel, type) {
                var obj = $filter('filter')(ctrl.model.data.contact, {
                    'type': type
                });

                if (obj.length <= 0) {
                    $log.debug('empty obj');
                    var newObj = {};
                    newObj.type = type;
                    newObj.number = '';
                    newObj['country_cd'] = '';
                    newObj['seq_num'] = '';

                    obj.push(newObj);
                }

                contactModel = obj[0];
                return contactModel;
            };

            var updateContactList = function() {
                ctrl.model.mobile['country_cd'] = ctrl.config.overseasAddressForm.formManager.getValue('mobileAreaCode');
                ctrl.model.mobile.number = ctrl.config.overseasAddressForm.formManager.getValue('mobileNumber');
                ctrl.factoryModel.contact.push(ctrl.model.mobile);

                if (ctrl.config.overseasAddressForm.formManager.getValue('houseNumber')) {
                    ctrl.model.house['country_cd'] = ctrl.config.overseasAddressForm.formManager.getValue('houseAreaCode');
                    ctrl.model.house.number = ctrl.config.overseasAddressForm.formManager.getValue('houseNumber');
                    ctrl.factoryModel.contact.push(ctrl.model.house);
                }

                if (ctrl.config.overseasAddressForm.formManager.getValue('officeNumber')) {
                    ctrl.model.office['country_cd'] = ctrl.config.overseasAddressForm.formManager.getValue('officeAreaCode');
                    ctrl.model.office.number = ctrl.config.overseasAddressForm.formManager.getValue('officeNumber');
                    ctrl.factoryModel.contact.push(ctrl.model.office);
                }

                if (ctrl.config.overseasAddressForm.formManager.getValue('othersNumber')) {
                    ctrl.model.other['country_cd'] = ctrl.config.overseasAddressForm.formManager.getValue('othersAreaCode');
                    ctrl.model.other.number = ctrl.config.overseasAddressForm.formManager.getValue('othersNumber');
                    ctrl.factoryModel.contact.push(ctrl.model.other);
                }

                ctrl.factoryModel.email = ctrl.config.overseasAddressForm.formManager.getValue('email');
            };

            function loadOtherCountryListOptions() {
                var dataPath = APP_CONFIG.API_PATHS.COUNTRYLIST;
                $timeout(function() {
                    $resource(dataPath).get({
                            lang: APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE
                        },

                        function(response) {
                            $log.debug('Country List loaded');
                            ctrl.model.otherCountryOptions = response.data.items;
                        },

                        function() {
                            //fail repsonse handling
                            $log.debug('Fail to load Country List');
                        });
                }, 0);
            }

            function loadAreaCodeListOptions() {
                var dataPath = APP_CONFIG.API_PATHS.DIAL_CODE;
                $timeout(function() {
                    $resource(dataPath).get({
                            lang: APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE
                        },

                        function(response) {
                            $log.debug('Area Code List loaded');
                            ctrl.model.areaCodeOptions = response.data.items;
                        },

                        function() {
                            //fail repsonse handling
                            $log.debug('Fail to load Area Code List');
                        });
                }, 0);
            }

            this.isOverSeaAddress = function(item) {
                if (item['is_overseas'] === 'N') {
                    ctrl.displayAddress = 'Block ' + (item.block ? item.block + ' ' : '') +
                        (item.street ? item.street + ' ' : '') +
                        (item['storey_and_unit_num'] ? ' #' + item['storey_and_unit_num'] + '-' : '') +
                        (item.building ? item.building + ' ' : ' ') +
                        (item['country_name'] ? item['country_name'] + ' ' : 'Singapore ') +
                        (item['postal_cd'] ? item['postal_cd'] + ' ' : '');

                    return false;
                }

                ctrl.displayAddress = (item['addr_1'] ? item['addr_1'] + ' ' : '') +
                    (item['addr_2'] ? item['addr_2'] + ' ' : '') +
                    (item['addr_3'] ? item['addr_3'] + ' ' : '') +
                    (item['addr_4'] ? item['addr_4'] + ' ' : '') +
                    (item['addr_5'] ? item['addr_5'] + ' ' : '') +
                    (item['addr_6'] ? item['addr_6'] + ' ' : '') +
                    (item['country_id'] ? item['country_id'] + ' ' : '');

                return true;
            };

            var selectAllPolicy = function() {
                ctrl.selection = [];

                if (ctrl.model.selectedPolicies) {

                    angular.forEach(ctrl.model.data['policy_addr'], function(item) {
                        item.selected = true;
                        $log.debug((item.selected === true));
                        if (item.selected) {
                            ctrl.selection.push(item.number);
                        }
                    });
                } else if (!ctrl.model.selectedPolicies) {
                    angular.forEach(ctrl.model.data['policy_addr'], function(item) {
                        item.selected = false;
                    });

                }
            };

            this.getDetails = function() {
                uiHelperService.showLoader();
                var path = APP_CONFIG.API_PATHS.PROFILE_CONTACT;

                var requestSuccessCallback = function(response) {
                    $log.debug('Call back');
                    $log.debug('Request data success on update page');
                    ctrl.model = response;

                    $log.debug(response);
                    ctrl.mobileNum = '';
                    ctrl.houseNum = '';
                    ctrl.officeNum = '';

                    // TODO: initialize value based on user's country
                    ctrl.model.localCountry = 'Singapore';
                    ctrl.model.country = {
                        name: ctrl.model.localCountry
                    };
                    loadOtherCountryListOptions();
                    loadAreaCodeListOptions();

                    ctrl.cka = {};
                    ctrl.model.house = contactFilter(ctrl.model.house, 'HOUSE');
                    ctrl.model.office = contactFilter(ctrl.model.office, 'OFFICE');
                    ctrl.model.other = contactFilter(ctrl.model.other, 'OTHERS');
                    ctrl.model.mobile = contactFilter(ctrl.model.mobile, 'MOBILE');

                    if (ctrl.model.data['policy_addr'].length > 0) {
                        ctrl.hasPolicyAddress = true;
                    } else {
                        ctrl.hasPolicyAddress = false;
                    }

                    if (ctrl.model.mobile['country_cd'] !== '' && ctrl.model.mobile.number !== '') {
                        ctrl.mobileNum = ctrl.model.mobile['country_cd'] + ' ' + ctrl.model.mobile.number;
                    } else {
                        ctrl.mobileNum = '-';
                    }

                    if (ctrl.model.house['country_cd'] !== '' && ctrl.model.house.number !== '') {
                        ctrl.houseNum = ctrl.model.house['country_cd'] + ' ' + ctrl.model.house.number;
                    } else {
                        ctrl.houseNum = '-';
                    }

                    if (ctrl.model.office['country_cd'] !== '' && ctrl.model.office.number !== '') {
                        ctrl.officeNum = ctrl.model.office['country_cd'] + ' ' + ctrl.model.office.number;
                    } else {
                        ctrl.officeNum = '-';
                    }

                    var ckaStatus = '';
                    if (response.data.cka && response.data.cka !== null && response.data.cka !== 'undefined') {
                        if (response.data.cka.status === 'F') {
                            ckaStatus = 'cfo.label.profileCkaFailed';
                        } else {
                            ckaStatus = 'cfo.label.profileCkaPassed';
                        }

                        ctrl.cka.status = ckaStatus;
                        ctrl.cka['submit_dt'] = $filter('date')(response.data.cka['submit_dt'], 'dd MMM yyyy') || '-';
                        ctrl.cka['expiry_dt'] = $filter('date')(response.data.cka['expiry_dt'], 'dd MMM yyyy') || '-';
                    } else if (response.data.cka === null) {
                        ctrl.cka.status = '-';
                        ctrl.cka['submit_dt'] = '-';
                        ctrl.cka['expiry_dt'] = '-';
                    }

                    /*set default value for profileUpdate page*/
                    if (ctrl.config) {
                        ctrl.config.personalDetailsForm.formManager.setValue('mobileAreaCode', ctrl.model.mobile['country_cd'] || '65');
                        ctrl.config.personalDetailsForm.formManager.setValue('mobileNumber', ctrl.model.mobile.number);

                        ctrl.config.personalDetailsForm.formManager.setValue('houseAreaCode', ctrl.model.house['country_cd'] || '65');
                        ctrl.config.personalDetailsForm.formManager.setValue('houseNumber', ctrl.model.house.number);

                        ctrl.config.personalDetailsForm.formManager.setValue('officeAreaCode', ctrl.model.office['country_cd'] || '65');
                        ctrl.config.personalDetailsForm.formManager.setValue('officeNumber', ctrl.model.office.number);

                        ctrl.config.personalDetailsForm.formManager.setValue('email', response.data.email);

                        ctrl.dataLoaded = true;
                        ctrl.model.selectedPolicies = true;
                        ctrl.selectAllPolicy();
                    }

                    ctrl.modelCopy = angular.copy(ctrl.model);

                    $timeout(function() {
                        $('.footable').footable();
                    }, 0);

                    uiHelperService.hideLoader();
                };

                $resource(path).get({}, requestSuccessCallback, requestFailedCallback);
            };

            var updateAddress = function() {

                for (var i = 0; i < ctrl.selection.length; i++) {
                    ctrl.tempAdd = {};
                    ctrl.tempAdd.number = ctrl.selection[i];

                    if (ctrl.isOverseaAddr === 'N') {
                        ctrl.tempAdd.block = ctrl.config.localAddressForm.formManager.getValue('block');
                        ctrl.tempAdd.street = ctrl.config.localAddressForm.formManager.getValue('street');
                        ctrl.tempAdd['storey_and_unit_num'] = ctrl.config.localAddressForm.formManager.getValue('storeyAndUnitNum');
                        ctrl.tempAdd.building = ctrl.config.localAddressForm.formManager.getValue('building');
                        ctrl.tempAdd['postal_cd'] = ctrl.config.localAddressForm.formManager.getValue('postalCode');
                        ctrl.tempAdd['is_overseas'] = ctrl.isOverseaAddr;
                    } else {
                        ctrl.tempAdd['addr_1'] = ctrl.config.overseasAddressForm.formManager.getValue('addr1');
                        ctrl.tempAdd['addr_2'] = ctrl.config.overseasAddressForm.formManager.getValue('addr2');
                        ctrl.tempAdd['addr_3'] = ctrl.config.overseasAddressForm.formManager.getValue('addr3');
                        ctrl.tempAdd['addr_4'] = ctrl.config.overseasAddressForm.formManager.getValue('addr4');
                        ctrl.tempAdd['addr_5'] = ctrl.config.overseasAddressForm.formManager.getValue('addr5');
                        ctrl.tempAdd['addr_6'] = ctrl.config.overseasAddressForm.formManager.getValue('addr6');
                        ctrl.tempAdd['country_id'] = ctrl.config.overseasAddressForm.formManager.getValue('countryName');
                        ctrl.tempAdd['is_overseas'] = ctrl.isOverseaAddr;
                    }

                    for (var j = 0; j < ctrl.model.data['policy_addr'].length; j++) {
                        if (ctrl.selection[i] === ctrl.model.data['policy_addr'][i].id) {
                            ctrl.tempAdd.number = ctrl.model.data['policy_addr'][i].number;
                        }
                    }

                    ctrl.factoryModel['policy_addr'].push(ctrl.tempAdd);
                }

            };

            var successCallback = function(response) {
                $log.debug('Response status: ' + response.statusText);
                $log.debug(response);

                var status = response.data.status;
                var message = response.data.message;

                if (status === 'success') {
                    routeService.goToPath('/profile-update/ack-success', null, {
                        'response': status,
                        'message': message,
                        'transactionRef': response.transactionRef
                    });
                } else {
                    routeService.goToPath('/profile-update/ack-failed', null, {
                        'response': status,
                        'message': message
                    });
                }

            };

            var failCallback = function(response) {
                var status = response.data.status;
                $log.debug('Response status: ' + response.statusText);
                $log.debug(response);
                routeService.goToPath('/profile-update/ack-failed', null, {
                    'response': status
                });
            };

            this.toConfirmationPage = function() {
                updateContactList();
                updateAddress();

                routeService.goToState('base.auth.profile-confirmation', {
                    newData: ctrl.factoryModel,
                    oldData: ctrl.modelCopy
                });
            };

            this.updateProfileDetails = function() {
                $log.debug(angular.toJson(ctrl.newData));
                return $http({
                    method: 'PUT',
                    data: ctrl.newData,
                    url: APP_CONFIG.API_PATHS.PROFILE_CONTACT
                }).then(successCallback, failCallback);
            };

            this.setSelectAllPolicy = function() {
                selectAllPolicy();
            };

            this.setSelectedPolicy = function(seqNumber, policyNumber) {
                function removePolicyFromSelection() {
                    $log.debug('unselect: ' + policyNumber);

                    ctrl.selection = ctrl.selection.filter(function(item) {
                        $log.debug('item id: ' + item);
                        $log.debug(item);
                        if (item === policyNumber) {
                            return false;
                        }

                        return true;
                    });

                    $log.debug('After removing: ' + ctrl.selection);
                }

                var policyAddresses = ctrl.model.data['policy_addr'];

                for (var j = 0; j < policyAddresses.length; j++) {
                    if (policyAddresses[j].number === policyNumber) {
                        if (policyAddresses[j].selected) {
                            ctrl.selection.push(policyAddresses[j].number);
                            $log.debug('selected policyId: ' + policyAddresses[j].number);
                            break;
                        } else if (!policyAddresses[j].selected) {
                            removePolicyFromSelection();
                            break;
                        }
                    }
                }

                // set radio button value
                if (ctrl.selection.length === ctrl.model.data['policy_addr'].length) {
                    ctrl.model.selectedPolicies = true;
                } else if (ctrl.selection.length === 0) {
                    ctrl.model.selectedPolicies = false;
                } else {
                    ctrl.model.selectedPolicies = false;
                }
            };

            var requestLocalAddressSuccessCallback = function(response) {
                $log.debug('Call back');
                $log.debug('Request data success');

                if (response.status === 'success' && response.total > 0) {
                    $timeout(function() {

                        ctrl.config.localAddressForm.formManager.setValue('block', response.data.block);
                        ctrl.config.localAddressForm.formManager.setValue('building', response.data['building_name']);
                        ctrl.config.localAddressForm.formManager.setValue('street', response.data['street_name']);

                    }, 0);
                }

            };

            this.getLocalAddress = function(postCode) {
                var addressPath = APP_CONFIG.API_PATHS.GET_LOCAL_ADDRESS + postCode;

                $resource(addressPath).get({}, requestLocalAddressSuccessCallback, requestFailedCallback);
            };

        };
    };

    angular.module('profileDetails').factory(factoryName, factory);
})();
