/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'nonGePolicyAddPage';

    /*@ngInject*/
    function nonGePolicyAddPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nonGePolicyAddCtrl,
            templateUrl: 'templates/dashboard/nonGePolicyAdd.html'
        };
    }

    /*@ngInject*/
    function nonGePolicyAddCtrl(routeService, uiHelperService, $http, APP_CONFIG, $log, $timeout, generalModalService, FormManager, nonGePolicyConfig) {
        var ctrl = this;

        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.formManager = new FormManager('addPolicyForm', 'edit', ctrl);
        ctrl.config.planName = nonGePolicyConfig.planName;
        ctrl.config.policyType = nonGePolicyConfig.policyType;
        ctrl.config.sumAssured = nonGePolicyConfig.sumAssured;
        ctrl.config.sumAssuredCurrency = nonGePolicyConfig.sumAssuredCurrency;
        ctrl.config.premiumCurrency = nonGePolicyConfig.premiumCurrency;
        ctrl.config.premiumFrequency = nonGePolicyConfig.premiumFrequency;
        ctrl.config.premiumAmount = nonGePolicyConfig.premiumAmount;
        ctrl.config.commencementDate = nonGePolicyConfig.commencementDate;
        ctrl.config.coverageEndDate = nonGePolicyConfig.coverageEndDate;

        ctrl.addPolicy = function() {
            var success = function(response) {
                routeService.goToState('base.auth.non-ge-policy-ack', {
                    response: response
                });
            };

            var error = function() {};

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    $http({
                        method: 'POST',
                        url: APP_CONFIG.API_PATHS.ADD_EXTERNAL_POLICY,
                        data: ctrl.formModel
                    }).success(success).error(error)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.resetForm = function() {
            ctrl.formManager.reset();
        };

        $timeout(function() {
            $('input').placeholder({
                customClass: 'customPlaceholder'
            });
        });
    }

    angular.module('dashboard').directive(directiveName, nonGePolicyAddPage);
})();
