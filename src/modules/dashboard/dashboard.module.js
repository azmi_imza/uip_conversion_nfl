(function() {
    'use strict';
    angular.module('dashboard', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(statePrefix.concat('dashboard-in'), {
                    url: '/dashboard',
                    template: '<div dashboard></div>',
                    data: {
                        screenCode: 'SCRNDASHBOARDIN'
                    }
                });

                $stateProvider.state(statePrefix.concat('dashboard-be'), {
                    url: '/dashboard-be',
                    template: '<div dashboard></div>',
                    data: {
                        screenCode: 'SCRNDASHBOARDBE'
                    }

                });

                $stateProvider.state(statePrefix.concat('non-ge-policy-add'), {
                    url: '/non-ge-policy-add',
                    template: '<div non-ge-policy-add-page></div>',
                    data: {
                        screenCode: '801021SCRNNONGEPOLICY'
                    }
                });

                $stateProvider.state(statePrefix.concat('non-ge-policy-modify'), {
                    url: '/non-ge-policy-modify',
                    template: '<div non-ge-policy-modify-page></div>',
                    data: {
                        screenCode: '801021SCRNNONGEPOLICY'
                    },
                    params: {
                        response: null,
                        selectedObject: null
                    }
                });

                $stateProvider.state(statePrefix.concat('non-ge-policy-delete'), {
                    url: '/non-ge-policy-delete',
                    template: '<div non-ge-policy-delete-page></div>',
                    data: {
                        screenCode: '801021SCRNNONGEPOLICY'
                    },
                    params: {
                        selectedObject: null
                    }
                });

                $stateProvider.state(statePrefix.concat('non-ge-policy-ack'), {
                    url: '/non-ge-policy-ack',
                    template: '<div non-ge-policy-ack-page></div>',
                    data: {
                        screenCode: '801021SCRNNONGEPOLICY'
                    },
                    params: {
                        response: null
                    }
                });
            });
})();
