/* eslint no-alert:0 */

(function() {
    'use strict';

    var directiveName = 'nonGePolicyModifyPage';

    /*@ngInject*/
    function nonGePolicyModifyPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nonGePolicyModifyCtrl,
            templateUrl: 'templates/dashboard/nonGePolicyModify.html'
        };
    }

    /*@ngInject*/
    function nonGePolicyModifyCtrl($timeout, $stateParams, $http, APP_CONFIG, routeService, uiHelperService, nonGePolicyConfig, FormManager) {
        var ctrl = this;
        ctrl.formModel = {};
        ctrl.config = {};
        ctrl.formManager = new FormManager('modifyPolicyForm', 'edit', ctrl);
        ctrl.config.planName = nonGePolicyConfig.planName;
        ctrl.config.policyType = nonGePolicyConfig.policyType;
        ctrl.config.sumAssured = nonGePolicyConfig.sumAssured;
        ctrl.config.sumAssuredCurrency = nonGePolicyConfig.sumAssuredCurrency;
        ctrl.config.premiumCurrency = nonGePolicyConfig.premiumCurrency;
        ctrl.config.premiumFrequency = nonGePolicyConfig.premiumFrequency;
        ctrl.config.premiumAmount = nonGePolicyConfig.premiumAmount;
        ctrl.config.commencementDate = nonGePolicyConfig.commencementDate;
        ctrl.config.coverageEndDate = nonGePolicyConfig.coverageEndDate;

        // ========================================
        // To test the data in stateParams when navigate back from non-ge-policy-delete page
        // ========================================

        if ($stateParams.response) {
            ctrl.formModel = $stateParams.response.data;
        } else if ($stateParams.selectedObject) {
            ctrl.formModel = $stateParams.selectedObject;
        } else if (!$stateParams.response || !$stateParams.selectedObject) {
            routeService.goToState('base.auth.dashboard-in');
        }

        // ========================================

        ctrl.updatePolicy = function() {
            var success = function(response) {
                routeService.goToState('base.auth.non-ge-policy-ack', {
                    response: response
                });
            };

            var error = function() {};

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    $http({
                        method: 'PUT',
                        url: APP_CONFIG.API_PATHS.UPDATE_EXTERNAL_POLICY.replace('{id}', ctrl.formModel.id),
                        data: ctrl.formModel
                    }).success(success).error(error)['finally'](function() {
                        uiHelperService.hideLoader();
                    });
                },

                function(response) {
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.cancelBtnClick = function() {
            //Route back to NON Ge policy Details page
            routeService.goToState('base.auth.policy-details-in', {
                'category': 'nonGePolicies',
                'policyId': ctrl.formModel.id
            });
        };

        $timeout(function() {
            $('input').placeholder({
                customClass: 'customPlaceholder'
            });
        });
    }

    angular.module('dashboard').directive(directiveName, nonGePolicyModifyPage);
})();
