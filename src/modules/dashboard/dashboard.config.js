(function() {
    'use strict';
    angular.module('dashboard')
        .value('nonGePolicyConfig', {
            planName: {
                name: 'product_name',
                mode: 'edit',
                label: {
                    name: 'cfo.label.planName',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding: 10px 15px;',
                columnCssClass: 'col-sm-6 bottom-padding',
                labelCssClass: 'col-xs-4 nopadding',
                inputCssClass: 'col-xs-8 nopadding',
                fieldClass: 'form-control__border-radius-none',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 20',
                        value: 20
                    },
                    patterns: [{
                        message: 'cfo.label.invalidInputType',
                        value: '^[a-zA-Z0-9\\s]+$',
                        id: '18839559-88fc-ed5e-d813-4b773294485b'
                    }]
                }
            },
            policyType: {
                name: 'category_id',
                mode: 'edit',
                label: {
                    name: 'cfo.label.policyType',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                defaultValue: '',
                columnCssClass: 'col-sm-6',
                labelCssClass: 'col-xs-4 nopadding',
                inputCssClass: 'col-xs-8 nopadding',
                inputStyle: 'border-radius: 0px',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                ddlType: 'static',
                url: 'ddl/MaintCategory',
                ddlItems: []
            },
            sumAssured: {
                name: 'sum_assured',
                mode: 'edit',
                label: {
                    name: 'cfo.label.sumAssured',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding-right:5px;',
                fieldClass: 'form-control__border-radius-none',
                columnCssClass: 'col-xs-10 bottom-padding',
                labelCssClass: '',
                inputCssClass: 'col-xs-12 nopadding',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: 8,
                        currencyDecimal: 0
                    }
                }
            },
            sumAssuredCurrency: {
                name: 'sum_assured_currency',
                mode: 'edit',
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: '',
                fieldClass: 'form-control__border-radius-none',
                columnCssClass: 'col-sm-2 bottom-padding',
                labelCssClass: '',
                inputCssClass: 'col-xs-12 nopadding',
                placeHolder: 'SGD',
                validators: {
                    isRequired: {
                        message: 'cfo.label.fieldIsRequired'
                    },
                    maxLength: {
                        message: 'Maximum field length is 3',
                        value: 3
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: 8,
                        currencyDecimal: 2
                    }
                }
            },
            premiumCurrency: {
                name: 'premium_currency',
                mode: 'edit',
                label: {
                    name: '',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: '',
                fieldClass: 'form-control__border-radius-none',
                columnCssClass: 'col-sm-2 bottom-padding',
                labelCssClass: '',
                inputCssClass: 'col-xs-12 nopadding',
                placeHolder: 'SGD',
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'premium',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    },
                    maxLength: {
                        message: 'Maximum field length is 3',
                        value: 3
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: 8,
                        currencyDecimal: 2
                    }
                }
            },
            premiumAmount: {
                name: 'premium',
                mode: 'edit',
                label: {
                    name: 'cfo.label.premiumAmount',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding-right:5px;',
                fieldClass: 'form-control__border-radius-none',
                columnCssClass: 'col-sm-10 bottom-padding',
                labelCssClass: '',
                inputCssClass: 'col-xs-12 nopadding',
                validators: {
                    groupValidation: {
                        type: 'isRequired',
                        message: 'cfo.label.fieldIsRequired',
                        members: [{
                            name: 'premium_currency',
                            message: 'cfo.label.fieldIsRequired'
                        }]
                    }
                },
                inputFormat: {
                    type: 'currency',
                    attributes: {
                        currencyFormat: false,
                        currencyInteger: 8,
                        currencyDecimal: 2
                    }
                }
            },
            premiumFrequency: {
                name: 'payment_frequency_id',
                mode: 'edit',
                label: {
                    name: 'cfo.label.paymentFrequency',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                defaultLabel: 'cfo.label.pleaseSelect',
                inputStyle: 'border-radius: 0px',
                labelCssClass: 'col-xs-4 nopadding',
                inputCssClass: 'col-xs-8 nopadding',
                defaultValue: '',
                columnCssClass: 'col-sm-6',
                validators: {},
                ddlType: 'static',
                url: 'ddl/MaintPymtFreq',
                ddlItems: []
            },
            commencementDate: {
                name: 'commencement_dt',
                mode: 'edit',
                label: {
                    name: 'cfo.label.commencementDate',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'datePicker-icon__NonGE form-control__border-radius-none',
                columnCssClass: 'col-sm-6 bottom-padding',
                labelCssClass: 'col-xs-4 nopadding',
                inputCssClass: 'col-xs-8 nopadding',
                validators: {},
                format: 'dd MMM yyyy',
                minDependency: [],
                maxDependency: [{
                    name: 'coverage_end_dt',
                    excludeSelected: false
                }]
            },
            coverageEndDate: {
                name: 'coverage_end_dt',
                mode: 'edit',
                label: {
                    name: 'cfo.label.coverageEndDate',
                    tooltip: '',
                    tooltipPlacement: ''
                },
                fieldStyle: 'padding: 10px 15px;',
                fieldClass: 'datePicker-icon__NonGE form-control__border-radius-none',
                columnCssClass: 'col-sm-6 bottom-padding',
                labelCssClass: 'col-xs-4 nopadding',
                inputCssClass: 'col-xs-8 nopadding',
                validators: {},
                format: 'dd MMM yyyy',
                minDependency: [{
                    name: 'commencement_dt',
                    excludeSelected: false
                }],
                maxDependency: []
            }
        });
})();
