(function() {
    'use strict';

    var factoryName = 'pieChartFactory';

    /*@ngInject*/
    var factory = function($log, $timeout, $resource, APP_CONFIG, pieChartConfig) {
        var PieChartFactory = function() {

            var paper;
            var chartData;
            var size;
            var sectorAngleArr;
            var middleAngle;
            var middleAngleArr;
            var angleArray;
            var iconArray;
            var textArray;
            var glowArray;
            var mainSet;
            var categoryCircleArray;
            var animatePathArray;

            function dataDeclaration() {
                $log.debug('Run function dataDeclaration');
                chartData = pieChartConfig.pieChartData;

                /*
                    MSIE: ie8, ie9, ie10
                    Trident: ie11
                    Edge: ie12
                */
                // this section to be change for handling for IE8

                // var isIE = /(MSIE|Trident\/|Edge\/)/i.test(navigator.userAgent);
                // console.log(isIE);
                // if (isIE) {
                //     paper = new Raphael('pieChartSection', 550, 550);
                //     // paper.setViewBox(-25, -25, 550, 600, true);
                //     paper.setSize(400, 400);
                // } else {
                paper = new Raphael('pieChartSection');
                paper.setSize(400, 400);
                // }

                size = 400;
                var total = 0;

                sectorAngleArr = [];
                middleAngle = 0;
                middleAngleArr = [];
                angleArray = [];
                iconArray = [];
                textArray = [];
                glowArray = [];
                categoryCircleArray = [];
                animatePathArray = [];
                mainSet = paper.set();

                total = chartData.length;

                for (var i = 0; i < total; i++) {
                    var angle = Math.abs(360 / total);
                    sectorAngleArr.push(angle);

                }

                paper.customAttributes.arc = function(startx, starty, value, tempTotal, R) {
                    var alpha = 360 / tempTotal * value,
                        a = (180 - alpha) * Math.PI / 180,
                        x = startx + R * Math.cos(a),
                        y = starty - R * Math.sin(a),
                        path;

                    if (tempTotal === value) {

                        path = [
                            ['M', startx - R, starty],
                            ['A', R, R, 0, 0, 1, startx - R - 0.01, starty - 0.01]
                        ];

                    } else {

                        path = [
                            ['M', startx - R, starty],
                            ['A', R, R, 0, +(alpha > 180), 1, x, y]
                        ];
                    }

                    return {
                        path: path
                    };
                };

                Raphael.el.animateAlong = function(path, duration, easing, callback) {
                    var element = this;
                    element.path = path;
                    element.pathLen = element.path.getTotalLength();
                    duration = (typeof duration === 'undefined') ? 5000 : duration;
                    easing = (typeof easing === 'undefined') ? 'linear' : duration;

                    //create an "along" function to take a variable from zero to 1 and return coordinates. Note we're using cx and cy specifically for a circle
                    paper.customAttributes.along = function(v) {
                        var point = this.path.getPointAtLength(v * this.pathLen),
                            attrs = {
                                cx: point.x,
                                cy: point.y
                            };

                        return attrs;
                    };

                    element.attr({
                        along: 0
                    }).animate({
                        along: 1
                    }, duration, easing, function() {
                        callback && callback.call(element);
                    });
                };

            }

            function createPieChartImage(src, xPos, yPos, width, height) {
                var image = paper.image(src, xPos, yPos, width, height);
                image.attr({
                    'cursor': 'hand'
                });
                image.hide();
                iconArray.push(image);
            }

            function createPieChartText(xPos, yPos, planName, fillColor) {
                planName = planName.toUpperCase();

                var text = paper.text(xPos, yPos, planName);
                text.attr({
                    fill: fillColor,
                    'font-size': '7px',
                    'font-family': 'Arial',
                    'cursor': 'hand'
                });
                text.hide();
                textArray.push(text);
            }

            function createCategoryCricle(idIndex, xPos, yPos, circleRadius, fillColor, strokeColor) {

                var categoryCircle = paper.circle(xPos, yPos, circleRadius);
                categoryCircle.attr({
                    fill: fillColor,
                    stroke: strokeColor
                });

                categoryCircleArray.push(categoryCircle);
                categoryCircleArray[idIndex].id = idIndex;

            }

            function middleAngleCoordinates(distance, middleAngleParam) {
                var middle = {};
                middle.x = (size / 2) + distance * Math.sin(Math.PI * middleAngleParam / 180);
                middle.y = (size / 2) - distance * Math.cos(Math.PI * middleAngleParam / 180);
                return middle;
            }

            //function to render images/icon and text on pie chart
            function renderTextandIcon() {
                $log.debug('Render text and Icon');
                var position = [];
                for (var l = 0; l < middleAngleArr.length; l++) {
                    position[l] = middleAngleCoordinates(size / 3, middleAngleArr[l] - 90);
                }

                for (var m = 0; m < chartData.length; m++) {
                    createPieChartImage(chartData[m].logo.image, position[m].x - chartData[m].logo.width / 2, position[m].y - chartData[m].logo.height / 1.5, chartData[m].logo.width, chartData[m].logo.height);
                    createPieChartText(position[m].x, position[m].y + 20, chartData[m].title, 'white');
                }

                $timeout(function() {
                    for (var n = 0; n < chartData.length; n++) {
                        iconArray[n].show();
                    }
                }, 1200);
            }

            function animateGlow(sliceId) {
                for (var a = 0; a < glowArray.length; a++) {
                    if (sliceId === a) {
                        glowArray[a].show();
                        glowArray[a].animate({
                            transform: 'S1.62'
                        }, 1000, 'linear');
                    }
                }
            }

            function calculateAngle() {
                $log.debug('Calculate Angles');
                var startAngle = 0;
                var endAngle = 0;

                for (var b = 0; b < chartData.length; b++) {

                    startAngle = endAngle;
                    endAngle = startAngle + sectorAngleArr[b];
                    middleAngle = (sectorAngleArr[b] * (b + 1) / 2) + (sectorAngleArr[b] * b / 2);
                    middleAngleArr.push(middleAngle);
                    angleArray.push((endAngle - startAngle));

                }

            }

            function undoPieSlicesTranslation(element) {

                element.isOnClick = false;
                element.animate({
                    transform: 's1'
                }, 1000, 'linear');
                element[1].animate({
                    transform: 'T0,0s1'
                }, 1000, 'linear');
                element[2].hide();
                glowArray[element.id].hide();
                glowArray[element.id].animate({
                    transform: 'S1'
                }, 1000, 'linear');

            }

            function wedge(ctrl) {

                var pieColor;
                var pieBorderColor;

                calculateAngle();

                var categoryCirclePosition = [];

                for (var l = 0; l < middleAngleArr.length; l++) {
                    categoryCirclePosition[l] = middleAngleCoordinates(size / 3, middleAngleArr[l] - 90);

                    var p = paper.path().attr({
                        arc: [size / 2, size / 2, middleAngleArr[l], 360, size / 3],

                        stroke: 'black',
                        'stroke-width': 1
                    });
                    p.hide();
                    animatePathArray.push(p);
                }

                for (var c = 0; c < chartData.length; c++) {
                    if (ctrl.chartData[c]['is_active_category'] === 'Y') {
                        pieColor = chartData[c].color;
                        pieBorderColor = chartData[c].borderColor;
                    } else {
                        pieColor = '#9B9B9B';
                        pieBorderColor = '#9B9B9B';
                    }

                    var g = paper.circle(categoryCirclePosition[c].x, categoryCirclePosition[c].y, 35).attr({
                        'stroke-opacity': 0
                    }).glow({
                        color: '#D8D8D8',
                        width: 15,
                        opacity: 1,
                        fill: true
                    });
                    g.id = c;
                    glowArray[c] = g;

                    createCategoryCricle(c, categoryCirclePosition[c].x, categoryCirclePosition[c].y, 35, pieColor, pieBorderColor);

                }

                for (var d = 0; d < glowArray.length; d++) {
                    glowArray[d].hide();
                }

                for (var z = 0; z < animatePathArray.length; z++) {
                    categoryCircleArray[z].animateAlong(animatePathArray[z], 1000);
                }

            }

            function controlGlowAndText(element, isOnHover) {
                if (isOnHover || element.isOnClick) {
                    element[2].show();
                } else if (!isOnHover && !element.isOnClick) {

                    element[2].hide();
                }
            }

            function setChartTooltipDetails(ctrl, id) {

                ctrl.categoryColor = chartData[id].color;
                ctrl.categoryTitle = ctrl.chartData[id]['category_name'];
                ctrl.categoryCode = ctrl.chartData[id]['category_cd'];
                ctrl.toolTipDetailMessage = ctrl.chartData[id]['category_other_description'];

                if (ctrl.categoryTitle === 'Life Protection' ||
                    ctrl.categoryTitle === 'Personal Accident Protection' ||
                    ctrl.categoryTitle === 'Health Protection') {
                    ctrl.isCoverMessage = true;
                } else {
                    ctrl.isCoverMessage = false;
                }

                ctrl.toolTipRemarks = chartData[id].remarkMessage;
                ctrl.tooltipPanelIcon = chartData[id].tooltipIcon;
                ctrl.tableModel.recommendationBenefits = ctrl.chartData[id]['benefit_coverage'];
                ctrl.showMessage = false;
                for (var x = 0; x < ctrl.tableModel.recommendationBenefits.length; x++) {
                    if (ctrl.tableModel.recommendationBenefits[x]['is_active_category_benefit'] !== 'Y') {
                        ctrl.showMessage = true;
                        break;
                    }
                }

                ctrl.tableModel.icon = chartData[id].logo.tableIcon;
            }

            function initiateDefaultCategory(ctrl, mainSetItem) {
                mainSetItem.isOnClick = true;
                mainSetItem[0].animate({
                    transform: 's1.55'
                }, 1000, 'linear');

                mainSetItem[1].animate({
                    transform: 'T0,-10s1.1'
                }, 1000, 'linear');

                mainSetItem[2].show();
                mainSetItem[2].animate({
                    transform: 'S2 2'
                }, 1000);

                animateGlow(mainSetItem.id);
                setChartTooltipDetails(ctrl, mainSetItem.id);
                ctrl.showToolTipPanel = true;
                ctrl.showCategoryTooltip = true;
            }

            function renderPieChart(ctrl, scope) {
                $log.debug('Run renderPieChart function');

                var chartCenterimg = paper.image('images/family.png', size / 2 - 125, size / 2 - 125, 250, 250);

                chartCenterimg.show();

                wedge(ctrl);

                ctrl.showRecomendedPlan = true;
                ctrl.isCoverMessage = false;
                ctrl.showMessage = false;
                ctrl.tableModel.recommendationBenefits = [];
                ctrl.badgeIcon1 = 'images/badge_covered.png';
                ctrl.badgeIcon2 = 'images/badge_not_covered.png';
                ctrl.badgeHeader1 = 'cfo.label.coverageBadgeCovered';
                ctrl.badgeHeader2 = 'cfo.label.coverageBadgeNotCovered';

                var isOnHover = false;

                var previousSlice = 0,
                    currentSlice = 0;

                var defaultCategory = -1;

                renderTextandIcon();

                //use mainSet to group pie slice, icon and text into 1 element
                for (var e = 0; e < chartData.length; e++) {
                    var set = paper.set();

                    set.push(categoryCircleArray[e]);
                    set.push(iconArray[e]);
                    set.push(textArray[e]);
                    set.id = e;
                    mainSet.push(set);
                    mainSet[e].isOnClick = false;
                }

                for (var t = 0; t < chartData.length; t++) {
                    if (ctrl.chartData[t]['category_name'] === 'Life Protection' && ctrl.chartData[t]['is_active_category'] === 'Y') {
                        defaultCategory = chartData[t].no;
                        break;
                    } else if (ctrl.chartData[t]['category_name'] !== 'Life Protection' && ctrl.chartData[t]['is_active_category'] === 'Y') {
                        if (defaultCategory < 0) {
                            defaultCategory = chartData[t].no;
                        }
                    }
                }

                // -------------------------------------------------------
                defaultCategory = defaultCategory - 1; //Minus 1 on the number get from charData config data because the actually mainSet indexes starts from 0
                if (defaultCategory > -1) {
                    $timeout(function() {
                        initiateDefaultCategory(ctrl, mainSet[defaultCategory]);
                        ctrl.displaySection = true;
                    }, 1300);
                    currentSlice = mainSet[defaultCategory].id;
                } else {
                    //Incase the user does NOT have any Inforce policy, show
                    // the recommended products for Life Protection category
                    //Life protection category index is '1'
                    // Yet to be tested in the event where a user does NOT have any policy
                    $timeout(function() {
                        setChartTooltipDetails(ctrl, 1);
                        ctrl.displaySection = true;
                        ctrl.showToolTipPanel = true;
                    }, 1300);
                }

                //setting up click even for every element in the mainSet array
                mainSet.forEach(function(element) {
                    $timeout(function() {
                        element.click(function() {

                            previousSlice = currentSlice;
                            currentSlice = element.id;
                            ctrl.showCategoryTooltip = true;
                            ctrl.showBenefitTooltip = false;
                            undoPieSlicesTranslation(mainSet[previousSlice]);
                            if (ctrl.displaySection === false) {
                                ctrl.displaySection = true;
                            }

                            element.isOnClick = true;
                            element.animate({
                                transform: 's1.55'
                            }, 1000, 'linear');
                            element[1].animate({
                                transform: 'T0,-10s1.1'
                            }, 1000, 'linear');

                            element[2].show();
                            element[2].animate({
                                transform: 'S2 2'
                            }, 1000);

                            //The glow animation. Not supported in IE 8
                            animateGlow(element.id);

                            //Display details to table
                            setChartTooltipDetails(ctrl, element.id);

                            scope.$apply();
                        });

                        element.mouseover(function() {

                            isOnHover = true;
                            if (element.isOnClick !== true && isOnHover === true) {
                                glowArray[element.id].show();
                            }

                            controlGlowAndText(element, isOnHover);
                        });

                        element.mouseout(function() {
                            isOnHover = false;

                            if (element.isOnClick !== true && isOnHover === false) {
                                glowArray[element.id].hide();
                            }

                            controlGlowAndText(element, isOnHover);

                        });

                    }, 1300);

                });
            }

            this.getTooltipDescription = function(ctrl, state, type, benefitDescription, selectedCategoryName) {
                if (state === 'open') {
                    ctrl.showToolTipPanel = true;
                } else if (state === 'close') {
                    ctrl.showToolTipPanel = false;
                }

                if (ctrl.showToolTipPanel) {
                    if (type === 'category') {
                        if (ctrl.showBenefitTooltip === true) {
                            ctrl.showBenefitTooltip = false;
                        }

                        if (ctrl.showCategoryTooltip === false) {
                            ctrl.showCategoryTooltip = true;
                        }
                    } else if (type === 'benefit') {
                        if (ctrl.showCategoryTooltip === true) {
                            ctrl.showCategoryTooltip = false;
                        }

                        if (ctrl.showBenefitTooltip === false) {
                            ctrl.showBenefitTooltip = true;
                        }
                    }
                }

                if (ctrl.showCategoryTooltip === true) {
                    for (var k = 0; k < ctrl.chartData.length; k++) {
                        if (ctrl.chartData[k]['category_name'] === ctrl.categoryTitle) {
                            ctrl.toolTipDetailMessage = ctrl.chartData[k]['category_other_description'];
                        }
                    }

                } else if (ctrl.showBenefitTooltip === true) {
                    ctrl.benefitName = benefitDescription;
                    for (var e = 0; e < ctrl.chartData.length; e++) {
                        if (ctrl.chartData[e]['category_name'] === selectedCategoryName) {
                            for (var f = 0; f < ctrl.chartData[e]['benefit_coverage'].length; f++) {
                                if (ctrl.chartData[e]['benefit_coverage'][f]['benefit_description'] === benefitDescription) {
                                    ctrl.toolTipDetailMessage = ctrl.chartData[e]['benefit_coverage'][f]['benefit_other_description'];
                                    if (ctrl.chartData[e]['benefit_coverage'][f]['benefit_description'] === 'Legacy Planning') {
                                        ctrl.specialTooltip = true;
                                    } else {
                                        ctrl.specialTooltip = false;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            this.getPieChartData = function(ctrl, scope) {

                var path = APP_CONFIG.API_PATHS.DASHBOARD_POLICY_CHART;
                var tempChartData = [];
                dataDeclaration();

                var successCallback = function(response) {
                    $log.debug('***********************************');
                    $log.debug('Request data success');
                    tempChartData = response.data;
                    ctrl.showToolTip = false;

                    for (var z = 0; z < chartData.length; z++) {
                        for (var x = 0; x < tempChartData.length; x++) {

                            if (tempChartData[x]['category_name'] === chartData[z].name) {
                                ctrl.chartData.push(tempChartData[x]);
                            }
                        }
                    }
                    renderPieChart(ctrl, scope);

                    $timeout(function() {
                        $('.footable').footable();
                    }, 0);
                };

                var failedCallback = function(response) {
                    $log.debug('Service data failed: ');
                    $log.debug('Response: ');
                    $log.debug(response);

                    // TODO: handle error
                    $log.error('Request failed. Please try again.');
                };

                $resource(path).get({}, successCallback, failedCallback);

            };

        };

        return new PieChartFactory();

    };

    angular.module('dashboard').factory(factoryName, factory);
})();
