(function() {
    'use strict';

    var serviceName = 'dashboardService';

    /*@ngInject*/
    function dashboardService($log, $timeout, $resource, $http) {

        this.resetViewport = function resetViewport() {
            $log.debug('*** resetViewport ***');
            $timeout(function() {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                $('#viewport').attr('content',
                    'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1');

                $timeout(function() {
                    $('#viewport').attr('content', 'width=device-width, initial-scale=1');
                }, 100);
            });
        };

        this.getPlanCategoryListing = function(path, ctrl, tableId) {

            var requestSuccessCallback = function(response) {
                $log.debug('Call back');
                $log.debug('Request data success');
                ctrl.model.tableList = response.data;
                $log.debug(response);
                ctrl.tableExpand = true;
                ctrl.arrowIcon = 'images/open_table_up.png';

                $timeout(function() {
                    $('#' + tableId).footable();
                });

            };

            var requestFailedCallback = function(response) {

                $log.debug('Service data failed: ');
                $log.debug('Response: ');
                $log.debug(response);

                // TODO: handle error
                $log.error('Request failed. Please try again.');
            };

            $resource(path).get({}, requestSuccessCallback, requestFailedCallback);

        };

        this.expandTableFunction = function(ctrl) {
            ctrl.tableExpand = !ctrl.tableExpand;
            if (ctrl.tableExpand) {
                ctrl.arrowIcon = 'images/open_table_up.png';
            } else {
                ctrl.arrowIcon = 'images/open_table_down.png';
            }
        };

        this.getPolicyList = function(policyTypes) {
            var success = function(response) {
                policyTypes.policyList = response.data || [];
                policyTypes.recordTotal = response['record_total'];
            };

            var error = function() {
                policyTypes.policyList = [];
            };

            var joint = policyTypes.apiPath.indexOf('?') < 0 ? '?' : '&';
            var url = policyTypes.apiPath + joint + 'size=' + policyTypes.size + '&page=' + policyTypes.page;

            return $http({
                method: 'GET',
                url: url
            }).success(success).error(error);
        };
    }

    angular.module('dashboard').service(serviceName, dashboardService);

})();
