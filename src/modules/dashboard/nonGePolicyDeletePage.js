(function() {
    'use strict';

    var directiveName = 'nonGePolicyDeletePage';

    /*@ngInject*/
    function nonGePolicyDeletePage() {
        return {
            scope: {},
            bindToController: true,
            controller: nonGePolicyDeleteCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/dashboard/nonGePolicyDelete.html'
        };
    }

    /*@ngInject*/
    function nonGePolicyDeleteCtrl($log, routeService, $stateParams, $http, APP_CONFIG, uiHelperService) {

        $log.debug('nonGePolicyDeleteCtrl');
        var ctrl = this;

        if (!$stateParams.selectedObject) {
            routeService.goToState('base.auth.dashboard-in');
        }

        ctrl.yesBtnClick = function() {

            var success = function(response) {
                routeService.goToState('base.auth.non-ge-policy-ack', {
                    response: response
                });
            };

            var error = function() {
                $log.debug('Error: ' + error);
            };

            uiHelperService.showLoader();
            $http({
                method: 'DELETE',
                url: APP_CONFIG.API_PATHS.DELETE_EXTERNAL_POLICY.replace('{id}', $stateParams.selectedObject.id),
                data: ctrl.formModel
            }).success(success).error(error)['finally'](function() {
                uiHelperService.hideLoader();
            });

        };

        ctrl.noBtnClick = function() {
            routeService.goToState('base.auth.non-ge-policy-modify', {
                'selectedObject': $stateParams.selectedObject
            });

        };
    }

    angular.module('dashboard').directive(directiveName, nonGePolicyDeletePage);
})();
