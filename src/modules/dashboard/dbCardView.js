/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'dbCardView';

    /*@ngInject*/
    function dbCardView() {
        return {
            scope: {
                cardData: '=',
                cardIndex: '=?',
                cardState: '=?',
                cardMeta: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dbCardViewCtrl,
            templateUrl: 'templates/dashboard/dbCardView.html'
        };
    }

    /*@ngInject*/

    function dbCardViewCtrl($http, $scope, $log, $timeout) {
        var ctrl = this;
        ctrl.cardData.collapse = false;

        if (ctrl.cardState !== 'policyType') {
            if (ctrl.cardState !== 'nonGePolicies') {
                ctrl.cardData.id = ctrl.cardIndex;
            }

            ctrl.cardData.header = ctrl.cardData['product_name'];
        }

        ctrl.initCard = function(id) {
            $timeout(function() {
                $('#' + id).on('hidden.bs.collapse shown.bs.collapse', function() {
                    $timeout(function() {
                        ctrl.cardData.collapse = !$('#' + id).hasClass('in');
                    });
                });
            });
        };

        $scope.$on('$destroy', function() {
            $('#' + ctrl.cardData.id).off('.collapse');
        });
    }

    angular.module('dashboard').directive(directiveName, dbCardView);

})();
