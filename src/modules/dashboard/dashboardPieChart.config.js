(function() {
    'use strict';
    angular.module('dashboard').value('pieChartConfig', {
        pieChartData: [{
            no: '1',
            name: 'Lifestyle Protection',
            title: 'Lifestyle',
            logo: {
                image: 'images/db_lifestyleprotection.png',
                tableIcon: 'images/db_lifestyle_protection_red.png',
                width: 41,
                height: 16
            },
            color: '#D21B8E',
            borderColor: '#BB3C8C',
            status: 'active',
            tooltipIcon: 'images/tooltip-lifestyle.png'
        }, {
            no: '2',
            name: 'Life Protection',
            title: 'Life',
            logo: {
                image: 'images/db_lifeprotection.png',
                tableIcon: 'images/db_lifeprotection_red.png',
                width: 30,
                height: 30
            },
            color: '#F7941E',
            borderColor: '#C6904E',
            status: 'active',
            tooltipIcon: 'images/tooltip-life.png'
        }, {
            no: '3',
            name: 'Health Protection',
            title: 'Health',
            logo: {
                image: 'images/db_healthprotection.png',
                tableIcon: 'images/db_healthprotection_red.png',
                width: 27,
                height: 27
            },
            color: '#00A14B',
            borderColor: '#377450',
            status: 'active',
            tooltipIcon: 'images/tooltip-health.png'
        }, {
            no: '4',
            name: 'Personal Accident Protection',
            title: 'PA',
            logo: {
                image: 'images/db_personalaccidentprotection.png',
                tableIcon: 'images/db_personalaccidentprotection_red.png',
                width: 31,
                height: 27
            },
            color: '#FFCB05',
            borderColor: '#B09629',
            status: 'active',
            tooltipIcon: 'images/tooltip-acc.png'
        }, {
            no: '5',
            name: 'Wealth Accumulation',
            title: 'Wealth',
            logo: {
                image: 'images/db_wealthaccumulation.png',
                tableIcon: 'images/db_wealthaccumulation_red.png',
                width: 21,
                height: 30
            },
            color: '#00B8DE',
            borderColor: '#1F93AD',
            status: 'active',
            tooltipIcon: 'images/tooltip-wealth.png'
        }, {
            no: '6',
            name: 'Retirement',
            title: 'Retire',
            logo: {
                image: 'images/db_retirementplanning.png',
                tableIcon: 'images/db_retirementplanning_red.png',
                width: 34,
                height: 25
            },
            color: '#21409A',
            borderColor: '#415085',
            status: 'active',
            tooltipIcon: 'images/tooltip-retire.png'
        }]
    });
})();
