/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'cvPolicyDetails';

    /*@ngInject*/
    function cvPolicyDetails() {
        return {
            scope: {
                cardData: '=',
                cardIndex: '=?',
                cardMeta: '=?',
                cardState: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: cvPolicyDetailsCtrl,
            templateUrl: 'templates/dashboard/cvPolicyDetails.html'
        };
    }

    /*@ngInject*/

    function cvPolicyDetailsCtrl($log, storageService, routeService, APP_CONST) {
        var ctrl = this;

        ctrl.goToDetails = function(category, policy) {
            var userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
            var toState = (userType === 'IN') ? 'policy-details-in' : 'policy-details-be';
            var policyId = (category === 'nonGePolicies') ? policy.id : policy.number;

            routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(toState), {
                category: category,
                policyId: policyId
            });
        };
    }

    angular.module('dashboard').directive(directiveName, cvPolicyDetails);

})();
