/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'dashboard';

    /*@ngInject*/
    function dashboard() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: dashboardCtrl,
            templateUrl: 'templates/dashboard/dashboard.html'
        };
    }

    /*@ngInject*/
    function dashboardCtrl(dashboardService, $http, $scope, $log, $window, $filter, storageService, APP_CONST, $rootScope, $timeout, $state, generalModalService, pieChartFactory, pieChartConfig, APP_CONFIG, _, uiHelperService, routeService) {
        var ctrl = this;
        ctrl.userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE) || 'IN';
        ctrl.tableModel = {};
        ctrl.infoIcon = 'images/Info.png';
        ctrl.userLoginName = storageService.get(APP_CONST.STORAGE_KEYS.SUBSCRIBERNAME);
        ctrl.windowWidth = '';
        ctrl.isPieChartDrawn = false;
        ctrl.iconLinkImage = 'images/icon_link.png';
        ctrl.tooltipPanelCloseButtonIcon = 'images/tooltipPanelClose.png';
        ctrl.tooltipIcon = 'images/tooltipIcon.png';
        ctrl.displaySection = false;
        ctrl.showToolTipPanel = false;
        ctrl.showBenefitTooltip = false;
        ctrl.showCategoryTooltip = false;
        ctrl.badgeTitle = 'Your Benefits Coverage';
        ctrl.chartData = [];
        ctrl.endOfDdl = false;

        ctrl.getBenefitNameObj = function(name) {
            return {
                benefitName: name
            };
        };

        ctrl.getCategoryName = function(name) {
            return {
                categoryName: name
            };
        };

        ctrl.getBenefitName = function(name) {
            return {
                benefitName: name
            };
        };

        ctrl.showTooltip = function(state, type, benefitDescription, selectedCategory) {
            pieChartFactory.getTooltipDescription(ctrl, state, type, benefitDescription, selectedCategory);
        };

        ctrl.policyTypes = [{
            id: 'lifeProtection',
            color: '#F89522',
            icon: {
                active: 'images/ic-lp.png',
                inactive: 'images/ic-lp-d.png'
            },
            header: 'cfo.label.lifeProtection',
            apiPath: APP_CONFIG.API_PATHS.PLAN_LIFE,
            size: 6,
            page: 1
        }, {
            id: 'healthProtection',
            color: '#01A14A',
            icon: {
                active: 'images/ic-hp.png',
                inactive: 'images/ic-hp-d.png'
            },
            header: 'cfo.label.healthProtection',
            apiPath: APP_CONFIG.API_PATHS.PLAN_HEALTH,
            size: 6,
            page: 1
        }, {
            id: 'personalAccident',
            color: '#FFCC08',
            icon: {
                active: 'images/ic-pa.png',
                inactive: 'images/ic-pa-d.png'
            },
            header: 'cfo.label.personalAccident',
            apiPath: APP_CONFIG.API_PATHS.PLAN_PA,
            size: 6,
            page: 1
        }, {
            id: 'wealthAccumulation',
            color: '#00B8DE',
            icon: {
                active: 'images/ic-wa.png',
                inactive: 'images/ic-wa-d.png'
            },
            header: 'cfo.label.wealthAccumulation',
            apiPath: APP_CONFIG.API_PATHS.PLAN_WEALTH,
            size: 6,
            page: 1
        }, {
            id: 'retirementPlanning',
            color: '#204497',
            icon: {
                active: 'images/ic-rp.png',
                inactive: 'images/ic-rp-d.png'
            },
            header: 'cfo.label.retirementPlanning',
            apiPath: APP_CONFIG.API_PATHS.PLAN_RETIREMENT,
            size: 6,
            page: 1
        }, {
            id: 'lifestyleProtection',
            color: '#D21B8E',
            icon: {
                active: 'images/ic-lsp.png',
                inactive: 'images/ic-lsp-d.png'
            },
            header: 'cfo.label.lifestyleProtection',
            apiPath: APP_CONFIG.API_PATHS.PLAN_LIFESTYLE,
            size: 6,
            page: 1
        }, {
            id: 'employeeBenefits',
            color: '#8951A0',
            icon: {
                active: 'images/ic-eb.png',
                inactive: 'images/ic-eb-d.png'
            },
            header: 'cfo.label.employeeBenefits',
            apiPath: APP_CONFIG.API_PATHS.EMPLOYEE,
            size: 6,
            page: 1
        }, {
            id: 'otherInforcePolicies',
            color: '#F0532B',
            icon: {
                active: 'images/ic-oi.png',
                inactive: 'images/ic-oi-d.png'
            },
            header: 'cfo.label.otherInforcePolicies',
            apiPath: APP_CONFIG.API_PATHS.OTHER_INFORCE,
            size: 6,
            page: 1
        }, {
            id: 'nonGePolicies',
            color: '#6252A3',
            icon: {
                active: 'images/ic-nge.png',
                inactive: 'images/ic-nge-d.png'
            },
            header: 'cfo.label.nonGePolicies',
            apiPath: APP_CONFIG.API_PATHS.PLAN_NONGE,
            size: 6,
            page: 1
        }];

        ctrl.policyTypesLabel = 'cfo.label.myPolicyTypes';
        ctrl.cardState = 'policyType';
        ctrl.selectedState = {
            label: ctrl.policyTypesLabel,
            count: ctrl.policyTypes.length
        };

        ctrl.loadMore = function() {
            var currentPosition = $('body').scrollTop();

            uiHelperService.showLoader();
            var card = ctrl.getCardMeta();
            card.page += 1;

            var success = function(response) {
                card.policyList = card.policyList.concat(response.data);
            };

            var error = function() {};

            var joint = card.apiPath.indexOf('?') < 0 ? '?' : '&';
            var url = card.apiPath + joint + 'size=' + card.size + '&page=' + card.page;

            $http({
                method: 'GET',
                url: url
            }).success(success).error(error)['finally'](function() {
                uiHelperService.hideLoader(currentPosition);
            });
        };

        ctrl.showLoadMore = function() {
            if (ctrl.cardState === 'policyType') {
                return false;
            } else {
                var show = false;
                _.forEach(ctrl.policyTypes, function(item) {
                    if (item.id === ctrl.cardState) {
                        if (item.policyList.length < item.recordTotal) {
                            show = true;
                        }

                        return;
                    }
                });

                return show;
            }
        };

        ctrl.togglePolicyList = function(event) {
            event.stopPropagation();
            ctrl.showPolicyTypes = !ctrl.showPolicyTypes;
        };

        ctrl.selectPolicyType = function(id, total) {
            if (total === 0) {
                return;
            }

            ctrl.updateCardState(id);
            ctrl.showPolicyTypes = false;
        };

        ctrl.getPolicies = function() {
            var policies = [];
            _.forEach(ctrl.policyTypes, function(item) {
                if (item.id === ctrl.cardState) {
                    policies = item.policyList;
                    return;
                }
            });

            return policies;
        };

        ctrl.getCardMeta = function() {
            var meta = '';
            _.forEach(ctrl.policyTypes, function(item) {
                if (item.id === ctrl.cardState) {
                    meta = item;
                    return;
                }
            });

            return meta;
        };

        ctrl.updateCardState = function(state) {
            if (ctrl.cardState === state) {
                return;
            }

            ctrl.cardState = '';
            $timeout(function() {
                ctrl.cardState = state;
                ctrl.showExpandAll = false;

                if (ctrl.cardState === 'policyType') {
                    ctrl.selectedState.label = ctrl.policyTypesLabel;
                    ctrl.selectedState.count = ctrl.policyTypes.length;
                } else {
                    _.forEach(ctrl.policyTypes, function(item) {
                        if (item.id === ctrl.cardState) {
                            ctrl.selectedState.label = item.header;
                            ctrl.selectedState.count = item.recordTotal ? item.recordTotal : '0';
                        }
                    });
                }

                uiHelperService.scrollTo($('#cardViewSection').offset().top);
            });
        };

        var collapseInProgress = false;

        ctrl.collapseAll = function() {
            if (collapseInProgress) {
                return;
            }

            collapseInProgress = true;
            var endIndex = 0;
            var array = ctrl.policyTypes;
            if (ctrl.cardState !== 'policyType') {
                _.forEach(ctrl.policyTypes, function(item) {
                    if (item.id === ctrl.cardState) {
                        array = item.policyList;
                    }
                });
            }

            var proceedCollapse = false;
            for (var k = array.length - 1; k >= 0; k--) {
                if ($('#' + array[k].id).hasClass('in')) {
                    endIndex = k;
                    proceedCollapse = true;
                    break;
                }
            }

            if (!proceedCollapse) {
                $timeout(function() {
                    ctrl.showExpandAll = true;
                    collapseInProgress = false;
                });
            } else {
                $('#' + array[endIndex].id).on('hidden.bs.collapse.collapseAll', function() {
                    $timeout(function() {
                        ctrl.showExpandAll = true;
                        collapseInProgress = false;
                    });

                    $('#' + array[endIndex].id).off('hidden.bs.collapse.collapseAll');
                });

                for (var i = 0; i <= endIndex; i++) {
                    if ($('#' + array[i].id).hasClass('in')) {
                        $('#' + array[i].id + 'Control').click();
                    }
                }
            }

        };

        ctrl.expandAll = function() {
            if (collapseInProgress) {
                return;
            }

            collapseInProgress = true;
            var endIndex = 0;
            var array = ctrl.policyTypes;
            if (ctrl.cardState !== 'policyType') {
                _.forEach(ctrl.policyTypes, function(item) {
                    if (item.id === ctrl.cardState) {
                        array = item.policyList;
                    }
                });
            }

            var proceedExpand = false;
            for (var k = array.length - 1; k >= 0; k--) {
                if (!$('#' + array[k].id).hasClass('in')) {
                    endIndex = k;
                    proceedExpand = true;
                    break;
                }
            }

            if (!proceedExpand) {
                $timeout(function() {
                    ctrl.showExpandAll = false;
                    collapseInProgress = false;
                });

            } else {
                $('#' + array[endIndex].id).on('shown.bs.collapse.expandAll', function() {
                    $timeout(function() {
                        ctrl.showExpandAll = false;
                        collapseInProgress = false;
                    });

                    $('#' + array[endIndex].id).off('shown.bs.collapse.expandAll');
                });

                for (var j = 0; j <= endIndex; j++) {
                    if (!$('#' + array[j].id).hasClass('in')) {
                        $('#' + array[j].id + 'Control').click();
                    }
                }
            }
        };

        ctrl.addNewNonGEPolicy = function() {
            routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat('non-ge-policy-add'));
        };

        ctrl.initializeWindowSize = function() {
            ctrl.windowWidth = $window.innerWidth;
        };

        ctrl.detectBrowserResize = function() {
            ctrl.initializeWindowSize();
            return $(window).on('resize', renderChart);
        };

        ctrl.validateUrl = function(event, hyperLink) {
            if (hyperLink.indexOf('://') < 0) {
                event.preventDefault();
            }
        };

        ctrl.initializeWindowSize();

        if (ctrl.windowWidth >= 1024) {
            ctrl.isPieChartDrawn = true;
            pieChartFactory.getPieChartData(ctrl, $scope);
        } else if (ctrl.windowWidth < 1024) {
            ctrl.isPieChartDrawn = false;
        }

        ctrl.detectBrowserResize();

        function renderChart() {
            ctrl.initializeWindowSize();

            if (ctrl.windowWidth >= 1024 && ctrl.isPieChartDrawn === false) {

                pieChartFactory.getPieChartData(ctrl, $scope);
                $(window).off('resize', renderChart);

            }

            return $scope.$apply();
        }

        var cardStateChange = $rootScope.$on('updateCardState', function(e, state) {
            ctrl.updateCardState(state);
        });

        $('html').on('click.hidePolicyType', function() {
            $timeout(function() {
                ctrl.showPolicyTypes = false;
            });
        });

        $scope.$on('$destroy', function() {
            $(window).off('resize', renderChart);
            cardStateChange();
            $('html').off('click.hidePolicyType');
        });

        $('#policyTypesDdl').scroll(function() {
            if ($('#policyTypesDdl').scrollTop() + $('#policyTypesDdl').innerHeight() >= $('#policyTypesDdl')[0].scrollHeight) {
                $timeout(function() {
                    ctrl.endOfDdl = true;
                });
            } else {
                if (ctrl.endOfDdl) {
                    $timeout(function() {
                        ctrl.endOfDdl = false;
                    });
                }
            }
        });

        // _.forEach(ctrl.policyTypes, function(item) {
        //     if (item.apiPath && !item.policyList) {
        //         var success = function(response) {
        //             item.policyList = response.data || [];
        //             item.recordTotal = response['record_total'];
        //         };

        //         var error = function() {
        //             item.policyList = [];
        //         };

        //         var joint = item.apiPath.indexOf('?') < 0 ? '?' : '&';
        //         var url = item.apiPath + joint + 'size=' + item.size + '&page=' + item.page;

        //         $http({
        //             method: 'GET',
        //             url: url
        //         }).success(success).error(error);
        //     }
        // });
        // $timeout(function() {
        //     if (ctrl.policyTypes[0].apiPath && !ctrl.policyTypes[0].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[0]);
        //     }
        // }, 100);

        // $timeout(function() {
        //     if (ctrl.policyTypes[1].apiPath && !ctrl.policyTypes[1].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[1]);
        //     }
        // }, 300);

        // $timeout(function() {
        //     if (ctrl.policyTypes[2].apiPath && !ctrl.policyTypes[2].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[2]);
        //     }
        // }, 500);

        // $timeout(function() {
        //     if (ctrl.policyTypes[3].apiPath && !ctrl.policyTypes[3].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[3]);
        //     }
        // }, 700);

        // $timeout(function() {
        //     if (ctrl.policyTypes[4].apiPath && !ctrl.policyTypes[4].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[4]);
        //     }
        // }, 900);

        // $timeout(function() {
        //     if (ctrl.policyTypes[5].apiPath && !ctrl.policyTypes[5].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[5]);
        //     }
        // }, 800);

        // if (ctrl.policyTypes[6].apiPath && !ctrl.policyTypes[6].policyList) {
        //     dashboardService.getPolicyList(ctrl.policyTypes[6]);
        // }

        // $timeout(function() {
        //     if (ctrl.policyTypes[7].apiPath && !ctrl.policyTypes[7].policyList) {
        //         dashboardService.getPolicyList(ctrl.policyTypes[7]);
        //     }
        // }, 400);

        // if (ctrl.policyTypes[8].apiPath && !ctrl.policyTypes[8].policyList) {
        //     dashboardService.getPolicyList(ctrl.policyTypes[8]);
        // }

        // dashboardService.getPolicyList();

        var successCallBack = function(policy) {

            if (ctrl.policyTypes[6].apiPath && !ctrl.policyTypes[6].policyList) {
                dashboardService.getPolicyList(ctrl.policyTypes[6]);
            }

            if (ctrl.policyTypes[8].apiPath && !ctrl.policyTypes[8].policyList) {
                dashboardService.getPolicyList(ctrl.policyTypes[8]);
            }

            ctrl.detailList = policy.data;

            if (ctrl.detailList) {
                for (var m = 0; m < ctrl.detailList.length; m++) {
                    if (ctrl.detailList[m]['plan_category'] === 'LIFE_PROTECTION') {
                        ctrl.policyTypes[0].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[0].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'HEALTH_PROTECTION') {
                        ctrl.policyTypes[1].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[1].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'PERSONAL_ACCIDENT') {
                        ctrl.policyTypes[2].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[2].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'WEALTH_ACCUMULATION') {
                        ctrl.policyTypes[3].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[3].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'RETIREMENT') {
                        ctrl.policyTypes[4].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[4].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'LIFESTYLE_PROTECTION') {
                        ctrl.policyTypes[5].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[5].recordTotal = ctrl.detailList[m]['record_total'];
                    } else if (ctrl.detailList[m]['plan_category'] === 'OTHERS') {
                        ctrl.policyTypes[7].policyList = ctrl.detailList[m]['policy_detail_list'];
                        ctrl.policyTypes[7].recordTotal = ctrl.detailList[m]['record_total'];
                    }
                }
            }
        };

        var failedCallBack = function() {

        };

        $http({
            method: 'GET',
            url: APP_CONFIG.API_PATHS.DASHBOARD_SUMMARY + '?size=6&page=1'
        }).success(successCallBack).error(failedCallBack);
    }

    angular.module('dashboard').directive(directiveName, dashboard);

})();
