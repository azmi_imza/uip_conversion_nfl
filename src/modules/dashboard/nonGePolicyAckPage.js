(function() {
    'use strict';

    var directiveName = 'nonGePolicyAckPage';

    /*@ngInject*/
    function nonGePolicyAckPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nonGePolicyAckCtrl,
            templateUrl: 'templates/dashboard/nonGePolicyAck.html'
        };
    }

    /*@ngInject*/
    function nonGePolicyAckCtrl(loginFactory, authService, routeService, $stateParams) {
        var ctrl = this;

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = true;
        ctrl.acknowledgement.isSuccessAck = true;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if ($stateParams.response) {
            if ($stateParams.response.status === 'success') {
                ctrl.acknowledgement.title = 'cfo.label.success';
                ctrl.acknowledgement.isSuccessAck = true;

                if ($stateParams.response.message) {
                    ctrl.acknowledgement.details.push({
                        message: $stateParams.response.message
                    });
                }
            } else {
                ctrl.acknowledgement.title = 'cfo.label.failed';
                ctrl.acknowledgement.isSuccessAck = false;

                if ($stateParams.response.message) {
                    ctrl.acknowledgement.details.push({
                        message: $stateParams.response.message
                    });
                }
            }
        }

        ctrl.okBtnClick = function() {
            routeService.goToState('base.auth.dashboard-in');
        };
    }

    angular.module('login').directive(directiveName, nonGePolicyAckPage);
})();
