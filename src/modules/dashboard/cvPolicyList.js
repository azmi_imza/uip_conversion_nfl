/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'cvPolicyList';

    /*@ngInject*/
    function cvPolicyList() {
        return {
            scope: {
                cardData: '=',
                cardIndex: '=?',
                cardState: '=?',
                cardMeta: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: cvPolicyListCtrl,
            templateUrl: 'templates/dashboard/cvPolicyList.html'
        };
    }

    /*@ngInject*/

    function cvPolicyListCtrl($log, uiHelperService, $rootScope, routeService, storageService, APP_CONST, $timeout) {
        var ctrl = this;

        ctrl.updateCardState = function(state) {
            $rootScope.$emit('updateCardState', state);
            /*ctrl.cardState = state;
            uiHelperService.scrollTo($('#cardViewSection').offset().top);*/
        };

        ctrl.goToDetails = function(category, policy) {
            var userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
            var toState = (userType === 'IN') ? 'policy-details-in' : 'policy-details-be';
            var policyId = (category === 'nonGePolicies') ? policy.id : policy.number;

            routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(toState), {
                category: category,
                policyId: policyId
            });

        };

        ctrl.setProductWidth = function(productId, statusId) {
            $timeout(function() {
                var statusWidth = $('#' + statusId).width() + 5; //5px width offset
                $('#' + productId).css('width', 'calc(100% - ' + statusWidth + 'px)');
            });
        };
    }

    angular.module('dashboard').directive(directiveName, cvPolicyList);

})();
