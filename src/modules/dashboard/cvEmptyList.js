/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'cvEmptyList';

    /*@ngInject*/
    function cvEmptyList() {
        return {
            scope: {
                cardData: '=',
                cardIndex: '=?'
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: cvEmptyListCtrl,
            templateUrl: 'templates/dashboard/cvEmptyList.html'
        };
    }

    /*@ngInject*/

    function cvEmptyListCtrl($log) {
        var ctrl = this;
    }

    angular.module('dashboard').directive(directiveName, cvEmptyList);

})();