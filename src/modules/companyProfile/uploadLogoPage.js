(function() {
    'use strict';

    var directiveName = 'uploadLogoPage';

    /*@ngInject*/
    function uploadLogoPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: uploadLogoCtrl,
            templateUrl: 'templates/companyProfile/uploadLogo.html'
        };
    }

    /*@ngInject*/
    function uploadLogoCtrl(generalModalService) {
        var ctrl = this;

        ctrl.confirm = function() {
            generalModalService.closeModal();
        };

        ctrl.cancel = function() {
            generalModalService.dismissModal();
        };

        ctrl.back = function() {
            generalModalService.dismissModal();
        };
    }

    angular.module('companyProfile').directive(directiveName, uploadLogoPage);
})();
