/* eslint no-alert:0 */

(function() {
    'use strict';

    var directiveName = 'companyProfilePage';

    /*@ngInject*/
    function companyProfilePage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: companyProfileCtrl,
            templateUrl: 'templates/companyProfile/companyProfile.html'
        };
    }

    /*@ngInject*/
    function companyProfileCtrl($log, companyProfileFactory) {
        var ctrl = this;

        companyProfileFactory.getCompanyList(function(objectReturn) {
            ctrl.mainCompany = objectReturn.data;
            ctrl.companyDetails = objectReturn.data;

            if (ctrl.mainCompany.subsidiaries) {
                ctrl.copyArr = [];
                ctrl.copyArr.push(ctrl.mainCompany['cif_num']);

                for (var i = 0; i < ctrl.mainCompany.subsidiaries.length; i++) {
                    if (ctrl.mainCompany.subsidiaries[i].parent) {
                        if (ctrl.copyArr.indexOf(ctrl.mainCompany.subsidiaries[i].parent) < 0) {
                            ctrl.copyArr.push(ctrl.mainCompany.subsidiaries[i].parent);
                        }
                    }
                }
            }

            ctrl.updateCompanyDetails(ctrl.companyDetails);

            ctrl.loaded = true;
        });

        ctrl.updateCompanyDetails = function(currentCompany, $event) {
            ctrl.companyDetails = currentCompany;

            if ($event) {
                $('.highlight').removeClass('highlight');
                $($event.target).addClass('highlight');
            }
        };
    }

    angular.module('companyProfile').directive(directiveName, companyProfilePage);
})();
