(function() {
    'use strict';

    var directiveName = 'companyProfileAcknowledgement';

    /*@ngInject*/
    function companyProfileAcknowledgement() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: companyProfileAcknowledgementCtrl,
            templateUrl: 'templates/companyProfile/companyProfileAcknowledgement.html'
        };
    }

    /*@ngInject*/
    function companyProfileAcknowledgementCtrl($stateParams, $state, routeService) {
        var ctrl = this;
        var message = {};

        ctrl.stateParams = $stateParams.params;

        ctrl.contentDetails = [];

        if (ctrl.stateParams.upload === true) {
            message.message = 'cfo.message.fileUploadSuccess';
        } else if (ctrl.stateParams.remove === true) {
            message.message = 'cfo.message.fileDeleteSuccess';
        } else if (ctrl.stateParams.upload === false) {
            message.message = 'cfo.message.fileUploadFailed';
        } else if (ctrl.stateParams.remove === false) {
            message.message = 'cfo.message.fileDeleteFailed';
        }

        ctrl.contentDetails.push(message);

        ctrl.back = function() {
            routeService.goToPath('/company-profile');
        };
    }

    angular.module('companyProfile').directive(directiveName, companyProfileAcknowledgement);
})();
