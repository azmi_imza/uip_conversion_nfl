(function() {
    'use strict';

    var factoryName = 'companyProfileFactory';

    /*@ngInject*/
    var factory = function($log, $resource, APP_CONFIG) {

        var CompanyProfile = function() {

            this.getCompanyList = function(success, error) {
                $resource(APP_CONFIG.API_PATHS.COMPANY_PROFILE_COMPANY).get({}, success, error);
            };
        };

        return new CompanyProfile();
    };

    angular.module('companyProfile')
        .factory(factoryName, factory);
})();
