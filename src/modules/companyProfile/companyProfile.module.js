(function() {
    'use strict';
    angular.module('companyProfile', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                var companyProfileState = statePrefix.concat('company-profile');
                $stateProvider.state(companyProfileState, {
                    url: '/company-profile',
                    template: '<div company-profile-page></div>',
                    data: {
                        screenCode: '801025SCRNCOMPANYPROFILEBE'
                    }
                });
            });
})();
