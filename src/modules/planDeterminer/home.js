(function() {
    'use strict';

    var directiveName = 'homePage';

    /*@ngInject*/
    function homePage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: homeCtrl,
            templateUrl: 'templates/planDeterminer/home.html'
        };
    }

    /*@ngInject*/
    function homeCtrl(routeService) {
        var ctrl = this;

    }

    angular.module('home').directive(directiveName, homePage);
})();
