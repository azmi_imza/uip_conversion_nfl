(function() {
    'use strict';

    var directiveName = 'insurancePlan';

    /*@ngInject*/
    function planPage() {
        return {
            bindToController: true,
            controllerAs: 'ctrl',
            controller: planCtrl,
            templateUrl: 'templates/planDeterminer/insurancePlan.html'
        };
    };

    /*@ngInject*/
    function planCtrl($scope, $location, globalData, routeService) {
        var ctrl = this;

        $scope.getPlan ;    /// model for the plan 
        $scope.medicalInsurancePriceYen ;   /// Pricing based on age
        $scope.lifeInsurancePriceYen ;      /// Pricing based on age

        var localCustomer = globalData.customer() ;
        var age = localCustomer.age ;

        /// Determine the logic for pricing based on age
        if (age < 60)
        {
            $scope.medicalInsurancePriceYen = 5544 ;
            $scope.lifeInsurancePriceYen = 6552 ;
        }
        else if (age >= 60 && age < 65)
        {
            $scope.medicalInsurancePriceYen = 5808 ;
            $scope.lifeInsurancePriceYen = 6864 ;
        }
        else if (age >= 65) /// 65 years and above
        {
            $scope.medicalInsurancePriceYen = 6336 ;
            $scope.lifeInsurancePriceYen = 7488 ;
        }


        $scope.planSelected = function(planName){
            $scope.customerData = globalData.customer() ;
            $scope.customerData.insurancePlan = planName ;

            if(planName.toUpperCase() == 'Medical Insurance'.toUpperCase())
                $scope.customerData.insurancePlanPrice = $scope.medicalInsurancePriceYen ;
            else if(planName.toUpperCase() == 'Life Insurance'.toUpperCase())
                $scope.customerData.insurancePlanPrice = $scope.lifeInsurancePriceYen ;

            $scope.switchLocation('/quotation') ;

        }

        $scope.switchLocation = function(newPath){
            console.log(globalData.customer().insurancePlan) ;
            $location.path(newPath) ;
        }


        /// Std javascript commands below for compatibility with accordion
        var accordion = document.getElementsByClassName('plan_accordion_button');
        for(var i=0; i<accordion.length; i++){
            accordion[i].onclick = function (){
                this.classList.toggle("active") ;
                this.nextElementSibling.classList.toggle("unhide") ;
            }
        }
    };

    angular.module('home').directive(directiveName, planPage);
})();