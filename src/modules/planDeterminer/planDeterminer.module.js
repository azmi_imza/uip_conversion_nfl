(function() {
    'use strict';

    angular.module('home', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var COMMON_STATE_PREFIX = $injector.get('APP_CONST').COMMON_STATE_PREFIX;

                $stateProvider.state(COMMON_STATE_PREFIX.concat('home'), {
                    url: '/home',
                    template: '<div home-page></div>'
                });


                $stateProvider.state(COMMON_STATE_PREFIX.concat('plan'), {
                    url: '/plan',
                    template: '<div insurance-plan></div>'
                });
            });
})();
