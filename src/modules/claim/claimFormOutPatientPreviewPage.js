(function() {
    'use strict';

    var directiveName = 'claimFormOutPatientPreviewPage';

    /*@ngInject*/
    function claimFormOutPatientPreviewPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimFormOutPatientPreviewCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimFormOutPatientPreview.html'
        };
    }

    /*@ngInject*/
    function claimFormOutPatientPreviewCtrl($rootScope, FormManager, $stateParams, $log, storageService, APP_CONFIG, $timeout, $scope, $resource, routeService, $http, uiHelperService, _) {

        $log.debug('claimFormOutPatientCtrl');

        $log.debug('claimFormOutPatientCtrl: stateParam', $stateParams);
        var ctrl = this;

        ctrl.formManager = new FormManager('outPatientView', 'edit', ctrl);
        ctrl.formModel = {};

        // new form
        if ($stateParams && $stateParams.selectedObject) {
            var selectedObject = $stateParams.selectedObject;
            ctrl.layoutStateData = selectedObject;
            ctrl.formModel = selectedObject;
            ctrl.showSubmit = true;
        } else if ($stateParams && $stateParams.selectedDetail) {
            $log.debug($stateParams && $stateParams.selectedObject);
            ctrl.additionalInfo = {};
            ctrl.additionalInfo['form_data_source'] = $stateParams.selectedDetail.path;
            ctrl.showSubmit = false;
        }

        ctrl.backToEditPage = function() {
            for (var key in ctrl.formModel) {
                if (key.indexOf('-') > -1) {
                    delete ctrl.formModel[key];
                }
            }

            if ($stateParams.selectedDetail) {
                routeService.goToState($stateParams.fromState);
            } else {
                routeService.goToState('base.auth.claim-form-out-patient', {
                    'selectedObject': ctrl.formModel,
                    'fromState': $stateParams.fromState,
                    'noDraft': $stateParams.noDraft,
                    'initialState': $stateParams.initialState
                });
            }
        };

        ctrl.submitPage = function() {
            var success = function(response) {
                $log.debug('out patient submit success');
                uiHelperService.hideLoader();
                response.claimAction = 'new';
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            var error = function(response) {
                $log.debug('out patient submit fail');
                uiHelperService.hideLoader();
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    var url = APP_CONFIG.API_PATHS.SUBMIT_CLAIM.replace('{claimStatus}', 'new');

                    for (var key in ctrl.formModel) {
                        if (_.indexOf(key, '-') > -1) {
                            delete ctrl.formModel[key];
                        }
                    }

                    var formModel = angular.copy(ctrl.formModel);
                    formModel['created_dt'] = new Date().getTime();
                    delete formModel['claim_details_group'];

                    $http({
                        method: 'POST',
                        url: url,
                        data: formModel
                    }).success(success).error(error);
                },

                function(response) {
                    $log.debug('out patient form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );

        };

        /*************************************
         Methods to load layout
        *************************************/
        var aurl = '';

        if ($stateParams.selectedDetail) {
            aurl = 'data/layout/SCRNOUTPATIENT_VIEW_NEW.json';
        } else {
            aurl = 'data/layout/SCRNOUTPATIENT_VIEW.json';
        }

        loadUI(aurl);

        function loadUI(url) {
            uiHelperService.showLoader();
            $resource(url).get({}, loadSuccess, loadFailed);

            function loadSuccess(data) {
                uiHelperService.hideLoader();
                ctrl.layoutData = angular.fromJson(data);

                ctrl.initialized = true;
            }

            function loadFailed() {
                $log.debug('fail to load layout');
                uiHelperService.hideLoader();
                routeService.goToHome();
            }
        }

        var formModelInit = $rootScope.$on('initFormModel_outPatientForm', function() {
            if ($stateParams.fromOverview === true) {
                ctrl.formManager.setValue('is_allow_file_upload', 'N');
            }
        });

        $scope.$on('$destroy', function() {
            formModelInit();
        });
    }

    angular.module('claim').directive(directiveName, claimFormOutPatientPreviewPage);
})();
