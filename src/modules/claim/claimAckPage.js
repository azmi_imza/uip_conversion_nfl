(function() {
    'use strict';

    var directiveName = 'claimAckPage';

    /*@ngInject*/
    function claimAckPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimAckCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimAck.html'
        };
    }

    /*@ngInject*/
    function claimAckCtrl($log, routeService, $stateParams) {

        $log.debug('claimAckCtrl');
        var ctrl = this;

        ctrl.acknowledgement = {};
        ctrl.acknowledgement.isVisibleAck = false;
        ctrl.acknowledgement.isSuccessAck = false;
        ctrl.acknowledgement.title = '';
        ctrl.acknowledgement.details = [];

        if ($stateParams.params) {
            ctrl.claimAction = $stateParams.params.claimAction;
            ctrl.claimStatus = $stateParams.params.status;
            ctrl.acknowledgement.isVisibleAck = true;

            if ($stateParams.params.status === 'success') {
                // if ($stateParams.params.claimAction === 'new') {
                //     ctrl.acknowledgement.title = 'cfo.label.fail';
                // } else if ($stateParams.params.claimAction === 'delete') {
                //     ctrl.acknowledgement.title = 'cfo.label.fail';
                // } else {
                //     ctrl.acknowledgement.title = 'cfo.label.fail';
                // }
                ctrl.acknowledgement.title = 'cfo.label.success';
                ctrl.acknowledgement.isSuccessAck = true;
                if ($stateParams.params.message) {
                    ctrl.acknowledgement.details.push({
                        message: $stateParams.params.message
                    });
                }
            } else {
                ctrl.acknowledgement.title = 'cfo.label.failed';
                if ($stateParams.params.message) {
                    ctrl.acknowledgement.details.push({
                        message: $stateParams.params.message
                    });
                }
            }
        }

        ctrl.okBtnClick = function() {
            var state = $stateParams.initialState;

            routeService.goToState(state);
        };

    }

    angular.module('claim').directive(directiveName, claimAckPage);
})();
