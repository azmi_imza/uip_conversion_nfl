(function() {
    'use strict';

    /*@ngInject*/
    var claimFactory = function(routeService, $log, $resource, APP_CONFIG) {

        var Claim = function() {

            this.deleteClaim = function(policyId, success, error) {

                var url = APP_CONFIG.API_PATHS.DELETE_CLAIM.replace('{claimId}', policyId);
                $resource(url)['delete']({}, success, error);
            };
        };

        return new Claim();
    };

    angular.module('claim')
        .factory('claimFactory', claimFactory);
})();
