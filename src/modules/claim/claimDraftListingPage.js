(function() {
    'use strict';

    var directiveName = 'claimDraftListingPage';

    /*@ngInject*/
    function claimDraftListingPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimDraftListingCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimDraftListing.html'
        };
    }

    /*@ngInject*/
    function claimDraftListingCtrl($rootScope, $log, storageService, APP_CONST, $state, $timeout, $scope, $resource, routeService, uiHelperService) {

        $log.debug('claimListingCtrl');
        var ctrl = this;
        ctrl.userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
        uiHelperService.showLoader();

        $rootScope.$on('getTableData', function(event, recordTotal) {
            uiHelperService.hideLoader();
            if (recordTotal < 1) {
                ctrl.routeToEligible(true);
            }
        });

        ctrl.routeToEligible = function(noDraft) {
            if (ctrl.userType === 'IN') {
                routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat('claim-listing-in'), {
                    'fromState': 'base.auth.claim-in',
                    'noDraft': noDraft,
                    'initialState': 'base.auth.claim-in'
                });
            } else if (ctrl.userType === 'CA') {
                routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat('claim-listing-be'), {
                    'fromState': 'base.auth.claim-be',
                    'noDraft': noDraft,
                    'initialState': 'base.auth.claim-be'
                });
            }
        };

        /************************************
         Method triggered from directive
         ************************************/
        ctrl.onRowClick = function() {
            $timeout(function() {
                var path = 'claim/' + ctrl.selectedObject.id;

                if (ctrl.selectedObject && ctrl.selectedObject.id && ctrl.selectedObject['claim_type']) {
                    var state = '';
                    var fromState = '';
                    var initialState = '';

                    if (ctrl.selectedObject['claim_type'] === 'OUT_PATIENT') {
                        state = 'claim-form-out-patient';
                    } else if (ctrl.selectedObject['claim_type'] === 'IN_PATIENT') {
                        state = 'claim-form-in-patient';
                    } else if (ctrl.selectedObject['claim_type'] === 'DENTAL') {
                        state = 'claim-form-dental';
                    } else if (ctrl.selectedObject['claim_type'] === 'MATERNITY') {
                        state = 'claim-form-maternity';
                    }

                    if (ctrl.userType === 'IN') {
                        fromState = 'base.auth.claim-in';
                        initialState = 'base.auth.claim-in';
                    } else if (ctrl.userType === 'CA') {
                        fromState = 'base.auth.claim-be';
                        initialState = 'base.auth.claim-be';
                    }

                    routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(state), {
                        'selectedDetail': {
                            path: path,
                            id: ctrl.selectedObject.id
                        },
                        'fromState': fromState,
                        'initialState': initialState
                    });
                }

            });

        };

        /*************************************
         Methods to load layout
         *************************************/
        var url = '';

        if (ctrl.userType === 'IN') {
            url = 'data/layout/801038SCRNDRAFTLISTINGIN.json';
        } else if (ctrl.userType === 'CA') {
            url = 'data/layout/801038SCRNDRAFTLISTINGBE.json';
        }

        uiHelperService.showLoader();
        $resource(url).get({}, loadSuccess, loadFailed);

        function loadSuccess(data) {
            uiHelperService.hideLoader();
            ctrl.layoutData = angular.fromJson(data);
            ctrl.initialized = true;
        }

        function loadFailed() {
            $log.debug('fail to load layout');
            uiHelperService.hideLoader();
            routeService.goToHome();
        }

    }

    angular.module('claim').directive(directiveName, claimDraftListingPage);
})();
