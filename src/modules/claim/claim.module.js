(function() {
    'use strict';
    angular.module('claim', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(statePrefix.concat('claim-in'), {
                    url: '/claim-in',
                    template: '<div claim-draft-listing-page></div>',
                    data: {
                        screenCode: '801038SCRNDRAFTLISTINGIN'
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-be'), {
                    url: '/claim-be',
                    template: '<div claim-draft-listing-page></div>',
                    data: {
                        screenCode: '801038SCRNDRAFTLISTINGBE'
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-listing-in'), {
                    url: '/claim-eligible-in',
                    template: '<div claim-eligible-listing-page></div>',
                    data: {
                        screenCode: '801038SCRNELIGIBLECLAIMTYPEIN'
                    },
                    params: {
                        selectedObject: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-listing-be'), {
                    url: '/claim-eligible-be',
                    template: '<div claim-eligible-listing-page></div>',
                    data: {
                        screenCode: '801038SCRNELIGIBLECLAIMTYPEBE'
                    },
                    params: {
                        selectedObject: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-out-patient'), {
                    url: '/claim-form-out-patient',
                    template: '<div claim-form-out-patient-page></div>',
                    data: {
                        screenCode: 'SCRNOUTPATIENT'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-out-patient-preview'), {
                    url: '/claim-form-out-patient-preview',
                    template: '<div claim-form-out-patient-preview-page></div>',
                    data: {
                        screenCode: 'SCRNOUTPATIENT'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null,
                        fromOverview: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-maternity'), {
                    url: '/claim-form-maternity',
                    template: '<div claim-form-maternity-page></div>',
                    data: {
                        screenCode: 'SCRNMATERNITY'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-maternity-preview'), {
                    url: '/claim-form-maternity-preview',
                    template: '<div claim-form-maternity-preview-page></div>',
                    data: {
                        screenCode: 'SCRNMATERNITY'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null,
                        fromOverview: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-in-patient'), {
                    url: '/claim-form-in-patient',
                    template: '<div claim-form-in-patient-page></div>',
                    data: {
                        screenCode: 'SCRNINPATIENT'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-in-patient-preview'), {
                    url: '/claim-form-in-patient-preview',
                    template: '<div claim-form-in-patient-preview-page></div>',
                    data: {
                        screenCode: 'SCRNINPATIENT'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null,
                        fromOverview: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-dental'), {
                    url: '/claim-form-dental',
                    template: '<div claim-form-dental-page></div>',
                    data: {
                        screenCode: 'SCRNDENTAL'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-form-dental-preview'), {
                    url: '/claim-form-dental-preview',
                    template: '<div claim-form-dental-preview-page></div>',
                    data: {
                        screenCode: 'SCRNDENTAL'
                    },
                    params: {
                        selectedObject: null,
                        selectedDetail: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null,
                        fromOverview: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-overview-in'), {
                    url: '/claim-overview',
                    template: '<div claim-overview-page></div>',
                    data: {
                        screenCode: 'SCRNOVERVIEWLISTINGIN'
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-overview-be'), {
                    url: '/claim-overview',
                    template: '<div claim-overview-page></div>',
                    data: {
                        screenCode: 'SCRNOVERVIEWLISTINGBE'
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-deletion'), {
                    url: '/claim-deletion',
                    template: '<div claim-deletion-page></div>',
                    data: {
                        screenCode: 'SCRNCLAIMDELETION'
                    },
                    params: {
                        selectedObject: null,
                        state: null,
                        fromState: null,
                        noDraft: null,
                        initialState: null
                    }
                });

                $stateProvider.state(statePrefix.concat('claim-ack'), {
                    url: '/claim-ack',
                    template: '<div claim-ack-page></div>',
                    data: {
                        screenCode: 'SCRNACK'
                    },
                    params: {
                        params: null,
                        initialState: null
                    }
                });
            });
})();
