(function() {
    'use strict';

    var directiveName = 'claimFormInPatientPage';

    /*@ngInject*/
    function claimFormInPatientPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimFormInPatientCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimFormInPatient.html'
        };
    }

    /*@ngInject*/
    function claimFormInPatientCtrl(claimFactory, APP_CONFIG, $http, uiHelperService, FormManager, routeService, $stateParams, $log, storageService, APP_CONST, $timeout, $scope, $resource, $rootScope) {

        $log.debug('claimFormInPatientCtrl');

        $log.debug('claimFormInPatientCtrl: stateParam', $stateParams);
        var ctrl = this;

        ctrl.formManager = new FormManager('inPatientForm', 'edit', ctrl);
        ctrl.formModel = {};

        // new form
        if ($stateParams && $stateParams.selectedObject) {
            var selectedObject = $stateParams.selectedObject;
            ctrl.layoutStateData = selectedObject;
            ctrl.formModel = selectedObject;
            ctrl.formModel.claimStatus = ctrl.formModel.claimStatus ? ctrl.formModel.claimStatus : 'new';
        } else if ($stateParams && $stateParams.selectedDetail) {
            $log.debug($stateParams && $stateParams.selectedObject);
            ctrl.additionalInfo = {};
            ctrl.additionalInfo['form_data_source'] = $stateParams.selectedDetail.path;
            ctrl.formModel.claimStatus = ctrl.formModel.claimStatus ? ctrl.formModel.claimStatus : 'draft';
            ctrl.formModel.claimRefId = ctrl.formModel.claimRefId ? ctrl.formModel.claimRefId : $stateParams.selectedDetail.id;
        }

        ctrl.goToPreview = function() {

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    $scope.$broadcast('fileupload_update_attached_file');
                    routeService.goToState('base.auth.claim-form-in-patient-preview', {
                        'selectedObject': ctrl.formModel,
                        'fromState': $stateParams.fromState,
                        'noDraft': $stateParams.noDraft,
                        'initialState': $stateParams.initialState
                    });
                },

                function(response) {
                    $log.debug('in patient form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.saveAsDraft = function() {

            var success = function(response) {
                $log.debug('in patient submit success');
                uiHelperService.hideLoader();
                response.claimAction = 'draft';
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            var error = function(response) {
                $log.debug('in patient submit fail');
                uiHelperService.hideLoader();
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    $scope.$broadcast('fileupload_update_attached_file');
                    uiHelperService.showLoader();
                    var url = '';
                    var method = '';
                    if (ctrl.formModel.claimStatus === 'new') {
                        url = APP_CONFIG.API_PATHS.SUBMIT_CLAIM.replace('{claimStatus}', 'draft');
                        method = 'POST';
                    } else {
                        url = APP_CONFIG.API_PATHS.UPDATE_CLAIM.replace('{claimId}', ctrl.formModel.claimRefId);
                        method = 'PUT';
                    }

                    ctrl.formModel['claimant_name'] = ctrl.formModel['claimant_cif_num_label'];

                    ctrl.formManager.mapFields(ctrl.layoutData.components[0].destMappings, ctrl.formModel);

                    $http({
                        method: method,
                        url: url,
                        data: ctrl.formModel
                    }).success(success).error(error);
                },

                function(response) {
                    $log.debug('maternity form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );
        };

        ctrl.backBtnClick = function() {
            var state = '';

            if ($stateParams.fromState) {
                state = $stateParams.fromState;
            }

            routeService.goToState(state, {
                'noDraft': $stateParams.noDraft,
                'initialState': $stateParams.initialState
            });
        };

        ctrl.deleteBtnClick = function() {
            routeService.goToState('base.auth.claim-deletion', {
                'selectedObject': ctrl.formModel,
                'state': 'base.auth.claim-form-in-patient',
                'fromState': $stateParams.fromState,
                'noDraft': $stateParams.noDraft,
                'initialState': $stateParams.initialState
            });
        };

        /*************************************
         Methods to load layout
        *************************************/

        var aurl = 'data/layout/SCRNINPATIENT.json';
        loadUI(aurl);

        function loadUI(url) {
            uiHelperService.showLoader();
            $resource(url).get({}, loadSuccess, loadFailed);

            function loadSuccess(data) {
                ctrl.layoutData = angular.fromJson(data);

                ctrl.initialized = true;
                uiHelperService.hideLoader();
            }

            function loadFailed() {
                uiHelperService.hideLoader();
                $log.debug('fail to load layout');
                routeService.goToHome();
            }
        }

        var formModelInit = $rootScope.$on('initFormModel_inPatientForm', function() {});

        var bankAccNumChange = $rootScope.$on('valueChange_bank_acc_num', function() {
            if (ctrl.formManager.getValue('bank_acc_num')) {
                //todo: get branch, set branch
                var success = function(response) {
                    if (response.data.items && response.data.items.length === 1) {
                        ctrl.formManager.setValue('bank_branch', response.data.items[0]['branch_id']);
                        ctrl.formManager.setValue('bank_branch_label', response.data.items[0]['branch_name']);
                        $rootScope.$emit('setDisable_bank_branch', true);
                        $rootScope.$emit('hideDropDown_bank_branch');
                    }
                };

                var fail = function() {
                    $log.error('Failed to request bank branch');
                };

                var branchUrl = APP_CONFIG.API_PATHS.CLAIM_BANK_BRANCH
                    .replace('{bank_id}', ctrl.formManager.getValue('bank_id'))
                    .replace('{bank_acc_num}', ctrl.formManager.getValue('bank_acc_num'));

                $http({
                    method: 'GET',
                    url: branchUrl
                }).success(success).error(fail);

            }
        });

        var bankNameChange = $rootScope.$on('valueChange_bank_id', function(e, isDirty) {
            if (isDirty) {
                ctrl.formManager.setValue('bank_branch', '');
                ctrl.formManager.setValue('bank_acc_num', '');
                $rootScope.$emit('setDisable_bank_branch', false);
            }
        });

        $scope.$on('$destroy', function() {
            formModelInit();
            bankAccNumChange();
            bankNameChange();
        });

    }

    angular.module('claim').directive(directiveName, claimFormInPatientPage);
})();
