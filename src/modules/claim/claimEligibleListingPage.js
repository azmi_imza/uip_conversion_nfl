(function() {
    'use strict';

    var directiveName = 'claimEligibleListingPage';

    /*@ngInject*/
    function claimEligibleListingPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimEligibleListingCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimEligibleListing.html'
        };
    }

    /*@ngInject*/
    function claimEligibleListingCtrl($stateParams, $log, storageService, APP_CONST, $state, $timeout, $scope, $resource, routeService, uiHelperService) {

        $log.debug('claimEligibleListingCtrl');
        var ctrl = this;

        ctrl.noDraft = $stateParams.noDraft;

        ctrl.backBtnClick = function() {

            var state = '';

            if ($stateParams.initialState) {
                state = $stateParams.initialState;
            }

            routeService.goToState(state);
        };

        /************************************
         Method triggered from directive
         ************************************/
        ctrl.onRowSelect = function() {

            $timeout(function() {
                var state = '';
                var fromState = '';

                //Object {label: "cfo.label.maternity", value: "MATERNITY", key: "claim_type"}
                ctrl.selectedObject['claim_type'] = ctrl.selectedValue.value;
                ctrl.selectedObject['claim_type_label'] = ctrl.selectedValue.label;

                if (ctrl.selectedValue.value === 'MATERNITY') {
                    state = 'claim-form-maternity';
                } else if (ctrl.selectedValue.value === 'IN_PATIENT') {
                    state = 'claim-form-in-patient';
                } else if (ctrl.selectedValue.value === 'DENTAL') {
                    state = 'claim-form-dental';
                } else if (ctrl.selectedValue.value === 'OUT_PATIENT') {
                    state = 'claim-form-out-patient';
                }

                if (ctrl.userType === 'IN') {
                    fromState = 'base.auth.claim-listing-in';
                } else if (ctrl.userType === 'CA') {
                    fromState = 'base.auth.claim-listing-be';
                }

                routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(state), {
                    'selectedObject': ctrl.selectedObject,
                    'fromState': fromState,
                    'noDraft': $stateParams.noDraft,
                    'initialState': $stateParams.initialState
                });
            });
        };

        /*************************************
         Methods to load layout
         *************************************/
        ctrl.userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
        var url = '';

        if (ctrl.userType === 'IN') {
            url = 'data/layout/801038SCRNELIGIBLECLAIMTYPEIN.json';
        } else if (ctrl.userType === 'CA') {
            url = 'data/layout/801038SCRNELIGIBLECLAIMTYPEBE.json';
        }

        uiHelperService.showLoader();
        $resource(url).get({}, loadSuccess, loadFailed);

        function loadSuccess(data) {
            uiHelperService.hideLoader();
            ctrl.layoutData = angular.fromJson(data);
            ctrl.initialized = true;
        }

        function loadFailed() {
            uiHelperService.hideLoader();
            $log.debug('fail to load layout');
            routeService.goToHome();
        }

    }

    angular.module('claim').directive(directiveName, claimEligibleListingPage);
})();
