(function() {
    'use strict';

    var directiveName = 'claimFormOutPatientPage';

    /*@ngInject*/
    function claimFormOutPatientPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimFormOutPatientCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimFormOutPatient.html'
        };
    }

    /*@ngInject*/
    function claimFormOutPatientCtrl(claimFactory, $stateParams, $log, storageService, APP_CONFIG, APP_CONST, $timeout, $http,
        $scope, $resource, routeService, FormManager, $rootScope, uiHelperService, _) {

        $log.debug('claimFormOutPatientCtrl');

        $log.debug('claimFormOutPatientCtrl: stateParam', $stateParams);
        var ctrl = this;

        ctrl.formManager = new FormManager('outPatientForm', 'edit', ctrl);
        ctrl.formModel = {};

        // new form
        if ($stateParams && $stateParams.selectedObject) {
            var selectedObject = $stateParams.selectedObject;
            ctrl.layoutStateData = selectedObject;
            ctrl.formModel = selectedObject;
            ctrl.formModel.claimStatus = ctrl.formModel.claimStatus ? ctrl.formModel.claimStatus : 'new';
        } else if ($stateParams && $stateParams.selectedDetail) {
            $log.debug($stateParams && $stateParams.selectedObject);
            ctrl.additionalInfo = {};
            ctrl.additionalInfo['form_data_source'] = $stateParams.selectedDetail.path;
            ctrl.formModel.claimStatus = ctrl.formModel.claimStatus ? ctrl.formModel.claimStatus : 'draft';
            ctrl.formModel.claimRefId = ctrl.formModel.claimRefId ? ctrl.formModel.claimRefId : $stateParams.selectedDetail.id;
        }

        if (!ctrl.formModel['out_patient_details']) {
            ctrl.formModel['out_patient_details'] = [{}];
        }

        function rebuildModel() {
            if (ctrl.formManager.getValue('claim_details_group')) {
                var claimDetailsGroup = ctrl.formManager.getValue('claim_details_group');
                var claimDetails = [];
                for (var groupIdx = 0; groupIdx < claimDetailsGroup.length - 1; groupIdx++) {
                    var detailsObj = {};
                    for (var rowIdx = 0; rowIdx < claimDetailsGroup[groupIdx].rows.length; rowIdx++) {
                        for (var colIdx = 0; colIdx < claimDetailsGroup[groupIdx].rows[rowIdx].columns.length; colIdx++) {
                            if (claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].type !== 'formAction') {
                                var key = claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name.split('-')[0];
                                detailsObj[key] = ctrl.formManager.getValue(claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name);
                                detailsObj[key + '_label'] = ctrl.formManager.getValue(claimDetailsGroup[groupIdx].rows[rowIdx].columns[colIdx].name + '_label');
                            }
                        }
                    }

                    detailsObj['claimant_name'] = detailsObj['claimant_cif_num_label'];
                    if (ctrl.formModel['out_patient_details'][groupIdx] && ctrl.formModel['out_patient_details'][groupIdx]['claim_ref_num']) {
                        detailsObj['claim_ref_num'] = ctrl.formModel['out_patient_details'][groupIdx]['claim_ref_num'];
                        detailsObj.id = ctrl.formModel['out_patient_details'][groupIdx].id;
                    }

                    claimDetails.push(detailsObj);
                }

                ctrl.formManager.setValue('out_patient_details', claimDetails);
            }
        }

        ctrl.goToPreview = function() {

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    $scope.$broadcast('fileupload_update_attached_file');

                    rebuildModel();
                    routeService.goToState('base.auth.claim-form-out-patient-preview', {
                        'selectedObject': ctrl.formModel,
                        'fromState': $stateParams.fromState,
                        'noDraft': $stateParams.noDraft,
                        'initialState': $stateParams.initialState
                    });
                },

                function(response) {
                    $log.debug('out patient form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );

        };

        ctrl.saveAsDraft = function() {

            var success = function(response) {
                $log.debug('out patient submit success');
                uiHelperService.hideLoader();
                response.claimAction = 'draft';
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            var error = function(response) {
                $log.debug('out patient submit fail');
                uiHelperService.hideLoader();
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    $scope.$broadcast('fileupload_update_attached_file');
                    rebuildModel();
                    var url = '';
                    var method = '';
                    if (ctrl.formModel.claimStatus === 'new') {
                        url = APP_CONFIG.API_PATHS.SUBMIT_CLAIM.replace('{claimStatus}', 'draft');
                        method = 'POST';
                    } else {
                        url = APP_CONFIG.API_PATHS.UPDATE_CLAIM.replace('{claimId}', ctrl.formModel.claimRefId);
                        method = 'PUT';
                    }

                    for (var key in ctrl.formModel) {
                        if (_.indexOf(key, '-') > -1) {
                            delete ctrl.formModel[key];
                        }
                    }

                    var formModel = angular.copy(ctrl.formModel);
                    formModel['created_dt'] = new Date().getTime();
                    delete formModel['claim_details_group'];

                    $http({
                        method: method,
                        url: url,
                        data: formModel
                    }).success(success).error(error);
                },

                function(response) {
                    $log.debug('out patient form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );

        };

        ctrl.backBtnClick = function() {
            var state = '';

            if ($stateParams.fromState) {
                state = $stateParams.fromState;
            }

            routeService.goToState(state, {
                'noDraft': $stateParams.noDraft,
                'initialState': $stateParams.initialState
            });
        };

        ctrl.deleteBtnClick = function() {
            routeService.goToState('base.auth.claim-deletion', {
                'selectedObject': ctrl.formModel,
                'state': 'base.auth.claim-form-out-patient',
                'fromState': $stateParams.fromState,
                'noDraft': $stateParams.noDraft,
                'initialState': $stateParams.initialState
            });
        };

        /*************************************
         Methods to load layout
        *************************************/

        var url = 'data/layout/SCRNOUTPATIENT.json';

        uiHelperService.showLoader();
        $resource(url).get({}, loadSuccess, loadFailed);

        function loadSuccess(data) {
            ctrl.layoutData = angular.fromJson(data);

            ctrl.initialized = true;
            uiHelperService.hideLoader();
        }

        function loadFailed() {
            uiHelperService.hideLoader();
            $log.debug('fail to load layout');
            routeService.goToHome();
        }

        var formModelInit = $rootScope.$on('initFormModel_outPatientForm', function() {});

        var bankAccNumChange = $rootScope.$on('valueChange_bank_acc_num', function() {
            if (ctrl.formManager.getValue('bank_acc_num')) {
                //todo: get branch, set branch
                var success = function(response) {
                    if (response.data.items && response.data.items.length === 1) {
                        ctrl.formManager.setValue('bank_branch', response.data.items[0]['branch_id']);
                        ctrl.formManager.setValue('bank_branch_label', response.data.items[0]['branch_name']);
                        $rootScope.$emit('setDisable_bank_branch', true);
                        $rootScope.$emit('hideDropDown_bank_branch');
                    }
                };

                var fail = function() {
                    $log.error('Failed to request bank branch');
                };

                var branchUrl = APP_CONFIG.API_PATHS.CLAIM_BANK_BRANCH
                    .replace('{bank_id}', ctrl.formManager.getValue('bank_id'))
                    .replace('{bank_acc_num}', ctrl.formManager.getValue('bank_acc_num'));

                $http({
                    method: 'GET',
                    url: branchUrl
                }).success(success).error(fail);

            }
        });

        var bankNameChange = $rootScope.$on('valueChange_bank_id', function(e, isDirty) {
            if (isDirty) {
                ctrl.formManager.setValue('bank_branch', '');
                ctrl.formManager.setValue('bank_acc_num', '');
                $rootScope.$emit('setDisable_bank_branch', false);
            }
        });

        $scope.$on('$destroy', function() {
            formModelInit();
            bankAccNumChange();
            bankNameChange();
        });

    }

    angular.module('claim').directive(directiveName, claimFormOutPatientPage);
})();
