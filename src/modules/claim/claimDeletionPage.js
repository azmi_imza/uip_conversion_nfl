(function() {
    'use strict';

    var directiveName = 'claimDeletionPage';

    /*@ngInject*/
    function claimDeletionPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimDeletionCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimDeletion.html'
        };
    }

    /*@ngInject*/
    function claimDeletionCtrl($log, routeService, $stateParams, claimFactory) {

        $log.debug('claimDeletionCtrl');
        var ctrl = this;

        ctrl.yesBtnClick = function() {
            var success = function(response) {
                response.claimAction = 'delete';
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            var error = function() {};

            var claimId = $stateParams.selectedObject['claim_id'];
            claimFactory.deleteClaim(claimId, success, error);

        };

        ctrl.noBtnClick = function() {
            routeService.goToState($stateParams.state, {
                'selectedObject': $stateParams.selectedObject,
                'fromState': $stateParams.fromState,
                'initialState': $stateParams.initialState
            });
        };
    }

    angular.module('claim').directive(directiveName, claimDeletionPage);
})();
