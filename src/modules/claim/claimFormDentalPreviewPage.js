(function() {
    'use strict';

    var directiveName = 'claimFormDentalPreviewPage';

    /*@ngInject*/
    function claimFormDentalPreviewPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimFormDentalPreviewCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimFormDentalPreview.html'
        };
    }

    /*@ngInject*/
    function claimFormDentalPreviewCtrl($rootScope, FormManager, $stateParams, $log, storageService, APP_CONFIG, $timeout, $scope, $resource, routeService, $http, uiHelperService) {

        $log.debug('claimFormDentalCtrl');

        $log.debug('claimFormDentalCtrl: stateParam', $stateParams);
        var ctrl = this;

        ctrl.formManager = new FormManager('dentalView', 'edit', ctrl);
        ctrl.formModel = {};

        // new form
        if ($stateParams && $stateParams.selectedObject) {
            var selectedObject = $stateParams.selectedObject;
            ctrl.layoutStateData = selectedObject;
            ctrl.formModel = selectedObject;
            ctrl.showSubmit = true;
        } else if ($stateParams && $stateParams.selectedDetail) {
            $log.debug($stateParams && $stateParams.selectedObject);
            ctrl.additionalInfo = {};
            ctrl.additionalInfo['form_data_source'] = $stateParams.selectedDetail.path;
            ctrl.showSubmit = false;
        }

        ctrl.backToEditPage = function() {
            if ($stateParams.selectedDetail) {
                routeService.goToState($stateParams.fromState);
            } else {
                routeService.goToState('base.auth.claim-form-dental', {
                    'selectedObject': ctrl.formModel,
                    'fromState': $stateParams.fromState,
                    'noDraft': $stateParams.noDraft,
                    'initialState': $stateParams.initialState
                });
            }
        };

        ctrl.submitPage = function() {
            var success = function(response) {
                $log.debug('dental submit success');
                uiHelperService.hideLoader();
                response.claimAction = 'new';
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            var error = function(response) {
                $log.debug('dental submit fail');
                uiHelperService.hideLoader();
                routeService.goToState('base.auth.claim-ack', {
                    params: response,
                    'initialState': $stateParams.initialState
                });
            };

            ctrl.formManager.submit(ctrl.formModel).then(function() {
                    uiHelperService.showLoader();
                    var url = APP_CONFIG.API_PATHS.SUBMIT_CLAIM.replace('{claimStatus}', 'new');
                    ctrl.formModel['claimant_name'] = ctrl.formModel['claimant_cif_num_label'];
                    $http({
                        method: 'POST',
                        url: url,
                        data: ctrl.formModel
                    }).success(success).error(error);
                },

                function(response) {
                    $log.debug('dental form validation fail: ' + response.inputName);
                    uiHelperService.scrollTo($('#' + response.inputName).offset().top);
                }

            );

        };

        /*************************************
         Methods to load layout
        *************************************/

        var aurl = 'data/layout/SCRNDENTAL_VIEW.json';
        loadUI(aurl);

        function loadUI(url) {
            $resource(url).get({}, loadSuccess, loadFailed);

            function loadSuccess(data) {

                ctrl.layoutData = angular.fromJson(data);

                ctrl.initialized = true;
            }

            function loadFailed() {
                $log.debug('fail to load layout');
                routeService.goToHome();
            }
        }

        var formModelInit = $rootScope.$on('initFormModel_dentalForm', function() {
            if ($stateParams.fromOverview === true) {
                ctrl.formManager.setValue('is_allow_file_upload', 'N');
            }
        });

        $scope.$on('$destroy', function() {
            formModelInit();
        });
    }

    angular.module('claim').directive(directiveName, claimFormDentalPreviewPage);
})();
