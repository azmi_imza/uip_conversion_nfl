(function() {
    'use strict';

    var directiveName = 'claimOverviewPage';

    /*@ngInject*/
    function claimOverviewPage() {
        return {
            scope: {},
            bindToController: true,
            controller: claimOverviewCtrl,
            controllerAs: 'ctrl',
            templateUrl: 'templates/claim/claimOverview.html'
        };
    }

    /*@ngInject*/
    function claimOverviewCtrl(uiHelperService, APP_CONFIG, componentsModalService, $http, $log, storageService, APP_CONST, $state, $timeout, $scope, $resource, routeService) {

        $log.debug('claimOverviewCtrl');
        var ctrl = this;
        ctrl.bubbleState = 'DRAFT';
        ctrl.dataArray = {};
        ctrl.claimGroupCount = {};

        /*************************************
         Methods to load layout
         *************************************/

        ctrl.userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE);
        var url = '';

        if (ctrl.userType === 'IN') {
            url = 'data/layout/SCRNOVERVIEWLISTINGIN.json';
        } else if (ctrl.userType === 'CA') {
            url = 'data/layout/SCRNOVERVIEWLISTINGBE.json';
        }

        $resource(url).get({}, loadSuccess, loadFailed);

        function loadSuccess(data) {
            ctrl.layoutData = angular.fromJson(data);
            ctrl.initialized = true;
        }

        function loadFailed() {
            $log.debug('fail to load layout');
            routeService.goToHome();
        }

        /*************************************
         Claim Status Bubble
         *************************************/

        ctrl.iconActive = [{
            iconImage: 'images/claim_draft_icon.png',
            text: 'cfo.label.draft',
            id: 'DRAFT',
            dataSize: 20
        }, {
            iconImage: 'images/claim_pending_icon.png',
            text: 'cfo.label.pending',
            id: 'PENDING',
            dataSize: 20
        }, {
            iconImage: 'images/claim_approved_icon.png',
            text: 'cfo.label.approved',
            id: 'APPROVED',
            dataSize: 20
        }, {
            iconImage: 'images/claim_rejected_icon.png',
            text: 'cfo.label.rejected',
            id: 'REJECTED',
            dataSize: 20
        }];

        var idx = 0;
        var claimStatusCode;
        var statusClaimUrl;

        function getClaimGroup() {
            claimStatusCode = ctrl.iconActive[idx].id;
            statusClaimUrl = APP_CONFIG.API_PATHS.STATUS_CLAIM.replace('{statusCode}', claimStatusCode);
            statusClaimUrl = statusClaimUrl.replace('{size}', ctrl.iconActive[idx].dataSize);

            uiHelperService.showLoader();

            $http({
                method: 'GET',
                url: statusClaimUrl
            }).success(function(response) {
                loadDataSuccess(response, claimStatusCode);
            }).error(function() {

            })

            ['finally'](function() {
                if (idx < ctrl.iconActive.length) {
                    getClaimGroup();
                    idx++;
                } else {
                    uiHelperService.hideLoader();
                }
            });
        }

        function loadDataSuccess(response, statusCode) {
            ctrl.dataArray[statusCode] = response.data;
            ctrl.claimGroupCount[statusCode] = response['record_total'];
        }

        ctrl.loadMore = function(state) {

            var dataSize;
            for (var i = 0; i < ctrl.iconActive.length; i++) {
                if (state === ctrl.iconActive[i].id) {
                    ctrl.iconActive[i].dataSize += 20;
                    dataSize = ctrl.iconActive[i].dataSize;
                }
            }

            statusClaimUrl = APP_CONFIG.API_PATHS.STATUS_CLAIM.replace('{statusCode}', state);
            statusClaimUrl = statusClaimUrl.replace('{size}', dataSize);

            $http({
                method: 'GET',
                url: statusClaimUrl
            }).success(function(response) {
                loadDataSuccess(response, state);
            }).error(function() {

            });
        };

        ctrl.setBubbleIconStyle = function(index) {
            if (index === 0 || index === 1 || index === 2 || index === 3) {
                return {
                    background: '#eeeeee url(' + ctrl.iconActive[index].iconImage + ') center center no-repeat'
                };
            }
        };

        ctrl.iconClickEvent = function(id) {
            ctrl.bubbleState = id;
        };

        /*************************************
         E-Statement
         *************************************/

        ctrl.openStatement = function(link) {

            // TODO: Link to pdf
            $http.get(link, {
                responseType: 'blob'
            }).success(function() {

                var template = '<file-preview-modal></file-preview-modal>';
                var backdrop = 'static';
                var size = 'lg';

                componentsModalService.modalStorage.fileData = 'https://cors-anywhere.herokuapp.com/http://www.cbu.edu.zm/downloads/pdf-sample.pdf';
                componentsModalService.modalStorage.ext = 'PDF';
                componentsModalService.openModalWithTemplate(template, backdrop, size);
            });
        };

        ctrl.routeToEligible = function() {
            if (ctrl.userType === 'IN') {
                routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat('claim-listing-in'), {
                    'fromState': 'base.auth.claim-overview-in',
                    'noDraft': false,
                    'initialState': 'base.auth.claim-overview-in'
                });
            } else if (ctrl.userType === 'CA') {
                routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat('claim-listing-be'), {
                    'fromState': 'base.auth.claim-overview-be',
                    'noDraft': false,
                    'initialState': 'base.auth.claim-overview-be'
                });
            }
        };

        ctrl.onRowClick = function() {

            $timeout(function() {
                var path = 'claim/' + ctrl.selectedObject.id;
                var state = '';
                var fromState = '';
                var initialState = '';

                if (ctrl.selectedObject['claim_status'] === 'Draft') {
                    if (ctrl.selectedObject && ctrl.selectedObject.id && ctrl.selectedObject['claim_type']) {
                        if (ctrl.selectedObject['claim_type'] === 'OUT_PATIENT') {
                            state = 'claim-form-out-patient';
                        } else if (ctrl.selectedObject['claim_type'] === 'IN_PATIENT') {
                            state = 'claim-form-in-patient';
                        } else if (ctrl.selectedObject['claim_type'] === 'DENTAL') {
                            state = 'claim-form-dental';
                        } else if (ctrl.selectedObject['claim_type'] === 'MATERNITY') {
                            state = 'claim-form-maternity';
                        }

                        if (ctrl.userType === 'IN') {
                            fromState = 'base.auth.claim-overview-in';
                            initialState = 'base.auth.claim-overview-in';
                        } else if (ctrl.userType === 'CA') {
                            fromState = 'base.auth.claim-overview-be';
                            initialState = 'base.auth.claim-overview-be';
                        }

                        routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(state), {
                            'selectedDetail': {
                                path: path,
                                id: ctrl.selectedObject.id
                            },
                            'fromState': fromState,
                            'initialState': initialState
                        });
                    }

                } else {
                    if (ctrl.selectedObject && ctrl.selectedObject.id && ctrl.selectedObject['claim_type']) {
                        if (ctrl.selectedObject['claim_type'] === 'OUT_PATIENT') {
                            state = 'claim-form-out-patient-preview';
                        } else if (ctrl.selectedObject['claim_type'] === 'IN_PATIENT') {
                            state = 'claim-form-in-patient-preview';
                        } else if (ctrl.selectedObject['claim_type'] === 'DENTAL') {
                            state = 'claim-form-dental-preview';
                        } else if (ctrl.selectedObject['claim_type'] === 'MATERNITY') {
                            state = 'claim-form-maternity-preview';
                        }

                        if (ctrl.userType === 'IN') {
                            fromState = 'base.auth.claim-overview-in';
                        } else if (ctrl.userType === 'CA') {
                            fromState = 'base.auth.claim-overview-be';
                        }

                        routeService.goToState(APP_CONST.AUTH_STATE_PREFIX.concat(state), {
                            'selectedDetail': {
                                path: path,
                                id: ctrl.selectedObject.id
                            },
                            'fromState': fromState,
                            'fromOverview': true
                        });
                    }
                }

            });
        };

        getClaimGroup();
    }

    angular.module('claim').directive(directiveName, claimOverviewPage);
})();
