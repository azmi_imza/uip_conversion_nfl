(function() {
    'use strict';

    var directiveName = 'policyDetailsPage';

    /*@ngInject*/
    function policyDetailsPage() {
        return {
            scope: {},
            bindToController: true,
            controllerAs: 'ctrl',
            controller: policyDetailsPageCtrl,
            templateUrl: 'templates/policyDetails/policyDetails.html'
        };
    }

    /*@ngInject*/
    function policyDetailsPageCtrl(uiHelperService, $filter, _, $scope, $http, APP_CONFIG, PolicyDetailsFactory, $rootScope, $stateParams, $timeout, $log) {

        $log.debug('Loaded Policy Details page');

        var ctrl = this;
        $log.debug('policyDetails page');
        ctrl.endOfDdl = false;
        var policySummary = [];
        ctrl.proceed = false;

        ctrl.policyTypes = [

            // This section of code is commented because for now, we do not need to list out all the policies
            // {
            //     id: 'allPolicies',
            //     icon: {
            //         active: '',
            //         inactive: ''
            //     },
            //     header: 'cfo.label.allPolicies',
            //     apiPath: APP_CONFIG.API_PATHS.POLICY + '?policyStatus=INFORCED&size=-1'
            // },
            {
                id: 'lifeProtection',
                icon: {
                    active: 'images/ic-lp.png',
                    inactive: 'images/ic-lp-d.png'
                },
                header: 'cfo.label.lifeProtection',
                apiPath: APP_CONFIG.API_PATHS.PLAN_LIFE
            }, {
                id: 'healthProtection',
                icon: {
                    active: 'images/ic-hp.png',
                    inactive: 'images/ic-hp-d.png'
                },
                header: 'cfo.label.healthProtection',
                apiPath: APP_CONFIG.API_PATHS.PLAN_HEALTH
            }, {
                id: 'personalAccident',
                icon: {
                    active: 'images/ic-pa.png',
                    inactive: 'images/ic-pa-d.png'
                },
                header: 'cfo.label.personalAccident',
                apiPath: APP_CONFIG.API_PATHS.PLAN_PA
            }, {
                id: 'wealthAccumulation',
                icon: {
                    active: 'images/ic-wa.png',
                    inactive: 'images/ic-wa-d.png'
                },
                header: 'cfo.label.wealthAccumulation',
                apiPath: APP_CONFIG.API_PATHS.PLAN_WEALTH
            }, {
                id: 'retirementPlanning',
                icon: {
                    active: 'images/ic-rp.png',
                    inactive: 'images/ic-rp-d.png'
                },
                header: 'cfo.label.retirementPlanning',
                apiPath: APP_CONFIG.API_PATHS.PLAN_RETIREMENT
            }, {
                id: 'lifestyleProtection',
                icon: {
                    active: 'images/ic-lsp.png',
                    inactive: 'images/ic-lsp-d.png'
                },
                header: 'cfo.label.lifestyleProtection',
                apiPath: APP_CONFIG.API_PATHS.PLAN_LIFESTYLE
            }, {
                id: 'otherInforcePolicies',
                icon: {
                    active: 'images/ic-oi.png',
                    inactive: 'images/ic-oi-d.png'
                },
                header: 'cfo.label.otherInforcePolicies',
                apiPath: APP_CONFIG.API_PATHS.OTHER_INFORCE
            }, {
                id: 'nonGePolicies',
                icon: {
                    active: 'images/ic-nge.png',
                    inactive: 'images/ic-nge-d.png'
                },
                header: 'cfo.label.nonGePolicies',
                apiPath: APP_CONFIG.API_PATHS.GET_EXTERNAL_POLICY
            }
        ];

        ctrl.productList = [];

        uiHelperService.showLoader();
        var success = function(response) {

            var nonGeCountSuccess = function(data) {

                ctrl.policyTypes[7].recordTotal = data['record_total'];

                if ($stateParams && $stateParams.category) {
                    ctrl.selectPolicyType($stateParams.category, $stateParams.policyId);
                } else {
                    _.forEach(ctrl.policyTypes, function(policy) {
                        if (policy.recordTotal > 0) {
                            ctrl.selectPolicyType(policy.id);
                            return false;
                        }
                    });
                }
            };

            var nonGeCountFail = function() {
                uiHelperService.hideLoader();
            };

            $http({
                method: 'GET',
                url: ctrl.policyTypes[7].apiPath + '?size=-1'
            }).success(nonGeCountSuccess).error(nonGeCountFail);

            policySummary = response.data;

            if (policySummary) {
                for (var i = 0; i < policySummary.length; i++) {
                    if (policySummary[i].code === 'LIFE_PROTECTION') {
                        ctrl.policyTypes[0].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'HEALTH_PROTECTION') {
                        ctrl.policyTypes[1].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'PERSONAL_ACCIDENT') {
                        ctrl.policyTypes[2].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'WEALTH_ACCUMULATION') {
                        ctrl.policyTypes[3].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'RETIREMENT') {
                        ctrl.policyTypes[4].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'LIFESTYLE_PROTECTION') {
                        ctrl.policyTypes[5].recordTotal = policySummary[i]['total_record'];
                    } else if (policySummary[i].code === 'OTHERS') {
                        ctrl.policyTypes[6].recordTotal = policySummary[i]['total_record'];
                    }
                }
            }
        };

        var error = function() {
            uiHelperService.hideLoader();
        };

        $http({
            method: 'GET',
            url: APP_CONFIG.API_PATHS.POLICY_SUMMARY
        }).success(success).error(error);

        ctrl.togglePolicyList = function(event) {
            event.stopPropagation();
            ctrl.showPolicyTypes = !ctrl.showPolicyTypes;
            ctrl.showProductList = false;
        };

        ctrl.toggleProductList = function(event) {
            event.stopPropagation();
            ctrl.showProductList = !ctrl.showProductList;
            ctrl.showPolicyTypes = false;
        };

        ctrl.selectPolicyType = function(id, policyId) {
            var initProductList = function(item) {
                ctrl.productList = item.productList;

                if (item.id === 'nonGePolicies') {
                    _.forEach(ctrl.productList, function(object) {
                        object.type = 'EXTERNAL';
                    });

                    ctrl.selectedProduct = ' ';

                    //If nonGEPolicyId parameter has a value pass in from $stateparam
                    if (policyId !== undefined) {
                        for (var j = 0; j < ctrl.productList.length; j++) {
                            if (ctrl.productList[j].id === policyId) {
                                ctrl.selectedPolicyType = id;
                                ctrl.displayProduct = ctrl.productList[j];
                                ctrl.selectedProduct = policyId;
                                break;
                            }
                        }
                    } else {
                        if (ctrl.productList.length > 0) {
                            ctrl.displayProduct = ctrl.productList[0];
                            ctrl.selectedProduct = ctrl.productList[0].id;
                        }
                    }

                    ctrl.proceed = true;

                } else {
                    if (policyId) {
                        _.forEach(ctrl.productList, function(product) {
                            if (product.number === policyId) {
                                ctrl.displayProduct = product;
                                ctrl.selectedProduct = policyId;
                                return;
                            }
                        });
                    } else {
                        if (ctrl.productList.length > 0) {
                            ctrl.displayProduct = ctrl.productList[0];
                            ctrl.selectedProduct = ctrl.productList[0].number;
                        }
                    }

                    ctrl.proceed = true;
                }
            };

            _.forEach(ctrl.policyTypes, function(item) {
                if (item.id === id && item.recordTotal) {
                    ctrl.displayProduct = {};

                    ctrl.displayPolicyType = item.header;
                    ctrl.displayPolicyTypeCount = item.recordTotal;

                    $timeout(function() {

                        if (item.productList && item.productList.length > 0) {
                            initProductList(item);
                        } else {
                            if (item.apiPath) {
                                uiHelperService.showLoader();
                                var successCallBack = function(response) {
                                    item.productList = response.data || [];
                                    item.productList = $filter('orderBy')(item.productList, 'number');
                                    item.recordTotal = item.productList ? item.productList.length : 0;

                                    initProductList(item);
                                };

                                var failedCallBack = function() {
                                    item.productList = [];
                                    ctrl.productList = [];
                                };

                                // var done = function() {
                                //     uiHelperService.hideLoader();
                                // };

                                var path = '';
                                if (item.id === 'nonGePolicies') {
                                    path = item.apiPath + '?size=-1';
                                } else {
                                    var joint = item.apiPath.indexOf('?') < 0 ? '?' : '&';
                                    path = item.apiPath + joint + 'size=-1';
                                }

                                $http({
                                    method: 'GET',
                                    url: path
                                }).success(successCallBack).error(failedCallBack);
                            }
                        }

                        ctrl.showPolicyTypes = false;
                    });
                }
            });
        };

        ctrl.selectProductList = function(item) {

            if (ctrl.proceed) {
                ctrl.displayProduct = {};
                $timeout(function() {
                    if (item.type === 'EXTERNAL') {
                        ctrl.selectedProduct = item.id;
                        ctrl.displayProduct = item;
                    } else {
                        ctrl.selectedProduct = item.number;
                        ctrl.displayProduct = item;
                    }

                    ctrl.showProductList = false;
                });
            }
        };

        $('#policyTypesDdl').scroll(function() {
            if ($('#policyTypesDdl').scrollTop() + $('#policyTypesDdl').innerHeight() >= $('#policyTypesDdl')[0].scrollHeight) {
                $timeout(function() {
                    ctrl.endOfDdl = true;
                });
            } else {
                if (ctrl.endOfDdl) {
                    $timeout(function() {
                        ctrl.endOfDdl = false;
                    });
                }
            }
        });

        $('html').on('click.hidePolicyType', function() {
            $timeout(function() {
                ctrl.showPolicyTypes = false;
            });
        });

        $('html').on('click.hideProductList', function() {
            $timeout(function() {
                ctrl.showProductList = false;
            });
        });

        $scope.$on('$destroy', function() {
            $('html').off('click.hidePolicyType');
        });

        $scope.$on('$destroy', function() {
            $('html').off('click.hideProductList');
        });
    }

    angular.module('policyDetails').directive(directiveName, policyDetailsPage);
})();
