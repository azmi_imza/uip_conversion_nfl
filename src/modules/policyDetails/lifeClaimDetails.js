(function() {
    'use strict';

    var directiveName = 'lifeClaimDetails';

    /*@ngInject*/
    function lifeClaimDetails() {
        return {
            scope: {
                policyDetails: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: lifeClaimDetailsCtrl,
            templateUrl: 'templates/policyDetails/lifeClaimDetails.html'
        };
    }

    /*@ngInject*/
    function lifeClaimDetailsCtrl($timeout, $log) {
        $log.debug('lifeClaimDetails.html page');
        var ctrl = this;
        ctrl.model = ctrl.policyDetails || [];

        $timeout(function() {
            $('.footable_policy').footable();
        });
    }

    angular.module('policyDetails').directive(directiveName, lifeClaimDetails);
})();
