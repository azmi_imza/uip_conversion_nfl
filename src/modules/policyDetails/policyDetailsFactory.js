/* eslint no-alert:0 */
(function() {
    'use strict';

    var factoryName = 'PolicyDetailsFactory';

    /*@ngInject*/
    var factory = function(_, $filter, $log, $resource, $http, APP_CONFIG, $timeout, storageService, APP_CONST) {
        return function(ctrl) {
            // TODO: create functions for CRUD and web service call
            ctrl.userType = storageService.get(APP_CONST.STORAGE_KEYS.USERTYPE) || 'IN';
            $log.debug(ctrl.userType);

            this.getPolicy = function() {
                return $http({
                    method: 'GET',
                    url: APP_CONFIG.API_PATHS.POLICY
                });
            };

            this.getPolicyDetails = function(policyId) {
                var successGetPolicyCallback = function(response) {
                    ctrl.model = response.data.data;
                    $log.debug(response);
                    ctrl.model.premiumDueDash = '-';
                    for (var i = 0; i < ctrl.model.plan.length; i++) {
                        if (ctrl.model.plan[i].planType === 'BASE') {
                            ctrl.model.status = ctrl.model.plan[i].status;
                        }
                    }

                    $timeout(function() {
                        $('.footable_policy').footable();
                    });
                };

                var failGetPolicyCallBack = function(response) {
                    //fail response handling
                    $log.debug('Fail to get PolicyID. Reason: ' + response);
                };

                //actual api call
                return $http({
                    method: 'GET',
                    url: APP_CONFIG.API_PATHS.POLICY + '/' + policyId
                }).then(successGetPolicyCallback, failGetPolicyCallBack);
            };

            this.loadPolicyProducts = function() {
                var path = APP_CONFIG.API_PATHS.POLICY_DDL;
                return $http({
                    method: 'GET',
                    url: path
                });
            };

            this.getPolicyProductDetailsById = function(policyId, policyState) {
                var path = '';
                var policystate = policyState ? policyState : '';

                if (policystate === 'proposal') {
                    path = APP_CONFIG.API_PATHS.PROPOSAL + '/' + policyId;
                } else if (policystate === 'claim') {
                    path = APP_CONFIG.API_PATHS.LIFE_CLAIM_EXTERNAL;
                } else if (policyState === 'general claim') {
                    path = APP_CONFIG.API_PATHS.GENERAL_CLAIM_EXTERNAL;
                } else {
                    path = APP_CONFIG.API_PATHS.POLICY + '/' + policyId;
                }

                $log.debug(path);
                return $http({
                    method: 'GET',
                    url: path
                }).then(function(response) {
                        ctrl.model = response.data.data;
                    },

                    function(response) {
                        //TODO: Revisit
                        alert('failed to get data. Reason: ' + response);
                    });
            };

            // this.getPolicyList = function(path) {
            //     var success = function(response) {
            //         if (response.data) {
            //             ctrl.productList = response.data;

            //             for (var i = 0; i < ctrl.productList.length; i++) {
            //                 if (ctrl.productList[i].status === 'TERMINATED') {
            //                     ctrl.productList.splice(i, 1);
            //                     i--;
            //                 }
            //             }

            //             ctrl.productList = $filter('orderBy')(ctrl.productList, 'number');

            //             ctrl.displayProduct = ctrl.productList[0];
            //             ctrl.selectedProduct = ctrl.productList[0].number;
            //         } else {
            //             ctrl.productList = [];
            //         }
            //     };

            //     var error = function() {
            //         ctrl.policyList = [];
            //     };

            //     return $http({
            //         method: 'GET',
            //         url: path
            //     }).success(success).error(error);
            // };
        };
    };

    angular.module('policyDetails').factory(factoryName, factory);
})();
