/* eslint no-alert:0 */
(function() {
    'use strict';

    var factoryName = 'PolicyDetailsService';

    /*@ngInject*/
    var factory = function($timeout) {

        var service = {

            expandAll: expandAll,
            collapseAll: collapseAll
        };

        return service;

        ////////hoisting

        function expandAll(ctrl, collapseInProgress) {
            if (collapseInProgress) {
                return;
            }

            collapseInProgress = true;
            var endIndex = 0;
            var array = ctrl.collapsibleArray;

            var proceedExpand = false;
            for (var k = array.length - 1; k >= 0; k--) {
                if ($('#' + array[k].id) && !$('#' + array[k].id).hasClass('in')) {
                    endIndex = k;
                    proceedExpand = true;
                    break;
                }
            }

            if (!proceedExpand) {
                $timeout(function() {
                    ctrl.showExpandAll = false;
                    collapseInProgress = false;
                });
            } else {
                $('#' + array[endIndex].id).on('shown.bs.collapse.expandAll', function() {
                    $timeout(function() {
                        ctrl.showExpandAll = false;
                        collapseInProgress = false;
                    });

                    $('#' + array[endIndex].id).off('shown.bs.collapse.expandAll');
                });

                for (var j = 0; j <= endIndex; j++) {
                    if ($('#' + array[j].id) && !$('#' + array[j].id).hasClass('in')) {
                        $('#' + array[j].id + 'Control').click();
                    }
                }
            }
        }

        function collapseAll(ctrl, collapseInProgress) {
            if (collapseInProgress) {
                return;
            }

            collapseInProgress = true;
            var endIndex = 0;
            var array = ctrl.collapsibleArray;

            var proceedExpand = false;
            for (var k = array.length - 1; k >= 0; k--) {
                if ($('#' + array[k].id) && $('#' + array[k].id).hasClass('in')) {
                    endIndex = k;
                    proceedExpand = true;
                    break;
                }
            }

            if (!proceedExpand) {
                $timeout(function() {
                    ctrl.showExpandAll = true;
                    collapseInProgress = false;
                });
            } else {
                $('#' + array[endIndex].id).on('hidden.bs.collapse.collapseAll', function() {
                    $timeout(function() {
                        ctrl.showExpandAll = true;
                        collapseInProgress = false;
                    });

                    $('#' + array[endIndex].id).off('hidden.bs.collapse.collapseAll');
                });

                for (var j = 0; j <= endIndex; j++) {
                    if ($('#' + array[j].id) && $('#' + array[j].id).hasClass('in')) {
                        $('#' + array[j].id + 'Control').click();
                    }
                }
            }
        }
    };

    angular.module('policyDetails').factory(factoryName, factory);
})();
