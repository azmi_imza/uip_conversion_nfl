(function() {
    'use strict';
    var directiveName = 'lifePolicyDetailsIlp';

    /*@ngInject*/
    function lifePolicyDetailsIlp() {
        return {
            scope: {
                policyDetails: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: lifePolicyDetailsIlpCtrl,
            templateUrl: 'templates/policyDetails/lifePolicyDetailsIlp.html'
        };
    }

    function lifePolicyDetailsIlpCtrl(utilService, uiHelperService, $filter, $timeout, $log, $http, APP_CONFIG, $scope, _, PolicyDetailsService) {
        $log.debug('life ILP');

        var ctrl = this;
        ctrl.model = {};
        ctrl.showContactNumber = true;
        ctrl.showInfoNumber = false;
        ctrl.showEmail = false;
        ctrl.collapsePanel = {};
        ctrl.showExpandAll = false;
        var collapseInProgress = false;
        ctrl.isMorePage = false;
        ctrl.isSla = false;

        ctrl.collapsibleArray = [{
            id: 'covBenefitInfoDetail'
        }, {
            id: 'polFinancialInfoDetail'
        }, {
            id: 'currFundHoldDetail'
        }, {
            id: 'fundDetail'
        }, {
            id: 'divCouInfoDetail'
        }, {
            id: 'fundTransRecordDetail'
        }];

        if (ctrl.policyDetails.number) {
            var success = function(response) {

                if (response.status === 'success') {

                    var successFinancialPlanner = function(financialPlannerDetails) {
                        ctrl.salesforce = financialPlannerDetails.data;
                    };

                    var failedFinancialPlanner = function() {};

                    var successFundTransaction = function(list) {
                        if (list.status === 'success') {
                            ctrl.pageSearchCriteria = list['page_search_criteria'];
                            ctrl.isMorePage = list['is_more_page'];
                            ctrl.model['life_fund_transaction_records'] = list.data;
                        } else {
                            ctrl.model['life_fund_transaction_records'] = [];
                        }
                    };

                    var failedFundTransaction = function() {
                        ctrl.model['life_fund_transaction_records'] = [];
                    };

                    ctrl.model = response.data || {};

                    $timeout(function() {
                        if (ctrl.model['life_ilp_plan_details'] !== undefined) {
                            ctrl.isSla = true;

                            _.forEach(ctrl.model['life_ilp_plan_details'], function(detail) {
                                ctrl.totalInvestmentValue = 0;
                                for (var e = 0; e < detail.fund.length; e++) {
                                    ctrl.totalInvestmentValue += detail.fund[e]['life_fund_total_value'];
                                }

                                detail['total_investment_value'] = ctrl.totalInvestmentValue;
                                ctrl.drawPieChart(detail, ctrl.isSla);
                            });
                        }
                    });

                    $timeout(function() {
                        if (ctrl.model['life_fund_details'] !== undefined) {
                            ctrl.isSla = false;
                            ctrl.totalInvestmentValue = 0;

                            _.forEach(ctrl.model['life_fund_details'], function(item) {
                                ctrl.totalInvestmentValue += item['life_fund_total_value'];
                            });

                            ctrl.drawPieChart(ctrl.model['life_fund_details'], ctrl.isSla);
                        }
                    });

                    if (ctrl.model['financial_planner_id']) {
                        $http({
                            method: 'GET',
                            url: APP_CONFIG.API_PATHS.SALESFORCE + '/' + ctrl.model['financial_planner_id']
                        }).success(successFinancialPlanner).error(failedFinancialPlanner);
                    }

                    $http({
                        method: 'GET',
                        url: APP_CONFIG.API_PATHS.LIST_FUND_TRANSACTION.replace('{policyNumber}', ctrl.policyDetails.number)
                    }).success(successFundTransaction).error(failedFundTransaction);
                }
            };

            var error = function() {

            };

            uiHelperService.showLoader();
            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.POLICY + '/' + ctrl.policyDetails.number
            }).success(success).error(error)['finally'](function() {
                uiHelperService.hideLoader();
            });
        }

        ctrl.initTable = function(id) {
            $timeout(function() {
                $('#' + id).footable();
            });
        };

        ctrl.initCollapse = function(id, expand) {
            ctrl.collapsePanel[id] = expand;
            $('#' + id).on('hidden.bs.collapse shown.bs.collapse', function() {
                $timeout(function() {
                    ctrl.collapsePanel[id] = $('#' + id).hasClass('in');
                });
            });
        };

        ctrl.toggleEmail = function() {
            ctrl.showEmail = !ctrl.showEmail;
            ctrl.showContactNumber = false;
            ctrl.showInfoNumber = false;
        };

        ctrl.toggleContactNumber = function() {
            ctrl.showEmail = false;
            ctrl.showContactNumber = !ctrl.showContactNumber;
            ctrl.showInfoNumber = false;
        };

        ctrl.toggleInfoNumber = function() {
            ctrl.showEmail = false;
            ctrl.showInfoNumber = !ctrl.showInfoNumber;
            ctrl.showContactNumber = false;
        };

        ctrl.expandAll = function() {
            PolicyDetailsService.expandAll(ctrl, collapseInProgress);
        };

        ctrl.collapseAll = function() {
            PolicyDetailsService.collapseAll(ctrl, collapseInProgress);
        };

        // This function is to enable the Total Investment Value Row for Fund Details to show without error.
        // Without this function, the colspan of the last row's td will cause the table's header to misbehave.
        ctrl.initTotalInvestmentRow = function() {
            $timeout(function() {
                ctrl.showTotalInvestmentRow = true;
            });
        };

        ctrl.initRepInfoNo = function() {
            $timeout(function() {
                var height = $('#representativeInfoNo').height();
                $('#representativeInfoNo > div').css('height', height + 'px');
                $('#representativeInfoNo > span').css('height', (height * 0.8) + 'px');

            });
        };

        ctrl.roundDown = function(value) {
            return utilService.floorValue(value);
        };

        function loadDataSuccess(response) {
            ctrl.isMorePage = response['is_more_page'];
            ctrl.pageSearchCriteria = response['page_search_criteria'];
            ctrl.model['life_fund_transaction_records'] = ctrl.model['life_fund_transaction_records'].concat(response.data);
            // $('#fundTransRecordDetailTable').data('page-size', ctrl.model['life_fund_transaction_records'].length);
            // $('#fundTransRecordDetailTable').trigger('footable_initialized');
        }

        function loadDataFail() {

        }

        ctrl.loadMore = function() {
            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.LIST_FUND_TRANSACTION.replace('{policyNumber}', ctrl.policyDetails.number) + '?pageSearchCriteria=' + ctrl.pageSearchCriteria
            }).success(function(response) {
                loadDataSuccess(response);
            }).error(function() {
                loadDataFail();
            });
        };

        ctrl.drawPieChart = function(detail, isSla) {
            var paper;
            var arc;
            var circle;
            var text;
            var colorArr = ['#D9F53F', '#5CDA21', '#31AFF5', '#2156D4'];
            var textArr = [];
            var pieData = [];
            var sectorAngleArr = [];
            var total = 0;
            var startAngle = 0;
            var endAngle = 0;
            var x1, x2, y1, y2 = 0;
            var middleAngle;
            var middleAngleArr = [];
            var size = 340;

            var totalValue = [];
            ctrl.totalEquities = 0;
            ctrl.totalFixedIncome = 0;
            ctrl.totalMixedAssets = 0;
            ctrl.totalMoneyMarket = 0;

            if (isSla) {
                _.forEach(detail.fund, function(item) {
                    if (item['life_fund_asset_category_code'] === '2') {
                        ctrl.totalEquities += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '3') {
                        ctrl.totalFixedIncome += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '4') {
                        ctrl.totalMixedAssets += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '1') {
                        ctrl.totalMoneyMarket += item['life_fund_total_value'];
                    }
                });
            } else {
                _.forEach(detail, function(item) {
                    if (item['life_fund_asset_category_code'] === '2') {
                        ctrl.totalEquities += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '3') {
                        ctrl.totalFixedIncome += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '4') {
                        ctrl.totalMixedAssets += item['life_fund_total_value'];
                    } else if (item['life_fund_asset_category_code'] === '1') {
                        ctrl.totalMoneyMarket += item['life_fund_total_value'];
                    }
                });
            }

            totalValue.push(ctrl.totalEquities);
            totalValue.push(ctrl.totalFixedIncome);
            totalValue.push(ctrl.totalMixedAssets);
            totalValue.push(ctrl.totalMoneyMarket);

            for (var sector = 0; sector < totalValue.length; sector++) {
                if (totalValue[sector] !== 0) {
                    var sectorData = {
                        value: totalValue[sector],
                        color: colorArr[sector]
                    };
                    pieData.push(sectorData);
                }
            }

            if (isSla) {
                paper = new Raphael('holder_' + detail['account_type']);
            } else {
                paper = new Raphael('holder_normal');
            }

            paper.setSize(size, size);

            //CALCULATE THE TOTAL
            for (var k = 0; k < pieData.length; k++) {
                total += pieData[k].value;
            }

            //CALCULATE THE ANGLES THAT EACH SECTOR SWIPES AND STORE IN AN ARRAY
            for (var i = 0; i < pieData.length; i++) {
                var angle = Math.abs(360 * pieData[i].value / total);
                var percentage = Math.abs(angle / 360 * 100);
                if (angle !== 0) {
                    sectorAngleArr.push(angle);
                }

                if (percentage % 1 === 0.5) {
                    percentage = Math.floor(percentage);
                } else {
                    percentage = Math.round(percentage);
                }

                textArr.push(percentage);
            }

            drawArcs();

            function drawArcs() {
                var temp = 0;
                for (var secAngle = 0; secAngle < sectorAngleArr.length; secAngle++) {
                    startAngle = endAngle;
                    endAngle = startAngle + sectorAngleArr[secAngle];

                    if (secAngle === 0) {
                        middleAngle = sectorAngleArr[secAngle] / 2;
                        temp += sectorAngleArr[secAngle];
                    } else {
                        middleAngle = temp + (sectorAngleArr[secAngle] / 2);
                        temp += sectorAngleArr[secAngle];
                    }

                    middleAngleArr.push(middleAngle);

                    x1 = parseInt(size / 2 + 160 * Math.cos(Math.PI * startAngle / 180));
                    y1 = parseInt(size / 2 + 160 * Math.sin(Math.PI * startAngle / 180));
                    x2 = parseInt(size / 2 + 160 * Math.cos(Math.PI * endAngle / 180));
                    y2 = parseInt(size / 2 + 160 * Math.sin(Math.PI * endAngle / 180));
                    var d = 'M170,170 L' + x1 + ',' + y1 + '  A160,160 0 ' + (+(endAngle - startAngle > 180)) + ',1 ' + x2 + ',' + y2 + ' z'; //1 means clockwise

                    arc = paper.path(d);
                    arc.attr({
                        'fill': pieData[secAngle].color,
                        'stroke': pieData[secAngle].color
                    });
                }

                var position = [];
                for (var midAngle = 0; midAngle < middleAngleArr.length; midAngle++) {
                    position[midAngle] = middleAngleCoordinates(size / 2 / 3.5 + 65, middleAngleArr[midAngle]);
                }

                for (var j = 0; j < textArr.length; j++) {
                    circle = paper.circle(position[j].x, position[j].y, 17);
                    circle.attr({
                        fill: 'black',
                        stroke: 'transparent',
                        'fill-opacity': 0.4
                    });

                    text = paper.text(position[j].x, position[j].y, textArr[j] + '%');
                    text.attr({
                        fill: '#ffffff',
                        'font-size': '12px',
                        'font-family': 'Arial'
                    });

                    if (textArr[j] < 5) {
                        text.hide();
                        circle.hide();
                    }

                    $('tspan', text.node).attr('dy', 5);

                }

                circle = paper.circle(size / 2, size / 2, 65);
                circle.attr({
                    fill: '#ffffff',
                    'stroke': '#ffffff'
                });

                var line;

                if (isSla) {
                    if (detail['account_type'] === 'PROTECTION') {
                        line = $filter('translate')('cfo.label.pieChartProtectionAccount');
                    } else if (detail['account_type'] === 'ACCUMULATION') {
                        line = $filter('translate')('cfo.label.piechartAccumulationAccount');
                    }
                } else {
                    line = $filter('translate')('cfo.label.pieChartAssetAllocation');
                }

                line = line.replace('\\n', '\n');

                text = paper.text(size / 2, size / 2, line);
                text.attr({
                    'font-size': '20px',
                    'font-family': 'Arial'
                });

                $('tspan:first-child', text.node).attr('dy', -5);
                $('tspan:last-child', text.node).attr('dy', 25);
            }

            function middleAngleCoordinates(distance, middleAngleParam) {
                var middle = {};
                middle.x = (size / 2) + distance * Math.cos(Math.PI * middleAngleParam / 180);
                middle.y = (size / 2) + distance * Math.sin(Math.PI * middleAngleParam / 180);
                return middle;
            }
        };
    }

    angular.module('policyDetails').directive(directiveName, lifePolicyDetailsIlp);
})();
