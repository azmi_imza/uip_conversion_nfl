(function() {
    'use strict';

    var directiveName = 'generalInsurancePolicyOthers';

    /*@ngInject*/
    function generalInsurancePolicyOthers() {
        return {
            scope: {
                policyDetails: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: generalInsurancePolicyOthersCtrl,
            templateUrl: 'templates/policyDetails/generalInsurancePolicyOthers.html'
        };
    }

    /*@ngInject*/
    function generalInsurancePolicyOthersCtrl(utilService, uiHelperService, APP_CONFIG, $http, $timeout, $log, PolicyDetailsService) {
        var ctrl = this;

        ctrl.model = {};
        ctrl.showContactNumber = true;
        ctrl.showInfoNumber = false;
        ctrl.showEmail = false;
        ctrl.collapsePanel = {};
        ctrl.showExpandAll = false;
        var collapseInProgress = false;

        $log.debug('lifePolicyDetailOthers page');

        ctrl.collapsibleArray = [{
            id: 'covBenefitInfoDetail'
        }, {
            id: 'polFinancialInfoDetail'
        }];

        if (ctrl.policyDetails.number) {
            var success = function(response) {

                var successFinancialPlanner = function(financialPlannerDetails) {
                    ctrl.salesforce = financialPlannerDetails.data;
                };

                var failedFinancialPlanner = function() {};

                ctrl.model = response.data || {};

                if (ctrl.model['financial_planner_id']) {
                    $http({
                        method: 'GET',
                        url: APP_CONFIG.API_PATHS.SALESFORCE + '/' + ctrl.model['financial_planner_id']
                    }).success(successFinancialPlanner).error(failedFinancialPlanner);
                }
            };

            var error = function() {

            };

            uiHelperService.showLoader();
            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.POLICY + '/' + ctrl.policyDetails.number
            }).success(success).error(error)['finally'](function() {
                uiHelperService.hideLoader();
            });
        }

        ctrl.initTable = function(id) {
            $timeout(function() {
                $('#' + id).footable();
            });
        };

        ctrl.initCollapse = function(id, expand) {
            ctrl.collapsePanel[id] = expand;
            $('#' + id).on('hidden.bs.collapse shown.bs.collapse', function() {
                $timeout(function() {
                    ctrl.collapsePanel[id] = $('#' + id).hasClass('in');
                });
            });
        };

        ctrl.toggleEmail = function() {
            ctrl.showEmail = !ctrl.showEmail;
            ctrl.showContactNumber = false;
            ctrl.showInfoNumber = false;
        };

        ctrl.toggleContactNumber = function() {
            ctrl.showEmail = false;
            ctrl.showContactNumber = !ctrl.showContactNumber;
            ctrl.showInfoNumber = false;
        };

        ctrl.toggleInfoNumber = function() {
            ctrl.showEmail = false;
            ctrl.showInfoNumber = !ctrl.showInfoNumber;
            ctrl.showContactNumber = false;
        };

        ctrl.expandAll = function() {
            PolicyDetailsService.expandAll(ctrl, collapseInProgress);
        };

        ctrl.collapseAll = function() {
            PolicyDetailsService.collapseAll(ctrl, collapseInProgress);
        };

        ctrl.initRepInfoNo = function() {
            $timeout(function() {
                var height = $('#representativeInfoNo').height();
                $('#representativeInfoNo > div').css('height', height + 'px');
                $('#representativeInfoNo > span').css('height', (height * 0.8) + 'px');
            });
        };

        ctrl.roundDown = function(value) {
            return utilService.floorValue(value);
        };
    }

    angular.module('policyDetails').directive(directiveName, generalInsurancePolicyOthers);
})();
