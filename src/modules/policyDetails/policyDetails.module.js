(function() {
    'use strict';
    angular.module('policyDetails', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var statePrefix = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                var policyDetailsIN = statePrefix.concat('policy-details-in');
                $stateProvider.state(policyDetailsIN, {
                    url: '/policy-details-in',
                    template: '<policy-details-page></policy-details-page>',
                    data: {
                        screenCode: 'SCRNPOLICYDETAILSIN'
                    },
                    params: {
                        'category': undefined,
                        'policyId': undefined
                    }
                });

                var policyDetailsBE = statePrefix.concat('policy-details-be');
                $stateProvider.state(policyDetailsBE, {
                    url: '/policy-details-be',
                    template: '<policy-details-page></policy-details-page>',
                    data: {
                        screenCode: 'SCRNPOLICYDETAILSBE'
                    },
                    params: {
                        'category': undefined,
                        'policyId': undefined
                    }
                });
            });
})();
