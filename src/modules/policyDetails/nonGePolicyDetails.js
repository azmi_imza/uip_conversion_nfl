/* eslint no-alert:0 */
(function() {
    'use strict';

    var directiveName = 'nonGePolicyDetails';

    /*@ngInject*/
    function nonGePolicyDetails() {
        return {
            scope: {
                model: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: nonGePolicyDetailsCtrl,
            templateUrl: 'templates/policyDetails/nonGePolicyDetails.html'
        };
    }

    /*@ngInject*/
    function nonGePolicyDetailsCtrl(utilService, uiHelperService, $log, $scope, $timeout, routeService, $http, APP_CONFIG, _) {
        $log.debug('nonGePolicyDetails page');

        var ctrl = this;
        ctrl.model = ctrl.model || {};
        ctrl.showDeletePolicySection = false;
        uiHelperService.hideLoader();

        ctrl.hasModel = function() {
            return !(_.isEmpty(ctrl.model));
        };

        ctrl.addNewNonGEPolicy = function() {
            routeService.goToState('base.auth.non-ge-policy-add');
        };

        ctrl.displayDeletePolicySection = function() {
            ctrl.showDeletePolicySection = !ctrl.showDeletePolicySection;
        };

        ctrl.updateNonGEPolicy = function(id) {

            var success = function(response) {
                routeService.goToState('base.auth.non-ge-policy-modify', {
                    response: response
                });
            };

            var error = function() {

            };

            uiHelperService.showLoader();
            $http({
                method: 'GET',
                url: APP_CONFIG.API_PATHS.GET_EXTERNAL_POLICY + '/' + id
            }).success(success).error(error);
        };

        ctrl.deleteNonGEPolicy = function(id) {

            var success = function(response) {
                routeService.goToState('base.auth.non-ge-policy-ack', {
                    response: response
                });
            };

            var error = function() {
                $log.debug('Error: ' + error);
            };

            uiHelperService.showLoader();
            $http({
                method: 'DELETE',
                url: APP_CONFIG.API_PATHS.DELETE_EXTERNAL_POLICY.replace('{id}', id),
                data: ctrl.model
            }).success(success).error(error);
        };

        ctrl.roundDown = function(value) {
            return utilService.floorValue(value);
        };
    }

    angular.module('policyDetails').directive(directiveName, nonGePolicyDetails);

})();
