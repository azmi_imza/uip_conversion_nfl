(function() {
    'use strict';

    var directiveName = 'lifeProposalDetails';

    /*@ngInject*/
    function lifeProposalDetails() {
        return {
            scope: {
                policyDetails: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: lifeProposalDetailsCtrl,
            templateUrl: 'templates/policyDetails/lifeProposalDetails.html'
        };
    }

    /*@ngInject*/
    function lifeProposalDetailsCtrl($log, PolicyDetailsFactory) {
        $log.debug('lifeProposalDetails page');

        var ctrl = this;
        var factory = new PolicyDetailsFactory(ctrl);
        ctrl.model = {};
        ctrl.model = ctrl.policyDetails;

        ctrl.getFinancialPlannerDetail = function() {
            factory.getFinancialPlannerModalService();
        };
    }

    angular.module('policyDetails').directive(directiveName, lifeProposalDetails);

})();
