(function() {
    'use strict';

    var directiveName = 'generalClaimDetails';

    /*@ngInject*/
    function generalClaimDetails() {
        return {
            scope: {
                policyDetails: '='
            },
            bindToController: true,
            controllerAs: 'ctrl',
            controller: generalClaimDetailsCtrl,
            templateUrl: 'templates/policyDetails/generalClaimDetails.html'
        };
    }

    /*@ngInject*/
    function generalClaimDetailsCtrl($timeout, $log) {
        var ctrl = this;
        ctrl.model = ctrl.policyDetails || [];
        $log.debug('general Claim Details page');

        $timeout(function() {
            $('.footable_policy').footable();
        });

    }

    angular.module('policyDetails').directive(directiveName, generalClaimDetails);
})();
