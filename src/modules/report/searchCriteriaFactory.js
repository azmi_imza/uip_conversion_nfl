(function() {
    'use strict';

    var factoryName = 'searchCriteriaFactory';

    /*@ngInject*/
    var factory = function($log, $timeout, $window, $http, $rootScope, storageService, formManagerService,
        APP_CONST, APP_CONFIG) {
        var SearchCriteriaFactory = function() {

            var mainContainer;
            var container1;
            var container2;
            var searchCriteriaInfo = [];
            var datePickerList = [];

            var createDropDownElement = function(dropDownObject) {

                var dropDownGrpElement = '';
                var isRequired = '';
                var asterisk = '';

                if (dropDownObject.mandatory === 'Y') {
                    asterisk = '*';
                    isRequired = 'is-required';
                }

                for (var i = 0; i < dropDownObject['gui_grp'].length; i++) {

                    var dropDownValue = angular.toJson(dropDownObject['gui_grp'][i]['gui_opt']);

                    dropDownGrpElement +=
                        '<td><div style=\'display:inline\'>' +
                        '<select class=\'w-select input_midsize\' style=\'width:250px\' name=\'' +
                        dropDownObject['gui_grp'][i]['gui_id'] + '\'' +
                        ' ng-model=\'model.' + dropDownObject['gui_grp'][i]['gui_id'] + '\' ' + isRequired +
                        ' is-required-error-msg=\'{{"DASHBOARD_VALIDATE_MANDATORY" | translate}}\'' +
                        ' ng-options=\'dd.opt_key as dd.opt_value for dd in ' + dropDownValue + '\'>' +
                        '<option value=\'\'>{{"REPORT_PLEASE_SELECT" | translate}}</option>' +
                        '</select>' +
                        '</div></td>';
                }

                var dropDownTemplate =
                    '<div style=\'margin-left:10px\'>' +
                    '<label>' + dropDownObject.label + ' ' +
                    '<span style="color:red">' + asterisk + '</span></label>' + '<table><tr>' +
                    dropDownGrpElement +
                    '</tr></table></div>';

                var dropDownElem = angular.element(dropDownTemplate);
                if (dropDownObject['gui_grp'][0].seq < 0) {
                    container2.prepend(dropDownElem);
                } else {
                    container1.append(dropDownElem);
                }
            };

            var createDateElement = function(dateObject) {

                var dateGrpElement = '';
                var isRequired = '';
                var asterisk = '';

                if (dateObject.mandatory === 'Y') {
                    asterisk = '*';
                    isRequired = 'is-required';
                }

                for (var i = 0; i < dateObject['gui_grp'].length; i++) {

                    datePickerList.push(dateObject['gui_grp'][i]['gui_id']);

                    dateGrpElement +=
                        '<td><div class=\'input-group col-xs-10 col-sm-6\' style=\'margin: 0 10px 10px 10px\'' +
                        ' name=\'' + dateObject['gui_grp'][i]['gui_id'] + '\' ' + isRequired +
                        ' ng-model=\'model.' + dateObject['gui_grp'][i]['gui_id'] + '\'' +
                        ' is-required-error-msg=\'{{"DASHBOARD_VALIDATE_MANDATORY" | translate}}\'>' +
                        '<input id=\'' + dateObject['gui_grp'][i]['gui_id'] + '\' type=\'text\' readonly' +
                        ' class=\'form-control\' ng-model=\'model.' + dateObject['gui_grp'][i]['gui_id'] + '\' />' +
                        '<span class=\'input-group-btn\'>' +
                        '<button type=\'button\'' +
                        ' class=\'btn btn-default\' ng-click=\'openPicker($event,"' +
                        dateObject['gui_grp'][i]['gui_id'] + '")\'>' +
                        '<i class=\'glyphicon glyphicon-calendar\'></i></button>' +
                        '</span>' +
                        '</div></td>';
                }

                var dateTemplate =
                    '<div>' +
                    '<label style="margin-left:10px">' + dateObject.label + ' ' +
                    '<span style="color:red">' + asterisk + '</span></label>' + '<table style="width:100%"><tr>' +
                    dateGrpElement +
                    '</tr></table></div>';

                var dateElem = angular.element(dateTemplate);
                if (dateObject['gui_grp'][0].seq < 0) {
                    container2.prepend(dateElem);
                } else {
                    container1.append(dateElem);
                }
            };

            var createRdbElement = function(rdbObject) {

                var rdbGrpElement = '';
                var isRequired = '';
                var asterisk = '';

                if (rdbObject.mandatory === 'Y') {
                    asterisk = '*';
                    isRequired = 'is-required';
                }

                for (var i = 0; i < rdbObject['gui_grp'].length; i++) {

                    rdbGrpElement += '<td><div ng-model=\'model.' + rdbObject['gui_grp'][i]['gui_id'] + '\'' +
                        ' name=\'' + rdbObject['gui_grp'][i]['gui_id'] + 'errorCheck\' ' + isRequired +
                        ' is-required-error-msg=\'{{"DASHBOARD_VALIDATE_MANDATORY" | translate}}\'><div class=\'w-form\'>';

                    for (var j = 0; j < rdbObject['gui_grp'][i]['gui_opt'].length; j++) {
                        var rdbValue = rdbObject['gui_grp'][i]['gui_opt'][j];
                        rdbGrpElement +=
                            '<div class=\'w-radio reqactions_radiobtn_sub float_left\'>' +
                            '<input class=\'w-radio-input\' type=\'radio\' style=\'float:left; margin-right:5px;\'' +
                            ' name=\'' + rdbObject['gui_grp'][i]['gui_id'] + '\'' +
                            ' value=\'' + rdbValue['opt_key'] + '\'' +
                            ' ng-model=\'model.' + rdbObject['gui_grp'][i]['gui_id'] + '\' ' +
                            '/><label class=\'w-form-label\' style=\'float:left\'' +
                            ' for=\'' + rdbValue['opt_key'] + '\'>' + rdbValue['opt_value'] + '</label>' +
                            '</div>';
                    }

                    rdbGrpElement += '</div></div></td>';
                }

                var rdbTemplate =
                    '<div>' +
                    '<label style="margin-left:10px">' + rdbObject.label + ' ' +
                    '<span style="color:red">' + asterisk + '</span></label>' + '<table><tr>' +
                    rdbGrpElement +
                    '</tr></table></div>';

                var rdbElem = angular.element(rdbTemplate);
                if (rdbObject['gui_grp'][0].seq < 0) {
                    container2.prepend(rdbElem);
                } else {
                    container1.append(rdbElem);
                }
            };

            this.buildElement = function(scope, iElement) {

                datePickerList = [];
                scope.model = {};

                mainContainer = angular.element(
                    '<div></div>'
                );

                container1 = angular.element(
                    '<div class="poldetails_wrapper">' +
                    '<div class="poldetails_header" style="margin-bottom:10px">' +
                    '<h4 class="h4 poldetails_h4_left">{{"common.header.enterSearchCriteria" | translate}}</h4>' +
                    '</div></div>'
                );

                container2 = angular.element(
                    '<div class="reqactions_wrapper">' +
                    '</div>'
                );

                searchCriteriaInfo = storageService.get('searchCriteriaInfo');

                if (!searchCriteriaInfo) {
                    return [];
                }

                searchCriteriaInfo.sort(function(a, b) {
                    if (a['gui_grp'][0].seq > b['gui_grp'][0].seq) {
                        return 1;
                    }

                    if (a['gui_grp'][0].seq < b['gui_grp'][0].seq) {
                        return -1;
                    }

                    return 0;
                });

                for (var i = 0; i < searchCriteriaInfo.length; i++) {

                    if (searchCriteriaInfo[i]['gui_typ'] === 'DDL') {
                        createDropDownElement(searchCriteriaInfo[i]);
                    } else if (searchCriteriaInfo[i]['gui_typ'] === 'DT') {
                        createDateElement(searchCriteriaInfo[i]);
                    } else if (searchCriteriaInfo[i]['gui_typ'] === 'RDB') {
                        createRdbElement(searchCriteriaInfo[i]);
                    }
                }

                var button = angular.element(
                    '<div class="w-clearfix entry_btns_wrapper">' +
                    '<input class="w-button red_btn" type="button"' +
                    ' value="{{\'common.rpt.generateReport\' | translate}}" ng-click="generateReport()">' +
                    '</div>');

                mainContainer.append(container1);
                mainContainer.append(container2);
                mainContainer.append(button);
                iElement.append(mainContainer);

                return datePickerList;
            };

            this.openDatePicker = function($event, picker) {
                picker.open();
                $event.stopPropagation();
                $event.preventDefault();
            };

            this.generateReport = function(scope) {
                var validationResult = formManagerService.runAllValidatorsOnSubmit();
                var guiData = [];

                if (validationResult) {
                    $('#maincontainer').addClass('disable-scroll');
                    $rootScope.showOverlay = true;
                    $rootScope.showLoading = true;

                    for (var i = 0; i < searchCriteriaInfo.length; i++) {
                        for (var j = 0; j < searchCriteriaInfo[i]['gui_grp'].length; j++) {
                            var selectedElement = {};
                            selectedElement['gui_id'] = searchCriteriaInfo[i]['gui_grp'][j]['gui_id'];
                            selectedElement['gui_value'] = scope.model[searchCriteriaInfo[i]['gui_grp'][j]['gui_id']];

                            if (searchCriteriaInfo[i]['gui_typ'] === 'DT' && selectedElement['gui_value']) {
                                selectedElement['gui_value'] =
                                    new Date(selectedElement['gui_value']).getTime().toString();
                            }

                            guiData.push(selectedElement);
                        }
                    }

                    var submittedReportType = angular.copy(storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_REPORT_TYPE));
                    var submittedTransactionType = angular.copy(storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_TRANSACTION_TYPE));
                    var submittedReportName = angular.copy(storageService.get(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_REPORT_NAME));

                    var reportRequestData = {};
                    reportRequestData['rpt_type'] = submittedReportType;
                    reportRequestData['trxn_type'] = submittedTransactionType;
                    reportRequestData.rpt = submittedReportName;
                    reportRequestData['gui_data'] = guiData;

                    $http({
                        method: 'POST',
                        data: reportRequestData,
                        url: APP_CONFIG.API_PATHS.REPORT_GENERATE
                    }).success(function(response) {
                        if (response.status === 'success') {
                            //TODO: Alert handling in proper way
                            $log.info('Report generation success: ' + response.message);
                            scope.showReportModal(response.message);
                        } else {
                            $log.error('Report generation failed: ' + response.message);
                        }

                        $rootScope.showOverlay = false;
                        $rootScope.showLoading = false;
                    }).error(function() {

                        //TODO: Alert handling in proper way
                        $log.error('Request failed. Please try again.');
                        $rootScope.showOverlay = false;
                        $rootScope.showLoading = false;
                    });
                }
            };
        };

        return new SearchCriteriaFactory();
    };

    angular.module('report')
        .factory(factoryName, factory);
})();
