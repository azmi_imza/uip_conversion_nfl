(function() {
    'use strict';
    var ctrlName = 'reportCtrl';

    /*@ngInject*/
    var controller = function($log, generalModalService, $window, $rootScope, $timeout, $scope,
        formManagerService, reportFactory, storageService, APP_CONST) {

        formManagerService.clearValidators();

        $scope.model = {};
        $scope.model.showCriteria = false;

        reportFactory.getReportType($scope);

        $scope.showReportModal = function(reportLink) {
            $log.debug('show report modal: ' + reportLink);
            var url = 'templates/report/viewReport.html';
            var ctrl = 'viewReportCtrl';
            var backdrop = 'static';
            var size = '';
            var resolve = {
                reportLink: function() {
                    return reportLink;
                }
            };

            var close = function() {
                $log.info('View Report Modal closed at: ' + new Date());
            };

            var dismiss = function() {
                $log.info('View Report Modal dismissed at: ' + new Date());
            };

            generalModalService.openModal(url, ctrl, backdrop, size, resolve, close, dismiss, 'modal-dialog-center');
        };

        $scope.changeReportType = function() {
            $scope.model.showCriteria = false;
            storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_REPORT_TYPE, $scope.model.reportType);
            reportFactory.getTransactionType($scope);
        };

        $scope.changeTransactionType = function() {
            $scope.model.showCriteria = false;
            storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_TRANSACTION_TYPE, $scope.model.transactionType);
            reportFactory.getReportName($scope);
        };

        $scope.searchCriteria = function() {
            $scope.model.showCriteria = false;
            storageService.set(APP_CONST.STORAGE_KEYS.LAST_SUBMITTED_REPORT_NAME, $scope.model.reportName);
            reportFactory.getSearchCriteria($scope);
        };
    };

    angular.module('report').controller(ctrlName, controller);
})();
