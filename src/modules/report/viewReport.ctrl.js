(function() {
    'use strict';
    var ctrlName = 'viewReportCtrl';

    /*@ngInject*/
    var controller = function($log, $window, $scope, generalModalService, reportLink) {
        $scope.reportLink = reportLink;
        $scope.dismiss = function() {
            generalModalService.dismissModal();
        };

        $scope.viewReport = function() {
            $log.debug('view report: ' + reportLink);
            $window.open(reportLink, '_blank');
            generalModalService.dismissModal();
        };
    };

    angular.module('report').controller(ctrlName, controller);
})();
