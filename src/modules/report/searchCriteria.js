(function() {
    'use strict';
    var directiveName = 'searchCriteria';

    /*@ngInject*/
    var searchCriteria = function(searchCriteriaFactory, $compile, $timeout) {
        return {
            restrict: 'E', // restrict to use directive by attribute only
            compile: function() {

                var link = function(scope, iElement) {

                    var datePickerList = searchCriteriaFactory.buildElement(scope, iElement);
                    var pickerList = {};
                    $compile(iElement.contents())(scope);

                    $timeout(function() {

                        if (!datePickerList.length) {
                            return;
                        }

                        for (var i = 0; i < datePickerList.length; i++) {
                            var datePicker = $('#' + datePickerList[i]).pickadate({
                                selectYears: true,
                                selectMonths: true,
                                format: 'dd mmm yyyy'
                            });

                            var picker = datePicker.pickadate('picker');
                            pickerList[datePickerList[i]] = picker;
                        }

                        if (pickerList.strFrDate && pickerList.strToDate) {
                            pickerList.strFrDate.set('max', new Date());
                            pickerList.strToDate.set('max', new Date());

                            pickerList.strFrDate.on('set', function(event) {
                                if (event.select) {
                                    pickerList.strToDate.set('min', pickerList.strFrDate.get('select'));
                                } else if ('clear' in event) {
                                    pickerList.strToDate.set('min', false);
                                }
                            });

                            pickerList.strToDate.on('set', function(event) {
                                if (event.select) {
                                    pickerList.strFrDate.set('max', pickerList.strToDate.get('select'));
                                } else if ('clear' in event) {
                                    pickerList.strFrDate.set('max', new Date());
                                }
                            });
                        }
                    }, 50);

                    scope.openPicker = function($event, id) {
                        searchCriteriaFactory.openDatePicker($event, pickerList[id]);
                    };

                    scope.generateReport = function() {
                        searchCriteriaFactory.generateReport(scope);
                    };
                };

                return {
                    post: link
                };
            }
        };
    };

    angular.module('report')
        .directive(directiveName, searchCriteria);
})();
