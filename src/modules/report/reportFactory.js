(function() {
    'use strict';

    var factoryName = 'reportFactory';

    /*@ngInject*/
    var factory = function($rootScope, $compile, $log, $resource, $timeout, $q, storageService,
        formManagerService, APP_CONFIG) {
        var Report = function() {
            var requestFailedCallback = function() {
                //TODO: Alert handling in proper way
                $log.error('Request failed. Please try again.');
            };

            this.getReportType = function(scope) {
                scope.reportTypeListLoaded = false;

                var requestSuccessCallback = function(response) {
                    scope.model.reportTypeOptions = response.data;
                    scope.model.transactionTypeOptions = [];
                    scope.model.reportNameOptions = [];
                    scope.reportTypeListLoaded = true;
                };

                $resource(APP_CONFIG.API_PATHS.REPORT_REPORTTYPE).get({}, requestSuccessCallback,
                    requestFailedCallback);
            };

            this.getTransactionType = function(scope) {
                scope.transactionTypeListLoaded = false;

                var requestSuccessCallback = function(response) {
                    scope.model.transactionTypeOptions = response.data;
                    scope.transactionTypeListLoaded = true;
                };

                if (!scope.model.reportType) {
                    scope.model.transactionTypeOptions = [];
                    scope.model.transactionType = '';
                    scope.model.reportNameOptions = [];
                    scope.model.reportName = '';
                    scope.transactionTypeListLoaded = false;
                    scope.reportNameListLoaded = false;
                } else {
                    $resource(APP_CONFIG.API_PATHS.REPORT_TRANSACTIONTYPE).get({
                        reportType: scope.model.reportType
                    }, requestSuccessCallback, requestFailedCallback);
                }
            };

            this.getReportName = function(scope) {
                scope.reportNameListLoaded = false;

                var requestSuccessCallback = function(response) {
                    scope.model.reportNameOptions = response.data;
                    scope.reportNameListLoaded = true;
                };

                if (!scope.model.transactionType) {
                    scope.model.reportNameOptions = [];
                    scope.model.reportName = '';
                    scope.reportNameListLoaded = false;
                } else {
                    $resource(APP_CONFIG.API_PATHS.REPORT_REPORTNAME).get({
                        reportType: scope.model.reportType,
                        transactionType: scope.model.transactionType
                    }, requestSuccessCallback, requestFailedCallback);
                }
            };

            this.getSearchCriteria = function(scope) {

                var requestSuccessCallback = function(response) {
                    storageService.set('searchCriteriaInfo', response.data);
                    formManagerService.clearValidators();
                    scope.model.showCriteria = false;
                    $timeout(function() {
                        scope.model.showCriteria = true;
                    }, 100);
                };

                if (scope.model.reportName) {
                    $resource(APP_CONFIG.API_PATHS.REPORT_SEARCHCRITERIA).get({
                        reportType: scope.model.reportType,
                        transactionType: scope.model.transactionType,
                        reportName: scope.model.reportName
                    }, requestSuccessCallback, requestFailedCallback);
                }
            };
        };

        return new Report();
    };

    angular.module('report')
        .factory(factoryName, factory);
})();
