(function() {
    'use strict';
    angular.module('report', [])
        .config(
            /*@ngInject*/
            function($stateProvider, $injector) {
                var AUTH_STATE_PREFIX = $injector.get('APP_CONST').AUTH_STATE_PREFIX;

                $stateProvider.state(AUTH_STATE_PREFIX.concat('report'), {
                    url: '/report',
                    templateUrl: 'templates/report/report.html',
                    controller: 'reportCtrl',
                    data: {
                        screenCode: 'report'
                    }
                });
            });
})();
