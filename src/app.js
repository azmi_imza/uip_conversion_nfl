/* eslint no-console:0 */
/* global FastClick */

(function() {
    'use strict';

    //TODO: refactor
    function initializeConfig(config, localConfig) {
        if (typeof config !== 'object') {
            config = JSON.parse(config);
        }

        if (localConfig) {
            if (typeof localConfig !== 'object') {
                localConfig = JSON.parse(localConfig);
            }

            $.extend(true, config, localConfig);
        }

        var apiPaths = config.API_PATHS;
        var settings = config.SETTINGS;

        for (var key in apiPaths) {
            if (!apiPaths.hasOwnProperty(key)) {
                continue;
            }

            apiPaths[key] = settings.API_PROTOCOL.concat('://', settings.API_HOST, settings.API_BASE_PATH, apiPaths[key]);
        }

        var appConfig = {};

        appConfig.API_PATHS = apiPaths;
        appConfig.SETTINGS = settings;
        angular.module('app').constant('APP_CONFIG', appConfig);
        angular.element(document).ready(function() {
            angular.bootstrap(document.getElementById('app'), ['app']);
        });
    }

    //TODO: refactor
    $.getJSON('config.json', function(config) {
        $.getJSON('config.local.json')
            .done(function(localConfig) {
                initializeConfig(config, localConfig);
            })
            .fail(function() {
                console.log('Local config json not found');
                initializeConfig(config);
            });
    });

    angular.module('app')
        .run(
            /*@ngInject*/
            function($location, $log, $document, routeService, storageService, authService, APP_CONST, APP_CONFIG) {
                $log.debug('app start');

                FastClick.attach($document[0].body);
                $log.debug('Sample me2:', APP_CONST.STORAGE_KEYS.SELECTED_LANG);
                $log.debug('Sample me3:', storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG));

                if (!storageService.get(APP_CONST.STORAGE_KEYS.SELECTED_LANG)) {
                    $log.debug('Sample me:', APP_CONST.STORAGE_KEYS.SELECTED_LANG);
                    $log.debug('set default language code:', APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE);
                    storageService.set(APP_CONST.STORAGE_KEYS.SELECTED_LANG, APP_CONFIG.SETTINGS.DEFAULT_LANG.CODE);
                }

                var authToken = authService.getAuthToken();

                if (authToken) {
                    storageService.set(APP_CONST.STORAGE_KEYS.SESSION_REFERENCE, new Date().getTime(), true);
                }

                var path = $location.path();
                $log.debug('$locationChangeStart path:', path);

                // handle root path, redirect to home page
                if (!path || path === '/') {
                    $log.debug('$locationChangeStart path empty, go to home url', routeService.getHomeUrl());
                    $location.replace().path(routeService.getHomeUrl());
                }
            });
})();
