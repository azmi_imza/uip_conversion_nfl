(function() {
    'use strict';
    angular.module('app', [
        'ngCookies',
        'ngResource',
        'ngMessages',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'pascalprecht.translate',
        'hfits.components',
        'app.constants',
        'common',
        /**'login',**/
        'dashboard',
        'policyDetails',
        'serviceRequest',
        'report',
        'profileDetails',
        'cka',
        'panelClinic',
        'companyProfile',
        'summaryActivity',
        'claim',
        'directChannel',
        'home'
    ]);
})();
