(function() {
    'use strict';
    var USER_TYPES = {
        'INDIVIDUAL': 'IN',
        'CORPORATE_ADMIN': 'CA',
        'MEMBER': 'IN'
    };

    var baseStatePrefix = 'base.';
    var commonStatePrefix = baseStatePrefix.concat('common.');
    var errorStatePrefix = baseStatePrefix.concat('error.');
    var authStatePrefix = baseStatePrefix.concat('auth.');

    angular.module('app.constants', [])
        .constant('APP_CONST', {
            'BASE_STATE_PREFIX': baseStatePrefix,
            'COMMON_STATE_PREFIX': commonStatePrefix,
            'ERROR_STATE_PREFIX': errorStatePrefix,
            'AUTH_STATE_PREFIX': authStatePrefix,
            'USER_TYPES': USER_TYPES,
            STORAGE_KEYS: {
                AUTH_TOKEN: 'authtoken',
                MENU_DATA: 'menu',
                USERNAME: 'username',
                USERID: 'userid',
                USER_TYPE: 'usertype',
                CKA_STATUS: 'ckastatus',
                SUBSCRIBERNAME: 'subscribername',
                SERV_REQ_ACTION: 'servreqaction',
                LAST_SUBMITTED_SERV_REQ: 'lastsubmittedservreq',
                SESSION_REMINDER_TIME: 'sessionreminder',
                SESSION_REMAINING_TIME: 'sessionremaining',
                TIMEOUT_ALERT: 'timeoutAlert',
                SESSION_REFERENCE: 'sessionReference',
                SELECTED_LANG: 'selectedlang',
                CURRENT_SCREEN_CODE: 'currentscreencode',
                LAST_SUBMITTED_REPORT_TYPE: 'lastsubmittedreporttype',
                LAST_SUBMITTED_TRANSACTION_TYPE: 'lastsubmittedtransactiontype',
                LAST_SUBMITTED_REPORT_NAME: 'lastsubmittedreportname',
                SESSION_LAST_ACTIVE_TIME: 'sessionlastactivetime',
                IS_SESSION_EXPIRED: 'sessionexpired',
                HQ_LONGITUDE: 'hq_longitude',
                HQ_LATITUDE: 'hq_latitude',
                SUM_ACTIVITY: 'sumActivity',
                LANG_RESOURCES: 'langresources',
                DYNAMIC_LAYOUT: 'dynamiclayout',
                DYNAMIC_LAYOUT_STATE_DATA: 'dynamiclayoutstatedata',
                PUBLICKEY: 'pkey',
                RANDOMKEY: 'rkey',
                MIGRATIONSTATE: 'mstate',
                SECURITYPOLICY: 'securityPolicy',
                VALID_1FA: 'valid1FA'
            },
            HTTP_HEADERS: {
                AUTH_TOKEN: 'X-Auth-Token',
                CONTENT_TYPE: 'Content-Type',
                USERNAME: 'X-Username',
                PASSWORD: 'X-Password'
            }
        })
        .constant('SERV_REQ_ACTION', {
            TOPUP: 'Topup',
            SEEKADVICE: 'SeekAdvice'
        });
})();
