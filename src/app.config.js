(function() {
    'use strict';
    angular.module('app')
        .config(
            /*@ngInject*/
            function($injector, $provide, $httpProvider, $urlRouterProvider,
                $logProvider, $translateProvider, APP_CONFIG) {
                // add lodash as injectable constant object
                $provide.constant('_', window._);

                // romove lodash from global reference to prevent direct usage
                try {
                    delete window._;
                } catch (e) {
                    window._ = undefined;
                }

                $logProvider.debugEnabled(APP_CONFIG.SETTINGS.DEBUG_MODE);

                $httpProvider.interceptors.push('authInterceptor');
                if (APP_CONFIG.SETTINGS.LOAD_LOCAL_RESOURCES) {
                    // load language resources from local
                    $translateProvider.useLoader('localResourceLoader');
                } else {
                    $translateProvider.useLoader('resourceLoader');
                }

                $translateProvider.useSanitizeValueStrategy(null);

                $provide.decorator('$state', function($delegate, $rootScope) {
                    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
                        $delegate.toState = toState;
                        $delegate.toParams = toParams;
                    });

                    return $delegate;
                });
            });
})();
