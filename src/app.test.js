(function() {
    'use strict';

    // temporary module for quick testing purpose, to be removed or excluded for production
    angular.module('app')
        .controller('testCtrl', /*@ngInject*/ function($scope) {
            $scope.test = 'test controller';

            // var headers = {
            //     headers: {
            //         'X-Username': 'user001',
            //         'X-Passsord': 'password'
            //     }
            // };

            // $http.get('http://localhost:3000/posts').then(function(response) {
            //     $log.debug(response);
            // });

            // $http.post('http://10.128.1.35:8080/uipcfo/rest/login').then(function(response) {
            //     $log.debug(response);
            // });

            // var Posts = $resource('http://localhost:3000/posts/:postId/comments', {
            //     postId: '@id'
            // });

            // var post = Posts.query({
            //     postId: 1
            // }, function(p) {
            //     $scope.response = p;
            // });
        });
})();
